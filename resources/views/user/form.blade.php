@section('content')

@php
    if(isset($data)){
        $id = $data->id;

        $u_first_name = $data->u_first_name;
        $username = $data->username;
        $email = $data->email;
        $id_ktp = $data->id_ktp;
        $u_phone_no = $data->u_phone_no;
        $type = 'update';
    }else {
        $id = '';
        $u_first_name = '';
        $username = '';
        $email = '';
        $id_ktp = '';
        $u_phone_no = '';
        $type = 'normal';
        
    }
@endphp
<div class="c-forms"> 
            <a class="i-button no-margin" href="{{route('user.index')}}">Kembali</a>   

    <div class="box-element" style="
    margin-top: 20px;
">
        <form id="form_tambah_data">
            
        <input type="hidden" name="id" id="id" value="{{$id}}">

        <input type="hidden" name="type" id="type" value="{{$type}}">

              <div class="i-label">
                <label for="text_field">Nama</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$u_first_name}}" type="text" name="nama" id="nama" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>


            <div class="i-label">
                <label for="text_field">Email</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$email}}" type="text" name="email" id="email" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             <div class="i-label">
                <label for="text_field">Phone Number</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$u_phone_no}}" type="text" name="phone" id="phone" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             <div class="i-label">
                <label for="text_field">KTP</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$id_ktp}}" type="text" name="ktp" id="ktp" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">Username</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    {{-- onkeyup="convertToNum(this)"  --}}
                    <input value="{{$username}}" type="text" name="username" id="username" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">Password</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    {{-- onkeyup="convertToNum(this)"  --}}
                    <input value="" type="password" name="password" id="password" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>
            
            

        <input type="button" style="margin: 20px !important;" onclick="TambahDataBaru();" class="i-button no-margin" value="Simpan" />

        </form>
    </div>
</div>
        
   

@include('user.action')
@endsection
@section('script')
    <script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
@stop

