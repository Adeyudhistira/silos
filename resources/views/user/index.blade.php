@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Daftar User</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">

              <div class="right">
                <a href="{{route('user.create')}}"><input type="button" class="icon16-button" value="Tambah Baru"></a>
                {{-- <a href="#"><input type="button" name="submit" id="show-dks" class="i-button no-margin" value="Tandai Batch" /></a> --}}
            </div>
            
        
           <table cellpadding="0" cellspacing="0" border="0" class="display" id="t_user">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Nomor Telepon</th>
                    <th>KTP</th>
                    <th style="width:200px;">Action</th>
                </tr>
            </thead>
            <tbody>
                @if($data)
                    @foreach($data as $item)
                        <tr style="height: 40px;">        
                            <td style="padding:15px;" align="center">{{$item->id}}</td>
                            <td style="padding:15px;" align="center">{{$item->username}}</td>
                            <td style="padding:15px;" align="center">{{$item->email}}</td>
                            <td style="padding:15px;" align="center">{{$item->u_phone_no}}</td>
                            <td style="padding:15px;" align="center">{{$item->id_ktp}}</td>
                            <td>
                             <a href="{{route('user.edit',$item->id)}}"><input type="button" class="icon16-button" value="Edit"></a>
                            <a onclick="HapusDataBaru({{$item->id}})"><input type="button" class="icon16-button" value="Hapus"></a>

                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        </div>
    </div>
</div>



@include('user.action')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
<script>
var t_user = $('#t_user').dataTable({
    "sPaginationType": "full_numbers"   
});
</script>
@stop


