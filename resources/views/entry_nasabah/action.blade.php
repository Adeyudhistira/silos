<script>

function get_list_ua() {
     var _items = "";
    $.ajax({
        type: 'GET',
        url: base_url + '/entry_nasabah/list_ua',
        success: function (res) {
                console.log(res['data']);
                
            
                  var data = $.parseJSON(res);
                _items = "<option value=''>Pilih</option>";
                
                $.each(data['data'], function (k,v) {
                    _items += "<option value='"+v.id+"'>"+v.firstName+"</option>";
                });

                $("#list_ua").html(_items);
                
        }
    }).done(function( res ) {
            
    }).fail(function(res) {
        swal("Terjadi Kesalahan! ");
    });
}

function get_list_uana() {
     var _items = "";
    $.ajax({
        type: 'GET',
        url: base_url + '/entry_nasabah/list_uana',
        success: function (res) {
                console.log(res['data']);
                
            
                  var data = $.parseJSON(res);
                _items = "<option value=''>Pilih</option>";
                
                $.each(data['data'], function (k,v) {
                    _items += "<option value='"+v.id+"'>"+v.firstName+"</option>";
                });

                $("#list_uana").html(_items);
                
        }
    }).done(function( res ) {
            
    }).fail(function(res) {
        swal("Terjadi Kesalahan! ");
    });
}

function getLokasi(set,target) {

     $(".loading-mail").show();
    var kelurahan = $('#'+set).val();

    var _items = "";
    $.ajax({
        type: 'GET',
        url: base_url + '/select/apikelurahan/' + kelurahan,
        success: function (res) {
            $(".loading-mail").hide();
                console.log(res['data']);
                
            
                  var data = $.parseJSON(res);
                _items = "<option value=''>Pilih Lokasi</option>";
                
                
                $.each(data['data'], function (k,v) {
                    _items += "<option value='"+v.id+"'>"+v.wilayah+"</option>";
                });

                $("#"+target).html(_items);
                
        }
    }).done(function( res ) {
         $(".loading-mail").hide();
            
    }).fail(function(res) {
        $(".loading-mail").hide();
        swal("Terjadi Kesalahan! ");
    });
}

function wadaw(data) {
    console.log(data);

     var target = $('#kode_dati2');

        target.find('option').remove().end();
        var valdata = $('#kode_dati1').val();

        @php
            $d_rsca = \DB::select("SELECT * FROM dati2");
        @endphp

        target.append('<option value="">Pilih</option>');
        @foreach($d_rsca as $item)
            if (valdata == {{$item->kode_dati}} ) {
                target.append('<option value="{{$item->kode_dati2}}">{{$item->nama_dati2}}</option>');
            }
        @endforeach
    
}

function provinsi(data) {
    console.log(data);

     var target = $('#id_kotakab');

        target.find('option').remove().end();

        @php
            $d_rsca = \DB::select("SELECT * FROM dati2");
        @endphp

        target.append('<option value="">Pilih</option>');
        @foreach($d_rsca as $item)
            if (data == {{$item->kode_dati}} ) {
                target.append('<option value="{{$item->kode_dati2}}">{{$item->nama_dati2}}</option>');
            }
        @endforeach
    
}

function get_kec(data,target) {
    console.log(data);

            var _items = "";
            $.ajax({
                type: 'GET',
                url: base_url + '/select/kecamatan/' + data,
                success: function (res) {
                    var data = $.parseJSON(res);
                    _items = "<option value=''>Pilih Kecamatan</option>";
                    $.each(data, function (k,v) {
                        _items += "<option value='"+v.kode+"'>"+v.nama+"</option>";
                    });

                    $("#"+target).html(_items);
                }
            });
    
}

function get_kel(data,target) {
    console.log(data);

            var _items = "";
            $.ajax({
                type: 'GET',
                url: base_url + '/select/kelurahan/' + data,
                success: function (res) {
                    var data = $.parseJSON(res);
                    _items = "<option value=''>Pilih Kelurahan</option>";
                    $.each(data, function (k,v) {
                        _items += "<option value='"+v.kode+"'>"+v.nama+"</option>";
                    });

                    $("#"+target).html(_items);
                }
            });
    
}

function convertToRupiah(objek) {
    var separator = ".";
    var a = objek.value;
    var b = a.replace(/[^\d]/g, "");
    var c = "";
    var panjang = b.length;
    var j = 0; for (var i = panjang; i > 0; i--) {
        j = j + 1; if (((j % 3) == 1) && (j != 1)) {
            c = b.substr(i - 1, 1) + separator + c;
        } else {
            c = b.substr(i - 1, 1) + c;
        }
    } objek.value = c;
}



function store_slik() {
    let myForm = document.getElementById('form_slik');
    let formData = new FormData(myForm);

     for (var pair of formData.entries()) {
        console.log(pair[0]+ ', ' + pair[1]); 
    }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });   
       $.ajax({
           type: "POST",
           url: '{{ route('entry_nasabah.store_slik') }}',
           data : formData,
           dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
          
           success: function( msg ) {
           }
       }).done(function( msg ) {
           console.log(msg);
                swal({   
                title: "Informasi",   
                text: msg['rm'],   
                type: "info",   
                confirmButtonColor: "#8cd4f5",   
                confirmButtonText: false,     
                closeOnConfirm: true
            }, function(isConfirm){   
                if (isConfirm) {     
                    window.location.href = '{{ route('entry_nasabah.daftar_prospek_lengkap') }}' ;
                } 
            });
            
        }).fail(function(msg) {
             swal("Terjadi Kesalahan! ");
        });
    

}

function store_appraisal() {
    let myForm = document.getElementById('form_appraisal');
    let formData = new FormData(myForm);

     for (var pair of formData.entries()) {
        console.log(pair[0]+ ', ' + pair[1]); 
    }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });   
       $.ajax({
           type: "POST",
           url: '{{ route('entry_nasabah.store_appraisal') }}',
           data : formData,
           dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
          
           success: function( msg ) {
           }
       }).done(function( msg ) {
        console.log(msg);
           var m;
           if(msg['rc']==200){
              m=msg['rm'];
           }else{
             m=msg['message']['rm'];
           }
                swal({   
                title: "Informasi",   
                text: m,   
                type: "info",   
                confirmButtonColor: "#8cd4f5",   
                confirmButtonText: false,     
                closeOnConfirm: true
            }, function(isConfirm){   
                if (isConfirm) {     
                    window.location.href = '{{ route('entry_nasabah.daftar_prospek_lengkap') }}' ;
                } 
            });
            
        }).fail(function(msg) {
             swal("Terjadi Kesalahan! ");
        });
    

}

function store(type_form) {
    let myForm = document.getElementById('form_entry_nasabah');
    let formData = new FormData(myForm);

    //  for (var pair of formData.entries()) {
    //     console.log(pair[0]+ ', ' + pair[1]); 
    // }
    console.log(formData.append('key2','value2'));
    var a1=$('#pendapatan_bulan').val();
    var a=a1.replace(/\./g,'');
    var b1=$('#pendapatan_lainnya').val();
    var b=b1.replace(/\./g,'');
    var c= parseInt(a);

    if(c<=4000001){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });   
       $.ajax({
           type: "POST",
           url: '{{ route('entry_nasabah.store') }}',
           data : formData,
           dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
          
           success: function( msg ) {
           }
       }).done(function( msg ) {
           console.log(msg);
            if(msg['rc']==1){
                swal("Informasi","Alamat Harus Mengadung Blok dan Nomor","info");
            }else{
                 swal({   
                    title: "Informasi",   
                    text: msg['rm'],   
                    type: "info",   
                    confirmButtonColor: "#8cd4f5",   
                    confirmButtonText: false,     
                    closeOnConfirm: true
                }, function(isConfirm){   
                        // if (isConfirm) {

                        //     // DATA BELUM LENGKAP
                        //     if (type_form == 1) {
                        //         window.location.href = '{{ route('entry_nasabah.daftar_prospek_nasabah') }}' ;
                        //     } else {
                        //         window.location.href = '{{ route('entry_nasabah.daftar_prospek_lengkap') }}' ;

                        //     }     
                        // } 

                        window.location.href = base_url +'/entry_nasabah/daftar_prospek_nasabah';
                    });

            }
           
            
        }).fail(function(msg) {
             swal("Terjadi Kesalahan! ");
        });
    
    }else{

        swal("Informasi","Gaji anda melebihi peraturan PPDPP","info");

    }


      

}

function addDokumen(key) {

    $(".loading-mail").show();
    var myForm = document.getElementById('form_doc_nasabah');
    var formData = new FormData(myForm);

    formData.append('key_file', key);

     for (var pair of formData.entries()) {
        console.log(pair[0]+ ', ' + pair[1]); 
    }

    var url_img ='';

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });   
       $.ajax({
           type: "POST",
           url: '{{ route('entry_nasabah.addDokumen') }}',
           data : formData,
           dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
          
           success: function( msg ) {
           }
       }).done(function( msg ) {

            $(".loading-mail").hide();

            console.log(msg);

            url_img = msg['data']['url_doc'];

            console.log(url_img);
            

           $("#img_show"+key).html("<img src='"+url_img+"' alt='' width='200'>");
           
                swal({   
                title: "Informasi",   
                text: msg['rm'],   
                type: "info",   
                confirmButtonColor: "#8cd4f5",   
                confirmButtonText: false,     
                closeOnConfirm: true
            }, function(isConfirm){   
                // if (isConfirm) {     
                //     location.replace('http://localhost/silos_final/entry_nasabah/523/edit');
                // } 
            });
            
        }).fail(function(msg) {
             swal("Terjadi Kesalahan! ");
             $(".loading-mail").hide(); 
        });
    

}


function getMaxPlafon() {
    let myForm = document.getElementById('form_entry_nasabah');
    let formData = new FormData(myForm);

     for (var pair of formData.entries()) {
        console.log(pair[0]+ ', ' + pair[1]); 
    }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });   
       $.ajax({
           type: "POST",
           url: '{{ route('entry_nasabah.maxPlafon') }}',
           data : formData,
           dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
          
           success: function( msg ) {
           }
       }).done(function( msg ) {
           console.log(msg);
           
            swal({   
                 title: "Info",   
                 text: "Max Plafon :"+ msg['data'] +"\n   Angsuran Perbulan :" + msg['angsuran'] + "\n Sesuaikan Plafon mengacu ke perhitungan Max Plafon ?",   
                 type: "info",   
                  showCancelButton: true,   
                  confirmButtonColor: "#e6b034",   
                  confirmButtonText: "Ya",   
                  cancelButtonText: "Tidak",   
                  closeOnConfirm: true,   
                  closeOnCancel: true
             }, function(isConfirm){   
                  if (isConfirm) {     
                    $("#Plafon").val(msg['data']);
                    $("#total_angsuran_new").val(msg['angsuran']);
                    $("#Plafon_flpp").val(msg['nilaiflpp']);
                    $("#Margin").val(msg['margin']);
                    $("#Uang_muka").val(msg['uang_muka']);
                  } 
             });
             return false;
            
        }).fail(function(msg) {
             swal("Terjadi Kesalahan! ");
        });
    

}

function hitungAngsuran() {
    let myForm = document.getElementById('form_entry_nasabah');
    let formData = new FormData(myForm);

     for (var pair of formData.entries()) {
        console.log(pair[0]+ ', ' + pair[1]); 
    }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });   
       $.ajax({
           type: "POST",
           url: '{{ route('entry_nasabah.hitungAngsuran') }}',
           data : formData,
           dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
          
           success: function( msg ) {
           }
       }).done(function( msg ) {
           console.log(msg);
           $("#total_angsuran_new").val(msg['data']);
           $("#Plafon").val(msg['plafon']);
           $("#Plafon_flpp").val(msg['nilaiflpp']);
           $("#Margin").val(msg['margin']);
            //     swal({   
            //     title: "Informasi",   
            //     text: msg['rm'],   
            //     type: "info",   
            //     confirmButtonColor: "#8cd4f5",   
            //     confirmButtonText: false,     
            //     closeOnConfirm: true
            // }, function(isConfirm){   
            //     // if (isConfirm) {     
            //     //     location.reload();
            //     // } 
            // });
            
        }).fail(function(msg) {
             swal("Terjadi Kesalahan! ");
        });
    

}



function update() {
    let myForm = document.getElementById('form_entry_nasabah_edit');
    let formData = new FormData(myForm);

    //  for (var pair of formData.entries()) {
    //     console.log(pair[0]+ ', ' + pair[1]); 
    // }
    console.log(formData.append('key2','value2'));
    var a1=$('#pendapatan_bulan').val();
    var a=a1.replace(/\./g,'');
    var b1=$('#pendapatan_lainnya').val();
    var b=b1.replace(/\./g,'');
    var c= parseInt(a);
    
    if(c<=4000001){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });   
       $.ajax({
           type: "POST",
           url: '{{ route('entry_nasabah.update_nasabah') }}',
           data : formData,
           dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
          
           success: function( msg ) {
           }
       }).done(function( msg ) {
           console.log(msg);
                if(msg['rc']==1){
                    swal("Informasi","Alamat Harus Mengadung kalimat blok dan nomor","info");
                }else{
                    swal({   
                        title: "Informasi",   
                        text: msg['rm'],   
                        type: "info",   
                        confirmButtonColor: "#8cd4f5",   
                        confirmButtonText: false,     
                        closeOnConfirm: true
                    }, function(isConfirm){   

                        window.location.href = base_url +'/entry_nasabah/daftar_prospek_nasabah';
                    });
                }
            
        }).fail(function(msg) {
             swal("Terjadi Kesalahan! ");
        });
    
    }else{

        swal("Informasi","Gaji anda melebihi peraturan PPDPP","info");

    }


      

}

</script>