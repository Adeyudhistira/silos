@section('content')

 
@php

    function funNum($data)
    {
       return number_format($data,0,',','.');
    }

@endphp

  @php
    $doc1 = "Tidak Ada Data";
    $doc1_id = "";
    $doc2 = "Tidak Ada Data";
    $doc2_id = "";
    $doc3 = "Tidak Ada Data";
    $doc3_id = "";
    $doc4 = "Tidak Ada Data";
    $doc4_id = "";

    if($rc==200){
    if ($data['documents']) {
        foreach ($data['documents'] as $key => $value) {

            if ($value['tipeDok'] == 1 ) {
                $doc1 = "<img style='border: 3px solid #d0d0d0;padding: 5px;margin: 5px;' src='".env('API_BASE_URL').$value['urlDoc']."' alt='' width='200'>";
                $doc1_id = $value['id'];
            }

            if ($value['tipeDok'] == 2 ) {
                $doc2 = "<img style='border: 3px solid #d0d0d0;padding: 5px;margin: 5px;' src='".env('API_BASE_URL').$value['urlDoc']."' alt='' width='200'>";
                $doc2_id = $value['id'];
            } 

            if ($value['tipeDok'] == 3 ) {
                $doc3 = "<img style='border: 3px solid #d0d0d0;padding: 5px;margin: 5px;' src='".env('API_BASE_URL').$value['urlDoc']."' alt='' width='200'>";
                $doc3_id =  $value['id'];
            } 

             if ($value['tipeDok'] == 9 ) {
                $doc4 = "<img style='border: 3px solid #d0d0d0;padding: 5px;margin: 5px;' src='".env('API_BASE_URL').$value['urlDoc']."' alt='' width='200'>";
                $doc4_id =  $value['id'];
            } 
        
        }
    } else {
        //echo "Tidak Ada Data";
    }

    }
    
@endphp
            
<div class="c-forms form-lebar"> 
            <a class="i-button no-margin" href="{{route('entry_nasabah.daftar_prospek_lengkap')}}">Kembali</a>   

    <div class="box-element" style="
    margin-top: 20px;
">
@if($rc==200)
        <form id="form_entry_nasabah" method="post" enctype="multipart/form-data">
            
        <input type="hidden" name="id" id="id" value="1">

            <input type="hidden" value="{{$id_prospek_new}}" name="id_prospek_new" id="id_prospek_new">    

            <div class="i-label">
                <label for="text_field">Data Pribadi</label>
            </div>   
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Nama Lengkap</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                <input value="{{$data['nmProspek']}}" type="text" name="nama_nasabah" id="nama_nasabah" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Nomor Identitas</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$data['noIdentitas']}}" type="text" name="no_identitas" id="no_identitas" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Alamat</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$data['alamat']}}" type="text" name="alamat" id="alamat" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            {{-- <div class="i-label">
                <label for="text_field">Provinsi
</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                @php
                    $dt = \DB::select("SELECT * FROM dati1");
                @endphp
                <select class="i-text required" onchange="wadaw(this.value)" name="kode_dati1" id="kode_dati1">
                    <option value="">Pilih</option>
                    @foreach ($dt as $item)
                        <option value="{{$item->kode_dati}}">{{$item->nama_dati}}</option>
                    @endforeach
                </select>
                
            </div>
            <div class="clearfix"></div> --}}

             
            <div class="i-label">
                <label for="text_field">Cari Kelurahan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="" type="text" name="nama_kelurahan" id="nama_kelurahan" class="i-text required"></input></div>
                    <input type="button" style="margin: 10px 0 !important;" onclick="getLokasi('nama_kelurahan','setlokasi');" class="i-button no-margin" value="Cari" />

            </div>

             <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">Pilih Lokasi
            </label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <select id="setlokasi" name="lokasipengguna" class="js-example-basic-single i-text required">
                    </select>
                    
            </div>
            <div class="clearfix"></div>
        
            {{-- <div class="i-label">
                <label for="text_field">Kota / Kab
</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <select id="kode_dati2" onchange="get_kec(this.value,'sid_kecamatan')" class="i-text required" name="kode_dati2">
                    </select>

                    
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Kecamatan
</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                @php
                    $dt = \DB::select("SELECT * FROM wilayah where kode like'1101%' and kode not in ('1101') and kode=SUBSTR(kode,1,6)");
                @endphp
                <select class="i-text required" onchange="get_kel(this.value,'sid_kelurahan')"  name="sid_kecamatan" id="sid_kecamatan">
                   <option value="">Pilih</option>
                    </select>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Kelurahan
</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                @php
                    $dt = \DB::select("SELECT * FROM wilayah where kode like'110101%' and kode not in ('110101')");
                @endphp
                <select class="i-text required" name="sid_kelurahan" id="sid_kelurahan">
                    </select>
            </div> --}}
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">No Telp Rumah
</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$data['noHp']}}" type="text" name="telp" id="telp" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">No HP</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$data['telp']}}" type="text" name="no_hp" id="no_hp" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div><div class="i-label">
                <label for="text_field">Tempat Lahir
</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$data['tempatLahir']}}" type="text" name="tpt_lahir" id="tpt_lahir" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Tanggal Lahir
</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$data['tglLahir']}}" type="date" name="tgl_lahir" id="tgl_lahir" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Nama Gadis Ibu Kandung
</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input  value="{{$data['sidNgik']}}" type="text" name="ibukandung" id="ibukandung" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Jenis Kelamin
</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                     <select class="i-text required" name="jns_kelamin" id="jns_kelamin">
                    <option value="">Pilih</option>
                    <option {{ ($data['idJenisKelamin'] == 0) ? "selected":"" }} value="0">Laki-Laki</option>
                    <option {{ ($data['idJenisKelamin'] == 1) ? "selected":"" }} value="1">Perempuan</option>
                   
                </select>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">NPWP</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{ $data['npwp']}}" type="text" name="npwp" id="npwp" class="i-text required" maxlength="15"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Status Menikah</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                     <select class="i-text required" name="stat_menikah" id="stat_menikah">
                    <option value="">Pilih</option>
                    <option {{ ($data['isMenikah'] == true) ? "selected":"" }} value="t">Menikah</option>
                    <option {{ ($data['isMenikah'] == false) ? "selected":"" }} value="f">Belum Menikah</option>
                </select>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field" id="nmPasangan1" @if($data['isMenikah']==false) style="display:none" @endif>Nama Pasangan</label>
                <input type="hidden" value="{{ $data['namaPasangan']}}" nama="nmPasanganFake" id="nmPasanganFake">
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input @if($data['isMenikah']==false) style="display:none" @endif value="{{ $data['namaPasangan']}}" type="text" name="nmPasangan" id="nmPasangan" class="i-text"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field" id="ktpPasangan1" @if($data['isMenikah']==false) style="display:none" @endif>KTP Pasangan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input @if($data['isMenikah']==false) style="display:none" @endif value="{{ $data['noKtpPasangan']}}" type="text" name="ktpPasangan" id="ktpPasangan" class="i-text" maxlength="16" minlength="16"></input></div>
                    <input type="hidden" value="{{ $data['noKtpPasangan']}}" nama="ktpPasanganFake" id="ktpPasanganFake">
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Catatan AO</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{ $data['aoNote']}}" type="text" name="aoNote" id="aoNote" class="i-text"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Pendapatan per Bulan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input onkeyup="convertToRupiah(this)" value="{{ number_format($data['pendapatanBulan'],0,',','.')}}" type="text" name="pendapatan_bulan" id="pendapatan_bulan" class="i-text required"></input></div>
                    <input value="{{ $data['pendapatanBulan']}}" type="hidden" id="pendapatan_bulan1"></input>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Take Home Pay</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input onkeyup="convertToRupiah(this)" value="{{ number_format($data['pendapatanLainnya'],0,',','.')}}" type="text" name="pendapatan_lainnya" id="pendapatan_lainnya" class="i-text required"></input>
                    <input value="{{$data['pendapatanLainnya']}}" type="hidden" id="pendapatan_lainnya1"></input>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Jenis Pekerjaan
</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                      @php
                    $dt = \DB::select("SELECT * FROM ref_pekerjaan");
                @endphp
                <select class="i-text required" name="id_pekerjaan" id="id_pekerjaan">
                    <option value="">Pilih</option>
                    @foreach ($dt as $item)
                        <option {{($data['idPekerjaan'] == $item->id_pekerjaan) ? "selected":"" }} value="{{$item->id_pekerjaan}}">{{$item->nm_pekerjaan}}</option>
                    @endforeach
                </select>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Sumber Pendapatan</label>

            </div>                                  
            <div class="section-right">
                <div class="section-input">
                @php
                    $dt = \DB::select("SELECT * FROM ref_sumber_pendapatan");
                @endphp
                <select class="i-text required" name="sumber_pendapatan" id="sumber_pendapatan">
                    <option value="">Pilih</option>
                    @foreach ($dt as $item)
                        <option @if($data['sumberPendapatanId']==$item->id_sumber) selected @endif value="{{$item->id_sumber}}">{{$item->keterangan}}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Pilih Produk
</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                @php
                    $dt = \DB::select("SELECT * FROM ref_product");
                @endphp
                <select class="i-text required" name="id_product" id="id_product">
                    <option value="">Pilih</option>
                    @foreach ($dt as $item)
                        <option {{($data['idProduct'] == $item->id) ? "selected":"" }} value="{{$item->id}}">{{$item->prod_name}}</option>
                    @endforeach
                </select>
                    {{-- <input value="" type="text" name="id_product" id="id_product" class="i-text required"></input></div> --}}
            </div>
            
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Struktur Pembiayaan</label>
            </div>   
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Harga Objek</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input onkeyup="convertToRupiah(this)" value="{{funNum($data['hargaObject'])}}" type="text" name="harga_object" id="harga_object" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Uang Muka</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input onkeyup="convertToRupiah(this)" value="{{funNum($data['uangMuka'])}}" type="text" name="uang_muka" id="Uang_muka" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Plafon</label>
            </div>                               
            <div class="section-right">
               
                <div class="section-input">
                    <input readonly onkeyup="convertToRupiah(this)" value="{{ funNum($data['plafon']) }}" type="text" name="Plafon" id="Plafon" class="i-text required"></input></div>
                    
                </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Nilai FLPP</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input readonly value="{{ funNum($data['plafonFlpp']) }}" onkeyup="convertToRupiah(this)" type="text" name="plafon_flpp" id="Plafon_flpp" class="i-text required"></input></div>
                    
                </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Margin</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input readonly value="{{ funNum($data['margin']) }}" type="text" name="margin" id="Margin" class="i-text required"></input></div>
            </div>
             <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Jangka Waktu</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$data['jangkaWaktu']}}" type="text" name="jangka_waktu" id="jangka_waktu" class="i-text required"></input>*Bln</div>
            </div>
            <div class="clearfix"></div>
              <div class="i-label">
                <label for="text_field">Angsuran per Bulan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input readonly onkeyup="convertToRupiah(this)" value="{{funNum($data['totalAngsuran'])}}" type="text" name="total_angsuran" id="total_angsuran_new" class="i-text required"></input></div>


                 
                    <input type="button" style="margin: 20px !important;" onclick="getMaxPlafon();" class="i-button no-margin" value="Max Plafon" />
                    <input type="button" style="margin: 20px !important;" onclick="hitungAngsuran();" class="i-button no-margin" value="Hitung Angsuran" />
            </div>
           <div class="clearfix"></div>

            
            {{-- <div class="i-label">
                <label for="text_field">wf_type_id
</label>
            </div>                                   --}}
            <div class="section-right">
                <div class="section-input">
                    <input readonly style="background: #dedede;" value="1" type="hidden" name="wf_type_id" id="wf_type_id" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            {{-- <div class="i-label">
                <label for="text_field">status_pembiayaan
</label>
            </div>                                   --}}
            <div class="section-right">
                <div class="section-input">
                    <input style="background: #dedede;" readonly value="0" type="hidden" name="status_pembiayaan" id="status_pembiayaan" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            {{-- <div class="i-label">
                <label for="text_field">user_id
</label>
            </div>                                   --}}
            <div class="section-right">
                <div class="section-input">
                    <input readonly style="background: #dedede;" value="{{Auth::user()->id}}" type="hidden" name="user_id" id="user_id" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>

            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Data Agunan
</label>
            </div>   
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Nama Developer</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                @php
                    $dt = \DB::select("SELECT * FROM ref_developer");
                @endphp
                <select class="i-text required" name="id_developer" id="id_developer">
                    <option value="">Pilih</option>
                    @foreach ($dt as $item)
                        <option value="{{$item->id}}">{{$item->developer_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Nama Perumahan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <select name="id_perumahan" id="id_perumahan" class="i-text required">
                    </select>

                </div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Alamat Object</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input @if($data['channel']['alamat']) value="{{$data['channel']['alamat']}}" @endif type="text" name="alamat1" id="alamat1" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">Cari Kelurahan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="" type="text" name="nama_kelurahan_agunan" id="nama_kelurahan_agunan" class="i-text required"></input></div>
                    <input type="button" style="margin: 10px 0 !important;" onclick="getLokasi('nama_kelurahan_agunan','setlokasiagunan');" class="i-button no-margin" value="Cari" />

            </div>

             <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">Pilih Lokasi
            </label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <select id="setlokasiagunan" name="lokasiagunan" class="js-example-basic-single i-text required">
                    </select>
                    
            </div>
            <div class="clearfix"></div>

            {{-- <div class="i-label">
                <label for="text_field">Provinsi
</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                        @php
                    $dt = \DB::select("SELECT * FROM dati1");
                @endphp
                <select class="i-text required" onchange="provinsi(this.value)" name="kode_dati" id="kode_dati">
                    <option value="">Pilih</option>
                    @foreach ($dt as $item)
                        <option value="{{$item->kode_dati}}">{{$item->nama_dati}}</option>
                    @endforeach
                </select>

              </div>
            {{-- <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Kota / Kab
</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <select id="id_kotakab" onchange="get_kec(this.value,'id_kecamatan')" class="i-text required" name="id_kotakab">
                    </select>
        </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Kecamatan
</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                
                <select class="i-text required" onchange="get_kel(this.value,'id_kelurahan')" name="id_kecamatan" id="id_kecamatan">
                   <option value="">Pilih</option>
                  
                    </select>
              </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Kelurahan
</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                  
                <select class="i-text required" name="id_kelurahan" id="id_kelurahan">
                    <option value="">Pilih</option>
                  
                    </select>
            </div> --}} 
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Luas Tanah (m2)
</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="" type="text" name="luas_tanah_fisik" id="luas_tanah_fisik" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Luas Bangunan (m2)
</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="" type="text" name="luas_imb" id="luas_imb" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>

            <div class="clearfix"></div>
            

           

        </form>
          
        <div class="clearfix"></div>
        <input type="button" value="Tambah Dokumen" class="i-button no-margin" onclick="tambahdok({{$id_prospek_new}})"> 
        <form id="form_doc_nasabah" method="post" enctype="multipart/form-data">
        <div class="i-label">
                <label for="text_field">Daftar Dokumen Nasabah</label>
            </div>   
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <table>
                <tr>
                    <td width="150px"> <b>Jenis Dokumen</b> </td>
                    <td width="150px"> <b>Preview</b> </td>
                    <td> <b>Action</b> </td>
                </tr>
                @if ($doc1 == "Tidak Ada Data")
                @else 
                <tr>
                    <td>KTP</td>
                    <td>      
                       <div id="img_show1">
                            @php echo $doc1; @endphp
                        </div>       
                    </td>
                    <td>
                        <input type="hidden" value="{{$doc1_id}}" name="id_doc1">
                        <input value="" onchange="addDokumen(1)" type="file" name="doc_file1" id="doc_file1" class="i-text required"></input></div>
                    </td>
                </tr>
                @endif
                 @if ($doc2 == "Tidak Ada Data")
                  @else  
                <tr>
                    <td>Foto Selfie</td>
                    <td>
                        <div id="img_show2">
                            @php echo $doc2; @endphp
                        </div>
                    </td>
                    <td>
                        <input type="hidden" value="{{$doc2_id}}" name="id_doc2">
                        <input value="" onchange="addDokumen(2)" type="file" name="doc_file2" id="doc_file2" class="i-text required"></input></div>
                    </td>
                </tr>
                 @endif
                 @if ($doc3 == "Tidak Ada Data")
                  @else  
                <tr>
                    <td>KK</td>
                    <td >
                        <div id="img_show3">
                            @php echo $doc3; @endphp
                        </div>
                        
                        
                    </td>
                    <td>
                        <input type="hidden" value="{{$doc3_id}}" name="id_doc3">
                        <input value="" onchange="addDokumen(3)" type="file" name="doc_file3" id="doc_file3" class="i-text required"></input></div>
                    </td>
                </tr>
                @endif

                @if ($doc4 == "Tidak Ada Data")
                  @else  
                <tr>
                    <td>Other</td>
                    <td >
                        <div id="img_show4">
                            @php echo $doc4; @endphp
                        </div>
                        
                        
                    </td>
                    <td>
                        <input type="hidden" value="{{$doc4_id}}" name="id_doc4">
                        <input value="" onchange="addDokumen(4)" type="file" name="doc_file4" id="doc_file4" class="i-text required"></input></div>
                    </td>
                </tr>
                @endif
            </table>

             <div class="i-divider"></div>
            <div class="clearfix"></div>

             <input type="button" style="margin: 20px !important;" onclick="store({{$type_form}});" class="i-button no-margin" value="Simpan" />
            <div class="i-divider"></div>
            {{-- <input type="button" style="margin: 20px !important;" onclick="addDokumen();" class="i-button no-margin" value="Upload Dokumen Baru" /> --}}

             <input value="{{$data['wilayah']['wilayah']}}" type="hidden" name="kelurahan" id="kelurahan1"></input>
        </form>
        @else
        <p>{{$rm}}</p>
        @endif

    </div>
</div>
        
@include('entry_nasabah.action')
@endsection
@section('script')
<script type="text/javascript">
    $(".loading-mail").show();
    var kelurahan = $('#kelurahan1').val();

    var _items = "";
    $.ajax({
        type: 'POST',
        url: base_url + '/select/apikelurahan1?id=' + kelurahan,
        success: function (res) {
            $(".loading-mail").hide();
                console.log(res['data']);
                
            
                  var data = $.parseJSON(res);
                
                
                $.each(data['data'], function (k,v) {
                    _items += "<option value='"+v.id+"'>"+v.wilayah+"</option>";
                });

                $("#setlokasi").html(_items);
                
        }
    }).done(function( res ) {
         $(".loading-mail").hide();
            
    }).fail(function(res) {
        $(".loading-mail").hide();
        swal("Terjadi Kesalahan! ");
    });

    $("#stat_menikah").change(function() {
        var sm = $('#stat_menikah option:selected').val();
        var nmPasanganFake = $('#nmPasanganFake').val();
        var ktpPasanganFake = $('#ktpPasanganFake').val();
        if(sm == 'f')
        {
            $('#nmPasangan').css('display','none');
            $('#ktpPasangan').css('display','none');
            $('#nmPasangan1').css('display','none');
            $('#ktpPasangan1').css('display','none');
            $('#nmPasangan').val('');
            $('#ktpPasangan').val('');
        } else {
            $('#nmPasangan').css('display','block');
            $('#ktpPasangan').css('display','block');
            $('#nmPasangan1').css('display','block');
            $('#ktpPasangan1').css('display','block');
            $('#nmPasangan').val(nmPasanganFake);
            $('#ktpPasangan').val(ktpPasanganFake);
        }
    });

     $( "#id_developer" ).change(function() {
      var _items = "";
    $.ajax({
                type: 'GET',
                url: base_url+'/select/perumahan/'+this.value,
                success: function (res) {

                    var data = $.parseJSON(res);
                    
                    $.each(data, function (k,v) {
                    console.log(v);
                        _items += "<option value='"+v.id+"'>"+v.residence_name+"</option>";
                    });
                    $('#id_perumahan').html(_items);
                }
        });
  });
function tambahdok(id) {
        window.location.href = base_url +'/tambah_dokumen_entry_nasabah?id='+id;
    }
</script>
<script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>

@stop

