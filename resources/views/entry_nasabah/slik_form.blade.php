@section('content')
<div class="c-forms">       
     <a class="i-button no-margin" href="{{route('entry_nasabah.daftar_prospek_lengkap')}}">Kembali</a>   
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>SLIK</h3><a href="" class="collapsable"></a></div>
        
        <div class="box-content no-padding">
           <form method="post" action="" class="i-validate"> 
                <fieldset>
                   <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">No KTP</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="no_ktp" readonly @if($data) value="{{$data[0]['noIdentitas']}}" @else value="{{$data1[0]->no_identitas}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Nasabah</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="nama" readonly @if($data) value="{{$data[0]['namaNasabah']}}" @else value="{{$data1[0]->nama_nasabah}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Alamat</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="alamat" readonly @if($data) value="{{$data[0]['alamat']}}" @else value="{{$data1[0]->alamat}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Tempat Lahir</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="tmp_lahir" readonly @if($data) value="{{$data[0]['tempatLahir']}}" @else value="{{$data1[0]->tpt_lahir}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Tanggal Lahir</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="tgl_lahir" readonly @if($data) value="{{date('d-M-Y',strtotime($data[0]['tanggalLahir']))}}" @else value="{{date('d-M-Y',strtotime($data1[0]->tgl_lahir))}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Pasangan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                @foreach ($data1 as $item)
                                <input type="text" name="nmPasangan" readonly @if($data1) value="{{$data1[0]->nama_p}}" @else value="{{$data[0]['namaPasangan']}}" @endif class="i-text i-form-tooltip"></input>
                                @endforeach
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">KTP Pasangan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="ktpPasangan" readonly @if($data1) value="{{$data1[0]->ktp_p}}" @else value="{{$data[0]['noIdentitasPasangan']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>  
                    @if($data)
                        <div class="section-center">
                            <input type="button" name="submit" id="submit" onclick="detail()" class="i-button no-margin" value="Hasil Slik Checking"/>                         
                        <div class="clearfix"></div>
                        </div>
                    @else                            
                        <div class="section-center">
                            <input type="button" name="submit" id="submit" onclick="kirimslik('{{$id}}','{{$data1[0]->id}}','{{$data1[0]->no_identitas}}','{{$data1[0]->nama_nasabah}}','{{$data1[0]->alamat}}','{{$data1[0]->tpt_lahir}}','{{$data1[0]->tgl_lahir}}','{{$data1[0]->nama_p}}','{{$data1[0]->ktp_p}}')" class="i-button no-margin" value="KIRIM PERMINTAAN SLIK CHECKING"/>                         
                        <div class="clearfix"></div>
                        </div>
                    @endif    

                    </section>

                    <div class="show-slik">
                        <section>
                            <div class="section-left-s">
                                <label for="text_tooltip">Status SLIK</label>
                            </div>                                  
                            <div class="section-right">                                         
                                <div class="section-input">
                                      <select disabled name="status" id="status" class="i-text required">
                                        @if($data)
                                        <option value="">Pilih</option>
                                        @foreach($data2 as $item)
                                        <option @if($data[0]['biCheckStatusId']==$item['id']) selected @endif value="{{$item['id']}}">{{$item['definition']}}</option>
                                        @endforeach
                                        
                                        @else
                                        <option value="">Masih Proses</option>
                                        
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </section>
                        <section>
                            <div class="section-left-s">
                                <label for="text_tooltip">Catatan</label>
                            </div>                                  
                            <div class="section-right">                                         
                                <div class="section-input">
                                    <input type="text" name="catatan" readonly @if($data) value="{{$data[0]['notes']}}" @else value="Masih Proses" @endif class="i-text i-form-tooltip"></input>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </section>
                        <section>
                            <div class="section-left-s">
                                <label for="text_tooltip">Total Angsuran</label>
                            </div>                                  
                            <div class="section-right">                                         
                                <div class="section-input">
                                    <input type="text" name="angsuran" readonly @if($data) value="{{number_format($data[0]['angsuran'],0,',','.')}}"  @else value="Masih Proses"  @endif class="i-text i-form-tooltip"></input>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </section>
                        <section>
                            <div class="section-left-s">
                                <label for="text_tooltip">File</label>
                            </div>                                  
                            <div class="section-right">                                         
                                <div class="section-input">
                                    @if($data)
                                        @if($data[0]['document'])
                                            <a style="color: rgb(255, 255, 255);" href="http://silos-dev.basys.co.id/{{$data[0]['document']['urlDoc']}}"><i class="fa fa-file-excel-o"></i> <span>Download File</span></a>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </section>
                    </div>
                    
                </fieldset>
            </form>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$('.show-slik').hide();
    function detail() {
        var id=$('#submit').val();
        
        if(id=='Hasil Slik Checking'){

            $('.show-slik').show();
            $('#submit').val('Tutup');

        }else{

            $('.show-slik').hide();
            $('#submit').val('Hasil Slik Checking');

        }
    }
    function kembali() {
        
        window.location.href = base_url +'/nasabah_on_proses';
    }

    function kirimslik(pembiayaanId,nasabahId,noIdentitas,namaNasabah,alamat,tempatLahir,tanggalLahir,namaP,ktpP) {
        
        $(".loading-mail").show();
        $.ajax({
            type: 'POST',
            url: base_url + '/kirimslik_entry_nasabah?pembiayaanId='+ pembiayaanId +'&&nasabahId='+ nasabahId +'&&noIdentitas='+ noIdentitas +'&&namaNasabah='+ namaNasabah +'&&alamat='+ alamat +'&&tempatLahir='+ tempatLahir +'&&tanggalLahir='+tanggalLahir+'&&namaP='+ namaP +'&&ktpP='+ ktpP,
            success: function (res) {
             
                $(".loading-mail").hide();
                console.log(res['data']);
                swal({   
                title: "Informasi",   
                text: "Berhasil",   
                type: "info",   
                confirmButtonColor: "#8cd4f5",   
                confirmButtonText: false,     
                closeOnConfirm: true
                }, function(isConfirm){   
                    if (isConfirm) {     
                        location.reload(); 
                    } 
                });
            }
        }).done(function( res ) {
             $(".loading-mail").hide();
                
        }).fail(function(res) {
            $(".loading-mail").hide();
            swal("Terjadi Kesalahan! ");
        });
    }
</script>
@stop
