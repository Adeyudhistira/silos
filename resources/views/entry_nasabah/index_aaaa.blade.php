@section('content')

<div class="c-forms">       
    <div class="box-element">
    <div class="box-head-light"><span class="forms-16"></span><h3> Data Nasabah Core Banking </h3><a href="" class="collapsable"></a></div>
           @if ($message = Session::get('success'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg success-msg ">{{ $message }}<a href="">×</a></div>
            </div>
            @endif


            @if ($message = Session::get('error'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg error-msg">{{ $message }}<a href="">×</a></div>
            </div>
            @endif

        
         
        {{-- <div class="box-content no-padding">
           <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
            <thead>
                <tr>
                    <th> Nama Nasabah</th>
                    <th>Alamat</th>
                    <th>no KTP</th>
                    <th>Nama Gadis Ibu Kandung</th>
                    <th>Action</th>
                </tr>
            </thead>
        
            <tbody>
                <tr>
                    <td>{{$data['nmProspect']}}</td>
                    <td>{{$data['alamat']}}</td>
                    <td>{{$data['noIdentitas']}}</td>
                    <td>-</td>
                    <td>
                        <a href="{{route('entry_nasabah.edit',1)}}"><input type="button" class="icon16-button" value="Pilih"></a>
                    </td>
                </tr>
              
            </tbody>
            <tfoot>
                <tr>
                   <th>Nama Nasabah</th>
                    <th>Alamat</th>
                    <th>no KTP</th>
                    <th>Nama Gadis Ibu Kandung</th>
                    <th>Action</th>
                </tr>
            </tfoot>
        </table> --}}
    </div>
</div>
</div>
<!-- modal content -->
<div id="modal-pengajuan" class="modal-container">
    <div class="modal-head"><h3>Konfirmasi Cabang Proses</h3></div>
    <div class="modal-body">
        <div class="i-label">
            <label for="text_field">Pilih Cabang</label>
        </div>                                  
        <div class="section-right">
            <div class="section-input">
              
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="modal-footer">
        <input type="submit" name="submit" id="" class="i-button no-margin" value="Kirim" />
        <div class="clearfix"></div>
    </div>
</div>
@include('entry_nasabah.action')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
@stop

