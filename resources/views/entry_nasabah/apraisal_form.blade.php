@section('content')


<div class="c-forms form-lebar"> 
            <a class="i-button no-margin" href="{{route('entry_nasabah.daftar_prospek_lengkap')}}">Kembali</a>   

    <div class="box-element" style="
    margin-top: 20px;
">
        <form id="form_appraisal" method="post" enctype="multipart/form-data">
            
            <input type="hidden" value="{{$id}}" name="id" id="id">    

            <div class="i-label">
                <label for="text_field">Konfirmasi User yang dituju</label>
            </div>   
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Pilih User Appraiser</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    
                <select class="i-text required" name="list_ua" id="list_ua">
                  
                </select>
                </div>
            </div>
            <div class="clearfix"></div>
             <div class="i-divider"></div>
             <div class="i-label">
                <label for="text_field">Pilih User Analyst</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    
                <select class="i-text required" name="list_uana" id="list_uana">
                  
                </select>
                </div>
            </div>
            <div class="clearfix"></div>
             <div class="i-divider"></div>
            <div class="clearfix"></div>

             <input type="button" style="margin: 20px !important;" onclick="store_appraisal();" class="i-button no-margin" value="Simpan" />
            <div class="i-divider"></div>
            {{-- <input type="button" style="margin: 20px !important;" onclick="addDokumen();" class="i-button no-margin" value="Upload Dokumen Baru" /> --}}


        </form>
    </div>
</div>
        
@include('entry_nasabah.action')
@endsection
@section('script')
    <script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>

    <script>
        get_list_ua();
        get_list_uana();
    </script>
@stop

