@section('content')

@php
    // dd($data['0']['nmProspek']);
@endphp
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Daftar Prospek</h3><a href="" class="collapsable"></a></div>
           @if ($message = Session::get('success'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg success-msg ">{{ $message }}<a href="">×</a></div>
            </div>
            @endif


            @if ($message = Session::get('error'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg error-msg">{{ $message }}<a href="">×</a></div>
            </div>
            @endif
         
        <div class="box-content no-padding">
           {{-- <a href="{{route('entry_prospek')}}"><input type="button" class="icon16-button data-16" value="Tambah Prospek"></a> --}}
           <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
            <thead>
                <tr>
                    <th>Nama Nasabah</th>
                    <th>Alamat</th>
                    <th>Produk</th>
                    <th>Cabang</th>
                    <th>Plafon</th>
                    <th>Jangka Waktu</th>
                    <th>Angsuran</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if($data)
                @if($rc=200)
                    @foreach($data as $item)
                        <tr>
                            <td>{{$item['nasabah']['namaNasabah']}}</td>
                            <td>{{$item['nasabah']['alamat']}}</td>
                            <td>{{$item['product']['prodName']}}</td>
                            <td>{{$item['branch']['branchName']}}</td>
                             <td>{{number_format($item['plafon'],0,',','.')}}</td>
                            <td>{{$item['jangkaWaktu']}} Bln</td>

                            <td>{{number_format($item['totalAngsuran'],0,',','.')}}</td>
                           
                            <td nowrap>
                                <a href="{{route('entry_nasabah.form_edit',['id'=> $item['id'],'id_nasabah'=> $item['nasabahId'],'id_prospek'=> $item['idProspek']])}}"><input type="button" class="icon16-button" value="Ubah Data"></a>
                                <a href="{{route('entry_nasabah.form_slik',['id'=> $item['id'],'id_nasabah'=> $item['nasabahId'] ])}}"><input type="button" class="icon16-button" value="Slik"></a>
                                @if ($item['bicheck']['biCheckStatusId']!=0)
                                <a href="{{route('entry_nasabah.form_appraisal',$item['id'])}}"><input type="button" class="icon16-button" value="Kirim Req Analisa"></a>
                                @endif
                              
                                {{-- <input type="button" class="button" onclick="proses({{$item->id}})" value="Proses Pengajuan"> --}}

                            </td>
                        </tr>
                    @endforeach
                    @else
                    <p>{{$rm}}</p>
                    @endif
                @endif
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection
@section('script')

<script type="text/javascript">
    function ubah(id) {
        window.location.href = base_url +'/ubah_prospek?id=' + id;
    }

    function pengajuan(id) {
        window.location.href = base_url +'/kirim_pengajuan?id=' + id;
    }
     function proses(id) {
        window.location.href = base_url +'/entry_nasabah';
    }
</script>
@stop
