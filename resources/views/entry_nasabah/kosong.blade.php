@section('content')

<div class="c-forms">       
    <div class="box-element">
    <div class="box-head-light"><span class="forms-16"></span><h3>{{$nama}}</h3><a href="" class="collapsable"></a></div>
          
         
        <div class="box-content">
           Data Tidak Ditemukan.. Klik Lanjutkan untuk Isi Kelengkapan Data Nasabah!!
           <a href="{{route('entry_nasabah.form_prospek',['id'=>$id,'type'=>1])}}"><input type="button" class="icon16-button" value="Lanjutkan"></a>
    </div>
</div>
</div>
<!-- modal content -->
<div id="modal-pengajuan" class="modal-container">
    <div class="modal-head"><h3>Konfirmasi Cabang Proses</h3></div>
    <div class="modal-body">
        <div class="i-label">
            <label for="text_field">Pilih Cabang</label>
        </div>                                  
        <div class="section-right">
            <div class="section-input">
              
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="modal-footer">
        <input type="submit" name="submit" id="" class="i-button no-margin" value="Kirim" />
        <div class="clearfix"></div>
    </div>
</div>
@include('entry_nasabah.action')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
@stop

