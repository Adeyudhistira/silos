@section('content')
<div class="c-forms">
<button class="i-button no-margin" onclick="kembali()">Kembali</button>       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Checklist PPDPP</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
           <form method="post" action="{{route('update_ppdpp_nasabah_approval')}}" class="i-validate"> 
            @csrf
                <fieldset>
                    @if($data[0]['checklist'])
                        <input type="hidden" name="idAgunanProperty" value="{{$data[0]['checklist'][0]['idAgunanProperty']}}">
                        @foreach($data[0]['checklist'] as $item)
                            <section>
                                <div class="section-left-s">
                                    <label for="text_tooltip">{{$item['label']}}</label>
                                </div>
                                <input type="hidden" value="{{$item['subParmId']}}" name="subParmId[]">                                  
                                <div class="section-right">                                         
                                    <div class="section-input">
                                        <select disabled name="valueParmId_{{$item['subParmId']}}" class="i-text">
                                            <option value="1" @if($item['valueParmId']==1) selected @endif>Ya</option>
                                            <option value="0" @if($item['valueParmId']==0) selected @endif>Tidak</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </section>     
                        @endforeach
                    @endif
                </fieldset>
            </form>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$('.detail-show').hide();
    function detail() {
        var id=$('#submit').val();
        
        if(id=='Lihat Detail Scoring'){

            $('.detail-show').show();
            $('#submit').val('Tutup');

        }else{

            $('.detail-show').hide();
            $('#submit').val('Lihat Detail Scoring');

        }
    }
    function kembali() {
        window.location.href = base_url +'/permohonan_cair_cbs';
    }
</script>
@stop