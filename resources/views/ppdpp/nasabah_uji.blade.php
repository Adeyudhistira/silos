@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Daftar Nasabah Uji Sudah Ada Nomor DKS</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
           <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>No Surat DKS</th>
                    <th>Tanggal Surat DKS</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if($data)
                    @php $no=0; @endphp
                    @foreach($data as $item)
                    @php $no++ @endphp    
                        <tr>
                            <td align="center">{{$no}}</td>
                            <td align="center">{{$item->no_surat_dks}}</td>
                            <td align="center">{{date('d M Y',strtotime($item->tgl_surat_dks))}}</td>
                            <td align="center"> 
                            <!--    
                            <a class="i-button no-margin" href="{{route('lihat_nasabah','"'.$item->no_surat_dks.'"')}}">Lihat List Nasabah</a>
                            --> 

                                <button class="i-button no-margin" onclick="show_detail('{{$item->no_surat_dks}}');">Lihat List Nasabah</button> 
                                <button class="i-button no-margin" onclick="dksBaru('{{$item->no_surat_dks}}','{{$item->tgl_surat_dks}}','{{$item->jumlah}}','{{$login[0]->batch_id}}')">Kirim DKS Baru</button> 
                                <button class="i-button no-margin" onclick="ambilIdUji('{{$item->no_surat_dks}}','{{$login[0]->batch_id}}')">Ambil ID Uji</button> 
                                <button class="i-button no-margin" onclick="kirimIdUji('{{$item->no_surat_dks}}','{{$login[0]->batch_id}}')">Kirim Data Uji</button>                               
                                <button class="i-button no-margin" onclick="getHasilUji('{{$item->no_surat_dks}}','{{$login[0]->batch_id}}')">Get Hasil Uji</button>                               
                                
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        </div>
    </div>
</div>
<!-- modal slik -->
<div id="modal_detail" class="modal-container" style="width: 1800px;max-height: 450px;display: block;overflow: scroll;">
    <div class="modal-head"><h3>List Detail</h3></div>
    <div class="modal-body">
          <table cellpadding="0" cellspacing="0" border="0" class="display" id="table_dks">
            <thead>
                <tr>
                    <th>ID Nasabah</th>
                    <th>Nama Nasabah</th>
                    <th>Nama Perumahan</th>
                    <th>Harga Rumah</th>
                    <th>Nilai KPR</th>
                    <th>Tenor</th>
                    <th>Angsuran</th>
                    <th style="width:1000px;">Action</th>
                </tr>
            </thead>
            <tbody id="datazz">
               
            </tbody>
        </table>
    </div>
</div>

@include('ppdpp.action')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
@stop
