@section('content')
<div class="c-forms">
<a class="i-button no-margin" href="{{route('nasabah_uji')}}">Kembali</a>   

    <div class="box-element" style="
    margin-top: 20px;
">
    <div class="box-head-light">
        <span class="forms-16"></span><h3>List Nasabah {{$kode}}</h3>
           
        </div>
         
        <div class="box-content no-padding">
           <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
            <thead>
                   <th>ID Nasabah</th>
                    <th>Nama Nasabah</th>
                    <th>Nama Perumahan</th>
                    <th>Harga Rumah</th>
                    <th>Nilai KPR</th>
                    <th>Tenor</th>
                    <th>Angsuran</th>
                    <th>Status Kirim Data Uji</th>
                    <th style="width:1000px;">Action</th>
            </thead>

            <tbody>
                @if($data)
                    @php $no=0; @endphp
                    @foreach($data as $item)
                    @php $no++ @endphp    
                        <tr>
                            <td align="center">{{$item->id}}</td>
                            <td align="center">{{$item->nama_nasabah}}</td>
                            <td align="center">{{$item->residence_name}}</td>
                            <td align="center">{{number_format($item->harga_object,0,',','.')}}</td>
                            <td align="center">{{number_format($item->plafon,0,',','.')}}</td>
                            <td align="center">{{$item->jangka_waktu}}</td>
                            <td align="center">{{number_format($item->total_angsuran,0,',','.')}}</td>
                            <td align="center">{{($item->keterangan) ? $item->keterangan: ""}}</td>

                            <td align="center"> 
                                <a class="i-button no-margin" href="{{route('permohonan_uji.show',[$item->id,'nasabah'])}}">Edit</a> 

                           <button class="i-button no-margin" onclick="kirimDataUji({{$item->id}},'{{$login[0]->batch_id}}')">Kirim Ulang Data Uji</button>
                            <button class="i-button no-margin" onclick="showAkad({{$item->id}},'{{$login[0]->batch_id}}')">Set Nomor Akad</button> 
                            <button class="i-button no-margin" onclick="updateStatusCairPPDPP({{$item->id}},'{{$login[0]->batch_id}}')">Set Status Cair PPDPP</button> 
                            <button class="i-button no-margin" onclick="updateStatusCairSMF({{$item->id}},'{{$login[0]->batch_id}}')">Set Status Cair SMF</button> 
              
                          </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        </div>
    </div>
</div>

<div id="modal_akad" class="modal-container">
    <div class="modal-head"><h3>Update Nomor Akad </h3></div>
    <div class="modal-body">
        <form id="form_akad">

            <input type="hidden" value="" id="id_nasabah"> 
             <div class="i-label">
                <label for="text_field">Nomor Rekening</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input onkeyup="convertToNum(this)" type="text" name="no_akad" id="no_akad" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">Tanggal Akad</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="date" name="tgl_akad" id="tgl_akad" class="i-text required"></input></div>
            </div>

        </form>
    </div>
    <div class="modal-footer">
        <input type="button" onclick="TambahAkad();" class="i-button no-margin" value="Simpan" />
        <div class="clearfix"></div>
    </div>
</div>
@include('ppdpp.action')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
@stop
