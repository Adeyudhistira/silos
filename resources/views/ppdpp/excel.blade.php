<style type="text/css">
  .page-break {
    page-break-after: always;
  }
  .tg tr > td,.tg  tr > th {border: 1px solid #000000;}
  .tg td{padding:10px 5px;word-break:normal;color:#333;}
  .tg th{font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;}
  .tg .tg-3wr7{font-weight:bold;font-size:12px;text-align:center}
  .tg .tg-ti5e{font-size:10px;text-align:center}
  .tg .tg-rv4w{font-size:10px;}
</style>

<table class="tg" border="1">
  <tr>
    <td>Nama</td>
    <td>:</td>
    <td>{{$module[0]->nama}}</td>
  </tr>
  <tr>
    <td>NIK</td>
    <td>:</td>
    <td>{{$module[0]->ktp}}</td>  
  </tr>
  <tr>
    <td>Id Berkas</td>
    <td>:</td>
    <td>{{$module[0]->id_berkas}}</td>
  </tr>
</table>
<table class="tg" border="1">
  <tr>
    <th>Angsuran Ke</th>
    <th>Bulan</th>
    <th>Tahun</th>
    <th>Tanggal Jatuh Tempo</th>
    <th>Sisa Pokok FLPP</th>
    <th>Nilai Angsuran FLPP</th>
    <th>Nilai Pokok FLPP</th>
    <th>Nilai Tarif FLPP</th>
    <th>Outstanding FLPP</th>
    <th>Status Pembayaran</th>
  </tr>
  @if($module)
  <tbody>
    @php $no=0; @endphp
    @foreach($module as $item)
    @php $no++ @endphp    
    <tr>
      <td>{{$no}}</td>
      <td>{{$item->d_bulan}}</td>
      <td>{{$item->d_tahun}}</td>
      <td>{{date('d M Y',strtotime($item->d_jatuh_tempo))}}</td>
      <td>{{number_format($item->d_sisapokok,0,',','.')}}</td>
      <td>{{number_format($item->d_flpp_pokok + $item->d_flpp_tarif,0,',','.')}}</td>
      <td>{{number_format($item->d_flpp_pokok,0,',','.')}}</td>
      <td>{{number_format($item->d_flpp_tarif,0,',','.')}}</td>
      <td>{{number_format($item->d_flpp_outstanding,0,',','.')}}</td>
      <td>--</td>
    </tr>
    @endforeach
  </tbody>
  @php 
  $total=0;
  $total1=0;
  $total2=0;
  @endphp
  @foreach($module as $item)
  @php 
  ($total +=$item->d_flpp_pokok + $item->d_flpp_tarif);
  ($total1 += $item->d_flpp_pokok);
  ($total2 += $item->d_flpp_tarif);
  @endphp
  @endforeach 
  <tfoot>
    <td>Total</td>
    <td>{{number_format($total,0,',','.')}}</td>
    <td>{{number_format($total1,0,',','.')}}</td>
    <td>{{number_format($total2,0,',','.')}}</td>
  </tfoot>
  @endif
</table>