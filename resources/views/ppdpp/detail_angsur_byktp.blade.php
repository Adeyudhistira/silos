@section('content')
<div class="c-forms">
<a class="i-button no-margin" href="#" onclick="history.back();">Kembali</a>   




@if($data)
<a class="i-button no-margin" href="#" onclick="cetakPdf({{$data[0]->ktp}})">Export to Pdf</a>  
<a class="i-button no-margin" href="#" onclick="cetakExcel({{$data[0]->ktp}})">Export to Excel</a>  
@endif


<br><br>

<div class="box-element">
        <div class="box-head-light"><span class="search-16"></span><h3>Filter</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding grey-bg">
            <form class="form" id="form-filter" method="GET"> 
                <input type="hidden" name="id" value="{{$id}}">
                <div class="clearfix"></div>
                <div class="i-label">
                <label for="text_field">Bulan</label>
                </div>                                  

                <div class="section-right">
                    <div class="section-input">
                        <select class="i-select" name="bulan">
                            <option value="01" @if($bulan=='01') selected @endif>Januari</option>
                            <option value="02" @if($bulan=='02') selected @endif>Februari</option>
                            <option value="03" @if($bulan=='03') selected @endif>Maret</option>
                            <option value="04" @if($bulan=='04') selected @endif>April</option>
                            <option value="05" @if($bulan=='05') selected @endif>Mei</option>
                            <option value="06" @if($bulan=='06') selected @endif>Juni</option>
                            <option value="07" @if($bulan=='07') selected @endif>Juli</option>
                            <option value="08" @if($bulan=='08') selected @endif>Agustus</option>
                            <option value="09" @if($bulan=='09') selected @endif>September</option>
                            <option value="10" @if($bulan=='10') selected @endif>Oktober</option>
                            <option value="11" @if($bulan=='11') selected @endif>November</option>
                            <option value="12" @if($bulan=='12') selected @endif>Desember</option>
                        </select>
                    </div>
                </div>
                <div class="i-divider"></div>
                <div class="clearfix"></div>

                <div class="clearfix"></div>
                <div class="i-label">
                <label for="text_field">Tahun</label>
                </div>                                  
                <div class="section-right">
                    <div class="section-input">
                        <input type="text" name="tahun" id="tahun" @if($tahun) value="{{$tahun}}" @endif maxlength="4" minlength="4" class="i-text required"></input>
                    </div>
                </div>
                <div class="i-divider"></div>
                <div class="clearfix"></div> 

                <div class="modal-footer">
                    <button type="submit" value="filter" id="filter" name='filter' class="icon16-button">Filter</button>
                    <button type="submit" value="reset" id="reset" name='reset' class="icon16-button">Reset</button>
                    <div class="clearfix"></div>
                </div>   
            </form>    
            
        </div>
    </div>


    <div class="box-element" style="
    margin-top: 20px;
">
    <div class="box-head-light">
            Nama&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{($data ? $data[0]->nama: '-')}}
            <br>
            Nik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: {{($data ? $data[0]->ktp : '-')}}
            <br>
            Id Berkas&nbsp; : {{($data ? $data[0]->id_berkas : '-')}}
        </div>
         
        <div class="box-content no-padding">
     
           <table cellpadding="0" cellspacing="0" border="0" class="display">
            <thead>
                    <th>Angsuran Ke</th>
                    <th>Bulan</th>
                    <th>Tahun</th>
                    <th>Tanggal Jatuh Tempo</th>
                    <th>Sisa Pokok FLPP</th>
                    <th>Nilai Angsuran FLPP</th>
                    <th>Nilai Pokok FLPP</th>
                    <th>Nilai Tarif FLPP</th>
                    <th>Outstanding FLPP</th>
                    <th>Status Pembayaran</th>
            </thead>
            @if($data)
            <tbody>
                @php $no=0; @endphp
                @foreach($data as $item)
                @php $no++ @endphp    
                    <tr>
                        <td>{{$no}}</td>
                        <td>{{$item->d_bulan}}</td>
                        <td>{{$item->d_tahun}}</td>
                        <td>{{date('d M Y',strtotime($item->d_jatuh_tempo))}}</td>
                        <td>{{number_format($item->d_sisapokok,0,',','.')}}</td>
                        <td>{{number_format($item->d_flpp_pokok + $item->d_flpp_tarif,0,',','.')}}</td>
                        <td>{{number_format($item->d_flpp_pokok,0,',','.')}}</td>
                        <td>{{number_format($item->d_flpp_tarif,0,',','.')}}</td>
                        <td>{{number_format($item->d_flpp_outstanding,0,',','.')}}</td>
                        <td>--</td>
                    </tr>
                @endforeach
            </tbody>
             @php 
              $total=0;
              $total1=0;
              $total2=0;
              @endphp
              @foreach($data as $item)
              @php 
              ($total +=$item->d_flpp_pokok + $item->d_flpp_tarif);
              ($total1 += $item->d_flpp_pokok);
              ($total2 += $item->d_flpp_tarif);
              @endphp
            @endforeach 
            <tfoot>
                <th>Total</th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th>{{number_format($total,0,',','.')}}</th>
                <th>{{number_format($total1,0,',','.')}}</th>
                <th>{{number_format($total2,0,',','.')}}</th>
                <th></th>
                <th></th>
            </tfoot>
            @endif
        </table>
        </div>
    </div>
</div>
@include('ppdpp.action')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
@stop
