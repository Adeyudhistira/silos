<style type="text/css">
.page-break {
    page-break-after: always;
}
.tg  {border-collapse:collapse;border-spacing:0;border:solid 1px #000 1;width: 100%; }
.tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
.tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
.tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
.tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
.tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
</style>
<table width="100%">
<tr>
<td width="25%" align="center"><img src="{{asset('img/a1.jpg')}}" style="width: 80px;height: 80px;"></td>
<td width="50%" align="center"><h3>Laporan Detail Angsuran By ID Berkas</h3></td>
<td width="25%" align="center">&nbsp;&nbsp;&nbsp;</td>
</tr>
</table>

<table class="tg">
  <tr>
    <td>Kode Bank</td>
    <td>:</td>
    <td>425</td>
  </tr>
  <tr>
    <td>Nama bank</td>
    <td>:</td>
    <td>BPD JAWA BARAT DAN BANTEN Syariah</td>  
  </tr>
  <tr>
    <td>Id Berkas</td>
    <td>:</td>
    <td>{{$module[0]->id_berkas}}</td>
  </tr>
</table>
<table class="tg">
 <tr>
  <th>Id Berkas</th>
  <th>Bulan</th>
  <th>Tahun</th>
  <th>Sisa Pokok FLPP</th>
  <th>Nilai Angsuran FLPP</th>
  <th>Nilai Pokok FLPP</th>
  <th>Nilai Tarif FLPP</th>
  <th>Outstanding FLPP</th>
</tr>
@if($module)
<tbody>
  @foreach($module as $item)
  <tr>
    <td>{{$item->id_berkas}}</td>
    <td>{{$item->d_bulan}}</td>
    <td>{{$item->d_tahun}}</td>
    <td>{{number_format($item->sisapokok,0,',','.')}}</td>
    <td>{{number_format($item->nilaiangsuran,0,',','.')}}</td>
    <td>{{number_format($item->pokok,0,',','.')}}</td>
    <td>{{number_format($item->tarif,0,',','.')}}</td>
    <td>{{number_format($item->outstanding,0,',','.')}}</td>
  </tr>
  @endforeach
</tbody>
@php 
$total=0;
$total1=0;
$total2=0;
@endphp
@foreach($module as $item)
@php 
($total +=$item->nilaiangsuran);
($total1 += $item->pokok);
($total2 += $item->tarif);
@endphp
@endforeach 
<tfoot>
  <th>Total</th>
  <th></th>
  <th></th>
  <th>{{number_format($total,0,',','.')}}</th>
  <th>{{number_format($total1,0,',','.')}}</th>
  <th>{{number_format($total2,0,',','.')}}</th>
  <th></th>
  <th></th>
</tfoot>
@endif
</table>