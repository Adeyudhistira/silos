@section('content')
<div class="c-forms">
<a class="i-button no-margin" href="{{route('master_ppdpp')}}">Kembali</a>   

    <div class="box-element" style="
    margin-top: 20px;
">
    <div class="box-head-light">
        <span class="forms-16"></span><h3>Detail Master PPDPP</h3>
           
        </div>
         
        <div class="box-content no-padding">
     
           <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
            <thead>
                    <th>Nomor KTP</th>
                    <th>Nama Nasabah</th>
                    <th>Nilai FLPP</th>
                    <th>Nilai Angsuran</th>
                    <th>Action</th>
            </thead>

            <tbody>
                @if($data)
                    @php 
                        $no=0; 
                        $login = collect(\DB::select("select * from logins"))->first();
                    @endphp
                    @foreach($data as $item)
                    @php $no++ @endphp    
                        <tr>
                            <td align="center">{{$item->id_ktp}}</td>
                            <td align="center">{{$item->nama}}</td>
                            <td align="center">{{number_format($item->nilai_flpp,0,',','.')}}</td>
                            <td align="center">{{number_format($item->nilai_angsuran,0,',','.')}}</td>
                            <td align="center"> 
                           <button class="i-button no-margin" onclick="JadwalAngsur('{{$item->id_ktp}}')">Lihat Jadwal Angsur</button>
                           <button class="i-button no-margin" onclick="GetSisaPembayaran('{{$item->id_ktp}}','{{$login->batch_id}}')">Get Sisa Pembayaran</button>
                                
                          </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        </div>
    </div>
</div>

<div id="modal_akad" class="modal-container">
    <div class="modal-head"><h3>Update Nomor Akad </h3></div>
    <div class="modal-body">
        <form id="form_akad">

            <input type="hidden" value="" id="id_nasabah"> 
             <div class="i-label">
                <label for="text_field">Nomor Akad</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input onkeyup="convertToNum(this)" type="text" name="no_akad" id="no_akad" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">Tanggal Surat DKS</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="tgl_akad" id="tgl_akad" class="i-text i-datepicker required"></input></div>
            </div>

        </form>
    </div>
    <div class="modal-footer">
        <input type="button" onclick="TambahAkad();" class="i-button no-margin" value="Simpan" />
        <div class="clearfix"></div>
    </div>
</div>
@include('ppdpp.action')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
@stop
