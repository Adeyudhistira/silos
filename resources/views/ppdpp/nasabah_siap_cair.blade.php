@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Daftar Nasabah Siap Cair</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
           <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
            <thead>
                <tr>
                    <th>ID Nasabah</th>
                    <th>Nomor DKS</th>
                    <th>Nama Nasabah</th>
                    <th>Nama Perumahan</th>
                    <th>Nilai KPR</th>
                    <th>ID Berkas</th>
                    <th>Nomor Cair</th>
                    <th>Tanggal Pecairan</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if($data)
                    @php $no=0; @endphp
                    @foreach($data as $item)
                    @php $no++ @endphp    
                        <tr>
                            <td align="center">{{$item->id}}</td>
                            <td align="center">{{$item->no_surat_dks}}</td>
                            <td align="center">{{$item->nama_nasabah}}</td>
                            <td align="center">{{$item->perumahan}}</td>
                            <td align="center">{{$item->harga_object}}</td>
                            <td align="center">{{$item->id_berkas}}</td>
                            <td align="center">{{$item->no_cair}}</td>
                            <td align="center">{{$item->tgl_pencairan}}</td>

                            <td align="center"> 
                            <button style="width: 150px;" class="i-button no-margin" onclick="ambilIdBerkas('{{$item->no_identitas}}','{{$item->id}}','{{$login[0]->batch_id}}')">Ambil ID Berkas</button>
                            
                            <button style="width: 150px;" class="i-button no-margin" onclick="cetakJadwalAngsur('{{$item->no_identitas}}','{{$login[0]->batch_id}}')">Cetak Jadwal Angsur</button> 
                            <button style="width: 150px;" class="i-button no-margin" onclick="updateStatusCair('{{$item->id}}','{{$login[0]->batch_id}}')">Update Status Cair CBS</button> 
              
                          </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        </div>
    </div>
</div>

@include('ppdpp.action_cair')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
@stop
