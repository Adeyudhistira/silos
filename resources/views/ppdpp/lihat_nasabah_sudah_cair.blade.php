@section('content')
<div class="c-forms">
<a class="i-button no-margin" href="{{route('lihat_nasabah_sudah_cair')}}">Kembali</a>   

    <div class="box-element" style="
    margin-top: 20px;
">
    <div class="box-head-light">
        <span class="forms-16"></span><h3>Detail Nasabah {{$kode}}</h3>
           
        </div>

        <div class="box-content no-padding">
           <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
            <thead>
                   <th>ID Nasabah</th>
                    <th>Nama Nasabah</th>
                    <th>Nama Perumahan</th>
                    <th>Harga Rumah</th>
                    <th>Nilai KPR</th>
                    <th>Tenor</th>
                    <th>Angsuran</th>
                    <th>Action</th>
            </thead>

            <tbody>
                @if($data)
                    @php $no=0; @endphp
                    @foreach($data as $item)
                    @php $no++ @endphp    
                        <tr>
                            <td align="center">{{$item->id}}</td>
                            <td align="center">{{$item->nama_nasabah}}</td>
                            <td align="center">{{$item->perumahan}}</td>
                            <td align="center">{{$item->harga_object}}</td>
                            <td align="center">{{$item->plafon}}</td>
                            <td align="center">{{$item->jangka_waktu}}</td>
                            <td align="center">{{$item->total_angsuran}}</td>

                            <td align="center"> 
                                <button style="width: 150px;" class="i-button no-margin" onclick="GetJadwalAngsur('{{$item->id}}','{{$login[0]->batch_id}}')">Lihat Jadwal Angsur</button> 

                          </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        </div>
    </div>
</div>

<div id="modal_akad" class="modal-container">
    <div class="modal-head"><h3>Update Nomor Akad </h3></div>
    <div class="modal-body">
        <form id="form_akad">

            <input type="text" value="" id="id_nasabah"> 
             <div class="i-label">
                <label for="text_field">Nomor Akad</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input onkeyup="convertToNum(this)" type="text" name="no_akad" id="no_akad" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">Tanggal Surat DKS</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="date" name="tgl_akad" id="tgl_akad" class="i-text required"></input></div>
            </div>

        </form>
    </div>
    <div class="modal-footer">
        <input type="button" onclick="TambahAkad();" class="i-button no-margin" value="Simpan" />
        <div class="clearfix"></div>
    </div>
</div>
@include('ppdpp.action')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
@stop
