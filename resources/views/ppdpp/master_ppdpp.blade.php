@section('content')
<div class="c-forms">
    <div class="box-element" style="
    margin-top: 20px;
">
    <div class="box-head-light">
        <span class="forms-16"></span><h3>Master PPDPP</h3>
           
        </div>
         
        <div class="box-content no-padding">
             <button style="float: right;margin: 10px !important;" class="i-button no-margin" onclick="tarikMasterData('{{$login[0]->batch_id}}')">Tarik Master Data</button>

           <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
            <thead>
                   <th>ID Berkas</th>
                    <th>No Percairan</th>
                    <th>Tanggal Pencairan</th>
                    <th>No Permintaan</th>
                    <th>Jumlah Debitur</th>
                    <th>Nilai FLPP</th>
                    <th>Status Cair</th>
                    <th>Action</th>
            </thead>

            <tbody>
                @if($data)
                    @php $no=0; @endphp
                    @foreach($data as $item)
                    @php $no++ @endphp    
                        <tr>
                            <td align="center"><a href="{{route('detail_master_ppdpp','"'.$item->id_berkas.'"')}}">{{$item->id_berkas}}</a> </td>
                            <td align="center">{{$item->no_cair}}</td>
                            <td align="center">{{date('d M Y',strtotime($item->tgl_pencairan))}}</td>
                            <td align="center">{{$item->no_permintaan}}</td>
                            <td align="center">{{number_format($item->count,0,',','.')}}</td>
                            <td align="center">{{number_format($item->tarif_flpp,0,',','.')}}</td>
                            <td align="center">{{$item->is_cair}}</td>

                            <td align="center"> 
                           <button class="i-button no-margin" onclick="setPencairan({{$item->id_berkas}},{{$item->sum}},'{{$login[0]->batch_id}}')">Set Pencairan</button>
                           <button class="i-button no-margin" onclick="GetJadwalAngsur('{{$item->id_berkas}}','{{$login[0]->batch_id}}')">Get Jadwal Angsur</button>
                           <button class="i-button no-margin" onclick="JadwalAngsurBy_idberkas('{{$item->id_berkas}}','{{$login[0]->batch_id}}')">Jadwal Angsuran</button>
                         
                          </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        </div>
    </div>
</div>

<div id="modal_master_data" class="modal-container">
    <div class="modal-head"><h3>Data Master</h3></div>
    <div class="modal-body">
        <form id="form_master_data">
            <div class="i-label">
            <label for="text_field">Periode Bulan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="bulan" maxlength="2" minlength="2" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Periode Tahun</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="tahun" maxlength="4" minlength="4" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>    
        </form>
    </div>
    <div class="modal-footer">
        <input type="button" onclick="processGetMaster();" class="i-button no-margin" value="Simpan" />
        <div class="clearfix"></div>
    </div>
</div>

@include('ppdpp.action')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
@stop
