@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Daftar Nasabah Permohonan Uji</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
            <div class="right">
                 <a href="#" id="showUpload"><input type="button" class="icon16-button" value="Upload Excel"></a>
                <a href="#" id="showTambah"><input type="button" class="icon16-button" value="Tambah Baru"></a>
                <input type="button" name="submit" onclick="show_batch('{{$login[0]->batch_id}}');" class="i-button no-margin" value="Tandai Batch" />

            </div>
           
           <table cellpadding="0" cellspacing="0" border="0" class="display" id="t_dks">
            <thead>
                <tr>
                    <th><input type="checkbox" name="select_all" value="1" id="example-select-all"></th>
                    <th>ID Nasabah</th>
                    <th>Nama Nasabah</th>
                    <th>Nama Perumahan</th>
                    <th>Harga Rumah</th>
                    <th>Nilai KPR</th>
                    <th>Tenor</th>
                    <th>Angsuran</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if($data)
                    @foreach($data as $item)
                        <tr style="height: 40px;">
                            <td style="padding:15px;" align="center"><input type="checkbox" name="id[]" value="{{$item->id}}"/></td>
                            <td style="padding:15px;" align="center">{{$item->id}}</td>
                            <td style="padding:15px;" align="center">{{$item->nama_nasabah}}</td>
                            <td style="padding:15px;" align="center">{{$item->residence_name}}</td>
                            <td style="padding:15px;" align="right">{{number_format($item->harga_object,0,',','.')}}</td>
                            <td style="padding:15px;" align="right">{{number_format($item->plafon,0,',','.')}}</td>
                            <td style="padding:15px;" align="right">{{$item->jangka_waktu}} Bln</td>
                            <td style="padding:15px;" align="right">{{number_format($item->total_angsuran,0,',','.')}}</td>
                            <td style="padding:15px;">
                                {{-- <button class="i-button no-margin" onclick="ShowEditPermohonan('{{$item->id}}')">Edit</button>                                --}}
                                <a class="i-button no-margin" style="display: block;" href="{{route('permohonan_uji.show',[$item->id,'normal'])}}">Edit</a> 
                                <a class="i-button no-margin" style="display: block;" href="{{route('ceklis_agunan.show',[$item->id])}}">Ceklis Agunan</a> 
                                <button class="i-button no-margin" onclick="hapus('{{$item->id}}')">Hapus</button>

                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        </div>
    </div>
</div>
<!-- modal slik -->
<div id="modal_upload" class="modal-container">
    <div class="modal-head"><h3>Upload</h3></div>
    <div class="modal-body">
        <form id="form_upload">
            <div class="i-label">
            <label for="text_field">Pilih File</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input minlength="10" type="file" name="file" id="file" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>  
        </form>
    </div>
    <div class="modal-footer">
        <input type="button" onclick="upload_data_uji();" class="i-button no-margin" value="Upload" />
        <div class="clearfix"></div>
    </div>
</div>

<div id="modal_dks" class="modal-container">
    <div class="modal-head"><h3>Data Permintaan</h3></div>
    <div class="modal-body">
        <form id="form_dks">
            <div class="i-label">
            <label for="text_field">No Surat DKS</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input minlength="10" type="text" name="no_surat" id="no_surat" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Tanggal Surat DKS</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="tgl" id="tgl" class="i-text i-datepicker required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>    
        </form>
    </div>
    <div class="modal-footer">
        <input type="button" onclick="processDKS($('#no_surat').val(),$('#tgl').val());" class="i-button no-margin" value="Simpan" />
        <div class="clearfix"></div>
    </div>
</div>

<div id="modal_tambah_data" style="max-height:450px;overflow:auto;">
    <div class="modal-head"><h3 id="title_form">Tambah Baru</h3></div>
    <div class="modal-body">
        <form id="form_tambah_data">
            <input type="hidden" name="id_nas" id="id_nas" value="kosong">
            <input type="hidden" name="id_mp" id="id_mp" value="kosong">
            <input type="hidden" name="id_maap" id="id_maap" value="kosong">

            <div class="i-label">
                <label for="text_field">No Identitas</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input onkeyup="convertToNum(this)" maxlength="16" minlength="16" type="text" name="no_identitas" id="no_identitas" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Nama Nasabah</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input type="text" name="nama_nasabah" id="nama_nasabah" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Pekerjaan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    @php
                        $dt = \DB::select("SELECT * FROM ref_pekerjaan");
                    @endphp
                    <select class="i-text required" name="id_pekerjaan" id="id_pekerjaan">
                        @foreach ($dt as $item)
                            <option value="{{$item->id_pekerjaan}}">{{$item->nm_pekerjaan}}</option>
                        @endforeach
                       
                    </select>
                </div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Jenis Kelamin</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <select class="i-text required" name="jk" id="jk">
                        <option value="1">Laki-laki</option>
                        <option value="0">Perempuan</option>
                    </select>
                </div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">NPWP</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input type="text" name="npwp" id="npwp" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Gaji Pokok</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input type="text" onkeyup="convertToRupiah(this)" name="gp" id="gp" class="i-text required"></input></div>
            </div>
             <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Telp</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input type="text" name="telp" id="telp" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Nama Pasangan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input type="text" name="nama_p" id="nama_p" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">KTP Pasangan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input type="text" onkeyup="convertToNum(this)" maxlength="16" minlength="16" name="ktp_p" id="ktp_p" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>


           

             <div class="i-label">
                <label for="text_field">Harga Rumah</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input onkeyup="convertToRupiah(this)" type="text" name="harga_object" id="harga_object" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             <div class="i-label">
                <label for="text_field">Nilai KPR</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input  onkeyup="convertToRupiah(this)" onchange="sAngsuran()" type="text" name="plafon" id="plafon" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             

             <div class="i-label">
                <label for="text_field">Tenor</label>
            </div>                                  
            <div class="section-right">
                <div onkeyup="sAngsuran()" maxlength="3" class="section-input"><input type="text" name="jangka_waktu" id="jangka_waktu" class="i-text required"></input>*bln</div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             <div class="i-label">
                <label for="text_field">Angsuran</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input oninput="convertToRupiah(this)" readonly type="text" name="total_angsuran" id="total_angsuran" class="i-text required"></input></div>
            </div>

             <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Suku Bunga KPR</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="bunga" id="bunga" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             <div class="i-label">
                <label for="text_field">Nilai FLPP</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input value="" onkeypress="maxFLPP()" onkeyup="convertToRupiah(this)" type="text" name="plafon_flpp" id="plafon_flpp" class="i-text required"></input></div>
                <div class="section-input" style="margin-left: 100px;" id="totalmax_flpp"></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>
                    <input type="hidden" name="perumahan" id="perumahan" class="i-text required"></input>

             {{-- <div class="i-label">
                <label for="text_field">Nama Perumahan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div> --}}

            <div class="i-label">
                <label for="text_field">Developer</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                     @php
                        $dt = \DB::select("SELECT * FROM ref_developer");
                    @endphp
                    <select class="i-text required" onchange="Gantii()" name="id_developer" id="id_developer">
                        <option value="">Pilih</option>
                        @foreach ($dt as $item)
                            <option value="{{$item->id}}">{{$item->developer_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

         

              <div class="i-label">
                <label for="text_field">Residence</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                     @php
                        $dt = \DB::select("SELECT * FROM ref_residence");
                    @endphp
                    <select class="i-text required" name="id_perumahan" id="id_perumahan">
                        {{-- @foreach ($dt as $item)
                            <option value="{{$item->id}}">{{$item->residence_name}}</option>
                        @endforeach --}}
                    </select>
                </div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

              

            <!--
             <div class="i-label">
                <label for="text_field">Nama Badan Hukum</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input type="text" name="nama_developer" id="nama_developer" class="i-text required"></input>
                </div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>
        -->

             <div class="i-label">
                <label for="text_field">Alamat</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="alamat" id="alamat" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            
             <div class="i-label">
                <label for="text_field">Cari Kelurahan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input type="text" name="nama_kelurahan" id="nama_kelurahan" class="i-text required"></input>
                </div>
                <input type="button" style="margin: 10px 0 !important;" onclick="getLokasi('nama_kelurahan','setlokasi');" class="i-button no-margin" value="Cari" />
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Pilih Lokasi</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <select id="setlokasi" name="lokasipengguna" class="js-example-basic-single i-text required">
                    </select>
                </div>
                
            </div>


            <div class="i-divider"></div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Kode Pos</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="kodepos" id="kodepos" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">Luas Tanah</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="luas_tanah_fisik" id="luas_tanah_fisik" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">Luas Bangunan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="luas_imb" id="luas_imb" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">Kode Jenis KPR</label>
            </div>                                  
            <div class="section-right">
                    @php
                        $dt = \DB::select("SELECT * FROM ref_jns_kpr");
                    @endphp
                    <select class="i-text required" name="kd_jns_kpr" id="kd_jns_kpr">
                        @foreach ($dt as $item)
                            <option value="{{$item->id}}">{{$item->definition}}</option>
                        @endforeach
                       
                    </select>

                {{-- <div class="section-input"><input type="text" name="kd_jns_kpr" id="kd_jns_kpr" class="i-text required"></input></div> --}}
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             <div class="i-label">
                <label for="text_field">No SP3K</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="no_sp3k" id="no_sp3k" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             <div class="i-label">
                <label for="text_field">Tanggal SP3K</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="date" name="tgl_sp3k" id="tgl_sp3k" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <!--
             <div class="i-label">
                <label for="text_field">No ID Uji</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="no_id_uji" id="no_id_uji" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>
            -->
           
        </form>
    </div>
    <div class="modal-footer">
        <input type="button" onclick="TambahData();" class="i-button no-margin" value="Simpan" />
        <div class="clearfix"></div>
    </div>
</div>

@endsection
@section('script')
    <script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
     <script>
                var mySelect = $('#id_perumahan');
                mySelect.find('option').remove().end();

                function Gantii() {
                     console.log("waww");

                    mySelect
                    .find('option')
                    .remove()
                    .end();
                    
                    var dataaa = $('#id_developer').val();

                    @php
                        $d_rsca = \DB::select("SELECT * FROM ref_residence");
                    @endphp

                    @foreach($d_rsca as $item)

                        if (dataaa == {{$item->id_developer}} ) {
                        mySelect.append('<option value="{{$item->id}}">{{$item->residence_name}}</option>');

                        }

                    @endforeach
                }

                $('#id_developer').on('change', function() {

                    
                   
                });

function hapus(id) {
    swal({
        title: "Hapus",
        text: "Anda Yakin akan Hapus?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#e6b034",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: base_url + '/prospek/delete/' + id,
                data: {
                    id: id
                },
                beforeSend: function () {
                },
                success: function (msg) {
                    console.log(msg);
                    swal({
                        title: "Informasi",
                        text: "berhasil Terhapus",
                        type: "info",
                        confirmButtonColor: "#8cd4f5",
                        confirmButtonText: "Ya",
                        closeOnConfirm: false
                    }, function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                        }
                    });

                }
            }).done(function (msg) {

            }).fail(function (msg) {
                swal("Terjadi Kesalahan! ");
            });

        } else {
            swal("Dibatalkan!");
        }
    });
}

function getLokasi(set,target) {

     $(".loading-mail").show();
    var kelurahan = $('#'+set).val();

    var _items = "";
    $.ajax({
        type: 'GET',
        url: base_url + '/select/apikelurahan/' + kelurahan,
        success: function (res) {
            $(".loading-mail").hide();
                console.log(res['data']);
                
            
                  var data = $.parseJSON(res);
                _items = "<option value=''>Pilih Lokasi</option>";
                
                
                $.each(data['data'], function (k,v) {
                    _items += "<option value='"+v.id+"'>"+v.wilayah+"</option>";
                });

                $("#"+target).html(_items);
                
        }
    }).done(function( res ) {
         $(".loading-mail").hide();
            
    }).fail(function(res) {
        $(".loading-mail").hide();
        swal("Terjadi Kesalahan! ");
    });
}


    </script>
@stop

