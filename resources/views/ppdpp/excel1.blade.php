<style type="text/css">
  .page-break {
    page-break-after: always;
  }
  .tg tr > td,.tg  tr > th {border: 1px solid #000000;}
  .tg td{padding:10px 5px;word-break:normal;color:#333;}
  .tg th{font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;}
  .tg .tg-3wr7{font-weight:bold;font-size:12px;text-align:center}
  .tg .tg-ti5e{font-size:10px;text-align:center}
  .tg .tg-rv4w{font-size:10px;}
</style>

<table class="tg" border="1">
  <tr>
    <td>Kode Bank</td>
    <td>:</td>
    <td>425</td>
  </tr>
  <tr>
    <td>Nama bank</td>
    <td>:</td>
    <td>BPD JAWA BARAT DAN BANTEN Syariah</td>  
  </tr>
  <tr>
    <td>Id Berkas</td>
    <td>:</td>
    <td>{{$module[0]->id_berkas}}</td>
  </tr>
</table>
<table class="tg" border="1">
 <tr>
  <th>Id Berkas</th>
  <th>Sisa Pokok FLPP</th>
  <th>Nilai Angsuran FLPP</th>
  <th>Nilai Pokok FLPP</th>
  <th>Nilai Tarif FLPP</th>
  <th>Outstanding FLPP</th>
</tr>
@if($module)
<tbody>
  @foreach($module as $item)
  <tr>
    <td>{{$item->id_berkas}}</td>
    <td>{{number_format($item->sisapokok,0,',','.')}}</td>
    <td>{{number_format($item->nilaiangsuran,0,',','.')}}</td>
    <td>{{number_format($item->pokok,0,',','.')}}</td>
    <td>{{number_format($item->tarif,0,',','.')}}</td>
    <td>{{number_format($item->outstanding,0,',','.')}}</td>
  </tr>
  @endforeach
</tbody>
@endif
</table>