<style type="text/css">
.page-break {
    page-break-after: always;
}
.tg  {border-collapse:collapse;border-spacing:0;border:solid 1px #000 1;width: 100%; }
.tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
.tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
.tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
.tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
.tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
</style>
<table width="100%">
<tr>
<td width="25%" align="center"><img src="{{asset('img/a1.jpg')}}" style="width: 80px;height: 80px;"></td>
<td width="50%" align="center"><h3>Laporan Detail Angsuran By KTP</h3></td>
<td width="25%" align="center">&nbsp;&nbsp;&nbsp;</td>
</tr>
</table>
<table class="tg">
  <tr>
    <td>Nama</td>
    <td>:</td>
    <td>{{$module[0]->nama}}</td>
  </tr>
  <tr>
    <td>NIK</td>
    <td>:</td>
    <td>{{$module[0]->ktp}}</td>  
  </tr>
  <tr>
    <td>Id Berkas</td>
    <td>:</td>
    <td>{{$module[0]->id_berkas}}</td>
  </tr>
</table>
<table class="tg">
  <tr>
    <th>Angsuran Ke</th>
    <th>Bulan</th>
    <th>Tahun</th>
    <th>Tanggal Jatuh Tempo</th>
    <th>Sisa Pokok FLPP</th>
    <th>Nilai Angsuran FLPP</th>
    <th>Nilai Pokok FLPP</th>
    <th>Nilai Tarif FLPP</th>
    <th>Outstanding FLPP</th>
    <th>Status Pembayaran</th>
  </tr>
  <tbody>
  @if($module)
    @php $no=0; @endphp
    @foreach($module as $item)
    @php $no++ @endphp    
    <tr>
      <td>{{$no}}</td>
      <td>{{$item->d_bulan}}</td>
      <td>{{$item->d_tahun}}</td>
      <td>{{date('d M Y',strtotime($item->d_jatuh_tempo))}}</td>
      <td>{{number_format($item->d_sisapokok,0,',','.')}}</td>
      <td>{{number_format($item->d_flpp_pokok + $item->d_flpp_tarif,0,',','.')}}</td>
      <td>{{number_format($item->d_flpp_pokok,0,',','.')}}</td>
      <td>{{number_format($item->d_flpp_tarif,0,',','.')}}</td>
      <td>{{number_format($item->d_flpp_outstanding,0,',','.')}}</td>
      <td>--</td>
    </tr>
    @endforeach
  </tbody>
  @php 
  $total=0;
  $total1=0;
  $total2=0;
  @endphp
  @foreach($module as $item)
  @php 
  ($total +=$item->d_flpp_pokok + $item->d_flpp_tarif);
  ($total1 += $item->d_flpp_pokok);
  ($total2 += $item->d_flpp_tarif);
  @endphp
  @endforeach 
  <tfoot>
    <th>Total</th>
    <th></th>
    <th></th>
    <th></th>
    <th></th>
    <th>{{number_format($total,0,',','.')}}</th>
    <th>{{number_format($total1,0,',','.')}}</th>
    <th>{{number_format($total2,0,',','.')}}</th>
    <th></th>
    <th></th>
  </tfoot>
  @endif
</table>