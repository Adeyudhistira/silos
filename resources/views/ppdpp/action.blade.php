{{-- <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script> --}}
<script>

function dksBaru(val_nosuratdks,val_tglsuratdks,val_total_nik,bacthid) {

    if(bacthid==null || bacthid==''){
        swal("Informasi",'Silahkan Login Adapter Terlebih Dahulu',"info")
    }else{



    swal({   
            title: "Kirim DKS Baru",   
            text: "Anda Yakin akan request Nomor DKS Baru untuk Batch ini?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                modal_dks.modal('hide');

                //ACTION SERVICE setNewDks
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: base_url + '/service/dksbaru?batchid='+bacthid,
                    data: {
                        nosuratdks: val_nosuratdks,
                        tglsuratdks: val_tglsuratdks,
	                    total_nik: val_total_nik
                    },
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        console.log(msg['rm']);
                          swal({   
                            title: msg['rm'],   
                            text: "proses "+msg['rm'],   
                            type: "info",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                location.reload();
                            } 
                        });
                        
                    }
                }).done(function (msg) {

                }).fail(function (msg) {
                    swal("Terjadi Kesalahan! ");
                });

              
                
            }else {
                swal("Dibatalkan!");
            } 
        });
     }
}

function ambilIdUji(val_nosuratdks,bacthid) {
     if(bacthid==null || bacthid==''){
        swal("Informasi",'Silahkan Login Adapter Terlebih Dahulu',"info")
    }else{

    swal({   
            title: "Ambil ID Uji",   
            text: "Anda Yakin akan request ID Uji untuk Batch ini?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                modal_dks.modal('hide');

                //ACTION SERVICE getMasterDKSNoSurat
                 $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: base_url + '/service/ambilIdUji?batchid='+bacthid,
                    data: {
                        nosuratdks: val_nosuratdks
                    },
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        console.log(msg);
                          swal({   
                            title: msg['rm'],   
                            text: "proses "+msg['rm'],   
                            type: "info",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                location.reload();
                            } 
                        });
                        
                    }
                }).done(function (msg) {

                }).fail(function (msg) {
                    swal("Terjadi Kesalahan! ");
                });
                
            }else {
                swal("Dibatalkan!");
            } 
        });
    }
}

//ON TABLE DEPAN
function kirimIdUji(val_nosuratdks,bacthid) {
     if(bacthid==null || bacthid==''){
        swal("Informasi",'Silahkan Login Adapter Terlebih Dahulu',"info")
    }else{

    swal({   
            title: " Kirim Data Uji",   
            text: "Anda Yakin akan Kirim Data Uji untuk Batch ini?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                modal_dks.modal('hide');

                //ACTION SERVICE setUjiNik
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: base_url + '/service/kirimIdUji?batchid='+bacthid,
                    data: {
                        nosuratdks: val_nosuratdks
                    },
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        console.log(msg);
                          swal({   
                            title: "Informasi",   
                            text: "Data Berhasil Terkirim",   
                            type: "info",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                location.reload();
                            } 
                        });
                        
                    }
                }).done(function (msg) {

                }).fail(function (msg) {
                    swal("Terjadi Kesalahan! ");
                });
                
            }else {
                swal("Dibatalkan!");
            } 
        });
    }
}

function kirimDataUji(val_id,bacthid) {
     if(bacthid==null || bacthid==''){
        swal("Informasi",'Silahkan Login Adapter Terlebih Dahulu',"info")
    }else{

    swal({   
            title: "Kirim Ulang Data Uji",   
            text: "Anda Yakin akan Kirim Data Uji untuk Nasabah  ini?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                modal_dks.modal('hide');
                
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: base_url + '/service/kirimDataUji?batchid='+bacthid,
                    data: {
                        val_id: val_id
                    },
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        console.log(msg);
                          swal({   
                            title: "Informasi",   
                            text: msg['rm'],   
                            type: "info",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                location.reload();
                            } 
                        });
                        
                    }
                }).done(function (msg) {

                }).fail(function (msg) {
                    swal("Terjadi Kesalahan! ");
                });
                
                
            }else {
                swal("Dibatalkan!");
            } 
        });
    }
}

function updateStatusCairPPDPP(val_id,bacthid) {
     if(bacthid==null || bacthid==''){
        swal("Informasi",'Silahkan Login Adapter Terlebih Dahulu',"info")
    }else{

    swal({   
            title: "Update Status Cair PPDPP",   
            text: "Anda Yakin akan Update status cair PPDPP untuk Nasabah  ini?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                modal_dks.modal('hide');
                
               $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: base_url + '/act/updateStatusCairPPDPP?batchid='+bacthid,
                    data: {
                        val_id: val_id
                    },
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        console.log(msg);
                          swal({   
                            title: "Berhasil",   
                            text: msg['rm'],   
                            type: "info",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                location.reload();
                            } 
                        });
                        
                    }
                }).done(function (msg) {

                }).fail(function (msg) {
                    swal("Terjadi Kesalahan! ");
                });
                
            }else {
                swal("Dibatalkan!");
            } 
        });
    }
}

function updateStatusCairSMF(val_id,bacthid) {
     if(bacthid==null || bacthid==''){
        swal("Informasi",'Silahkan Login Adapter Terlebih Dahulu',"info")
    }else{

    swal({   
            title: "Update Status Cair SMF",   
            text: "Anda Yakin akan Update status cair SMF untuk Nasabah  ini?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                modal_dks.modal('hide');
                
              $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: base_url + '/act/updateStatusCairSMF?batchid='+bacthid,
                    data: {
                        val_id: val_id
                    },
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        console.log(msg);
                          swal({   
                            title: "Berhasil",   
                            text: msg['rm'],   
                            type: "info",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                location.reload();
                            } 
                        });
                        
                    }
                }).done(function (msg) {

                }).fail(function (msg) {
                    swal("Terjadi Kesalahan! ");
                });
                
            }else {
                swal("Dibatalkan!");
            } 
        });
    }
}

function showAkad(val_id,bacthid) {
     if(bacthid==null || bacthid==''){
        swal("Informasi",'Silahkan Login Adapter Terlebih Dahulu',"info")
    }else{

    //  $('#modal_akad').modal('show');
       $('#modal_akad').modal({onShow: function(dlg) {
        $(dlg.container).css('height','auto');
        $('#modal_akad').css('overflow', 'auto');
    }});

        $('#id_nasabah').val(val_id);


    // var modal_akad = $('#modal_akad');

    // $('#modal_akad').modal('show');
     console.log(val_id);
     }   
}

function TambahAkad() {
    swal({   
            title: "Update Nomor Akad",   
            text: "Anda Yakin akan Update Nomor Akad?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                //ACTION SERVICE getMasterDKSNoSurat
                 $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: base_url + '/act/updateNoAkad',
                    data: {
                        no_akad:  $('#no_akad').val(),
                        tgl_akad:  $('#tgl_akad').val(),
                        id_nasabah:  $('#id_nasabah').val()


                    },
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        console.log(msg);
                          swal({   
                            title: "Informasi",   
                            text:  msg['rm'],   
                            type: "info",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                location.reload();
                            } 
                        });
                        
                    }
                }).done(function (msg) {

                }).fail(function (msg) {
                    swal("Terjadi Kesalahan! ");
                });
                
            }else {
                swal("Dibatalkan!");
            } 
        });
}

function GetJadwalAngsur(id,bacthid) {
     if(bacthid==null || bacthid==''){
        swal("Informasi",'Silahkan Login Adapter Terlebih Dahulu',"info")
    }else{

     swal({   
            title: "Get Jadwal Angsur",   
            text: "Anda Yakin akan Get Jadwal Angsur?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: base_url + '/service/GetJadwalAngsur?batchid='+bacthid,
                    data: {
                        id: id
                    },
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        console.log(msg);
                          swal({   
                            title: "Informasi",   
                            text: msg['rm'],   
                            type: "info",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                location.reload();
                            } 
                        });
                        
                    }
                }).done(function (msg) {

                }).fail(function (msg) {
                    swal("Terjadi Kesalahan! ");
                });
                
            }else {
                swal("Dibatalkan!");
            } 
        });
    }
}

function show_detail(id){
    

    
    modal_detail.modal({onShow: function(dlg) {
        $(dlg.container).css('height','auto');
        $('#modal_detail').css('overflow', 'auto');
    }});

      $('#table_dks').dataTable({
              "bServerSide": true,
        "bDestroy": true,
        'sPaginationType': 'full_numbers',
        'bProcessing': true,
        "sAjaxSource": base_url + "/dks/table/" + id,
        "sAjaxDataProp": "data",
        "aoColumns": [
            {mDataProp: 'id', name: 'id'},
            {mDataProp: 'nama_nasabah', name: 'nama_nasabah'},
            {mDataProp: 'perumahan', name: 'perumahan'},
            {mDataProp: 'harga_object', name: 'harga_object'},
            {mDataProp: 'plafon', name: 'plafon'},
            {mDataProp: 'jangka_waktu', name: 'jangka_waktu'},
            {mDataProp: 'total_angsuran', name: 'total_angsuran'},
            {mDataProp: 'action', name: 'action'}

        ],
    });
}

function UpdateDataNasabahUji() {
    swal({   
            title: "Update Nasabah Uji",   
            text: "Anda Yakin akan Update Nasabah Uji ?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {
                
                swal({   
                    title: "Berhasil",   
                    text: "Sukses",   
                    type: "info",   
                    confirmButtonColor: "#8cd4f5",   
                    confirmButtonText: "Ya",     
                    closeOnConfirm: false
                }, function(isConfirm){   
                    if (isConfirm) {     
                        location.reload();
                    } 
                });
            
            }else {
                swal("Dibatalkan!");
            } 
        });
}

function tarikMasterData(bacthid) {

    if(bacthid==null || bacthid==''){
        swal("Informasi",'Silahkan Login Adapter Terlebih Dahulu',"info")
    }else{


     $('#modal_master_data').modal({onShow: function(dlg) {
        $(dlg.container).css('height','auto');
        $('#modal_master_data').css('overflow', 'auto');
    }});

 }
}

function setPencairan(id_berkas,nilaimutasicair,bacthid) {
    if(bacthid==null || bacthid==''){
        swal("Informasi",'Silahkan Login Adapter Terlebih Dahulu',"info")
    }else{


    swal({   
            title: "set Pencairan",   
            text: "Anda Yakin akan set Pencairan Id Berkas ini?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                $.LoadingOverlay("show");

              $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: base_url + '/service/setPencairan?batchid='+bacthid,
                    data: {
                        id_berkas: id_berkas,
                        nilaimutasicair: nilaimutasicair

                    },
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        console.log(msg);
                            $.LoadingOverlay("hide");

                          swal({   
                            title: "Berhasil",   
                            text: msg['rm'],   
                            type: "info",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                location.reload();
                            } 
                        });
                        
                    }
                }).done(function (msg) {

                }).fail(function (msg) {
                    swal("Terjadi Kesalahan! ");
                });
                
            }else {
                swal("Dibatalkan!");
            } 
        });
    }
}

function processGetMaster() {
    swal({   
            title: "Get Master",   
            text: "Anda Yakin akan Mengambil Data Master ?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {
                
                var _form_data = new FormData($('#form_master_data')[0]);
                $.LoadingOverlay("show");
                  $.ajax({
                        type: 'POST',
                        url: base_url + '/get-master-data',
                        data: _form_data,
                        processData: false,
                        contentType: false,
                        success: function (res) {
                            //var parse = $.parseJSON(res);
                            if (res.rc == 0 || res.rc == '00') {
                                //modal_upd.modal('hide');
                               // hideLoading(true);
                                //showInfo(parse.msg);
                //                form_tambah[0].reset();
                                $('#form_master_data').trigger('reset');
                                $.LoadingOverlay("hide");
                                   swal({   
                                        title: "Berhasil",   
                                        text: "Data Berhasil",   
                                        type: "success",   
                                        confirmButtonColor: "#8cd4f5",   
                                        confirmButtonText: "Ya",     
                                        closeOnConfirm: false
                                    }, function(isConfirm){   
                                        if (isConfirm) {     
                                            location.reload();
                                        } 
                                    });
                                    return false;
                            }else{
                                $.LoadingOverlay("hide");
                                    swal({   
                                        title: "Informasi",   
                                        text: res.rm,   
                                        type: "info",   
                                        confirmButtonColor: "#8cd4f5",   
                                        confirmButtonText: "Ya",     
                                        closeOnConfirm: false
                                    });

                            }
                        }
                    });

            
            }else {
                swal("Dibatalkan!");
            } 
        });
}

function getHasilUji(no_dks,bacthid) {
     if(bacthid==null || bacthid==''){
        swal("Informasi",'Silahkan Login Adapter Terlebih Dahulu',"info")
    }else{

    swal({   
            title: "Get Hasil Uji",   
            text: "Anda Yakin akan Get Hasil Uji?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                
              $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: base_url + '/service/getHasilUji?batchid='+bacthid,
                    data: {
                        no_dks: no_dks
                    },
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        console.log(msg);
                          swal({   
                            title: "Berhasil",   
                            text: msg['rm'],   
                            type: "info",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                location.reload();
                            } 
                        });
                        
                    }
                }).done(function (msg) {

                }).fail(function (msg) {
                    swal("Terjadi Kesalahan! ");
                });
                
            }else {
                swal("Dibatalkan!");
            } 
        });
    }
}

function GetSisaPembayaran(ktp,batchid) {
    console.log(ktp);
    console.log(batchid);
    console.log(base_url + '/service/GetSisaPembayaran');
    

     if(batchid==null || batchid==''){
        swal("Informasi",'Silahkan Login Adapter Terlebih Dahulu',"info")
    }else{
         swal({   
            title: "Get Sisa Pembayaran",   
            text: "Anda Yakin akan Get Sisa Pembayaran?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                
              $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: base_url + '/service/GetSisaPembayaran?batchid='+batchid,
                    data: {
                        ktp: ktp,
                        batchid: batchid

                    },
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        console.log(msg);
                          swal({   
                            title: "Informasi",   
                            text: msg,   
                            type: "info",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Close",     
                            closeOnConfirm: true
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                // location.reload();
                            } 
                        });
                        
                    }
                }).done(function (msg) {

                }).fail(function (msg) {
                    swal("Terjadi Kesalahan! ");
                });
                
            }else {
                swal("Dibatalkan!");
            } 
        });
    }

   
    // }
}

</script>