@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Daftar Nasabah Sudah Cair</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
           <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
            <thead>
                <tr>
                    <th>ID Berkas</th>
                    <th>Nomor Cair</th>
                    <th>Tanggal Pecairan</th>
                    <th>Jumlah Debitur</th>
                    <th>Nilai FLPP</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if($data)
                    @php $no=0; @endphp
                    @foreach($data as $item)
                    @php $no++ @endphp    
                        <tr>
                            <td align="center">{{$item->id_berkas}}</td>
                            <td align="center">{{$item->no_cair}}</td>
                            <td align="center">{{$item->tgl_pencairan}}</td>
                            <td align="center">{{$item->tgl_pencairan}}</td>
                            <td align="center">{{$item->tgl_pencairan}}</td>

                            <td align="center"> 
                            <a class="i-button no-margin" href="{{route('lihat_detail_nasabah_sudah_cair','"'.$item->id_berkas.'"')}}">Lihat Detail Nasabah</a> 

                            <button style="width: 150px;" class="i-button no-margin" onclick="JadwalAngsurBy_idberkas('{{$item->no_identitas}}')">Lihat Jadwal Angsur</button> 
              
                          </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        </div>
    </div>
</div>

@include('ppdpp.action_cair')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
@stop
