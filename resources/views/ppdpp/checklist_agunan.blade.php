@section('content')
<div class="c-forms"> 
            <a class="i-button no-margin" href="{{route('permohonan_uji')}}">Kembali</a>   

    <div class="box-element" style="margin-top: 20px;">
        <form id="form_tambah_data">
            <input type="hidden" name="id_agunan" id="id_agunan" value="{{$data[0]->id_agunan_property}}">
            

            @foreach ($data as $item)

                <div class="i-label" style="width: 300px;">
                <label for="text_field">{{$item->param_name}}</label>
                </div>                                  
                <div class="section-right">
                    <div class="section-input">
                    <select class="i-text required" name="value_parm_id" id="value_parm_id_{{$item->sub_parm_id}}">
                            <option {{($item->value_parm_id == 0) ? "selected":"" }} value="0">Tidak</option>
                            <option {{($item->value_parm_id == 1) ? "selected":"" }} value="1">Ya</option>

                        </select>
                </div>
                <div class="i-divider"></div>
                <div class="clearfix"></div>

            @endforeach

            <div class="i-label" style="width: 300px;">
                <label for="text_field">ID SLF</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input type="text" class="i-text" name="id_slf" id="id_slf" required>
                </div>
                <div class="i-divider"></div>
                <div class="clearfix"></div>

            <div class="i-label" style="width: 300px;">
                <label for="text_field">Rekomendasi</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <select class="i-select" id="rekomendasi" name="rekomendasi">
                        <option value="1">Layak</option>
                        <option value="2">Tidak Layak</option>
                    </select>
                </div>
                <div class="i-divider"></div>
                <div class="clearfix"></div>    



            @php
                $login_batch = collect(\DB::select("select * from logins"))->first();
            @endphp

            


            <input type="button" style="margin: 20px !important;" onclick="TambahCheck('{{$data[0]->id_agunan_property}}',{{count($data)}},'{{$login_batch->batch_id}}');" class="i-button no-margin" value="Simpan" />

        </form>
    </div>
</div>
        
   


@endsection
@section('script')
    <script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
    
     <script>
         
             

                function TambahCheck(id,jumlah,bacthid) {

                    info = [];

                    for (var i = 1; i <= jumlah; i++) {
                        info[i] = $("#value_parm_id_"+i).val();
                    }
                    
                    console.log(info);
                    
                     $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: base_url + '/checklist_agunan/edit?batchid='+bacthid,
                        data: {
                            id: id,
                            info:info
                        },
                        beforeSend: function () {
                        },
                        success: function (msg) {
                            console.log(msg);
                            
                           
                            swal({   
                                title: "Berhasil",   
                                text: "Data Berhasil Disimpan",   
                                type: "info",   
                                confirmButtonColor: "#8cd4f5",   
                                confirmButtonText: "Ya",     
                                closeOnConfirm: false
                            }, function(isConfirm){   
                                if (isConfirm) {     
                                    window.location.href = base_url+ "/permohonan_uji"
                                } 
                            });
                            
                        }
                    }).done(function (msg) {

                    }).fail(function (msg) {
                        swal("Terjadi Kesalahan! ");
                    });
                  
                }
            </script>
@stop

