<script>
    
function ambilIdBerkas(ktp,id_nasabah,bacthid) {
    if(bacthid==null || bacthid==''){
        swal("Informasi",'Silahkan Login Adapter Terlebih Dahulu',"info")
    }else{

    swal({   
            title: "ID Berkas",   
            text: "Anda Yakin akan Request ID Berkas untuk Nasabah  ini ?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: base_url + '/service/ambilIdBerkas?batchid='+bacthid,
                    data: {
                        ktp: ktp,
                        id_nasabah : id_nasabah
                    },
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        console.log(msg);
                          swal({   
                            title: "Informasi",   
                            text: msg['rm'],   
                            type: "info",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                location.reload();
                            } 
                        });
                        
                    }
                }).done(function (msg) {

                }).fail(function (msg) {
                    swal("Terjadi Kesalahan! ");
                });
                
                
            }else {
                swal("Dibatalkan!");
            } 
        });
    }
}

function ambilDataPencairan() {
    
}

function cetakJadwalAngsur(ktp,batchid) {

    window.location.href = base_url +'/jadwalAngsurByKTP?id='+ ktp;
    
}

function updateStatusCair(id_nasabah,bacthid) {
    if(bacthid==null || bacthid==''){
        swal("Informasi",'Silahkan Login Adapter Terlebih Dahulu',"info")
    }else{

    swal({   
            title: "Update Status Cair CBS",   
            text: "Anda Yakin akan update Status Cair CBS Nasabah ini?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: base_url + '/act/updateStatusCair?batchid='+bacthid,
                    data: {
                        id_nasabah : id_nasabah
                    },
                    beforeSend: function () {
                    },
                    success: function (msg) {
                        console.log(msg);
                          swal({   
                            title: "Berhasil",   
                            text: "Berhasil Terkirim",   
                            type: "info",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                location.reload();
                            } 
                        });
                        
                    }
                }).done(function (msg) {

                }).fail(function (msg) {
                    swal("Terjadi Kesalahan! ");
                });
                
                
            }else {
                swal("Dibatalkan!");
            } 
        });
    }
}

</script>