<script>
    
function TambahDataBaru() {
    console.log("tambahBaru");

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "POST",
        url: base_url + '/user/tambah',
        data: {
            type: $('#type').val(),
            email: $('#email').val(),
            phone: $('#phone').val(),
            ktp: $('#ktp').val(),
            username: $('#username').val(),
            password: $('#password').val()
        },
        beforeSend: function () {
        },
        success: function (msg) {

        }
    }).done(function (msg) {
        window.location.href = base_url + '/user';

    }).fail(function (msg) {

    });
    
}

</script>