@section('content')
<div class="c-forms">
<a class="i-button no-margin" href="#" onclick="history.back();">Kembali</a>   

@if($data)
<a class="i-button no-margin" href="#" onclick="cetakPdf1({{$data[0]->id_berkas}})">Export to Pdf</a>  
<a class="i-button no-margin" href="#" onclick="cetakExcel1({{$data[0]->id_berkas}})">Export to Excel</a>  
@endif 
    <div class="box-element" style="
    margin-top: 20px;
">
    <div class="box-head-light">
            Kode Bank&nbsp;: 425
            <br>
            Nama Bank: BPD JAWA BARAT DAN BANTEN Syariah
            <br>
            Id Berkas&nbsp;&nbsp;&nbsp;&nbsp;: {{($data ? $data[0]->id_berkas : '-')}}
        </div>
         
        <div class="box-content no-padding">
     
           <table cellpadding="0" cellspacing="0" border="0" class="display">
            <thead>
                    <th>Id Berkas</th>
                    <th>Bulan</th>
                    <th>Tahun</th>
                    <th>Sisa Pokok FLPP</th>
                    <th>Nilai Angsuran FLPP</th>
                    <th>Nilai Pokok FLPP</th>
                    <th>Nilai Tarif FLPP</th>
                    <th>Outstanding FLPP</th>
            </thead>
            @if($data)
            <tbody>
                @foreach($data as $item)
                    <tr>
                        <td>{{$item->id_berkas}}</td>
                        <td>{{$item->d_bulan}}</td>
                        <td>{{$item->d_tahun}}</td>
                        <td>{{number_format($item->sisapokok,0,',','.')}}</td>
                        <td>{{number_format($item->nilaiangsuran,0,',','.')}}</td>
                        <td>{{number_format($item->pokok,0,',','.')}}</td>
                        <td>{{number_format($item->tarif,0,',','.')}}</td>
                        <td>{{number_format($item->outstanding,0,',','.')}}</td>
                    </tr>
                @endforeach
            </tbody>
            @php 
              $total=0;
              $total1=0;
              $total2=0;
              @endphp
              @foreach($data as $item)
              @php 
              ($total +=$item->nilaiangsuran);
              ($total1 += $item->pokok);
              ($total2 += $item->tarif);
              @endphp
            @endforeach 
            <tfoot>
                <th>Total</th>
                <th></th>
                <th></th>
                <th>{{number_format($total,0,',','.')}}</th>
                <th>{{number_format($total1,0,',','.')}}</th>
                <th>{{number_format($total2,0,',','.')}}</th>
                <th></th>
                <th></th>
            </tfoot>
            @endif
        </table>
        </div>
    </div>
</div>
@include('ppdpp.action')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
@stop
