@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Edit Dokumen</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
            <form method="post" action="{{route('update_dok')}}" enctype="multipart/form-data" class="i-validate"> 
               @csrf
                <fieldset>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Jenis Dokumen</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="hidden" name="id" id="id" value="{{$id}}" class="i-text i-form-tooltip"></input>
                                <input type="hidden" name="idprospek" id="idprospek" value="{{$data[0]->id_prospek}}" class="i-text i-form-tooltip"></input>
                                @php
                                $ref = \DB::select("select * from ref_jenis_dokumen");
                                @endphp
                                <select class="i-select" name="idjenis">
                                    @foreach($ref as $item)
                                    <option value="{{$item->id}}" @if($data[0]->tipe_dok==$item->id) selected @endif>{{$item->description}}</option>
                                    @endforeach
                                </select>    
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">File</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="file" name="dok_file" id="dok_file"></input>    
                                <p>{{$data[0]->url_doc}}</p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    
                    <section>
                        <button type="submit" class="i-button no-margin">Simpan</button>
                        <div class="clearfix"></div>
                    </section>

                </fieldset>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $( "#kota" ).change(function() {
      var _items = "";
    $.ajax({
                type: 'GET',
                url: base_url+'/select/dati/'+this.value,
                success: function (res) {

                    var data = $.parseJSON(res);
                    
                    $.each(data, function (k,v) {
                    console.log(v);
                        _items += "<option value='"+v.kode_dati+"'>"+v.nama_dati+"</option>";
                    });
                    $('#provinsi').html(_items);
                }
        });
  });
</script>
@stop
