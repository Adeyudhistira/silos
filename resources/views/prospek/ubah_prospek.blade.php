@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Data Prospek Pembiayaan Konsumer</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
            @if ($message = Session::get('error'))
         <div class="section-content offset-one-third">                              
             <div class="alert-msg error-msg">{{ $message }}<a href="">×</a></div>
         </div>
         @endif
            <form method="post" action="{{route('daftar')}}" class="i-validate"> 
               @csrf
                <fieldset>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Produk</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="hidden" name="id" id="id" value="{{$data['id']}}" class="i-text i-form-tooltip"></input>
                                <input type="hidden" name="idprod" id="idprod" value="{{$data['idProduct']}}" class="i-text i-form-tooltip"></input>
                                <input type="text" readonly  name="produk" id="produk" value="{{$data['product']['prodName']}}" class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                    <h3><u>Data Nasabah</u></h3>   
                        
                        <input type="hidden" id="chanel" name="chanel" value="0">
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Pelanggan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="nama_pelanggan" id="nama_pelanggan" value="{{$data['nmProspek']}}" required class="i-text i-form-tooltip myclass"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nomor KTP</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="noktp" id="noktp" value="{{$data['noIdentitas']}}" onkeypress="return hanyaAngka(event)" required maxlength="16" minlength="16" class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Jenis Kelamin</label>
                        </div>                                  
                        <div class="section-right">           
                            <div class="section-input i-transform i-span jqtransformdone">
                                <span class="i-transform"><input type="radio" id="jk" name="jk" value="0" checked="checked" class="i-transform"></span><label style="cursor: pointer;">Laki - laki</label>
                                <span class="i-transform"><input type="radio" id="jk" name="jk" value="1" class="i-transform"></span><label style="cursor: pointer;">Perempuan</label>
                            </div> 
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Tempat Lahir</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="tmp_lahir" id="tmp_lahir" value="{{$data['tempatLahir']}}" required class="i-text i-form-tooltip myclass"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Tanggal Lahir</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="date" name="tgl_lahir" id="tgl_lahir" value="{{$data['tglLahir']}}" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Alamat</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <textarea required name="alamat" id="alamat" class="i-text i-form-tooltip myclass">{{$data['alamat']}} </textarea>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">No Telepon/Hp</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="no_telp" value="{{$data['telp']}}" onkeypress="return hanyaAngka(event)" maxlength="13" id="no_telp" required class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div> 
                        <div class="section-left-s">
                            <label for="text_tooltip">NPWP</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="npwp" value="{{$data['npwp']}}" onkeypress="return hanyaAngka(event)" maxlength="15" id="npwp" class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>  
                        <div class="section-left-s">
                            <label for="text_tooltip">Status Menikah</label>
                        </div>                                  
                        <div class="section-right">           
                            <div class="section-input i-transform i-span jqtransformdone">
                                <span class="i-transform">
                                    <input type="radio" id="sm" name="sm" value="t" @if($data['isMenikah']==true) checked="checked" @endif class="i-transform"></span><label style="cursor: pointer;">Menikah</label>
                                <span class="i-transform">
                                    <input type="radio" id="sm" name="sm" value="f" @if($data['isMenikah']==false) checked="checked" @endif class="i-transform"></span><label style="cursor: pointer;">Belum Menikah</label>
                            </div> 
                        </div>
                        <div class="clearfix"></div> 
                        <div class="section-left-s">
                            <label for="text_tooltip" id="nmpasangan1" @if($data['isMenikah']==false) style="display:none" @endif>Nama Pasangan</label>
                        </div> 
                        <div class="section-right">                                         
                            <div class="section-input">
                            <input type="text" value="{{$data['namaPasangan']}}" name="nmpasangan" id="nmpasangan" class="i-text i-form-tooltip" @if($data['isMenikah']==false) style="display:none" @endif></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>  
                        <div class="section-left-s">
                            <label for="text_tooltip" id="ktppasangan1" @if($data['isMenikah']==false) style="display:none" @endif>No KTP Pasangan</label>
                        </div> 
                        <div class="section-right">                                         
                            <div class="section-input">
                            <input type="text" value="{{$data['noKtpPasangan']}}" name="ktppasangan" id="ktppasangan" class="i-text i-form-tooltip" maxlength="16" minlength="16" @if($data['isMenikah']==false) style="display:none" @endif></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Catatan AO</label>
                        </div> 
                        <div class="section-right">                                         
                            <div class="section-input">
                            <input type="text" value="{{$data['aoNote']}}" name="aonote" id="aonote" class="i-text i-form-tooltip" maxlength="200"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Cari Kelurahan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input value="" type="text" name="nama_kelurahan_agunan" id="nama_kelurahan_agunan" class="i-text"></input>
                            </div>
                            <input type="button" style="margin: 10px 0 !important;" onclick="getLokasi('nama_kelurahan_agunan','setlokasiagunan');" class="i-button no-margin" value="Cari" />
                        </div>
                        <div class="clearfix"></div>  

                    <div class="section-left-s">
                            <label for="text_tooltip">Pilih Lokasi</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                 <select id="setlokasiagunan" name="lokasiagunan" class="js-example-basic-single i-text required">
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>   
                        

                    </section>
                    <section>
                    <h3><u>Data Pekerjaan</u></h3>   
                        <div class="section-left-s">
                            <label for="text_tooltip">Jenis Pekerjaan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <select required class="i-select form-control" id="job" name="job" tabindex="1">
                                   <option value="">Pilih Pekerjaan</option>
                                   @foreach($job as $item)
                                   <option value="{{$item->id_pekerjaan}}" @if($data['idPekerjaan']==$item->id_pekerjaan) selected @endif>{{$item->nm_pekerjaan}}</option>
                                   @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Kantor</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="nm_kantor" value="{{$data['officeName']}}" id="nm_kantor" required class="i-text i-form-tooltip myclass"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Alamat Kantor</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <textarea name="alamat_kantor" id="alamat_kantor" class="i-text i-form-tooltip myclass" required>{{$data['officeAddress']}} </textarea>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">No telepon Kantor</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" value="{{$data['officePhone']}}" name="no_telp_kantor" onkeypress="return hanyaAngka(event)" maxlength="13" id="no_telp_kantor" required class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Sumber Pendapatan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                @php
                                $dt = \DB::select("SELECT * FROM ref_sumber_pendapatan");
                                @endphp
                                <select class="i-text required" name="sumber_pendapatan" id="sumber_pendapatan">
                                    <option value="">Pilih</option>
                                    @foreach ($dt as $item)
                                    <option @if($data['sumberPendapatanId']==$item->id_sumber) selected @endif value="{{$item->id_sumber}}">{{$item->keterangan}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Gaji Perbulan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="gaji" id="gaji" value="{{number_format($data['pendapatanBulan'],0,',','.')}}" onkeypress="return hanyaAngka(event)" onkeyup="convertToRupiah(this)" required class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Take Home Pay</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="pendapatan" value="{{number_format($data['pendapatanLainnya'],0,',','.')}}" id="pendapatan" onkeypress="return hanyaAngka(event)" onkeyup="convertToRupiah(this)" class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>     
                    </section>
                    <section>
                    <h3><u>Data Pembiayaan</u></h3>   
                        <div class="section-left-s">
                            <label for="text_tooltip">Harga Objek</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="harga_objek" id="harga_objek" value="{{number_format($data['hargaObject'],0,',','.')}}" readonly  class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Uang Muka</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="uang_muka" id="uang_muka" value="{{number_format($data['uangMuka'],0,',','.')}}"  readonly class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Jangka Waktu</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="jangka_waktu" id="jangka_waktu" value="{{$data['jangkaWaktu']}}"  readonly class="i-text i-form-tooltip"></input> <b>Bln</b>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Angsuran</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="angsuran" value="{{number_format($data['totalAngsuran'],0,',','.')}}" id="angsuran"  readonly class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>     
                    </section>

                    <section>
                        <input type="submit" name="submit" class="i-button no-margin" value="Simpan" />
                        <div class="clearfix"></div>
                    </section>
                        <input value="{{$data['wilayah']['wilayah']}}" type="hidden" name="kelurahan2" id="kelurahan2"></input>
                </fieldset>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">



    $(".loading-mail").show();
    var kelurahan = $('#kelurahan2').val();
     var _items = "";
    $.ajax({
        type: 'POST',
        url: base_url + '/select/apikelurahan1?id=' + kelurahan,
        success: function (res) {
            $(".loading-mail").hide();
                console.log(res['data']);
                
            
                  var data = $.parseJSON(res);
                
                
                $.each(data['data'], function (k,v) {
                    _items += "<option value='"+v.id+"'>"+v.wilayah+"</option>";
                });

                $("#setlokasiagunan").html(_items);
                
        }
    }).done(function( res ) {
         $(".loading-mail").hide();
            
    }).fail(function(res) {
        $(".loading-mail").hide();
        swal("Terjadi Kesalahan! ");
    });

    $( "#kota" ).change(function() {
      var _items = "";
    $.ajax({
                type: 'GET',
                url: base_url+'/select/dati/'+this.value,
                success: function (res) {

                    var data = $.parseJSON(res);
                    
                    $.each(data, function (k,v) {
                    console.log(v);
                        _items += "<option value='"+v.kode_dati+"'>"+v.nama_dati+"</option>";
                    });
                    $('#provinsi').html(_items);
                }
        });
  });

  $("#sm").change(function() {
    var sm = $('#sm:checked').val();
    if(sm=='f')
    {
        $('#nmpasangan').css('display','none');
        $('#ktppasangan').css('display','none');
        $('#nmpasangan1').css('display','none');
        $('#ktppasangan1').css('display','none');
    } else {
        $('#nmpasangan').css('display','block');
        $('#ktppasangan').css('display','block');
        $('#nmpasangan1').css('display','block');
        $('#ktppasangan1').css('display','block');
    }
  });

    function getLokasi(set,target) {

     $(".loading-mail").show();
    var kelurahan = $('#'+set).val();

    var _items = "";
    $.ajax({
        type: 'GET',
        url: base_url + '/select/apikelurahan/' + kelurahan,
        success: function (res) {
            $(".loading-mail").hide();
                console.log(res['data']);
                
            
                  var data = $.parseJSON(res);
                _items = "<option value=''>Pilih Lokasi</option>";
                
                
                $.each(data['data'], function (k,v) {
                    _items += "<option value='"+v.id+"'>"+v.wilayah+"</option>";
                });

                $("#"+target).html(_items);
                
        }
    }).done(function( res ) {
         $(".loading-mail").hide();
            
    }).fail(function(res) {
        $(".loading-mail").hide();
        swal("Terjadi Kesalahan! ");
    });
}
</script>
@stop
