@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Data Prospek Pembiayaan Konsumer</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
             @if ($message = Session::get('error'))
         <div class="section-content offset-one-third">                              
             <div class="alert-msg error-msg">{{ $message }}<a href="">×</a></div>
         </div>
         @endif
            <form method="post" action="{{route('daftar')}}" class="i-validate"> 
               @csrf
                <fieldset>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Produk</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="hidden" name="id" id="id" value="" class="i-text i-form-tooltip"></input>

                                <input type="hidden" name="idprod" id="idprod" value="{{$product[0]->id}}" class="i-text i-form-tooltip"></input>
                                <input type="text" readonly  name="produk" id="produk" value="{{$product[0]->prod_name}}" class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                    <h3><u>Data Nasabah</u></h3>   
                    <input type="hidden" id="chanel" name="chanel" value="0">
                            
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Pelanggan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="nama_pelanggan" onkeypress="return huruf(event)" minlength="3" id="nama_pelanggan" required class="i-text i-form-tooltip myclass" maxlength="40"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nomor KTP</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="noktp" id="noktp" onkeypress="return hanyaAngka(event)" required maxlength="16" minlength="16" class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Jenis Kelamin</label>
                        </div>                                  
                        <div class="section-right">           
                            <div class="section-input i-transform i-span jqtransformdone">
                                <span class="i-transform">
                                    <input type="radio" id="jk" name="jk" value="1" checked="checked" class="i-transform"></span><label style="cursor: pointer;">Laki - laki</label>
                                <span class="i-transform">
                                    <input type="radio" id="jk" name="jk" value="2" class="i-transform"></span><label style="cursor: pointer;">Perempuan</label>
                            </div> 
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Tempat Lahir</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="tmp_lahir" onkeypress="return huruf(event)" maxlength="100" minlength="3" id="tmp_lahir" required class="i-text i-form-tooltip myclass"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Tanggal Lahir</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="date" name="tgl_lahir" required id="tgl_lahir" value="{{$tgl}}" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Alamat</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <textarea required name="alamat" onkeypress="return Alphabets(event);" id="alamat" class="i-text i-form-tooltip myclass"> </textarea> <span id="lblError" style="color: red"></span>
                            </div>
                        </div>
                        
                        <!--
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Kota</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <select required class="i-select form-control" id="kota" name="kota" tabindex="1">
                                   <option value="">Pilih Kota</option>
                                   @foreach($dati2 as $item)
                                   <option value="{{$item->kode_dati2}}">{{$item->nama_dati2}}</option>
                                   @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Provinsi</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <select required  id="provinsi" class="i-text" name="provinsi">
                                </select>
                            </div>
                        </div>
                    -->
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">No Telepon/Hp</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="no_telp" onkeypress="return hanyaAngka(event)" maxlength="13" minlength="7" id="no_telp" required class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>  
                        <div class="section-left-s">
                            <label for="text_tooltip">NPWP</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="npwp" onkeypress="return hanyaAngka(event)" maxlength="15" minlength="15" id="npwp" class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>  
                        <div class="section-left-s">
                            <label for="text_tooltip">Status Menikah</label>
                        </div>                                  
                        <div class="section-right">           
                            <div class="section-input i-transform i-span jqtransformdone">
                                <span class="i-transform">
                                    <input type="radio" id="sm" name="sm" value="t" checked="checked" class="i-transform"></span><label style="cursor: pointer;">Menikah</label>
                                <span class="i-transform">
                                    <input type="radio" id="sm" name="sm" value="f" class="i-transform"></span><label style="cursor: pointer;">Belum Menikah</label>
                            </div> 
                        </div>
                        <div class="clearfix"></div> 
                        <div class="section-left-s">
                            <label for="text_tooltip" id="nmpasangan1">Nama Pasangan</label>
                        </div> 
                        <div class="section-right">                                         
                            <div class="section-input">
                            <input type="text" name="nmpasangan" id="nmpasangan" maxlength="40" onkeypress="return huruf(event)" class="i-text i-form-tooltip" ></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>  
                        <div class="section-left-s">
                            <label for="text_tooltip" id="ktppasangan1">No KTP Pasangan</label>
                        </div> 
                        <div class="section-right">                                         
                            <div class="section-input">
                            <input type="text" name="ktppasangan" id="ktppasangan" onkeypress="return hanyaAngka(event)" class="i-text i-form-tooltip" maxlength="16" minlength="16"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Catatan AO</label>
                        </div> 
                        <div class="section-right">                                         
                            <div class="section-input">
                            <input type="text" name="aonote" id="aonote" onkeypress="return Alphabets(event);" class="i-text i-form-tooltip" maxlength="200"></input><span id="lblError" style="color: red"></span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                       <!-- 
                        <div class="section-left-s">
                            <label for="text_tooltip">Cabang Pemproses</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <select required class="i-select" id="branch" name="branch" tabindex="1">
                                   <option value="">Pilih Cabang</option>
                                   @foreach($cabang as $item)
                                   <option value="{{$item->id}}">{{$item->branch_name}}</option>
                                   @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>  
                    -->
                    <div class="section-left-s">
                            <label for="text_tooltip">Cari Kelurahan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input value="" type="text" name="nama_kelurahan_agunan" id="nama_kelurahan_agunan" class="i-text required"></input>
                            </div>
                            <input type="button" style="margin: 10px 0 !important;" onclick="getLokasi('nama_kelurahan_agunan','setlokasiagunan');" class="i-button no-margin" value="Cari" />
                        </div>
                        <div class="clearfix"></div>  

                    <div class="section-left-s">
                            <label for="text_tooltip">Pilih Lokasi</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                 <select id="setlokasiagunan" name="lokasiagunan" class="js-example-basic-single i-text required">
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>  

                    </section>
                    <section>
                    <h3><u>Data Pekerjaan</u></h3>   
                        <div class="section-left-s">
                            <label for="text_tooltip">Jenis Pekerjaan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <select required class="i-select form-control" id="job" name="job" tabindex="1">
                                   <option value="">Pilih Pekerjaan</option>
                                   @foreach($job as $item)
                                   <option value="{{$item->id_pekerjaan}}">{{$item->nm_pekerjaan}}</option>
                                   @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Kantor</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="nm_kantor" onkeypress="return huruf(event)"  maxlength="150" id="nm_kantor" required class="i-text i-form-tooltip myclass"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Alamat Kantor</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <textarea name="alamat_kantor" maxlength="150" onkeypress="return Alphabets(event);" id="alamat_kantor" class="i-text i-form-tooltip myclass" required> </textarea></input><span id="lblError" style="color: red"></span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">No telepon Kantor</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="no_telp_kantor" onkeypress="return hanyaAngka(event)" maxlength="13" id="no_telp_kantor" required class="i-text i-form-tooltip" minlength="7"></input>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Sumber Pendapatan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                @php
                                $dt = \DB::select("SELECT * FROM ref_sumber_pendapatan");
                                @endphp
                                <select class="i-text required" name="sumber_pendapatan" id="sumber_pendapatan">
                                    <option value="">Pilih</option>
                                    @foreach ($dt as $item)
                                    <option value="{{$item->id_sumber}}">{{$item->keterangan}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Gaji Perbulan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="gaji" id="gaji" onkeypress="return hanyaAngka(event)" onkeyup="convertToRupiah(this)" maxlength="9" required class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Take Home Pay</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="pendapatan" id="pendapatan" onkeypress="return hanyaAngka(event)" onkeyup="convertToRupiah(this)" maxlength="9" class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>     
                    </section>
                    <section>
                    <h3><u>Data Pembiayaan</u></h3>   
                        <div class="section-left-s">
                            <label for="text_tooltip">Harga Objek</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="harga_objek" id="harga_objek" readonly value="{{$harga}}" class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Uang Muka</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="uang_muka" id="uang_muka" value="{{$uangmuka}}" readonly class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Jangka Waktu</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="jangka_waktu" id="jangka_waktu" value="{{$jangka}}" readonly class="i-text i-form-tooltip"></input> <b>Thn</b>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="section-left-s">
                            <label for="text_tooltip">Angsuran</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="angsuran" id="angsuran" value="{{number_format($angsuran,0,',','.')}}" readonly class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>     
                    </section>
                    <section>
                    <section>
                        <input type="submit" name="submit" id="" class="i-button no-margin" value="Simpan" />
                        <div class="clearfix"></div>
                    </section>

                </fieldset>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $( "#kota" ).change(function() {
      var _items = "";
    $.ajax({
                type: 'GET',
                url: base_url+'/select/dati/'+this.value,
                success: function (res) {

                    var data = $.parseJSON(res);
                    
                    $.each(data, function (k,v) {
                    console.log(v);
                        _items += "<option value='"+v.kode_dati+"'>"+v.nama_dati+"</option>";
                    });
                    $('#provinsi').html(_items);
                }
        });
  });

  $("#sm").change(function() {
    var sm = $('#sm:checked').val();
    if(sm=='f')
    {
        $('#nmpasangan').css('display','none');
        $('#ktppasangan').css('display','none');
        $('#nmpasangan1').css('display','none');
        $('#ktppasangan1').css('display','none');
    } else {
        $('#nmpasangan').css('display','block');
        $('#ktppasangan').css('display','block');
        $('#nmpasangan1').css('display','block');
        $('#ktppasangan1').css('display','block');

        document.getElementById("nmpasangan").required;
        document.getElementById("ktppasangan").required;
        document.getElementById("nmpasangan1").required;
        document.getElementById("ktppasangan1").required;
    }
  });

  function getLokasi(set,target) {

     $(".loading-mail").show();
    var kelurahan = $('#'+set).val();

    var _items = "";
    $.ajax({
        type: 'GET',
        url: base_url + '/select/apikelurahan/' + kelurahan,
        success: function (res) {
            $(".loading-mail").hide();
                console.log(res['data']);
                
            
                  var data = $.parseJSON(res);
                _items = "<option value=''>Pilih Lokasi</option>";
                
                
                $.each(data['data'], function (k,v) {
                    _items += "<option value='"+v.id+"'>"+v.wilayah+"</option>";
                });

                $("#"+target).html(_items);
                
        }
    }).done(function( res ) {
         $(".loading-mail").hide();
            
    }).fail(function(res) {
        $(".loading-mail").hide();
        swal("Terjadi Kesalahan! ");
    });
}
  
</script>
@stop
