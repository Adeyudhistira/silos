@section('content')
<div class="c-forms">
<input type="button" value="Tambah Dokumen" class="i-button no-margin" onclick="tambahdok({{$data['id']}})"> 
<input type="button" value="Selesai" class="i-button no-margin" onclick="selesai()"> 
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Upload Dokumen Nasabah</h3><a href="" class="collapsable"></a></div>
        @if ($message = Session::get('success'))
            <div class="section-content offset-one-third">                              
             <div class="alert-msg success-msg ">{{ $message }}<a href="">×</a></div>
         </div>
         @endif


         @if ($message = Session::get('error'))
         <div class="section-content offset-one-third">                              
             <div class="alert-msg error-msg">{{ $message }}<a href="">×</a></div>
         </div>
         @endif
        <div class="box-content no-padding">

            <fieldset>
                <section>
                   <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
                    <thead>
                        <tr>
                            <th>Jenis Dokumen</th>
                            <th>Nama Dokumen</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($data)
                            @foreach($data['documents'] as $item)
                            @php
                            $doc = "<img style='border: 3px solid #d0d0d0;padding: 5px;margin: 5px;' src='".env('API_BASE_URL').$item['urlDoc']."' alt='' width='200'>";
                            @endphp
                            <tr>
                                <td align="center">{{$item['description']}}</td>
                                <td align="center">@php echo $doc; @endphp</td>
                                <td align="center"><input type="button" value="Edit" onclick="edit({{$item['idProspek']}},{{$item['id']}})" class="i-button no-margin"></td>
                            </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>   
            </section> 
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
     function tambahdok(id) {
        window.location.href = base_url +'/tambah_dokumen?id='+id;
    }
    function edit(idProspek,id) {
        window.location.href = base_url +'/edit_dokumen?id='+id+'&&idprospek='+idProspek;
    }
    function selesai() {
            window.location.href = base_url +'/daftar_prospek_baru';     
        
        
    }
    
</script>
@stop
