@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Daftar Prospek Baru</h3><a href="" class="collapsable"></a></div>
           @if ($message = Session::get('success'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg success-msg ">{{ $message }}<a href="">×</a></div>
            </div>
            @endif


            @if ($message = Session::get('error'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg error-msg">{{ $message }}<a href="">×</a></div>
            </div>
            @endif
         
        <div class="box-content no-padding">
            <input type="button" class="icon16-button" onclick="tambah()" value="Tambah Prospek">
           <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
            <thead>
                <tr>
                    <th>Nama Nasabah</th>
                    <th>Alamat</th>
                    <th>Produk</th>
                    <th>Plafon</th>
                    <th>Jangka Waktu</th> 
                    <th>Angsuran</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if($data)
                    @foreach($data as $item)
                        <tr>
                            <td>{{$item['nmProspek']}}</td>
                            <td>{{$item['alamat']}}</td>
                            <td>{{$item['product']['prodName']}}</td>
                            <td>{{number_format($item['plafon'],0,',','.')}}</td>
                            <td nowrap>{{$item['jangkaWaktu']}} Bln</td>
                            <td>{{number_format($item['totalAngsuran'],0,',','.')}}</td>
                            <td nowrap>
                                <input type="button" class="button" onclick="ubah({{$item['id']}})" value="Ubah Data">
                                <input type="button" class="button" onclick="pengajuan({{$item['id']}})" value="Kirim Prospek">
                                
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection
@section('script')

<script type="text/javascript">
    
    function tambah(){
        window.location.href=base_url +'/entry_prospek'; 
    }

    function ubah(id) {
        window.location.href = base_url +'/ubah_prospek?id=' + id;
    }

    function pengajuan(id) {
        window.location.href = base_url +'/kirim_pengajuan?id=' + id;
    }
     function proses(id) {
        window.location.href = base_url +'/entry_nasabah';
    }
</script>
@stop
