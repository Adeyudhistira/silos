@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Konfirmasi Cabang Proses</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
            <form method="post" action="{{route('pengajuan')}}" class="i-validate"> 
               @csrf
                <fieldset>
                    <input type="hidden" name="id" id="id" value="{{$id}}" class="i-text i-form-tooltip"></input>
                    <section>  
                        <div class="section-left-s">
                            <label for="select">Pilih Cabang</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <select required class="i-select" id="branch" name="branch" tabindex="1">
                                   <option value="">Pilih Cabang</option>
                                   @foreach($cabang as $item)
                                   <option value="{{$item->id}}">{{$item->branch_name}}</option>
                                   @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
     
                    </section>

                    <section>
                        <input type="submit" name="submit" id="" class="i-button no-margin" value="Simpan" />
                        <div class="clearfix"></div>
                    </section>

                </fieldset>
            </form>
        </div>
    </div>
</div>
@endsection