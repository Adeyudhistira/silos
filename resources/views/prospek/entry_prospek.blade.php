@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Silahkan Lakukan Simulasi Pengajuan Pembiayaan</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
            <form class="form-horizontal" id="form-filter" method="GET"> 
                <fieldset>
                    <section>
                        <div class="section-left-s">
                            <label for="select">Pilih Produk</label> 
                        </div>                                  
                        <div class="section-right">                                        
                            <div class="section-input">
                                @php
                                $prod=\DB::select("select * from ref_product order by seq asc");
                                @endphp
                                <select class="i-text i-form-tooltip required" required id="prod" name="prod">
                                   <option value="">Pilih Product</option>
                                   @foreach($prod as $item)
                                   <option value="{{$item->id}}" @if($idprod==$item->id) selected @endif>{{$item->prod_name}}</option>
                                   @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Harga Objek</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" onkeypress="return hanyaAngka(event)" maxlength="11" onkeyup="convertToRupiah(this)"  name="harga" id="harga" class="i-text i-form-tooltip required" @if($harga) value="{{$harga}}" @endif></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Uang Muka</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" onkeypress="return hanyaAngka(event)" maxlength="11" onkeyup="convertToRupiah(this)" name="uang_muka" id="uang_muka" class="i-text i-form-tooltip required" @if($uang_muka) value="{{$uang_muka}}" @endif></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Tanggal Lahir</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                            <input type="date" required name="tgl" id="tgl" class="i-text i-form-tooltip" @if($tgl) value="{{$tgl}}" @endif></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <input type="button" value="Kembali" onclick="kembali()" class="i-button no-margin">
                         <button type="submit" onclick="validasi()" value="simulasi" id="simulasi" name='simulasi' class="i-button no-margin"> Simulasi</button>
                        <div class="clearfix"></div>
                    </section>
                </fieldset>
            </form>
        </div>
    </div>

    <div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Daftar Angsuran</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
            @if($data)
            <table cellpadding="0" cellspacing="0" border="0" class="display table table-striped table-hover mb-0">
                <thead>
                    <tr>
                        <th>Jangka Waktu</th>
                        <th>Angsuran</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($data as $item)

                    <tr>
                        <td style="text-align: center;">{{$item->tahun}} Tahun</td>
                        <td style="text-align: center;">{{number_format($item->angsuran)}}</td>
                        <td style="text-align: center;"><input type="button" class="i-button no-margin" onclick="getdata('{{$idprod}}','{{$harga}}','{{$uang_muka}}','{{$tgl}}','{{$item->tahun}}','{{$item->angsuran}}')" value="Pilih"></a></td>
                    </tr>

                    @endforeach

                </tbody>
            </table>
            @else
                @if($idprod)
                    <p>Batas usia Minimal 21 Thn dan Maximal 50 Thn</p>
                @endif
            @endif
        </div>
    </div>
</div>

</div>

@endsection
@section('script')

<script type="text/javascript">
    function getdata(idproduk,harga,uang_muka,tgl,jangka,angsuran) {
        window.location.href = base_url +'/data_prospek?idproduk=' + idproduk +'&&harga='+ harga +'&&uangmuka='+ uang_muka +'&&tgl='+ tgl +'&&jangka='+ jangka +'&&angsuran='+ angsuran;
    }
</script>
<!--
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
   
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

-->

<!--  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
-->
 <script>
        $('.datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
</script>

<script type="text/javascript">
    function kembali(){
        window.location.href=base_url +'/daftar_prospek_baru'; 
    }
    function validasi(){

        var a1=$('#harga').val();
        var a=a1.replace(/\./g,'');
        var b1=$('#uang_muka').val();
        var b=b1.replace(/\./g,'');
        var c= parseInt(a);


        if(parseInt(b) > parseInt(a)){
            swal("Informasi","uang muka tidak boleh lebih besar dari harga objek","info");
        }

        var date = document.getElementById('tgl').value;
 
        if(date === ""){
            alert("Please complete the required field!");
        }else{
            var today = new Date();
            var birthday = new Date(date);
            var year = 0;
            if (today.getMonth() < birthday.getMonth()) {
                year = 1;
            } else if ((today.getMonth() == birthday.getMonth()) && today.getDate() < birthday.getDate()) {
                year = 1;
            }
            var age = today.getFullYear() - birthday.getFullYear() - year;
     
            if(age < 0){
                age = 0;
            }

            if(age < 21){
                swal("Informasi","usia anda : "+age+" Tahun \n Batas usia Pengajuan minimal 21 Tahun","info");
            }
            if(age > 50){
                swal("Informasi","usia anda : "+age+" Tahun \n Batas usia Pengajuan masimal 50 Tahun","info");
            }
        }
        

    }
</script>
@stop

