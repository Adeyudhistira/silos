<div id="error-page-container">
    <h1>500</h1>
    <hr />
    <span>Oops! Something went wrong. Internal server error!</span>
    <a href="{{ route('home') }}">Back to home</a>
</div>