@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Daftar Pra Booking</h3><a href="" class="collapsable"></a></div>
           @if ($message = Session::get('success'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg success-msg ">{{ $message }}<a href="">×</a></div>
            </div>
            @endif


            @if ($message = Session::get('error'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg error-msg">{{ $message }}<a href="">×</a></div>
            </div>
            @endif
        
        <div class="box-content no-padding">
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
                <thead>
                    <tr>
                        <th>Nama Nasabah</th>
                        <th>Alamat</th>
                        <th>Produk</th>
                        <th>Cabang</th>
                        <th>Plafon</th>
                        <th>Jangka Waktu</th>
                        <th>Angsuran</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if($data)
                        @foreach($data as $item)
                        <tr>
                            @if($item['nasabah'])
                              <td>{{$item['nasabah']['namaNasabah']}}</td>
                              <td>{{$item['nasabah']['alamat']}}</td>  
                            @else
                              <td>-</td>
                              <td>-</td>  
                            @endif
                            @if($item['product'])
                              <td>{{$item['product']['prodName']}}</td>
                            @else
                              <td>-</td>  
                            @endif
                            @if($item['branch'])
                              <td>{{$item['branch']['branchName']}}</td>
                            @else
                              <td>-</td>  
                            @endif
                             <td>{{number_format($item['plafon'],0,',','.')}}</td>
                            <td>{{$item['jangkaWaktu']}} Bln</td>

                            <td>{{number_format($item['totalAngsuran'],0,',','.')}}</td>
                           
                            @if($item['wfstatus'])
                              <td>{{$item['wfstatus']['statusDefinition']}}</td>
                            @else
                              <td>-</td>  
                            @endif
                            <td>
                                 <!-- <input type="button" class="button" onclick="edit({{$item['nasabah']['id']}})" value="EDIT"> -->
                                <input type="button" class="button" onclick="slik({{$item['id']}})" value="SLIK">
                                <input type="button" class="button" onclick="appraisal({{$item['id']}})" value="APPRAISAL">
                                <input type="button" class="button" onclick="analisa({{$item['id']}})" value="ANALISA">
                                <!--
                                <input type="button" class="button" onclick="sla({{$item['id']}})" value="SLA DETAIL">
                                -->
                                 @if($item['agunan'])
                                <input type="button" class="button" onclick="ppdpp({{$item['agunan']['id']}})" value="CHECKLIST PPDPP">
                                @endif
                               
                                <input type="button" class="button" onclick="kirimproses({{$item['id']}})" value="KIRIM PROSES SELANJUTNYA">
                                <input type="button" class="button" onclick="kirimkeao({{$item['id']}})" value="KIRIM BALIK KE AO">

                               
                            </td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table> 
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
   //  function edit(id) {
   //      window.location.href = base_url +'/edit_nasabah?id=' + id;
   //  }

   function slik(id) {
        window.location.href = base_url +'/slik_pra_booking_review?id=' + id;
    }

    function appraisal(id) {
        window.location.href = base_url +'/appraisal_pra_booking_review?id=' + id;
    }

    function analisa(id) {
        window.location.href = base_url +'/analisa_pra_booking_review?id=' + id;
    }

    // function sla(id) {
    //     window.location.href = base_url +'/sla_pra_booking_review?id=' + id;
    // }

    function ppdpp(id) {
        window.location.href = base_url +'/ppdpp_pra_booking_review?id=' + id;
    }

   //  function proses_approval(id) {
   //      window.location.href = base_url +'/proses_approval?id=' + id;
   //  }

    function kirimproses(id) {
        swal({   
            title: "Kirim",
            text: "Anda Yakin akan Kirim ke Proses Selanjutnya?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#e6b034",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm){   
          if (isConfirm) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: base_url + '/kirim_proses_pbr?id=' + id,
                beforeSend: function () {
                },
                success: function (msg) {
                    console.log(msg);
                    swal({
                        title: "Informasi",
                        text: "berhasil dikirim",
                        type: "success",
                        confirmButtonColor: "#8cd4f5",
                        confirmButtonText: "Ya",
                        closeOnConfirm: false
                    }, function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                        }
                    });

                }
            }).done(function (msg) {

            }).fail(function (msg) {
                swal("Terjadi Kesalahan! ");
            });

          } else {
            swal("Dibatalkan!");
          } 
        });
    }
</script>
@stop

