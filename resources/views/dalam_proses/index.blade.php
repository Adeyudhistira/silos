@section('content')
<div class="c-elements">
    <div class="bredcrumbs">
        <ul>
            <li><a href="#">dashboard</a></li>
            <li><a href="#">dalam proses</a></li>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div class="box-element">
        <div class="box-head"><span class="forms-16"></span><h3>Quick actions</h3><a href="" class="collapsable"></a></div>
        <div class="box-content">
            <ul class="actions">
                <li><div><a href="{{route('daftar_prospek_baru')}}" class="edit-32">New Entry</a></div></li>
                <li><div><a href="{{route('entry_nasabah.daftar_prospek_nasabah')}}" class="edit-32">Prospek Baru</a></div></li>
                <li><div><a href="{{route('entry_nasabah.daftar_prospek_lengkap')}}" class="users-32">Nasabah Baru</a></div></li>
                 <li><div><a href="{{route('nasabah_on_proses')}}" class="data-32">Appraisal</a></div></li>
                <li><div><a href="{{route('nasabah_cair')}}" class="charts-32">Analisa</a></div></li>
                <li><div><a href="{{route('nasabah_approval')}}" class="settings-32">Approval</a></div></li>
                <li><div><a href="{{route('nasabah_approval2')}}" class="settings-32">Approval Approver2</a></div></li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>


<div class="clearfix"></div>

</div>

@endsection

