@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Daftar Nasabah Baru</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
            <form method="post" action="" class="i-validate"> 
                <fieldset>
                   <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Nasabah</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Alamat</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Produk</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Cabang</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Plafon</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Angsuran</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>                              
                        <div class="section-center">
                            <input type="submit" name="submit" id="" class="i-button no-margin" value="Ubah Data" />
                            <input type="button" name="submit" id="show-slik" class="i-button no-margin" value="SLIK" />
                            <input type="button" name="submit" id="show-appraisal" class="i-button no-margin" value="KIRIM REQ APPRAISAL" />                         
                        <div class="clearfix"></div>
                        </div>
                        
                    </section>

                </fieldset>
            </form>
        </div>
    </div>
</div>
<!-- modal slik -->
<div id="modal-slik" class="modal-container">
    <div class="modal-head"><h3>Data Permintaan</h3></div>
    <div class="modal-body">
        <div class="i-label">
            <label for="text_field">No KTP</label>
        </div>                                  
        <div class="section-right">
            <div class="section-input"><input type="text" name="text_field" id="text_field" class="i-text required"></input></div>
        </div>
        <div class="i-divider"></div>
        <div class="clearfix"></div>

        <div class="i-label">
            <label for="text_field">Nama Nasabah</label>
        </div>                                  
        <div class="section-right">
            <div class="section-input"><input type="text" name="text_field" id="text_field" class="i-text required"></input></div>
        </div>
        <div class="i-divider"></div>
        <div class="clearfix"></div>

        <div class="i-label">
            <label for="text_field">Alamat</label>
        </div>                                  
        <div class="section-right">
            <div class="section-input"><input type="text" name="text_field" id="text_field" class="i-text required"></input></div>
        </div>
        <div class="i-divider"></div>
        <div class="clearfix"></div>

        <div class="i-label">
            <label for="text_field">Tempat Lahir</label>
        </div>                                  
        <div class="section-right">
            <div class="section-input"><input type="text" name="text_field" id="text_field" class="i-text required"></input></div>
        </div>
        <div class="i-divider"></div>
        <div class="clearfix"></div>

        <div class="i-label">
            <label for="text_field">Tanggal Lahir</label>
        </div>                                  
        <div class="section-right">
            <div class="section-input"><input type="text" name="text_field" id="text_field" class="i-text required"></input></div>
        </div>
        <div class="i-divider"></div>
        <div class="clearfix"></div>
    </div>
    <div class="modal-footer">
        <input type="submit" name="submit" id="show-hasil-slik" class="i-button no-margin" value="KIRIM PERMINTAAN SLIK CHECKING" />
        <div class="clearfix"></div>
    </div>
</div>

<!-- modal hasil checking -->
<div id="modal-hasil-slik" class="modal-container">
    <div class="modal-head"><h3>Hasil Slik Checking</h3></div>
    <div class="modal-body">
        <div class="i-label">
            <label for="text_field">Status Slik</label>
        </div>                                  
        <div class="section-right">
            <div class="section-input"><input type="text" name="text_field" id="text_field" class="i-text required"></input></div>
        </div>
        <div class="i-divider"></div>
        <div class="clearfix"></div>

        <div class="i-label">
            <label for="text_field">Catatan</label>
        </div>                                  
        <div class="section-right">
            <div class="section-input"><input type="text" name="text_field" id="text_field" class="i-text required"></input></div>
        </div>
        <div class="i-divider"></div>
        <div class="clearfix"></div>

        <div class="i-label">
            <label for="text_field">Total Angsuran</label>
        </div>                                  
        <div class="section-right">
            <div class="section-input"><input type="text" name="text_field" id="text_field" class="i-text required"></input></div>
        </div>
        <div class="i-divider"></div>
        <div class="clearfix"></div>
    </div>
    <div class="modal-footer">
        <input type="submit" name="submit" id="" class="i-button no-margin" value="Tutup" />
        <div class="clearfix"></div>
    </div>
</div>



<!-- modal appraisal -->
<div id="modal-appraisal" class="modal-container">
    <div class="modal-head"><h3>Konfirmasi User Appraisal</h3></div>
    <div class="modal-body">
        <div class="i-label">
            <label for="text_field">Pilih User Appraisal</label>
        </div>                                  
        <div class="section-right">
            <div class="section-input"><input type="text" name="text_field" id="text_field" class="i-text required"></input></div>
        </div>
        <div class="i-divider"></div>
        <div class="clearfix"></div>
    </div>
    <div class="modal-footer">
        <input type="submit" name="submit" id="" class="i-button no-margin" value="Kirim" />
        <div class="clearfix"></div>
    </div>
</div>
@endsection

