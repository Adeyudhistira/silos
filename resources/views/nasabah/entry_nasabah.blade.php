@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Entry Nasabah</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
            <form method="post" action="" class="i-validate"> 
                <fieldset>
                    <section>
                        <div class="alert-msg error-msg">
                            Data Tidak Ditemukan..<a href="">×</a>
                            <p>Klik Lanjutkan untuk Isi Kelengkapan Data Nasabah!!</p>
                            <input type="submit" name="submit" id="" class="i-button no-margin" value="Lanjutkan.." />
                        </div>
                    </section>
                    <section>
                        <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
                            <thead>
                                <tr>
                                    <th>Nama Nasabah</th>
                                    <th>Alamat</th>
                                    <th>No KTP</th>
                                    <th>Nama Gadis Ibu Kandung</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </section>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Detail Nasabah</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
            <form method="post" action="" class="i-validate"> 
                <fieldset>
                    <section>
                        <h3><u>Data Pribadi</u></h3>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Lengkap</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Nomor Identitas</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Alamat</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Kelurahan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Kecamatan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Kota</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Provinsi</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">No Telp Rumah</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">No Hp</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Tempat Lahir</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Tanggal Lahir</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Gadis Ibu Kandung</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Jenis Kelamin</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Pendapatan perbulan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Pendapatan Lainnya</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Jenis Pekerjaan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Sumber Pendapatan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>

                    <section>
                        <h3><u>Struktur Pembiayaan</u></h3>
                        <div class="section-left-s">
                            <label for="text_tooltip">Harga Objek</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Uang Muka</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Jangka Waktu</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Margin</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Angsuran Perbulan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Max Plafon</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>

                     <section>
                        <h3><u>Dana Agunan</u></h3>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Developer</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Perumahan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Alamat Object</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Kota</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="section-left-s">
                            <label for="text_tooltip">Provinsi</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" name="text_tooltip" title="Additional form information" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <h3><u>Daftar Dokumen Nasabah</u></h3>
                        <input type="submit" name="submit" id="" class="i-button no-margin" value="Upload Dokumen Baru" />
                        <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
                            <thead>
                                <tr>
                                    <th>Jenis Dokumen</th>
                                    <th>Preview</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </section>
                    <section>
                        <input type="submit" name="submit" id="" class="i-button no-margin" value="Simpan" />
                    </section>
                </fieldset>
            </form>
        </div>
    </div>
</div>
@endsection

