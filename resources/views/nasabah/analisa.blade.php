@section('content')
<div class="c-forms">
<button class="i-button no-margin" onclick="kembali()">Kembali</button>       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Credit Analysis</h3><a href="" class="collapsable"></a></div>
           @if ($message = Session::get('success'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg success-msg ">{{ $message }}<a href="">×</a></div>
            </div>
            @endif


            @if ($message = Session::get('error'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg error-msg">{{ $message }}<a href="">×</a></div>
            </div>
            @endif
         
        <div class="box-content no-padding">
            <form method="post" action="{{route('update_analis_nasabah_on_proses')}}" class="i-validate"> 
            @csrf
            @if($rc==200)
            <fieldset>
                   <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Nasabah</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly name="nama" @if($data) value="{{$data['namaNasabah']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Plafon</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly name="plafon"  @if($data) value="{{$data['plafon']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Jangka Waktu</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly name="jangka_waktu" @if($data) value="{{$data['jangkaWaktu']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">RITI</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly name="riti" @if($data) value="{{$data['riti']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">BI Checking</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly name="bi_check" @if($data) value="{{$data['biCheck']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Collateral Coverage</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly name="coll_cover" @if($data) value="{{$data['collCover']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Kesimpulan Analisa</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly name="kesimpulan"  class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Catatan Lainnya</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly name="catatan" class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Score</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly name="score" @if($data) value="{{$data['score']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>                              
                        <div class="section-center">
                            <input type="button" name="submit" id="submit" onclick="detail()" class="i-button no-margin" value="Lihat Detail Scoring" />                         
                        <div class="clearfix"></div>
                        </div>
                        
                    </section>

                    <!--detail-->
                   <div class="detail-show">
                    <input type="hidden" name="idProduct" value="{{$data['idProduct']}}">
                    <input type="hidden" name="idpembiayaan" value="{{$idpembiayaan}}">
                    @if($data['scoringNonRatio'])
                        @foreach($data['scoringNonRatio'] as $det)
                       <section>
                            <input type="hidden" name="idGroup_{{$det['idSubGroup']}}" value="{{$det['idGroup']}}">
                            <input type="hidden" name="idSubGroup[]" value="{{$det['idSubGroup']}}">
                            <div class="section-left-s">
                                <label for="text_tooltip">{{$det['label']}}</label>
                            </div>                                  
                            <div class="section-right">                                         
                                <div class="section-input">
                                    @if($det['option'])
                                       <select class="i-text" name="nilai_{{$det['idSubGroup']}}">
                                        @foreach($det['option'] as $item)
                                            <option value="{{$item['value']}}" @if($item['value']==$det['answer']) selected @endif>{{$item['description']}}</option>
                                        @endforeach 
                                        </select>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </section>
                       
                        @endforeach
                    @endif
                    @if($data['scoringRatio'])
                        @foreach($data['scoringRatio'] as $det)
                       <section>
                            <input type="hidden" name="idGroup1_{{$det['idSubGroup']}}" value="{{$det['idGroup']}}">
                            <input type="hidden" name="idSubGroup1[]" value="{{$det['idSubGroup']}}">
                            <div class="section-left-s">
                               <!-- <label for="text_tooltip">{{$det['label']}}</label>-->
                            </div>                                  
                            <div class="section-right">                                         
                                <div class="section-input">
                                    @if($det['option'])
                                    <input type="hidden" name="nilai1_{{$det['idSubGroup']}}" value="$det['option']['value']">
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </section>
                       
                        @endforeach
                    @endif
                <section>                              
                    <div class="section-center">
                        <button type="submit" class="i-button no-margin">Simpan</button>                         
                        <div class="clearfix"></div>
                    </div>

                </section>
                </fieldset>    
            @else
            <p>{{$rm}}</p>
            @endif
            
        </div>
            </form>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$('.detail-show').hide();
    function detail() {
        var id=$('#submit').val();
        
        if(id=='Lihat Detail Scoring'){

            $('.detail-show').show();
            $('#submit').val('Tutup');

        }else{

            $('.detail-show').hide();
            $('#submit').val('Lihat Detail Scoring');

        }
    }
    function kembali() {
        window.location.href = base_url +'/nasabah_on_proses';
    }
</script>
@stop