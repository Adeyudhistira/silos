@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Detail Informasi Agunan</h3><a href="" class="collapsable"></a></div>
         @if ($message = Session::get('success'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg success-msg ">{{ $message }}<a href="">×</a></div>
            </div>
            @endif


            @if ($message = Session::get('error'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg error-msg">{{ $message }}<a href="">×</a></div>
            </div>
            @endif
        <div class="box-content no-padding">
           <form method="post" action="{{ route('update_property_nasabah_on_proses') }}" class="i-validate"> 
                  @csrf
                  <input type="hidden" name="id_pembiayaan" @if($data) value="{{$data['idPembiayaan']}}" @endif  class="i-text i-form-tooltip" readonly></input>
                  <input type="hidden" name="nama_developer" @if($data) value="{{$data['idDeveloper']}}" @endif  class="i-text i-form-tooltip" readonly></input>
                  <input type="hidden" name="nama_perumahan" @if($data) value="{{$data['idPerumahan']}}" @endif  class="i-text i-form-tooltip" readonly></input>
                  <input type="hidden" name="id" @if($data) value="{{$data['id']}}" @endif  class="i-text i-form-tooltip" readonly></input>
                <fieldset>
                   <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Jenis Agunan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">

                                <input type="text" name="id_agunana" @if($data) value="{{$data['agunantype']['definition']}}" @endif  class="i-text i-form-tooltip" readonly></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nomor Sertifikat</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" required name="no_sertifikasi" @if($data) value="{{$data['noSertifikat']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Pemilik Serfifikat</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" required name="nama_pemilik" @if($data) value="{{$data['namaSertifikat']}}" @endif  class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Luas Tanah Sertifikat (m2)</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" required name="luas_tanah" onkeypress="return hanyaAngka(event)" @if($data) value="{{$data['luasTanahSertifikat']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Luas Imb (m2)</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" required name="luas_bangunan" onkeypress="return hanyaAngka(event)" @if($data) value="{{$data['luasImb']}}" @endif class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Luas Tanah Fisik (m2)</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" value="{{$data['luasTanahFisik']}}" required name="luas_tanah_fisik" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Luas Bangunan Fisik (m2)</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" value="{{$data['luasFisik']}}" required name="luas_bangunan_fisik" class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nilai Appraisal</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" required name="nilai" onkeypress="return hanyaAngka(event)" onkeyup="convertToRupiah(this)" @if($data) value="{{number_format($data['nilaiAppraisal'],0,',','.')}}" @endif class="i-text"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Alamat Object</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" required name="alamat" @if($data) value="{{$data['alamat']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Cari Kelurahan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input value="" type="text" name="nama_kelurahan_agunan" id="nama_kelurahan_agunan" class="i-text"></input>
                            </div>
                                <input type="button" style="margin: 10px 0 !important;" onclick="getLokasi('nama_kelurahan_agunan','setlokasiagunan');" class="i-button no-margin" value="Cari" />
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Pilih Lokasi</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <select id="setlokasiagunan" name="lokasiagunan" class="js-example-basic-single i-text required">
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <input value="{{$data['wilayah']['wilayah']}}" type="hidden" name="kelurahan2" id="kelurahan2"></input>
                    
                    <section>
                        <input type="button" value="Tambah Dokumen" class="i-button no-margin" onclick="tambahdok({{$data['id']}})"> 
                        <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
                            <thead>
                                <tr>
                                    <th>Jenis Dokumen</th>
                                    <th>Nama Dokumen</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($data['document'])
                                    @foreach($data['document'] as $doc)
                                        <tr>
                                           <td>{{$doc['jenis']['description']}}</td>
                                           <td>{{$doc['urlDoc']}}</td> 
                                        </tr>
                                    @endforeach
                                @endif
                            </tbody>
                        </table> 
                        <div class="clearfix"></div>  
                    </section>


                    <section>                              
                        <div class="section-center">
                            <button type="submit" class="i-button no-margin">Simpan</button>                       
                        <div class="clearfix"></div>
                        </div>
                        
                    </section>

                </fieldset>
            </form>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
     function tambahdok(id) {
        window.location.href = base_url +'/tambah_dokumen_nasabah_on_proses?id='+id;
    }
    function edit(id) {
        window.location.href = base_url +'/edit_dokumen_nasabah_on_proses?id='+id;
    }
</script>
<script type="text/javascript">
    $(".loading-mail").show();
    var kelurahan = $('#kelurahan2').val();

    var _items = "";
    $.ajax({
        type: 'POST',
        url: base_url + '/select/apikelurahan1?id=' + kelurahan,
        success: function (res) {
            $(".loading-mail").hide();
                console.log(res['data']);
                
            
                  var data = $.parseJSON(res);
                
                
                $.each(data['data'], function (k,v) {
                    _items += "<option value='"+v.id+"'>"+v.wilayah+"</option>";
                });

                $("#setlokasiagunan").html(_items);
                
        }
    }).done(function( res ) {
         $(".loading-mail").hide();
            
    }).fail(function(res) {
        $(".loading-mail").hide();
        swal("Terjadi Kesalahan! ");
    });


    $(document).ready(function(){
        $("#kota").change(function(){
            $('#kec').LoadingOverlay("show"); 
            var _items = "";
            $.ajax({
                type: 'GET',
                url: 'select/kecamatan/' + this.value,
                success: function (res) {
                    var data = $.parseJSON(res);
                    _items = "<option value=''>Pilih Kecamatan</option>";
                    $.each(data, function (k,v) {
                        _items += "<option value='"+v.kode+"'>"+v.nama+"</option>";
                    });
                    $('#kec').LoadingOverlay("hide");
                    $("#kec").html(_items);
                }
            });


        });

        $("#kec").change(function () {
            console.log("asup");
            $('#kel').LoadingOverlay("show");
            var _items = "";
            $.ajax({
                type: 'GET',
                url: 'select/kelurahan/' + this.value,
                success: function (res) {
                    var data = $.parseJSON(res);
                    _items = "<option value=''>Pilih Kelurahan</option>";
                    $.each(data, function (k,v) {
                        _items += "<option value='"+v.kode+"'>"+v.nama+"</option>";
                    });
                    $('#kel').LoadingOverlay("hide");
                    $('#kel').html(_items);
                }
            });

        });



});

function tes(id){
      $(".loading-mail").show();
    var _items = "";
    $.ajax({
        type: 'GET',
        url: base_url + '/select/apikelurahan/' + id,
        success: function (res) {
            $(".loading-mail").hide();
                console.log(res['data']);
                
            
                  var data = $.parseJSON(res);
                _items = "<option value=''>Pilih Lokasi</option>";
                
                
                $.each(data['data'], function (k,v) {
                    _items += "<option value='"+v.id+"'>"+v.wilayah+"</option>";
                });

                $("#"+target).html(_items);
                
        }
    }).done(function( res ) {
         $(".loading-mail").hide();
            
    }).fail(function(res) {
        $(".loading-mail").hide();
        swal("Terjadi Kesalahan! ");
    });
}

function getLokasi(set,target) {

     $(".loading-mail").show();
    var kelurahan = $('#'+set).val();

    var _items = "";
    $.ajax({
        type: 'GET',
        url: base_url + '/select/apikelurahan/' + kelurahan,
        success: function (res) {
            $(".loading-mail").hide();
                console.log(res['data']);
                
            
                  var data = $.parseJSON(res);
                _items = "<option value=''>Pilih Lokasi</option>";
                
                
                $.each(data['data'], function (k,v) {
                    _items += "<option value='"+v.id+"'>"+v.wilayah+"</option>";
                });

                $("#"+target).html(_items);
                
        }
    }).done(function( res ) {
         $(".loading-mail").hide();
            
    }).fail(function(res) {
        $(".loading-mail").hide();
        swal("Terjadi Kesalahan! ");
    });
} 
</script>
@stop
