@section('content')
<div class="c-forms">  
<button class="i-button no-margin" onclick="kembali()">Kembali</button>       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>History Process Per Nasabah</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
           <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
            <thead>
                <tr>
                    <th>Waktu Process</th>
                    <th>Nama Process</th>
                    <th>Durasi</th>
                </tr>
            </thead>
            <tbody>
                @if($data)
                    @foreach($data as $item)
                        <tr>
                            <td align="center">{{date('d-M-Y H:s:i',strtotime($item['createdAt']))}}</td>
                            <td align="center">{{$item['processName']}}</td>
                            <td align="center">{{$item['diff']}}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
  function kembali() {
        window.location.href = base_url +'/nasabah_on_proses';
    }
</script>
@stop

