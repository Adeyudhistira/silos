@section('content')

@php
    if(isset($data)){
        $id = $data->id;

        $developer_name = $data->developer_name;
        $address = $data->address;
        $akta_no = $data->akta_no;
        $direktur_utama = $data->direktur_utama;
        $siup = $data->siup;
        $type = 'update';
    }else {
        $id = '';
        $developer_name = '';
        $address = '';
        $akta_no = '';
        $direktur_utama = '';
        $siup = '';
        $type = 'normal';
    }
@endphp
<div class="c-forms"> 
            <a class="i-button no-margin" href="{{route('developer.index')}}">Kembali</a>   

    <div class="box-element" style="
    margin-top: 20px;
">
        <form id="form_tambah_data">
            
        <input type="hidden" name="id" id="id" value="{{$id}}">
        <input type="hidden" name="type" id="type" value="{{$type}}">

              <div class="i-label">
                <label for="text_field">Nama Developer</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$developer_name}}" type="text" name="developer_name" id="developer_name" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>


            <div class="i-label">
                <label for="text_field">Alamat</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$address}}" type="text" name="address" id="address" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             <div class="i-label">
                <label for="text_field">Nomer Akta</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$akta_no}}" type="text" name="akta_no" id="akta_no" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             <div class="i-label">
                <label for="text_field">Direktur Utama</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$direktur_utama}}" type="text" name="direktur_utama" id="direktur_utama" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">SIUP</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    {{-- onkeyup="convertToNum(this)"  --}}
                    <input value="{{$siup}}" type="text" name="siup" id="siup" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>            

        <input type="button" style="margin: 20px !important;" onclick="TambahDataBaru();" class="i-button no-margin" value="Simpan" />

        </form>
    </div>
</div>
        
   

@include('developer.action')
@endsection
@section('script')
    <script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
@stop

