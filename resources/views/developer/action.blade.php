<script>



function TambahDataBaru() {
    console.log("tambahBaru");

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
$.ajax({
        type: "POST",
        url: base_url + '/developer/tambah',
        data: {
            type: $('#type').val(),
            developer_name: $('#developer_name').val(),
            address: $('#address').val(),
            akta_no: $('#akta_no').val(),
            direktur_utama: $('#direktur_utama').val(),
            siup: $('#siup').val(),
            id: $('#id').val()
            

        },
        beforeSend: function () {
        },
        success: function (msg) {

        }
    }).done(function (msg) {
        window.location.href = base_url + '/developer';

    }).fail(function (msg) {

    });
    
}



function HapusDataBaru(id) {
    swal({
        title: "Hapus",
        text: "Anda Yakin akan Hapus?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#e6b034",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: base_url + '/developer/delete/' + id,
                data: {
                    id: id
                },
                beforeSend: function () {
                },
                success: function (msg) {
                    console.log(msg);
                    swal({
                        title: "Informasi",
                        text: "berhasil Terhapus",
                        type: "info",
                        confirmButtonColor: "#8cd4f5",
                        confirmButtonText: "Ya",
                        closeOnConfirm: false
                    }, function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                        }
                    });

                }
            }).done(function (msg) {

            }).fail(function (msg) {
                swal("Terjadi Kesalahan! ");
            });

        } else {
            swal("Dibatalkan!");
        }
    });
}

</script>