@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Daftar Developer</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">

              <div class="right">
                <a href="{{route('developer.create')}}"><input type="button" class="icon16-button" value="Tambah Baru"></a>
                {{-- <a href="#"><input type="button" name="submit" id="show-dks" class="i-button no-margin" value="Tandai Batch" /></a> --}}
            </div>
            
        
           <table cellpadding="0" cellspacing="0" border="0" class="display" id="t_wilayah">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Nama Developer</th>
                    <th>Alamat</th>
                    <th>No Akta</th>
                    <th>Direktur Utama</th>
                    <th>SIUP</th>
                    <th style="width:200px;">Action</th>
                </tr>
            </thead>
            <tbody>
                @if($data)
                    @foreach($data as $item)
                        <tr style="height: 40px;">        
                            <td style="padding:15px;" align="center">{{$item->id}}</td>
                            <td style="padding:15px;" align="center">{{$item->developer_name}}</td>
                            <td style="padding:15px;" align="center">{{$item->address}}</td>
                            <td style="padding:15px;" align="center">{{$item->akta_no}}</td>
                            <td style="padding:15px;" align="center">{{$item->direktur_utama}}</td>
                            <td style="padding:15px;" align="center">{{$item->siup}}</td>

                            <td>
                                <a href="{{route('developer.edit',$item->id)}}"><input type="button" class="icon16-button" value="Edit"></a>
                                <a onclick="HapusDataBaru({{$item->id}})"><input type="button" class="icon16-button" value="Hapus"></a>

                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        </div>
    </div>
</div>



@include('developer.action')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
<script>
var t_wilayah = $('#t_wilayah').dataTable({
    "sPaginationType": "full_numbers"   
});
</script>
@stop


