@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Approved - Pra Uji KP</h3><a href="" class="collapsable"></a></div>
           @if ($message = Session::get('success'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg success-msg ">{{ $message }}<a href="">×</a></div>
            </div>
            @endif


            @if ($message = Session::get('error'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg error-msg">{{ $message }}<a href="">×</a></div>
            </div>
            @endif
         
        <div class="box-content no-padding">
            <div class="right">
                <input type="button" name="submit" onclick="show_batch();" class="i-button no-margin" value="Tandai Batch" />
            </div>
           <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
            <thead>
                <tr>
                    <th><input type="checkbox" name="select_all" value="1" id="example-select-all"></th>
                    <th>Nama Nasabah</th>
                    <th>Alamat</th>
                    <th>Produk</th>
                    <th>Cabang</th>
                    <th>Plafon</th>
                    <th>Jangka Waktu</th>
                    <th>Angsuran</th>
                    <th>Status</th>
                    <th>Status PPDPP</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                
                @if($data)
                    @foreach($data as $item)
                        <tr>
                            <td style="padding:15px;" align="center"><input type="checkbox" name="id[]" value="{{$item['nasabahId']}}"/></td>
                            @if($item['nasabah'])
                              <td>{{$item['nasabah']['namaNasabah']}}</td>
                              <td>{{$item['nasabah']['alamat']}}</td>  
                            @else
                              <td>-</td>
                              <td>-</td>  
                            @endif
                            @if($item['product'])
                              <td>{{$item['product']['prodName']}}</td>
                            @else
                              <td>-</td>  
                            @endif
                            @if($item['branch'])
                              <td>{{$item['branch']['branchName']}}</td>
                            @else
                              <td>-</td>  
                            @endif
                             <td>{{number_format($item['plafon'],0,',','.')}}</td>
                            <td>{{$item['jangkaWaktu']}} Bln</td>

                            <td>{{number_format($item['totalAngsuran'],0,',','.')}}</td>
                           
                            @if($item['wfstatus'])
                              <td>{{$item['wfstatus']['statusDefinition']}}</td>
                            @else
                              <td>-</td>  
                            @endif
                            <td>
                                @if($item['rcuji'])
                                {{$item['rcuji']['keterangan']}}
                                @endif
                            </td>
                            <td>
                                <input type="button" class="button" onclick="slik({{$item['id']}})" value="SLIK">
                                <input type="button" class="button" onclick="appraisal({{$item['id']}})" value="APPRAISAL">
                                <input type="button" class="button" onclick="analisa({{$item['id']}})" value="ANALISA">
                                @if($item['agunan'])
                                <input type="button" class="button" onclick="ppdpp({{$item['agunan']['id']}})" value="CHECKLIST PPDPP">
                                @endif
                                @if($item['rcuji'])
                                <input type="button" class="button" onclick="kirimproses({{$item['id']}})" value="KIRIM BALIK KE AO">
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
</div>
<div id="modal_dks" class="modal-container">
    <div class="modal-head"><h3>Data Permintaan</h3></div>
    <div class="modal-body">
        <form id="form_dks">
            <div class="i-label">
            <label for="text_field">No Surat DKS</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input minlength="10" type="text" name="no_surat" id="no_surat" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Tanggal Surat DKS</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="tgl" id="tgl" class="i-text i-datepicker required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>    
        </form>
    </div>
    <div class="modal-footer">
        <input type="button" onclick="processDKS($('#no_surat').val(),$('#tgl').val());" class="i-button no-margin" value="Simpan" />
        <div class="clearfix"></div>
    </div>
</div>

@endsection
@section('script')
<script type="text/javascript">
    $('#example-select-all').click(function (e) {
    $(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
});

    var modal_dks = $('#modal_dks');
    var form_dks = $('#form_dks');

    function show_batch(){
        let value = [];
        $('input[type="checkbox"]').each(function(){
            if(this.checked){
                value.push(this.value);
            }
        });

        if(value.length == 0){
            swal("Peringatan!", "Minimal pilih satu", "warning");
        }else{

            modal_dks.modal({onShow: function(dlg) {
                $(dlg.container).css('height','auto')
            }}); 
        }
    }
    function processDKS(nosurat,tgl) {
     //_create_form.parsley().validate();
   // var _form_data = new FormData(form_dks[0]);

    /*for (var pair of _form_data.entries()) {
        console.log(pair[0]+ ', ' + pair[1]);
    }*/
   // if(_create_form.parsley().isValid()){
      //  submitdata('dks/update',_form_data,t_dks,modal_dks,form_dks[0]);
    //}

    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal("Peringatan!", "Minimal pilih satu", "warning");
    }else{
       swal({   
        title: "Informasi",   
        text: "Anda yakin ingin mengirim record yang dipilih?",   
        type: "info",   
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",   
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false 
    }, function(isConfirm){   
        if (isConfirm) {     
            modal_dks.modal('hide');
            $.ajax({
                type: 'POST',
                url: base_url+'/dks_nasabah_pra_uji/update?no_surat='+ nosurat +'&&tgl='+ tgl,
                data: {value: value},
                async: false,
                success: res => {
                    console.log(res);
                }
            }).always(() => {
                value = [];

                    //t_dks.reload();
                    //t_dks.draw();
                });
            form_dks[0].reset();

            swal({   
                title: "Berhasil",   
                text: "Data Berhasil Dikirim",   
                type: "success",   
                confirmButtonColor: "#8cd4f5",   
                confirmButtonText: "Ya",     
                closeOnConfirm: false
            }, function(isConfirm){   
                if (isConfirm) {     
                    location.reload();
                } 
            });

        }else{
           swal("Batal!", "Batal", "info");
       } 
   });
       return false;

   }

}

   function slik(id) {
        window.location.href = base_url +'/slik_nasabah_pra_uji?id=' + id;
    }

    function appraisal(id) {
        window.location.href = base_url +'/appraisal_nasabah_pra_uji?id=' + id;
    }

    function analisa(id) {
        window.location.href = base_url +'/analisa_nasabah_pra_uji?id=' + id;
    }

    function sla(id) {
        window.location.href = base_url +'/sla_nasabah_pra_uji?id=' + id;
    }

    function ppdpp(id) {
        window.location.href = base_url +'/ppdpp_nasabah_pra_uji?id=' + id;
    }

    function proses_approval(id) {
        window.location.href = base_url +'/proses_approval?id=' + id;
    }

    function kirimproses(id) {
    swal({
        title: "Informasi",
        text: "Kirim Data Ke Ao?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#e6b034",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: base_url + '/approve_nasabah_pra_uji?id=' + id,
                beforeSend: function () {
                },
                success: function (msg) {
                    console.log(msg);
                    swal({
                        title: "Informasi",
                        text: msg['rm'],
                        type: "info",
                        confirmButtonColor: "#8cd4f5",
                        confirmButtonText: "Ya",
                        closeOnConfirm: false
                    }, function (isConfirm) {
                        if (isConfirm) {
                            window.location.href = base_url +'/nasabah_pra_uji';
                        }
                    });

                }
            }).done(function (msg) {

            }).fail(function (msg) {
                swal("Terjadi Kesalahan! ");
            });

        } else {
            swal("Dibatalkan!");
        }
    });
}
</script>
@stop

