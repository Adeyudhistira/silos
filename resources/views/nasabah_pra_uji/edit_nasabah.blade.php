@section('content')
<div class="c-forms"> 
            <a class="i-button no-margin" href="{{route('nasabah_uji')}}">Kembali</a>   

    <div class="box-element" style="
    margin-top: 20px;
">
        <form id="form_tambah_data">
            
            <input type="hidden" name="id_nas" id="id_nas" value="{{$data->id_nas}}">
            <input type="hidden" name="id_mp" id="id_mp" value="{{$data->id_mp}}">
            <input type="hidden" name="id_maap" id="id_maap" value="{{$data->id_maap}}">

            <div class="i-label">
                <label for="text_field">No Identitas</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                <input value="{{$data->no_identitas}}" onkeyup="convertToNum(this)" type="text" maxlength="16" minlength="16" name="no_identitas" id="no_identitas" class="i-text required" required></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Nama Nasabah</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input required value="{{$data->nama_nasabah}}" type="text" name="nama_nasabah" id="nama_nasabah" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Pekerjaan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    @php
                        $dt = \DB::select("SELECT * FROM ref_pekerjaan");
                    @endphp
                    <select required class="i-text required" name="id_pekerjaan" id="id_pekerjaan">
                        @foreach ($dt as $item)
                            <option {{($item->id_pekerjaan == $data->id_pekerjaan) ? "selected":"" }} value="{{$item->id_pekerjaan}}">{{$item->nm_pekerjaan}}</option>
                        @endforeach
                       
                    </select>
                </div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Jenis Kelamin</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <select required class="i-text required" name="jk" id="jk">
                        <option  {{(1 == $data->id_pekerjaan) ? "selected":"" }} value="1">Laki-laki</option>
                        <option  {{(0 == $data->id_pekerjaan) ? "selected":"" }} value="0">Perempuan</option>
                    </select>
                </div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">NPWP</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input required value="{{$data->npwp}}"  type="text" name="npwp" id="npwp" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Gaji Pokok</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input required onkeyup="convertToRupiah(this)" value="{{ number_format($data->pendapatan_bulan,0,',','.')}}"  type="text" name="gp" id="gp" class="i-text required"></input></div>
            </div>
             <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Telp</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input required value="{{$data->telp}}"  type="text" name="telp" id="telp" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Nama Pasangan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input required value="{{$data->nama_p}}" type="text" name="nama_p" id="nama_p" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">KTP Pasangan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input required value="{{$data->ktp_p}}" type="text" name="ktp_p" id="ktp_p" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>


            {{-- <div class="i-label">
                <label for="text_field">Nama Perumahan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"> --}}
                    <input value="{{$data->perumahan}}" type="hidden" name="perumahan" id="perumahan" class="i-text required"></input>
                {{-- </div> --}}
            {{-- </div> --}}
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             <div class="i-label">
                <label for="text_field">Harga Rumah</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input required onkeyup="convertToRupiah(this)" value="{{ number_format($data->harga_object,0,',','.')}}" type="text" name="harga_object" id="harga_object" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             <div class="i-label">
                <label for="text_field">Nilai KPR</label>
            </div>                                  
            <div class="section-right">
                <div  class="section-input"><input onkeyup="convertToRupiah(this)" value="{{ number_format($data->plafon,0,',','.')}}"  onchange="sAngsuran()" type="text" name="plafon" id="plafon" class="i-text required" required></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             <div class="i-label">
                <label for="text_field">Tenor</label>
            </div>                                  
            <div class="section-right">
                <div onkeyup="sAngsuran()" class="section-input"><input value="{{$data->jangka_waktu}}" type="text" name="jangka_waktu" id="jangka_waktu" class="i-text required" required></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             <div class="i-label">
                <label for="text_field">Angsuran</label>
            </div>                                  
            <div class="section-right">
                <div onkeyup="convertToNum(this)" class="section-input"><input onkeyup="convertToRupiah(this)" value="{{ number_format($data->total_angsuran,0,',','.')}}" type="text" name="total_angsuran" id="total_angsuran" class="i-text required" required></input></div>
            </div>

             <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Suku Bunga KPR</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input required value="{{$data->bunga}}" type="text" name="bunga" id="bunga" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            

             <div class="i-label">
                <label for="text_field">Nilai FLPP</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input onkeyup="convertToRupiah(this)" value="{{ number_format($data->plafon_flpp,0,',','.')}}" type="text" name="plafon_flpp" id="plafon_flpp" class="i-text required" required></input></div>
                 <div class="section-input" style="margin-left: 100px;" id="totalmax_flpp"></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>
            
            <div class="i-label">
                <label for="text_field">Developer</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                     @php
                        $dt = \DB::select("SELECT * FROM ref_developer");
                    @endphp
                    <select required class="i-text required" onchange="Gantii()" name="id_developer" id="id_developer">
                        <option value="">Pilih</option>
                        @foreach ($dt as $item)
                            <option value="{{$item->id}}" @if($item->id == $data->id_developer) selected @endif>{{$item->developer_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

         

              <div class="i-label">
                <label for="text_field">Residence</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                     @php
                        $dt = \DB::select("SELECT * FROM ref_residence");
                    @endphp
                    <select required class="i-text required" name="id_perumahan" id="id_perumahan">
                        @foreach ($dt as $item)
                            <option value="{{$item->id}}" @if($item->id == $data->id_perumahan) selected @endif>{{$item->residence_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             <div class="i-label">
                <label for="text_field">Alamat</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input required value="{{$data->alamat}}" type="text" name="alamat" id="alamat" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

<!--
            <div class="i-label">
                <label for="text_field">Kota / Kabupaten</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$data->kota_kab}}" type="text" name="kota_kab" id="kota_kab" class="i-text required"></input>
                </div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

-->


            <div class="i-label">
                <label for="text_field">Cari Kelurahan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                     <input value="" type="text" name="nama_kelurahan" id="nama_kelurahan" class="i-text required"></input>
                    <input type="button" style="margin: 10px 0 !important;" onclick="getLokasi('nama_kelurahan','setlokasi');" class="i-button no-margin" value="Cari" />   
                </div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Pilih Lokasi</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                     <select id="setlokasi" name="lokasipengguna" class="js-example-basic-single i-text required">
                    </select>
                </div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>


             <div class="i-label">
                <label for="text_field">Kode Pos</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input required value="{{$data->kodepos}}" type="text" name="kodepos" id="kodepos" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">Luas Tanah</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input value="{{$data->luas_tanah_fisik}}" type="text" name="luas_tanah_fisik" id="luas_tanah_fisik" class="i-text required" required></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">Luas Bangunan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input required value="{{$data->luas_imb}}" type="text" name="luas_imb" id="luas_imb" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">Kode Jenis KPR</label>
            </div>                                  
            <div class="section-right">
                   @php
                        $dt = \DB::select("SELECT * FROM ref_jns_kpr");
                    @endphp
                    <select class="i-text required" required name="kd_jns_kpr" id="kd_jns_kpr">
                        @foreach ($dt as $item)
                            <option {{($item->id == $data->kd_jns_kpr) ? "selected":"" }} value="{{$item->id}}">{{$item->definition}}</option>
                        @endforeach
                       
                    </select>

                {{-- <div class="section-input"><input value="{{$data->kd_jns_kpr}}" type="text" name="kd_jns_kpr" id="kd_jns_kpr" class="i-text required"></input></div> --}}
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             <div class="i-label">
                <label for="text_field">No SP3K</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input required value="{{$data->no_sp3k}}" type="text" name="no_sp3k" id="no_sp3k" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             <div class="i-label">
                <label for="text_field">Tanggal SP3K</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input required value="{{$data->tgl_sp3k}}" type="date" name="tgl_sp3k" id="tgl_sp3k" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <!--
            <div class="i-label">
                <label for="text_field">No ID Uji</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input readonly value="{{$data->no_id_uji}}" type="text" name="no_id_uji" id="no_id_uji" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>
            -->

        <input type="button" style="margin: 20px !important;" onclick="TambahData();" class="i-button no-margin" value="Simpan" />

        </form>
    </div>
</div>
        
   


@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>

<script>

   var mySelect = $('#id_perumahan');
   mySelect.find('option').remove().end();

   function Gantii() {
       console.log("waww");

       mySelect
       .find('option')
       .remove()
       .end();

       var dataaa = $('#id_developer').val();

       @php
       $d_rsca = \DB::select("SELECT * FROM ref_residence");
       @endphp

       @foreach($d_rsca as $item)

       if (dataaa == {{$item->id_developer}} ) {
        mySelect.append('<option value="{{$item->id}}">{{$item->residence_name}}</option>');

    }

    @endforeach
}

function getLokasi(set,target) {

     $(".loading-mail").show();
    var kelurahan = $('#'+set).val();

    var _items = "";
    $.ajax({
        type: 'GET',
        url: base_url + '/select/apikelurahan/' + kelurahan,
        success: function (res) {
            $(".loading-mail").hide();
                console.log(res['data']);
                
            
                  var data = $.parseJSON(res);
                _items = "<option value=''>Pilih Lokasi</option>";
                
                
                $.each(data['data'], function (k,v) {
                    _items += "<option value='"+v.id+"'>"+v.wilayah+"</option>";
                });

                $("#"+target).html(_items);
                
        }
    }).done(function( res ) {
         $(".loading-mail").hide();
            
    }).fail(function(res) {
        $(".loading-mail").hide();
        swal("Terjadi Kesalahan! ");
    });
}
</script>
@stop

