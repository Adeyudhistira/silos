@section('content')

@php
    if(isset($data)){
        $kode = $data->kode;
        $nama = $data->nama;
        $type = 'update';
    }else {
        $kode = '';
        $nama = '';
        $type = 'normal';
        
    }
@endphp
<div class="c-forms"> 
            <a class="i-button no-margin" href="{{route('wilayah.index')}}">Kembali</a>   

<div class="c-forms">     
<a class="i-button no-margin" href="{{route('permohonan_uji')}}">Kembali</a>    
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Forms</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
            <form id="form_tambah_data">
            
        <input type="hidden" name="type" id="type" value="{{$type}}">

            <div class="i-label">
                <label for="text_field">Kode</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    {{-- onkeyup="convertToNum(this)"  --}}
                    <input value="{{$kode}}" type="text" name="kode_wilayah" id="kode_wilayah" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Nama Wilayah</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$nama}}" type="text" name="nama_wilayah" id="nama_wilayah" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

        <input type="button" style="margin: 20px !important;" onclick="TambahWilayah();" class="i-button no-margin" value="Simpan" />

        </form>

        </div>
    </div>
</div>

   


@endsection
@section('script')
    <script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
@stop

