<style type="text/css">
	.page-break {
		page-break-after: always;
	}
	.tg tr > td,.tg  tr > th {border: 1px solid #000000;}
	.tg td{padding:10px 5px;word-break:normal;color:#333;}
	.tg th{font-weight:normal;padding:10px 5px;word-break:normal;color:#333;background-color:#f0f0f0;}
	.tg .tg-3wr7{font-weight:bold;font-size:12px;text-align:center}
	.tg .tg-ti5e{font-size:10px;text-align:center}
	.tg .tg-rv4w{font-size:10px;}
</style>
<style type="text/css">
  .page-break {
    page-break-after: always;
  }
  .tg  {border-collapse:collapse;border-spacing:0;border:solid 1px #000 1;width: 100%; }
  .tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
  .tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
  .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
  .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
</style>
<table class="tg" border="1">
  <thead>
    <tr>
      <th>ID Nasabah</th>
      <th>Nama</th>
      <th>Nilai KPR</th>
      <th>No Rek Operasi</th>
      <th>Outstanding PPDPP</th>
      <th>Outstanding Bank</th>
      <th>Mutasi Pokok PPDPP</th>
      <th>Mutasi Pokok Bank</th>
      <th>Mutasi Tarif PPDPP</th>
      <th>Mutasi Tarif Bank</th>
      <th>Denda Pokok PPDPP</th>
      <th>Denda Pokok Bank</th>
      <th>Denda Tarif PPDPP</th>
      <th>Denda Tarif Bank</th>
      <th>Tanggal Bayar Angsuran</th>
      <th>Status Lapor</th>
    </tr>
  </thead>
  <tbody>
    @if($module)
    @foreach($module as $item)
    <tr>
      <td>{{$item->idnasabah}}</td>
      <td>{{$item->nama_nasabah}}</td>
      <td>{{number_format($item->harga_object,0,',','.')}}</td>
      <td>{{$item->no_rek_operasi}}</td>
      <td>{{$item->nilai_outstanding}}</td>
      <td>{{$item->nilai_outstanding_bank}}</td>
      <td>{{$item->nilai_mutasi_pokok}}</td>
      <td>{{$item->nilai_mutasi_pokok_bank}}</td>
      <td>{{$item->nilai_mutasi_tarif}}</td>
      <td>{{$item->nilai_mutasi_tarif_bank}}</td>
      <td>{{$item->nilai_denda_pokok}}</td>
      <td>{{$item->nilai_denda_pokok_bank}}</td>
      <td>{{$item->nilai_denda_tarif}}</td>
      <td>{{$item->nilai_denda_tarif_bank}}</td>
      <td>{{$item->tgl_bayar}}</td>
      <td>{{$item->is_lapor}}</td>
    </tr>
    @endforeach
    @endif
  </tbody>
</table>