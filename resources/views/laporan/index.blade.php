@section('content')
<div class="c-elements">
    <div class="bredcrumbs">
        <ul>
            <li><a href="#">dashboard</a></li>
            <li><a href="#">laporan nasabah dinamis</a></li>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div class="box-element">
        <div class="box-head-light"><span class="search-16"></span><h3>Filter</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding grey-bg">
            <form class="form" id="form-filter" method="GET"> 
                <div class="clearfix"></div>
                <div class="i-label">
                <label for="text_field">Bulan</label>
                </div>                                  

                <div class="section-right">
                    <div class="section-input">
                        <select class="i-select" name="bulan">
                            <option value="01" @if($bulan=='01') selected @endif>Januari</option>
                            <option value="02" @if($bulan=='02') selected @endif>Februari</option>
                            <option value="03" @if($bulan=='03') selected @endif>Maret</option>
                            <option value="04" @if($bulan=='04') selected @endif>April</option>
                            <option value="05" @if($bulan=='05') selected @endif>Mei</option>
                            <option value="06" @if($bulan=='06') selected @endif>Juni</option>
                            <option value="07" @if($bulan=='07') selected @endif>Juli</option>
                            <option value="08" @if($bulan=='08') selected @endif>Agustus</option>
                            <option value="09" @if($bulan=='09') selected @endif>September</option>
                            <option value="10" @if($bulan=='10') selected @endif>Oktober</option>
                            <option value="11" @if($bulan=='11') selected @endif>November</option>
                            <option value="12" @if($bulan=='12') selected @endif>Desember</option>
                        </select>
                    </div>
                </div>
                <div class="i-divider"></div>
                <div class="clearfix"></div>

                <div class="clearfix"></div>
                <div class="i-label">
                <label for="text_field">Tahun</label>
                </div>                                  
                <div class="section-right">
                    <div class="section-input">
                        <input type="text" name="tahun" id="tahun" @if($tahun) value="{{$tahun}}" @endif maxlength="4" minlength="4" class="i-text required"></input>
                    </div>
                </div>
                <div class="i-divider"></div>
                <div class="clearfix"></div> 

                <div class="modal-footer">
                    <button type="submit" value="filter" id="filter" name='filter' class="icon16-button">Filter</button>
                    <div class="clearfix"></div>
                </div>   
            </form>    
            
        </div>
    </div>

    <div class="box-element">
        <div class="box-head-light"><span class="data-16"></span><h3>Action</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding grey-bg">
            
                <input type="button"  id="show-cbs" class="icon16-button" value="Upload Data dari CBS">
                <input type="button"  id="show-dks" onclick="UpdatePPDPP('{{$bulan}}','{{$tahun}}','{{$login[0]->batch_id}}');" class="icon16-button" value="Update Data PPDPP" />
                <input type="button"  id="show-dks" onclick="MutasiPPDPP('{{$bulan}}','{{$tahun}}','{{$login[0]->batch_id}}');" class="icon16-button" value="Kirim Data ke PPDPP" />
                <input type="button"  id="show-tambah" class="icon16-button" value="Tambah Data Manual" />
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="box-element">
        <div class="box-head-light"><span class="charts-16"></span><h3>Laporan Nasabah Dinamis</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding grey-bg">
           <form class="form" id="form-filter" method="GET">
                <button type="submit" value="excel" id="excel" name='excel' class="icon16-button">Export to Excel</button>  
            </form>
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="t_laporan">
                <thead>
                    <tr>
                        <th>ID Nasabah</th>
                        <th>Nama</th>
                        <th>Nilai KPR</th>
                        <th>No Rek Operasi</th>
                        <th>Outstanding PPDPP</th>
                        <th>Outstanding Bank</th>
                        <th>Mutasi Pokok PPDPP</th>
                        <th>Mutasi Pokok Bank</th>
                        <th>Mutasi Tarif PPDPP</th>
                        <th>Mutasi Tarif Bank</th>
                        <th>Denda Pokok PPDPP</th>
                        <th>Denda Pokok Bank</th>
                        <th>Denda Tarif PPDPP</th>
                        <th>Denda Tarif Bank</th>
                        <th>Tanggal Bayar Angsuran</th>
                        <th>Status Lapor</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                    @if($data)
                        @foreach($data as $item)
                        <tbody>
                            <tr>
                                <td>{{$item->idnasabah}}</td>
                                <td nowrap>{{$item->nama_nasabah}}</td>
                                <td>{{number_format($item->harga_object,0,',','.')}}</td>
                                <td>{{$item->no_rek_operasi}}</td>
                                <td>{{number_format($item->nilai_outstanding,0,',','.')}}</td>
                                <td>{{number_format($item->nilai_outstanding_bank,0,',','.')}}</td>
                                <td>{{number_format($item->nilai_mutasi_pokok,0,',','.')}}</td>

                                <td>{{number_format($item->nilai_mutasi_pokok_bank,0,',','.')}}</td>
                                <td>{{number_format($item->nilai_mutasi_tarif,0,',','.')}}</td>

                                <td>{{number_format($item->nilai_mutasi_tarif_bank,0,',','.')}}</td>
                                <td>{{number_format($item->nilai_denda_pokok,0,',','.')}}</td>
                                <td>{{number_format($item->nilai_denda_pokok_bank,0,',','.')}}</td>
                                <td>{{number_format($item->nilai_denda_tarif,0,',','.')}}</td>
                                <td>{{number_format($item->nilai_denda_tarif_bank,0,',','.')}}</td>
                                <td nowrap>{{ ($item->tgl_bayar ?  date('d M Y',strtotime($item->tgl_bayar)) : '-')}}</td>
                                <td>{{$item->is_lapor}}</td>
                                <td>
                                    <input type="button" name="submit" id="show-dks" onclick="showPercepat('{{$item->ktp}}','{{$item->bulan_identitas}}','{{$item->tahun_identitas}}','{{$item->no_rek_program}}','{{$item->no_rek_kelola}}','{{$item->no_rek_operasi}}','{{$item->no_rek_pemilik}}','{{$item->nilai_mutasi_pokok}}','{{$item->nilai_mutasi_tarif}}','{{$login[0]->batch_id}}');" class="icon16-button" value="Pelunasan Pokok Dipercepat" />
                                </td>
                            </tr>
                        @endforeach

                </tbody>
                        @php 
                        $total=0;
                        $total1=0;
                        $total2=0;
                        $total3=0;
                        $total4=0;
                        $total5=0;
                        $total6=0;
                        $total7=0;
                        $total8=0;
                        $total9=0;
                        $total10=0;
                        @endphp
                        @foreach($data as $item)
                        @php 
                        ($total +=$item->nilai_outstanding);
                        ($total1 += $item->nilai_outstanding_bank);
                        ($total2 += $item->nilai_mutasi_pokok);
                        ($total3 += $item->nilai_mutasi_pokok);

                        ($total4 += $item->nilai_mutasi_pokok_bank);
                        ($total5 += $item->nilai_mutasi_tarif);

                        ($total6 += $item->nilai_mutasi_tarif_bank);
                        ($total7 += $item->nilai_denda_pokok);

                        ($total8 += $item->nilai_denda_tarif);
                        ($total9 += $item->nilai_denda_tarif_bank);
                        @endphp
                        @endforeach 
                        <tfoot>
                            <th>Total</th>
                            <th></th>
                            <th></th>
                            <th></th>

                            <th>{{number_format($total,0,',','.')}}</th>
                            <th>{{number_format($total1,0,',','.')}}</th>
                            <th>{{number_format($total2,0,',','.')}}</th>    
                            <th>{{number_format($total3,0,',','.')}}</th>
                            <th>{{number_format($total4,0,',','.')}}</th>
                            <th>{{number_format($total5,0,',','.')}}</th>
                            <th>{{number_format($total6,0,',','.')}}</th>
                            <th>{{number_format($total7,0,',','.')}}</th>
                            <th>{{number_format($total8,0,',','.')}}</th>
                            <th>{{number_format($total9,0,',','.')}}</th>



                            <th></th>
                            <th></th>
                            <th></th>
                        </tfoot>
                    @endif


            </table>
            <div class="clearfix"></div>
        </div>
    </div>
<div class="clearfix"></div>
</div>

<!-- modal slik -->
<div id="modal_cbs" class="modal-container">
    <div class="modal-head"><h3>Upload Data dari CBS</h3></div>
    <div class="modal-body">
        <form id="form_cbs">
            <div class="i-label">
            <label for="text_field">Nama File</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="file" name="file" id="file" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div> 
        </form>
    </div>
    <div class="modal-footer">
        <input type="button" onclick="processUploadCBS();" class="i-button no-margin" value="Upload" />
        <div class="clearfix"></div>
    </div>
</div>


<!-- modal tambah -->
<div id="modal_tambah" class="modal-container">
    <div class="modal-head"><h3>Tambah Data</h3></div>
    <div class="modal-body">
        <form id="form_tambah">
            <div class="i-label">
            <label for="text_field">No KTP</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="no_ktp" id="no_ktp" maxlength="16" minlength="16" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">Bulan Laporan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="bulan" id="bulan" maxlength="2" minlength="2" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">Tahun Laporan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="tahun" id="tahun" maxlength="4" minlength="4" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">Bayar Pokok</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="bayar_pokok" id="bayar_pokok" onkeypress="return hanyaAngka(event)" onkeyup="convertToRupiah(this)" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">Bayar Tarif</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="bayar_tarif" id="bayar_tarif" onkeypress="return hanyaAngka(event)" onkeyup="convertToRupiah(this)" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">Bayar Denda Pokok</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="denda_pokok" id="denda_pokok" onkeypress="return hanyaAngka(event)" onkeyup="convertToRupiah(this)" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">Bayar Denda Tarif</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="denda_tarif" id="denda_tarif" onkeypress="return hanyaAngka(event)" onkeyup="convertToRupiah(this)"  class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">Saldo Pokok</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="saldo_pokok" id="saldo_pokok" onkeypress="return hanyaAngka(event)" onkeyup="convertToRupiah(this)"  class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">Tanggal Bayar</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="tgl_bayar" id="tgl_bayar" class="i-text i-datepicker required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

        </form>
    </div>
    <div class="modal-footer">
        <input type="button" onclick="processInsert();" class="i-button no-margin" value="Simpan" />
        <div class="clearfix"></div>
    </div>
</div>


<!-- modal percepat -->
<div id="modal_percepat" class="modal-container">
    <div class="modal-head"><h3>Pelunasan Pokok Dipercepat</h3></div>
    <div class="modal-body">
        <form id="form_percepat">
            <div class="i-label">
            <label for="text_field">No KTP</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="noktp" id="noktp" readonly maxlength="16" minlength="16" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">Bulan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="bulan" id="bulan_cepat" maxlength="2" minlength="2" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">Tahun</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="tahun" id="tahun_cepat" maxlength="4" minlength="4" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">No Rek Program</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="no_rek_program" id="no_rek_program" onkeypress="return hanyaAngka(event)" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">No Rek Kelola</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="no_rek_kelola" id="no_rek_kelola" onkeypress="return hanyaAngka(event)"  class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">No Rek Operasi</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="no_rek_operasi" id="no_rek_operasi" onkeypress="return hanyaAngka(event)"  class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">No Rek Pengirim</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="no_rek_pengirim" id="no_rek_pengirim" onkeypress="return hanyaAngka(event)"  class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

           <!--
            <div class="i-label">
            <label for="text_field">Nilai Mutasi Pokok Cepat</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="nilai_mutasi_pokok_cepat" id="nilai_mutasi_pokok_cepat" onkeypress="return hanyaAngka(event)" onkeyup="convertToRupiah(this)"  class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">Nilai Mutasi Tarif Cepat</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="text" name="nilai_mutasi_tarif_cepat" onkeypress="return hanyaAngka(event)" onkeyup="convertToRupiah(this)" id="nilai_mutasi_tarif_cepat" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

        -->

            <div class="i-label">
            <label for="text_field">Tanggal Mutasi Cepat</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="date" name="tgl_mutasi_cepat" id="tgl_mutasi_cepat" class="i-text required" required></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">Tanggal Laporan KTP</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="date" name="tgl_laporan" id="tgl_laporan" class="i-text" required></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">Tanggal Mutasi Pokok</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="date" name="tgl_mutasi_pokok" id="tgl_mutasi_pokok" class="i-text" required></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">Tanggal Mutasi Tarif</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="date" name="tgl_mutasi_tarif" id="tgl_mutasi_tarif" class="i-text" required></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

        </form>
    </div>
    <div class="modal-footer">
        <input type="button" onclick="processPercepat();" class="i-button no-margin" value="Simpan" />
        <div class="clearfix"></div>
    </div>
</div>

@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/content/laporan.js') }}"></script>
@stop

