@section('content')
<div class="c-forms">       
    <button class="i-button no-margin" onclick="kembali()">Kembali</button>
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Kirim Data Nasabah</h3><a href="" class="collapsable"></a></div>
        
        <div class="box-content no-padding">
           <form method="post" action="{{ route('kirim_data_permohonan_cair') }}" class="i-validate"> 
                @csrf
                <fieldset>
                   <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Pilih User</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="hidden" value="{{$id}}" name="id"></input>
                                <select name="iduser" id="iduser" class="i-text required">
                                    @if($combo)
                                        <option value="">Pilih User</option>
                                        @foreach($combo as $item)
                                        <option value="{{$item->id}}">{{$item->u_first_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>                              
                        <div class="section-center">
                            <button type="submit" class="i-button no-margin">Simpan</button>                     
                        <div class="clearfix"></div>
                        </div>
                        
                    </section>

                </fieldset>
            </form>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    function kembali() {
        window.location.href = base_url +'/permohonan_akad';
    }
</script>
@stop