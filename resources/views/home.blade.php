@section('content')
<div class="c-overview">
    <div class="bredcrumbs">
        <ul>
            <li><a href="#">dashboard</a></li>
            <li><a href="#">overview</a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
<!--
    <div class="box-element">
        <div class="box-head"><span class="forms-16"></span><h3>Quick actions</h3><a href="" class="collapsable"></a></div>
        <div class="box-content">
            <ul class="actions">
                <li><div><a href="#" class="edit-32">Prospek</a><span>Tambah Prospek Baru</span></div></li>
                <li><div><a href="#" class="users-32">Proses</a><span>Daftar Nasabah Proses</span></div></li>
                <li><div><a href="#" class="data-32">Data</a><span>Kelengkapan Data Nasabah</span></div></li>
                <li><div><a href="#" class="charts-32">Approval</a><span>Lakukan Approval</span></div></li>
                <li><div><a href="#" class="settings-32">Appraisal</a><span>Lakukan Appraisal</span></div></li>
                <li><div><a href="#" class="finance-32">Analysis</a><span>Lakukan Analisa</span></div></li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>
-->
@if($login[0]->batch_id)
    <input type="button"  id="show-dks" onclick="UpdateKuota('{{$login[0]->batch_id}}');" class="icon16-button" value="Update Kuota Nilai" />
    <input type="button"  id="show-dks" onclick="UpdateUnit('{{$login[0]->batch_id}}');" class="icon16-button" value="Update Kuota Unit" />
    <input type="button"  id="show-dks" onclick="UpdateData('{{$login[0]->batch_id}}');" class="icon16-button" value="Sinkronisasi Data" />
@endif
    <div class="right">
        
        @if($login[0]->batch_id)
        <input type="button"  id="show-dks" onclick="logout('{{$login[0]->batch_id}}');" class="icon16-button" value="Logout PPDPP" />
        <input type="button"  onclick="show_change('{{$login[0]->batch_id}}');" class="icon16-button" value="change password PPDPP" />
        @else
        <input type="button" onclick="show_login();" class="icon16-button" value="Login PPDPP" />
        @endif     
    </div>
    
    <div class="box-element">
        <div class="box-head"><span class="info-16"></span><h3>Statistik Pengajuan</h3><a href="" class="collapsable"></a></div>
        <div class="box-content">
            <ul class="notifications">
                <li><div>{{number_format($kuota[0]->kuota_nilai/1000000,0,',','.')}}<span>Kuota Nilai</span><span class="green">(dalam jutaan)</span></div></li>
                <li><div>{{number_format($kuota[0]->kuota_unit,0,',','.')}}<span>Kuota Unit</span><span class="red">(dalam satuan penuh)</span></div></li>
                <li><div>{{number_format($bulan_ini[0]->nilai,0,',','.')}}<span>Pencairan Bln ini</span><span class="green">(dalam jutaan)</span></div></li>
                <li><div>{{number_format($tahun_ini[0]->nilai,0,',','.')}}<span>Pencairan Tahun ini</span><span class="grey">(dalam jutaan)</span></div></li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="box-element">
        <div class="box-head">
            <span class="info-16"></span><h3>Saldo Rekening</h3><a href="" class="collapsable"></a>

            <div class="right">
                <input type="button" onclick="UpdateSaldo('{{$cek}}');" class="icon16-button" value="Update Saldo" />
            </div>
        </div>
        <div class="box-content">
            <ul class="notifications">
                <li style="width:33%;"><div>@if($kelola) {{number_format($kelola[0]->saldo,0,',','.')}} @else 0 @endif<span>Saldo Rekening Kelola</span><span class="green">(dalam rupiah penuh)</span></div></li>
                <li style="width:33%;"><div>@if($program) {{number_format($program[0]->saldo,0,',','.')}} @else 0 @endif<span>Saldo Rekening Program</span><span class="red">(dalam rupiah penuh)</span></div></li>
                <li style="width:33%;"><div>@if($operasi) {{number_format($operasi[0]->saldo,0,',','.')}} @else 0 @endif<span>Saldo Rekening Operasi</span><span class="green">(dalam rupiah penuh)</span></div></li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<!-- modal slik -->
<div id="modal_login" class="modal-container">
    <div class="modal-head"><h3>Silahkan Login</h3></div>
    <div class="modal-body">
        <form id="form_login">
            <div class="i-label">
            <label for="text_field">Password</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="password" name="password" id="password" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div> 
        </form>
    </div>
    <div class="modal-footer">
        <input type="button" onclick="processLogin();" class="i-button no-margin" value="Login" />
        <div class="clearfix"></div>
    </div>
</div>


<!-- modal slik -->
<div id="modal_change" class="modal-container">
    <div class="modal-head"><h3>Change Password</h3></div>
    <div class="modal-body">
        <form id="form_change">
            @csrf
            <input type="hidden" name="batch_id" id="batch_id" class="i-text required"></input>
            <div class="i-label">
            <label for="text_field">Password Lama</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="password" name="password_baru" id="password_baru" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
            <label for="text_field">Password Baru</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input"><input type="password" name="password_lama" id="password_lama" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div> 
        </form>
    </div>
    <div class="modal-footer">
        <input type="button" onclick="processChange();" class="i-button no-margin" value="Change Password" />
        <div class="clearfix"></div>
    </div>
</div>

@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/content/dashboard.js') }}"></script>
@stop

