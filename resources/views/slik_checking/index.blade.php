@section('content')
<div class="c-elements">
    <div class="bredcrumbs">
        <ul>
            <li><a href="#">dashboard</a></li>
            <li><a href="#">slik checking</a></li>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div class="box-element">
        <div class="box-head"><span class="forms-16"></span><h3>Quick actions</h3><a href="" class="collapsable"></a></div>
        <div class="box-content">
            <ul class="actions">
                <li><div><a href="{{route('permintaan')}}" class="edit-32">Permintaan</a></div></li>
                <li><div><a href="{{route('selesai')}}" class="edit-32">Selesai</a></div></li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>


<div class="clearfix"></div>

</div>

@endsection

