@section('content')
<div class="c-forms">       
    <button class="i-button no-margin" onclick="kembali()">Kembali</button>
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>SLIK</h3><a href="" class="collapsable"></a></div>
        
        <div class="box-content no-padding">
           <form method="post" action="{{ route('update_checking') }}" enctype="multipart/form-data" class="i-validate"> 
            @csrf
             <input type="hidden" name="id" value="{{$data[0]['id']}}" class="i-text i-form-tooltip"></input>
                <fieldset>
                   <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">No KTP</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="no_ktp" readonly @if($data) value="{{$data[0]['noIdentitas']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Nasabah</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="nama" readonly @if($data) value="{{$data[0]['namaNasabah']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Alamat</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="alamat" readonly @if($data) value="{{$data[0]['alamat']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Tempat Lahir</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="tmp_lahir" readonly @if($data) value="{{$data[0]['tempatLahir']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Tanggal Lahir</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="tgl_lahir" readonly @if($data) value="{{date('d-M-Y',strtotime($data[0]['tempatLahir']))}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>                              
                        <div class="section-center">
                            <input type="button" name="submit" id="submit" onclick="detail()" class="i-button no-margin" value="Hasil Slik Checking" />                         
                        <div class="clearfix"></div>
                        </div>
                        
                    </section>

                    <div class="show-slik">
                        <section>
                            <div class="section-left-s">
                                <label for="text_tooltip">Status SLIK</label>
                            </div>                                  
                            <div class="section-right">                                         
                                <div class="section-input">
                                    <select name="status" id="status" class="i-text required">
                                        <option value="">Pilih</option>
                                        @foreach($data1 as $item)
                                        <option @if($data[0]['biCheckStatusId']==$item['id']) selected @endif value="{{$item['id']}}">{{$item['definition']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </section>
                        <section>
                            <div class="section-left-s">
                                <label for="text_tooltip">Catatan</label>
                            </div>                                  
                            <div class="section-right">                                         
                                <div class="section-input">
                                    <input type="text" name="catatan" value="{{$data[0]['notes']}}"  class="i-text i-form-tooltip"></input>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </section>
                        <section>
                            <div class="section-left-s">
                                <label for="text_tooltip">Total Angsuran</label>
                            </div>                                  
                            <div class="section-right">                                         
                                <div class="section-input">
                                    <input type="text" onkeyup="convertToRupiah(this)" name="angsuran" value="{{number_format($data[0]['angsuran'],0,',','.')}}"  class="i-text i-form-tooltip"></input>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </section>
                        <section>
                            <div class="section-left-s">
                                <label for="text_tooltip">Upload Dokumen</label>
                            </div>
                            <div class="section-right">
                                <input type="file" name="file" id="file"></input> *max 100Mb    
                            </div>
                            <div class="clearfix"></div>
                        </section>                              
                        <section>
                        <div class="section-center">
                            <button type="submit" class="i-button no-margin">Simpan</button>                       
                        <div class="clearfix"></div>
                        </div>
                        
                    </section>
                    </div>

                </fieldset>
            </form>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$('.show-slik').hide();
    function detail() {
        var id=$('#submit').val();
        
        if(id=='Hasil Slik Checking'){

            $('.show-slik').show();
            $('#submit').val('Tutup');

        }else{

            $('.show-slik').hide();
            $('#submit').val('Hasil Slik Checking');

        }
    }
    function kembali() {
        
        window.location.href = base_url +'/permintaan';
    }
</script>
@stop