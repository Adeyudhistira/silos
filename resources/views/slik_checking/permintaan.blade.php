@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Daftar Permintaan Checking</h3><a href="" class="collapsable"></a></div>
           @if ($message = Session::get('success'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg success-msg ">{{ $message }}<a href="">×</a></div>
            </div>
            @endif


            @if ($message = Session::get('error'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg error-msg">{{ $message }}<a href="">×</a></div>
            </div>
            @endif
         
        <div class="box-content no-padding">
           <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
            <thead>
                <tr>
                    <th>No KTP</th>
                    <th>Nama Nasabah</th>
                    <th>Tempat Lahir</th>
                    <th>Tgl Lahir</th>
                    <th>Alamat</th>
                    <th>Angsuran</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                
                @if($data)
                    @foreach($data as $item)
                        <tr>
                            <td>{{$item['noIdentitas']}}</td>
                            <td>{{$item['namaNasabah']}}</td>
                            <td>{{$item['tempatLahir']}}</td>
                            <td>{{$item['tanggalLahir']}}</td>
                            <td>{{$item['alamat']}}</td>
                            <td>{{$item['angsuran']}}</td>
                            <td>{{$item['status']['definition']}}</td>
                            <td>
                                <input type="button" class="button" onclick="slik({{$item['pembiayaanId']}})" value="Update">
                               
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
 function slik(id) {
        window.location.href = base_url +'/slik_checking/' + id;
    }
</script>
@stop

