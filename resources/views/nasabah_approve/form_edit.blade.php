@section('content')

 
@php

 
@endphp

  @php
    $doc1 = "Tidak Ada Data";
    $doc1_id = "";
    $doc2 = "Tidak Ada Data";
    $doc2_id = "";
    $doc3 = "Tidak Ada Data";
    $doc3_id = "";
    $doc4 = "Tidak Ada Data";
    $doc4_id = "";


    if ($data4['documents']) {
        foreach ($data4['documents'] as $key => $value) {

            if ($value['tipeDok'] == 1 ) {
                $doc1 = "<a href='".env('API_BASE_URL').$value['urlDoc']."' class='image-link'><img style='border: 3px solid #d0d0d0;padding: 5px;margin: 5px;' src='".env('API_BASE_URL').$value['urlDoc']."' alt='' width='200'></a>";
                $doc1_id = $value['id'];
            }

            if ($value['tipeDok'] == 2 ) {
                $doc2 = "<img style='border: 3px solid #d0d0d0;padding: 5px;margin: 5px;' src='".env('API_BASE_URL').$value['urlDoc']."' alt='' width='200'>";
                $doc2_id = $value['id'];
            } 

            if ($value['tipeDok'] == 3 ) {
                $doc3 = "<img style='border: 3px solid #d0d0d0;padding: 5px;margin: 5px;' src='".env('API_BASE_URL').$value['urlDoc']."' alt='' width='200'>";
                $doc3_id =  $value['id'];
            } 

             if ($value['tipeDok'] == 9 ) {
                $doc4 = "<img style='border: 3px solid #d0d0d0;padding: 5px;margin: 5px;' src='".env('API_BASE_URL').$value['urlDoc']."' alt='' width='200'>";
                $doc4_id =  $value['id'];
            } 
        
        }
    } else {
        //echo "Tidak Ada Data";
    }
    
@endphp
            
<div class="c-forms form-lebar"> 
            <a class="i-button no-margin" href="{{route('entry_nasabah.daftar_prospek_lengkap')}}">Kembali</a>   

    <div class="box-element" style="margin-top: 20px;">
        <form id="form_entry_nasabah_edit" method="post" enctype="multipart/form-data">
            <input type="hidden" name="idpembiayaan" value="{{$idpembiayaan}}">
            <input type="hidden" name="idnasabah" value="{{$idnasabah}}">
            <input type="hidden" name="idprospek" value="{{$idprospek}}">
            <div class="i-label">
                <label for="text_field">Data Pribadi</label>
            </div>   
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Nama Lengkap</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                <input value="{{$data['namaNasabah']}}" type="text" name="nama_nasabah" id="nama_nasabah" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Nomor Identitas</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$data['noIdentitas']}}" type="text" name="no_identitas" id="no_identitas" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Alamat</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$data['alamat']}}" type="text" name="alamat" id="alamat" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
             
            <div class="i-label">
                <label for="text_field">Cari Kelurahan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="" type="text" name="nama_kelurahan" id="nama_kelurahan" class="i-text "></input></div>
                    <input type="button" style="margin: 10px 0 !important;" onclick="getLokasi('nama_kelurahan','setlokasi');" class="i-button no-margin" value="Cari" />

            </div>

             <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">Pilih Lokasi
            </label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <select id="setlokasi" name="lokasipengguna" class="js-example-basic-single i-text required">
                    </select>
                    
            </div>
            <div class="clearfix"></div>
        
            <div class="i-label">
                <label for="text_field">No Telp Rumah</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$data['telp']}}" type="text" name="telp" id="telp" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">No HP</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$data['noHp']}}" type="text" name="no_hp" id="no_hp" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div><div class="i-label">
                <label for="text_field">Tempat Lahir</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$data['tptLahir']}}" type="text" name="tpt_lahir" id="tpt_lahir" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Tanggal Lahir</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input  type="date" value="{{$data['tglLahir']}}" name="tgl_lahir" id="tgl_lahir" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Nama Gadis Ibu Kandung</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$data['sidNgik']}}" type="text" name="ibukandung" id="ibukandung" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Jenis Kelamin</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                     <select class="i-text required" name="jns_kelamin" id="jns_kelamin">
                    <option value="">Pilih</option>
                    <option {{ ($data['jnsKelamin'] == 0) ? "selected":"" }} value="0">Laki-Laki</option>
                    <option {{ ($data['jnsKelamin'] == 1) ? "selected":"" }} value="1">Perempuan</option>
                </select>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">NPWP</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{ $data['npwp']}}" type="text" name="npwp" id="npwp" class="i-text required" maxlength="15"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Status Menikah</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                     <select class="i-text required" name="stat_menikah" id="stat_menikah">
                    <option {{ ($data['isMenikah'] == true) ? "selected":"" }} value="t">Menikah</option>
                    <option {{ ($data['isMenikah'] == false) ? "selected":"" }} value="f">Belum Menikah</option>
                </select>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field" id="nmPasangan1" @if($data['isMenikah']==false) style="display:none" @endif>Nama Pasangan</label>
                <input type="hidden" value="{{ $data['namaP']}}" nama="nmPasanganFake" id="nmPasanganFake">
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input @if($data['isMenikah']==false) style="display:none" @endif value="{{ $data['namaP']}}" type="text" name="nmPasangan" id="nmPasangan" class="i-text"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field" id="ktpPasangan1" @if($data['isMenikah']==false) style="display:none" @endif>KTP Pasangan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input @if($data['isMenikah']==false) style="display:none" @endif value="{{ $data['ktpP']}}" type="text" name="ktpPasangan" id="ktpPasangan" class="i-text" maxlength="16" minlength="16"></input></div>
                    <input type="hidden" value="{{ $data['ktpP']}}" nama="ktpPasanganFake" id="ktpPasanganFake">
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Catatan AO</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{ $data1[0]['aoNote']}}" type="text" name="aoNote" id="aoNote" class="i-text"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Pendapatan per Bulan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input onkeyup="convertToRupiah(this)" value="{{ number_format($data['pendapatanBulan'],0,',','.')}}" type="text" name="pendapatan_bulan" id="pendapatan_bulan" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Take Home Pay</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input onkeyup="convertToRupiah(this)" value="{{ number_format($data['pendapatanLainnya'],0,',','.')}}" type="text" name="pendapatan_lainnya" id="pendapatan_lainnya" class="i-text required"></input>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Jenis Pekerjaan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                      @php
                    $dt = \DB::select("SELECT * FROM ref_pekerjaan");
                @endphp
                <select class="i-text required" name="id_pekerjaan" id="id_pekerjaan">
                    <option value="">Pilih</option>
                    @foreach ($dt as $item)
                        <option {{($data['idPekerjaan'] == $item->id_pekerjaan) ? "selected":"" }} value="{{$item->id_pekerjaan}}">{{$item->nm_pekerjaan}}</option>
                    @endforeach
                </select>
            </div>
             <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Sumber Pendapatan</label>

            </div>                                  
            <div class="section-right">
                <div class="section-input">
                @php
                    $dt = \DB::select("SELECT * FROM ref_sumber_pendapatan");
                @endphp
                <select class="i-text required" name="sumber_pendapatan" id="sumber_pendapatan">
                    <option value="">Pilih</option>
                    @foreach ($dt as $item)
                        <option @if($data['sumberPendapatan']==$item->id_sumber) selected @endif value="{{$item->id_sumber}}">{{$item->keterangan}}</option>
                    @endforeach
                </select>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Pilih Produk</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                @php
                    $dt = \DB::select("SELECT * FROM ref_product");
                @endphp
                <select class="i-text required" name="id_product" id="id_product">
                    <option value="">Pilih</option>
                    @foreach ($dt as $item)
                        <option @if($data1[0]['prodId']==$item->id) selected @endif value="{{$item->id}}">{{$item->prod_name}}</option>
                    @endforeach
                </select>
            </div>
            
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Struktur Pembiayaan</label>
            </div>   
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Harga Objek</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input onkeyup="convertToRupiah(this)" value="{{number_format($data1[0]['hargaObject'],0,',','.')}}" type="text" name="harga_object" id="harga_object" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Uang Muka</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input onkeyup="convertToRupiah(this)" value="{{number_format($data1[0]['uangMuka'],0,',','.')}}" type="text" name="uang_muka" id="Uang_muka" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Plafon</label>
            </div>                               
            <div class="section-right">
               
                <div class="section-input">
                    <input readonly onkeyup="convertToRupiah(this)"  value="{{number_format($data4['plafon'],0,',','.')}}" type="text" name="Plafon" id="Plafon" class="i-text required"></input></div>
                    
                </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Nilai FLPP</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input readonly  onkeyup="convertToRupiah(this)" value="{{number_format($data4['plafonFlpp'],0,',','.')}}" type="text" name="plafon_flpp" id="Plafon_flpp" class="i-text required"></input></div>
                    
                </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Margin</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input readonly  type="text" name="margin" value="{{number_format($data4['margin'],0,',','.')}}" id="Margin" class="i-text required"></input></div>
            </div>
             <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Jangka Waktu</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$data1[0]['jangkaWaktu']}}" type="text" name="jangka_waktu" id="jangka_waktu" class="i-text required"></input>*Bln</div>
            </div>
            <div class="clearfix"></div>
              <div class="i-label">
                <label for="text_field">Angsuran per Bulan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input readonly onkeyup="convertToRupiah(this)" value="{{number_format($data1[0]['totalAngsuran'],0,',','.')}}" type="text" name="total_angsuran" id="total_angsuran_new" class="i-text required"></input></div>


                 
                    <input type="button" style="margin: 20px !important;" onclick="getMaxPlafon();" class="i-button no-margin" value="Max Plafon" />
                    <input type="button" style="margin: 20px !important;" onclick="hitungAngsuran();" class="i-button no-margin" value="Hitung Angsuran" />
            </div>
           <div class="clearfix"></div>
            <div class="section-right">
                <div class="section-input">
                    <input readonly style="background: #dedede;" value="1" type="hidden" name="wf_type_id" id="wf_type_id" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="section-right">
                <div class="section-input">
                    <input style="background: #dedede;" readonly value="0" type="hidden" name="status_pembiayaan" id="status_pembiayaan" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="section-right">
                <div class="section-input">
                    <input readonly style="background: #dedede;" value="{{Auth::user()->id}}" type="hidden" name="user_id" id="user_id" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>

            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Data Agunan</label>
            </div>   
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Nama Developer</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                @php
                $a=\DB::select("SELECT * FROM ref_developer");
                @endphp
                <select class="i-text required" name="id_developer" id="id_developer">
                    <option value="">Pilih</option>
                    @foreach($a as $item)
                    <option @if($data3[0]['idDeveloper']==$item->id) selected @endif value="{{$item->id}}">{{$item->developer_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Nama Perumahan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                @php
                 $b=\DB::select("SELECT * FROM ref_residence");
                @endphp
                <select class="i-text required" name="id_perumahan" id="id_perumahan">
                    <option value="">Pilih</option>
                    @foreach($b as $item)
                    <option @if($data3[0]['idPerumahan']==$item->id) selected @endif value="{{$item->id}}">{{$item->residence_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Alamat Object</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input  type="text" value="{{$data3[0]['alamat']}}" name="alamat1" id="alamat1" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>

            <div class="i-label">
                <label for="text_field">Cari Kelurahan</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="" type="text" name="nama_kelurahan_agunan" id="nama_kelurahan_agunan" class="i-text "></input></div>
                    <input type="button" style="margin: 10px 0 !important;" onclick="getLokasi('nama_kelurahan_agunan','setlokasiagunan');" class="i-button no-margin" value="Cari" />

            </div>

             <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">Pilih Lokasi
            </label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <select id="setlokasiagunan" name="lokasiagunan" class="js-example-basic-single i-text required">
                    </select>
                    
            </div>
            
           
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Luas Tanah (m2)</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$data3[0]['luasTanahSertifikat']}}" type="text" name="luas_tanah_fisik" id="luas_tanah_fisik" class="i-text required"></input></div>
            </div>
            <div class="clearfix"></div>
            <div class="i-label">
                <label for="text_field">Luas Bangunan (m2)</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$data3[0]['luasImb']}}" type="text" name="luas_imb" id="luas_imb" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>

            <div class="clearfix"></div>
            

           

        </form>
          
        <div class="clearfix"></div>

        <form id="form_doc_nasabah" method="post" enctype="multipart/form-data">
        <div class="i-label">
                <label for="text_field">Daftar Dokumen Nasabah</label>
            </div>   
            <div class="i-divider"></div>
            <div class="clearfix"></div>

            <table>
                <tr>
                    <td width="150px"> <b>Jenis Dokumen</b> </td>
                    <td width="150px"> <b>Preview</b> </td>
                    <td> <b>Action</b> </td>
                </tr>
                @if ($doc1 == "Tidak Ada Data")
                @else 
                <tr>
                    <td>KTP</td>
                    <td>      
                       <div id="img_show1">
                            @php echo $doc1; @endphp
                        </div>       
                    </td>
                    <td>
                        <input type="hidden" value="{{$doc1_id}}" name="id_doc1">
                        <input value="" onchange="addDokumen(1)" type="file" name="doc_file1" id="doc_file1" class="i-text required"></input></div>
                    </td>
                </tr>
                @endif
                 @if ($doc2 == "Tidak Ada Data")
                  @else  
                <tr>
                    <td>Foto Selfie</td>
                    <td>
                        <div id="img_show2">
                            @php echo $doc2; @endphp
                        </div>
                    </td>
                    <td>
                        <input type="hidden" value="{{$doc2_id}}" name="id_doc2">
                        <input value="" onchange="addDokumen(2)" type="file" name="doc_file2" id="doc_file2" class="i-text required"></input></div>
                    </td>
                </tr>
                 @endif
                 @if ($doc3 == "Tidak Ada Data")
                  @else  
                <tr>
                    <td>KK</td>
                    <td >
                        <div id="img_show3">
                            @php echo $doc3; @endphp
                        </div>
                        
                        
                    </td>
                    <td>
                        <input type="hidden" value="{{$doc3_id}}" name="id_doc3">
                        <input value="" onchange="addDokumen(3)" type="file" name="doc_file3" id="doc_file3" class="i-text required"></input></div>
                    </td>
                </tr>
                @endif

                @if ($doc4 == "Tidak Ada Data")
                  @else  
                <tr>
                    <td>Other</td>
                    <td >
                        <div id="img_show4">
                            @php echo $doc4; @endphp
                        </div>
                        
                        
                    </td>
                    <td>
                        <input type="hidden" value="{{$doc4_id}}" name="id_doc4">
                        <input value="" onchange="addDokumen(4)" type="file" name="doc_file4" id="doc_file4" class="i-text required"></input></div>
                    </td>
                </tr>
                @endif
            </table>

             <div class="i-divider"></div>
            <div class="clearfix"></div>

             <input type="button" style="margin: 20px !important;" onclick="update();" class="i-button no-margin" value="Simpan" />
            <div class="i-divider"></div>
            {{-- <input type="button" style="margin: 20px !important;" onclick="addDokumen();" class="i-button no-margin" value="Upload Dokumen Baru" /> --}}

            <input value="{{$data['wilayah']['wilayah']}}" type="hidden" name="kelurahan" id="kelurahan1"></input>
            <input value="{{$data3[0]['wilayah']['wilayah']}}" type="hidden" name="kelurahan" id="kelurahan2"></input>
        </form> 
  

    </div>
    

</div>
       <!--  Modal content for the lion image example -->
 <!-- 
  <div class="modal fade pop-up-2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel-2" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" id="myLargeModalLabel-2">Lion</h4>
        </div>
        <div class="modal-body">
        <img src="http://i.imgur.com/kzGVqbd.jpg" class="img-responsive img-rounded center-block" alt="">
        </div>
      </div>
    </div>
  </div>

-->
    
@include('entry_nasabah.action')
@endsection
@section('script')
<script type="text/javascript">
 $(document).ready(function() { 
                $("#a").change(); 
                alert($('#kelurahan1').val());
        }); 

$(".loading-mail").show();
    var kelurahan = $('#kelurahan1').val();

    var _items = "";
    $.ajax({
        type: 'POST',
        url: base_url + '/select/apikelurahan1?id=' + kelurahan,
        success: function (res) {
            $(".loading-mail").hide();
                console.log(res['data']);
                
            
                  var data = $.parseJSON(res);
                
                
                $.each(data['data'], function (k,v) {
                    _items += "<option value='"+v.id+"'>"+v.wilayah+"</option>";
                });

                $("#setlokasi").html(_items);
                
        }
    }).done(function( res ) {
         $(".loading-mail").hide();
            
    }).fail(function(res) {
        $(".loading-mail").hide();
        swal("Terjadi Kesalahan! ");
    });
    $(".loading-mail").show();
    var kelurahan = $('#kelurahan2').val();

    var _items = "";
    $.ajax({
        type: 'POST',
        url: base_url + '/select/apikelurahan1?id=' + kelurahan,
        success: function (res) {
            $(".loading-mail").hide();
                console.log(res['data']);
                
            
                  var data = $.parseJSON(res);
                
                
                $.each(data['data'], function (k,v) {
                    _items += "<option value='"+v.id+"'>"+v.wilayah+"</option>";
                });

                $("#setlokasiagunan").html(_items);
                
        }
    }).done(function( res ) {
         $(".loading-mail").hide();
            
    }).fail(function(res) {
        $(".loading-mail").hide();
        swal("Terjadi Kesalahan! ");
    });

    $("#stat_menikah").change(function() {
        var sm = $('#stat_menikah option:selected').val();
        var nmPasanganFake = $('#nmPasanganFake').val();
        var ktpPasanganFake = $('#ktpPasanganFake').val();
        if(sm == 'f')
        {
            $('#nmPasangan').css('display','none');
            $('#ktpPasangan').css('display','none');
            $('#nmPasangan1').css('display','none');
            $('#ktpPasangan1').css('display','none');
            $('#nmPasangan').val('');
            $('#ktpPasangan').val('');
        } else {
            $('#nmPasangan').css('display','block');
            $('#ktpPasangan').css('display','block');
            $('#nmPasangan1').css('display','block');
            $('#ktpPasangan1').css('display','block');
            $('#nmPasangan').val(nmPasanganFake);
            $('#ktpPasangan').val(ktpPasanganFake);
        }
    });

    function update() {
    let myForm = document.getElementById('form_entry_nasabah_edit');
    let formData = new FormData(myForm);

    //  for (var pair of formData.entries()) {
    //     console.log(pair[0]+ ', ' + pair[1]); 
    // }
    console.log(formData.append('key2','value2'));
    var a1=$('#pendapatan_bulan').val();
    var a=a1.replace(/\./g,'');
    var b1=$('#pendapatan_lainnya').val();
    var b=b1.replace(/\./g,'');
    var c= parseInt(b);
    
    if(c<=4000001){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });   
       $.ajax({
           type: "POST",
           url: '{{ route('entry_nasabah.update_nasabah') }}',
           data : formData,
           dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
          
           success: function( msg ) {
           }
       }).done(function( msg ) {
           console.log(msg);
                if(msg['rc']==1){
                    swal("Informasi","Alamat Harus Mengadung kalimat blok dan nomor","info");
                }else{
                    swal({   
                        title: "Informasi",   
                        text: msg['rm'],   
                        type: "info",   
                        confirmButtonColor: "#8cd4f5",   
                        confirmButtonText: false,     
                        closeOnConfirm: true
                    }, function(isConfirm){   

                        window.location.href = base_url +'/entry_nasabah/daftar_prospek_nasabah';
                    });
                }
            
        }).fail(function(msg) {
             swal("Terjadi Kesalahan! ");
        });
    
    }else{

        swal("Informasi","Gaji anda melebihi peraturan PPDPP","info");

    }


      

}
    
</script>
<link rel="stylesheet" href="viewbox.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="jquery.viewbox.min.js"></script>  
    <script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
@stop

