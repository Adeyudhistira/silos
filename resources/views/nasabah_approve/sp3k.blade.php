@section('content')
<div class="c-forms">       
    <button class="i-button no-margin" onclick="kembali()">Kembali</button>
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Cetak SP4K</h3><a href="" class="collapsable"></a></div>
        
        <div class="box-content no-padding">
           <form method="post" action="{{ route('update_sp4k') }}" class="i-validate"> 
                @csrf
                <fieldset>
                   <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">No SP4K</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="hidden" value="{{$id}}" name="id"></input>
                                <input type="text" name="sp3k" class="required i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Tgl SP4K</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="date" name="tgl"  class="required i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>                              
                        <div class="section-center">
                            <button type="submit" class="i-button no-margin">Simpan</button>                     
                        <div class="clearfix"></div>
                        </div>
                        
                    </section>

                </fieldset>
            </form>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    function kembali() {
        window.location.href = base_url +'/nasabah_approve';
    }
</script>
@stop