@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Daftar Nasabah Approval 2</h3><a href="" class="collapsable"></a></div>
           @if ($message = Session::get('success'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg success-msg ">{{ $message }}<a href="">×</a></div>
            </div>
            @endif


            @if ($message = Session::get('error'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg error-msg">{{ $message }}<a href="">×</a></div>
            </div>
            @endif
         
        <div class="box-content no-padding">
           <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
            <thead>
                <tr>
                    <th>Nama Nasabah</th>
                    <th>Alamat</th>
                    <th>Produk</th>
                    <th>Cabang</th>
                    <th>Plafon</th>
                    <th>Jangka Waktu</th>
                    <th>Angsuran</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if($data)
                    @if($rc=='200')
                    @foreach($data as $item)
                        <tr>
                            @if($item['nasabah'])
                              <td>{{$item['nasabah']['namaNasabah']}}</td>
                              <td>{{$item['nasabah']['alamat']}}</td>  
                            @else
                              <td>-</td>
                              <td>-</td>  
                            @endif
                            @if($item['product'])
                              <td>{{$item['product']['prodName']}}</td>
                            @else
                              <td>-</td>  
                            @endif
                            @if($item['branch'])
                              <td>{{$item['branch']['branchName']}}</td>
                            @else
                              <td>-</td>  
                            @endif
                            <td>{{number_format($item['plafon'],0,',','.')}}</td>
                            <td>{{$item['jangkaWaktu']}} Bln</td>
                            <td>{{number_format($item['totalAngsuran'],0,',','.')}}</td>

                            @if($item['wfstatus'])
                              <td>{{$item['wfstatus']['statusDefinition']}}</td>
                            @else
                              <td>-</td>  
                            @endif
                            <td>
                                <input type="button" class="button" onclick="slik({{$item['id']}})" value="SLIK">
                                <input type="button" class="button" onclick="appraisal({{$item['id']}})" value="APPRAISAL">
                                <input type="button" class="button" onclick="analisa({{$item['id']}})" value="ANALISA">
                                <input type="button" class="button" onclick="sla({{$item['id']}})" value="SLA DETAIL">
                                <input type="button" class="button" onclick="proses_approval({{$item['id']}})" value="APPROVAL PROCESS">
                                @if($item['agunan'])
                                <input type="button" class="button" onclick="ppdpp({{$item['agunan']['id']}})" value="CHECKLIST PPDPP">
                                @endif
                                
                            </td>
                        </tr>
                    @endforeach
                    @else
                       <p style="color: red">{{$rm}}</p> 
                    @endif
                @endif
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    function slik(id) {
        window.location.href = base_url +'/slik_nasabah_approval2?id=' + id;
    }

    function appraisal(id) {
        window.location.href = base_url +'/appraisal_nasabah_approval2?id=' + id;
    }

    function analisa(id) {
        window.location.href = base_url +'/analisa_nasabah_approval2?id=' + id;
    }

    function sla(id) {
        window.location.href = base_url +'/sla_nasabah_approval2?id=' + id;
    }

    function ppdpp(id) {
        window.location.href = base_url +'/ppdpp_nasabah_approval2?id=' + id;
    }

    function proses_approval(id) {
        window.location.href = base_url +'/proses_approval?id=' + id;
    }

</script>
@stop

