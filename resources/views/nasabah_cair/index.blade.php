@section('content')
<div class="c-forms">       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Daftar Permohonan Analisa</h3><a href="" class="collapsable"></a></div>
           @if ($message = Session::get('success'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg success-msg ">{{ $message }}<a href="">×</a></div>
            </div>
            @endif


            @if ($message = Session::get('error'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg error-msg">{{ $message }}<a href="">×</a></div>
            </div>
            @endif
         
        <div class="box-content no-padding">
           <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
            <thead>
                <tr>
                    <th>Nama Nasabah</th>
                    <th>Alamat</th>
                    <th>Produk</th>
                    <th>Cabang</th>
                    <th>Plafon</th>
                    <th>Jangka Waktu</th>
                    <th>Angsuran</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                
                @if($data)
                    @foreach($data as $item)
                        <tr>
                            @if($item['nasabah'])
                              <td>{{$item['nasabah']['namaNasabah']}}</td>
                              <td>{{$item['nasabah']['alamat']}}</td>  
                            @else
                              <td>-</td>
                              <td>-</td>  
                            @endif
                            @if($item['product'])
                              <td>{{$item['product']['prodName']}}</td>
                            @else
                              <td>-</td>  
                            @endif
                            @if($item['branch'])
                              <td>{{$item['branch']['branchName']}}</td>
                            @else
                              <td>-</td>  
                            @endif
                             <td>{{number_format($item['plafon'],0,',','.')}}</td>
                            <td>{{$item['jangkaWaktu']}} Bln</td>

                            <td>{{number_format($item['totalAngsuran'],0,',','.')}}</td>
                           
                            @if($item['wfstatus'])
                              <td>{{$item['wfstatus']['statusDefinition']}}</td>
                            @else
                              <td>-</td>  
                            @endif
                            <td>
                                <input type="button" class="button" onclick="slik({{$item['id']}})" value="SLIK">
                                <input type="button" class="button" onclick="appraisal({{$item['id']}})" value="APPRAISAL">
                                <input type="button" class="button" onclick="analisa({{$item['id']}})" value="ANALISA">
                                <!--
                                <input type="button" class="button" onclick="sla({{$item['id']}})" value="SLA DETAIL">
                                -->
                                 @if($item['agunan'])
                                <input type="button" class="button" onclick="ppdpp({{$item['agunan']['id']}})" value="CHECKLIST PPDPP">
                                @endif
                               
                                    <input type="button" class="button" onclick="kirimproses({{$item['id']}})" value="KIRIM PROSES SELANJUTNYA">
                               
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    function slik(id) {
        window.location.href = base_url +'/slik_nasabah_cair?id=' + id;
    }

    function appraisal(id) {
        window.location.href = base_url +'/appraisal_nasabah_cair?id=' + id;
    }

    function analisa(id) {
        window.location.href = base_url +'/analisa_nasabah_cair?id=' + id;
    }

    function sla(id) {
        window.location.href = base_url +'/sla_nasabah_cair?id=' + id;
    }

    function ppdpp(id) {
        window.location.href = base_url +'/ppdpp_nasabah_cair?id=' + id;
    }
      function kirimproses(id) {

        swal({   
           title: "Info",   
           text: "proses data selanjutnya",   
           type: "info",   
           showCancelButton: true,   
           confirmButtonColor: "#e6b034",   
           confirmButtonText: "Ya",   
           cancelButtonText: "Tidak",   
           closeOnConfirm: true,   
           closeOnCancel: true
       }, function(isConfirm){   
          if (isConfirm) {     
            
                $(".loading-mail").show();
                $.ajax({
                    type: 'POST',
                    url: base_url + '/kirimproses?pembiayaanId='+ id ,
                    success: function (res) {
                     
                        $(".loading-mail").hide();
                        console.log(res['data']);
                        swal({   
                        title: "Informasi",   
                        text: "Berhasil",   
                        type: "info",   
                        confirmButtonColor: "#8cd4f5",   
                        confirmButtonText: false,     
                        closeOnConfirm: true
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                location.reload(); 
                            } 
                        });
                    }
                }).done(function( res ) {
                     $(".loading-mail").hide();
                        
                }).fail(function(res) {
                    $(".loading-mail").hide();
                    swal("Terjadi Kesalahan! ");
                });
        } 
    });
        return false;
        
       
    }
</script>
@stop

