@section('content')
<div class="c-forms"> 
<button class="i-button no-margin" onclick="kembali()">Kembali</button>      
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Detail Informasi Agunan</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
           <form method="post" action="{{ route('update_property_nasabah_cair') }}" class="i-validate"> 
                  @csrf
                  <input type="hidden" name="id_pembiayaan" @if($data) value="{{$data['idPembiayaan']}}" @endif  class="i-text i-form-tooltip" readonly></input>
                  <input type="hidden" name="nama_developer" @if($data) value="{{$data['namaDeveloper']}}" @endif  class="i-text i-form-tooltip" readonly></input>
                  <input type="hidden" name="id" @if($data) value="{{$data['id']}}" @endif  class="i-text i-form-tooltip" readonly></input>
                <fieldset>
                   <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Jenis Agunan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">

                                <input type="text" name="id_agunana" @if($data) value="{{$data['agunantype']['definition']}}" @endif  class="i-text i-form-tooltip" readonly></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nomor Sertifikat</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" required name="no_sertifikasi" @if($data) value="{{$data['noSertifikat']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Pemilik Serfifikat</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" required name="nama_pemilik" @if($data) value="{{$data['namaSertifikat']}}" @endif  class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Luas Tanah (m2)</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" required name="luas_tanah" onkeypress="return hanyaAngka(event)" @if($data) value="{{$data['luasFisik']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Luas Bangunan (m2)</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" required name="luas_bangunan" onkeypress="return hanyaAngka(event)" @if($data) value="{{$data['luasImb']}}" @endif class="i-text i-form-tooltip"></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nilai Appraisal</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" required name="nilai" onkeypress="return hanyaAngka(event)" onkeyup="convertToRupiah(this)" @if($data) value="{{number_format($data['nilaiAppraisal'],0,',','.')}}" @endif class="i-text"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Alamat Object</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" required name="alamat" @if($data) value="{{$data['alamat']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Wilayah</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" required name="wilayah" @if($data) value="{{$data['wilayah']['wilayah']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                </fieldset>
            </form>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        $("#kota").change(function(){
            $('#kec').LoadingOverlay("show"); 
            var _items = "";
            $.ajax({
                type: 'GET',
                url: 'select/kecamatan/' + this.value,
                success: function (res) {
                    var data = $.parseJSON(res);
                    _items = "<option value=''>Pilih Kecamatan</option>";
                    $.each(data, function (k,v) {
                        _items += "<option value='"+v.kode+"'>"+v.nama+"</option>";
                    });
                    $('#kec').LoadingOverlay("hide");
                    $("#kec").html(_items);
                }
            });


        });

        $("#kec").change(function () {
            console.log("asup");
            $('#kel').LoadingOverlay("show");
            var _items = "";
            $.ajax({
                type: 'GET',
                url: 'select/kelurahan/' + this.value,
                success: function (res) {
                    var data = $.parseJSON(res);
                    _items = "<option value=''>Pilih Kelurahan</option>";
                    $.each(data, function (k,v) {
                        _items += "<option value='"+v.kode+"'>"+v.nama+"</option>";
                    });
                    $('#kel').LoadingOverlay("hide");
                    $('#kel').html(_items);
                }
            });

        });



});
function kembali() {
        window.location.href = base_url +'/nasabah_cair';
    } 
</script>
@stop
