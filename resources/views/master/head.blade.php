<!-- CSS: implied media=all -->
<link rel="stylesheet" href="{{ asset('css/table.css') }}">
<link rel="stylesheet" href="{{ asset('css/fullcalendar.css') }}">
<link rel="stylesheet" href="{{ asset('css/simplemodal.css') }}">
<link rel="stylesheet" href="{{ asset('css/jquery.gritter.css') }}">
<link rel="stylesheet" href="{{ asset('css/jquery.wysiwyg.css') }}">
<link rel="stylesheet" href="{{ asset('css/chosen.css') }}">
<link rel="stylesheet" href="{{ asset('css/jquery-ui-1.8.16.custom.css') }}">
<link rel="stylesheet" href="{{ asset('css/elfinder.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/jqtransform.css') }}">
<link rel="stylesheet" href="{{ asset('css/style.css') }}">
<link rel="stylesheet" href="{{ asset('css/v2.css') }}">
<link rel="stylesheet" href="{{ asset('css/loader.css') }}">
<!--alerts CSS -->
<link href="{{asset('vendors/sweetalert/dist/sweetalert.css')}}" rel="stylesheet" type="text/css">
<!-- end CSS-->

<!-- CSS Media Queries for Standard Devices -->
<!--[if !IE]><!-->
<link rel="stylesheet" href="{{ asset('css/devices/smartphone.css') }}" media="only screen and (min-width : 320px) and (max-width : 767px)">
<link rel="stylesheet" href="{{ asset('css/devices/ipad.css') }}" media="only screen and (min-width : 768px) and (max-width : 1024px)"> 
<!--<![endif]-->
    <link href="{{ asset('css/select2.css') }}" rel="stylesheet" />


<style type="text/css">
.myclass{
    text-transform:capitalize;
}
</style>