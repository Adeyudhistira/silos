<header>
  <img src="{{asset('img/bjb_syariah.png')}}" style="height:95px;
  width:180px;">
  <!--
  <div id="main-navigation">
    <ul>
      <li><a href="#dashboard" class="active" id="dashboard-m"><span class="dashboard-32" title="Dashboard area">Dashboard</span></a></li>
      <li><a href="#elements" id="elements-m"><span class="files-32" title="Elements area">Elements</span></a></li>
      <li><a href="#forms" id="forms-m"><span class="forms-32" title="Forms area">Forms</span></a></li>
      <li><a href="#charts" id="charts-m"><span class="charts-32" title="Charts area">Charts</span></a></li>
      <li><a href="#docs" id="docs-m"><span class="file-32" title="Charts area">Docs</span></a></li>
    </ul>
  </div>
-->
  <div id="profile">
    <div id="user-data">Welcome: {{Auth::user()->username}}<br />{{Auth::user()->email}}</div>
  
    <div class="clearfix"></div>
    <div id="user-notifications">
      <ul>
        <!--
        <li><a class="notifications-16 tt-top-center" title="2 new notifications"><span class="notification">2</span></a></li>
        <li><a class="messages-16 tt-top-center" title="3 new messages"><span class="notification">3</span></a></li>
        -->
        <li><a class="settings-16 tt-top-center" title="change your settings"></a></li>
        <li><a class="profile-16 tt-top-center" title="go to your profile"></a></li>                        
        <li>
          <a id="logout" href="{{ route('logout') }}" class="logout-16 tt-top-center" title="click here to logout" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">logout</a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST"
                  style="display: none;">
                  {{ csrf_field() }}
                  </form>
        </li>
      </ul>                    
      <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="clearfix"></div>
</header>