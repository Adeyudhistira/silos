<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    
    <title>SILOS - FLPP</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/logo.jpg') }}">
    <link href='http://fonts.googleapis.com/css?family=Ubuntu+Condensed|Ubuntu' rel='stylesheet' type='text/css'>   
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow' rel='stylesheet' type='text/css'>
    <meta http-equiv="Cache-control" content="no-cache">
    <meta http-equiv="Expires" content="-1">
    @include('master.head')

    <style>
      .container {
        background: inherit !important;
      }

      .loading-mail {
        display: none;
        align-items: center;
        justify-content: center;
        background-color: #418bca;
        position: fixed;
        z-index: 10000;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        opacity: 0.9;
      }

      .myprogress span {
        font-size: 14px;
        color: #fff;
        font-weight: 500;
        text-align: center;
        display: block;
        margin-top: 40%;
      }

      #loaderzz {
      animation: animate 1.5s linear infinite;
      clip: rect(0, 80px, 80px, 40px);
      height: 80px;
      width: 80px;
      position: absolute;
      left: calc(50% - 40px);
      top: calc(50% - 40px);
      }
      @keyframes animate {
      0% {
        transform: rotate(0deg)
      }
      100% {
        transform: rotate(220deg)
      }
      }
      #loaderzz:after {
      animation: animate2 1.5s ease-in-out infinite;
      clip: rect(0, 80px, 80px, 40px);
      content:'';
      border-radius: 50%;
      height: 80px;
      width: 80px;
      position: absolute;
      }
      @keyframes animate2 {
      0% {
        box-shadow: inset #fff 0 0 0 17px;
        transform: rotate(-140deg);
      }
      50% {
        box-shadow: inset #fff 0 0 0 2px;
      }
      100% {
        box-shadow: inset #fff 0 0 0 17px;
        transform: rotate(140deg);
      }
      }
      .select_file{
        background: white;
      width: 100%;
      padding: 8px 20px;
      color: #a5a2a2;
      font-size: 13px;
      font-weight: 100;
      border-radius: 5px;
      border: 1px solid #cccccc;
      }
    </style>
</head>

<body>
   <div class="loading-mail">
  <div class="container">
    <div class="myprogress">
      <span>Mohon Tunggu, Sedang Dalam Proses...</span>
        <div id="loaderzz"></div>
    </div>
  </div>
</div>
  <div id="body-container">
    <div id="container">
      @include('master.header')
      <div id="main" role="main">
        <div id="sub-navigation">
          <div id="navigation-search">
            <form>
              <input type="text" name="search" id="search" placeholder="Search"></input>
            </form>
          </div>


          <ul>
            <div id="dashboard"></div>
          
          </ul>

            <div class="clearfix"></div>
          </div>
          <div id="main-container">
            <div id="body-section">
              <div id="loader-wrapper">
                <div id="loader"></div>

                <div class="loader-section section-left"></div>
                <div class="loader-section section-right"></div>

              </div>
                <div id="left-menu">
                    @include('master.menu')
                </div>
                <div id="content-container">
                    <div id="content">
                      @yield('content')     
                    </div>
                </div>
                <div class="clearfix"></div>
            </div> <!--! end of dashboard -->
          </div> <!--! end of #main-container -->
        </div>
          <footer>
              <span></span>
          </footer>
      </div> <!--! end of #container -->
    
    </div> <!--! end of #body-container -->
    
@include('master.script')
<div id="data_script">@yield('script')</div>
  <script type="text/javascript">
   $.ajax({
        type: 'GET',
        url: base_url + '/select/dashboard',
        success: function (res) {
            var data = $.parseJSON(res);
            console.log(res.newentry);
            $('#dashboard').append('<li><a title="New tickets" class="blue tt-top-center">'+res.newentry+'</a><span>New Entry</span></li>');

            $('#dashboard').append('<li><a style="background-color: #1E90FF;" class="tt-top-center">'+res.newprospect+'</a><span>Prospek Baru</span></li>');
            $('#dashboard').append('<li><a style="background-color: #1E90FF;" class="tt-top-center">'+res.newcustomer+'</a><span>Nasabah Baru</span></li>');
            $('#dashboard').append('<li><a style="background-color: #1E90FF;" class="tt-top-center">'+res.appraisal+'</a><span>Appraisal</span></li>');
            $('#dashboard').append(' <li><a style="background-color: #1E90FF;" class="tt-top-center">'+res.analisa+'</a><span>Analisa</span></li>');
            $('#dashboard').append('<li><a style="background-color: #1E90FF;" class="tt-top-center">'+res.approval+'</a><span>Approval</span></li>');
            $('#dashboard').append('<li><a class="green tt-top-center">'+res.approved+'</a><span>Approved</span></li>');
            $('#dashboard').append('<li><a style="background-color: #FF8C00;" class="tt-top-center">'+res.cair+'</a><span>Cair</span></li>');
}
    });
</script>
</body>
</html>



































