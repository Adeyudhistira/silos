
<ul>
  
  <li {!! (Request::is('home') ? 'class="menu-trigger active"' : ' ') !!}><a href="#" onclick="ajaxMenu(event,'home')" class="overview-16" id="c-overview">Dashboard</a></li>
  <li {!! (Request::is('dalam_proses') || Request::is('data_prospek') || Request::is('entry_prospek') || Request::is('daftar_prospek_baru') ? 'class="menu-trigger active"' : ' ') !!} ><a href="#" onclick="ajaxMenu(event,'dalam_proses')"  class="elements-16" id="c-elements">Dalam Proses</a></li>

  
  <li {!! (Request::is('sudah_disetujui') ? 'class="menu-trigger active"' : ' ') !!}><a href="#" onclick="ajaxMenu(event,'sudah_disetujui')" class="widgets-16" id="c-widgets">Sudah Disetujui</a></li>

   <li {!! (Request::is('slik_checking') ? 'class="menu-trigger active"' : ' ') !!}><a href="#" onclick="ajaxMenu(event,'slik_checking')" class="widgets-16" id="c-widgets">Slik Checking</a></li>


  <li {!! (Request::is('laporan') ? 'class="menu-trigger active"' : ' ') !!}><a href="#" onclick="ajaxMenu(event,'laporan')" class="widgets-16" id="c-widgets">Laporan</a></li>

  <li ><a href="#" onclick="ajaxMenu(event,'rekening_koran')" class="widgets-16" id="c-widgets">Cetak Rekening Koran</a></li>
  
  <li ><a href="#" onclick="ajaxMenu(event,'wilayah')" class="widgets-16" id="c-widgets">Wilayah</a></li>
  <li ><a href="#" onclick="ajaxMenu(event,'user')" class="widgets-16" id="c-widgets">User</a></li>
  <li ><a href="#" onclick="ajaxMenu(event,'developer')" class="widgets-16" id="c-widgets">Developer</a></li>
  <li ><a href="#" onclick="ajaxMenu(event,'residence')" class="widgets-16" id="c-widgets">Residence</a></li>



</ul>
<div class="clearfix"></div>
