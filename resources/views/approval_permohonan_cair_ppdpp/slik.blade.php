@section('content')
<div class="c-forms">       
    <button class="i-button no-margin" onclick="kembali()">Kembali</button>
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>SLIK</h3><a href="" class="collapsable"></a></div>
        
        <div class="box-content no-padding">
           <form method="post" action="" class="i-validate"> 
                <fieldset>
                   <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">No KTP</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="no_ktp" readonly @if($data) value="{{$data[0]['noIdentitas']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Nasabah</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="nama" readonly @if($data) value="{{$data[0]['namaNasabah']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Alamat</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="alamat" readonly @if($data) value="{{$data[0]['alamat']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Tempat Lahir</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="tmp_lahir" readonly @if($data) value="{{$data[0]['tempatLahir']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Tanggal Lahir</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" name="tgl_lahir" readonly @if($data) value="{{date('d-M-Y',strtotime($data[0]['tempatLahir']))}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>                              
                        <div class="section-center">
                            <input type="button" name="submit" id="submit" onclick="detail()" class="i-button no-margin" value="Hasil Slik Checking" />                         
                        <div class="clearfix"></div>
                        </div>
                        
                    </section>

                    <div class="show-slik">
                        <section>
                            <div class="section-left-s">
                                <label for="text_tooltip">Status SLIK</label>
                            </div>                                  
                            <div class="section-right">                                         
                                <div class="section-input">
                                     <select disabled name="status" id="status" class="i-text required">
                                        <option value="">Pilih</option>
                                        @foreach($data1 as $item)
                                        <option @if($data[0]['biCheckStatusId']==$item['id']) selected @endif value="{{$item['id']}}">{{$item['definition']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </section>
                        <section>
                            <div class="section-left-s">
                                <label for="text_tooltip">Catatan</label>
                            </div>                                  
                            <div class="section-right">                                         
                                <div class="section-input">
                                    <input type="text" name="catatan" readonly @if($data) value="{{$data[0]['notes']}}" @else value="Masih Proses" @endif class="i-text i-form-tooltip"></input>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </section>
                        <section>
                            <div class="section-left-s">
                                <label for="text_tooltip">Total Angsuran</label>
                            </div>                                  
                            <div class="section-right">                                         
                                <div class="section-input">
                                    <input type="text" name="angsuran" readonly @if($data) value="{{number_format($data[0]['angsuran'],0,',','.')}}"  @else value="Masih Proses"  @endif class="i-text i-form-tooltip"></input>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </section>
                        <section>
                            <div class="section-left-s">
                                <label for="text_tooltip">File</label>
                            </div>                                  
                            <div class="section-right">                                         
                                <div class="section-input">
                                    @if($data)
                                        @if($data[0]['document'])
                                            <a style="color: rgb(255, 255, 255);" href="http://silos-dev.basys.co.id/{{$data[0]['document']['urlDoc']}}"><i class="fa fa-file-excel-o"></i> <span>Download File</span></a>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </section>
                    </div>

                </fieldset>
            </form>
    </div>
</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$('.show-slik').hide();
    function detail() {
        var id=$('#submit').val();
        
        if(id=='Hasil Slik Checking'){

            $('.show-slik').show();
            $('#submit').val('Tutup');

        }else{

            $('.show-slik').hide();
            $('#submit').val('Hasil Slik Checking');

        }
    }
    function kembali() {
        window.location.href = base_url +'/approval_permohonan_cair_ppdpp';
    }
</script>
@stop