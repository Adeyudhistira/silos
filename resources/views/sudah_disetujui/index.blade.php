@section('content')
<div class="c-widgets">
    <div class="bredcrumbs">
        <ul>
            <li><a href="#">dashboard</a></li>
            <li><a href="#">sudah disetujui</a></li>
        </ul>
        <div class="clearfix"></div>
    </div>

    <div class="box-element">
        <div class="box-head"><span class="forms-16"></span><h3>Quick actions</h3><a href="" class="collapsable"></a></div>
        <div class="box-content">
            <ul class="actions">
                <li>
                    <div>
                        <a href="{{route('nasabah_approve')}}" class="edit-32">Approved - Cetak SP4</a>
                    </div>
                </li>
                <li>
                    <div>
                        <a href="{{route('nasabah_pra_uji')}}" class="edit-32">Approved - Pra Uji KP</a>
                    </div>
                </li>
                <li>
                    <div>
                        <a href="{{route('hasil_pra_uji')}}" class="edit-32">Approved - Pra Uji Done</a>
                    </div>
                </li>
                <li><div><a  href="{{route('pra_booking_review')}}" class="edit-32">Approved - PBR</a></div></li>
                <li><div><a  href="{{route('pra_booking_review_done')}}" class="edit-32">Approved - PBR Done</a></div></li>
                <li>
                    <div>
                        <a href="{{route('akad')}}" class="edit-32">Approved - Akad</a>
                    </div>
                </li>
                <li>
                    <div>
                        <a href="{{route('booking_review')}}" class="edit-32">Approved - Booking Review</a>
                    </div>
                </li>
                <li>
                    <div>
                        <a href="{{route('approval_br')}}" class="edit-32">Approved - Approval Booking Review</a>
                    </div>
                </li>
                <li>
                    <div>
                        <a href="{{route('permohonan_cair')}}" class="edit-32">Approved - Permohonan Cair PPDPP</a>
                    </div>
                </li>

                <li><div><a  href="{{route('approval_permohonan_cair_ppdpp')}}" class="edit-32">Approved - Approval Permohonan Cair PPDPP</a></div></li>
                <li><div><a  href="{{route('update_status_siap_cair')}}" class="edit-32">Approved - Update Status Siap Cair</a></div></li>
                <li><div><a  href="{{route('permohonan_cair_cbs')}}" class="edit-32">Approved - Permohonan Cair CBS</a></div></li>
                <li><div><a  href="{{route('approval_permohonan_cair_cbs')}}" class="edit-32">Approved - Approval Permohonan Cair CBS</a></div></li>


                <li><div><a  href="{{route('permohonan_uji')}}" class="edit-32">Daftar Nasabah Permohonan Uji</a></div></li>
                <li><div><a  href="{{route('nasabah_uji')}}" class="users-32">Daftar Batch Permohonan Uji</a></div></li>
                <li><div><a  href="{{route('lihat_nasabah_siap_cair')}}" class="charts-32">Daftar Nasabah Siap Cair</a></div></li>
                <li><div><a  href="{{route('lihat_nasabah_sudah_cair')}}" class="settings-32">Daftar Nasabah Sudah Cair</a></div></li>
                <li><div><a  href="{{route('master_ppdpp')}}"  class="finance-32">Master PPDPP</a></div></li>

               
                
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>

   


<div class="clearfix"></div>

</div>

@endsection

