@section('content')

@php
    if(isset($data)){
        $id = $data->id;

        $id_developer = $data->id_developer;
        $residence_name = $data->residence_name;
        $wilayah = $data->wilayah;
        $jumlah_unit = $data->jumlah_unit;
        $harga_unit = $data->harga_unit;
        $type = 'update';
    }else {
        $id = '';
        $id_developer = '';
        $residence_name = '';
        $wilayah = '';
        $jumlah_unit = '';
        $harga_unit = '';
        $type = 'normal';
    }
@endphp
<div class="c-forms"> 
            <a class="i-button no-margin" href="{{route('residence.index')}}">Kembali</a>   

    <div class="box-element" style="
    margin-top: 20px;
">
        <form id="form_tambah_data">
            
        <input type="hidden" name="id" id="id" value="{{$id}}">
        <input type="hidden" name="type" id="type" value="{{$type}}">

              <div class="i-label">
                <label for="text_field">Nama Developer</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                         @php
                        $dt = \DB::select("SELECT * FROM ref_developer");
                    @endphp
                    <select class="i-text required" onchange="Gantii()" name="id_developer" id="id_developer">
                        <option value="">Pilih</option>
                        @foreach ($dt as $item)
                            <option {{($item->id == $id_developer) ? "selected":"" }} value="{{$item->id}}">{{$item->developer_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>


            <div class="i-label">
                <label for="text_field">Nama Residence</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$residence_name}}" type="text" name="residence_name" id="residence_name" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

             <div class="i-label">
                <label for="text_field">Wilayah</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                     @php
                        $query = \DB::select("SELECT * FROM wilayah where kode=SUBSTR(kode,1,4)");
                    @endphp
                    <select class="i-select required" name="wilayah" id="wilayah" required>
                      <option value="">Pilih Kab / Kota</option>  
                        @foreach($query as $item)
                           <option value="{{$item->nama}}" @if($item->nama==$wilayah) selected @endif>{{$item->nama}}</option> 
                        @endforeach 
                    </select>
                   </div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>
            <input value="0" type="hidden" name="jumlah_unit" id="jumlah_unit" class="i-text required"></input>
            <input value="0" type="hidden" name="harga_unit" id="harga_unit" class="i-text required"></input>
        <!--    
            <div class="i-label">
                <label for="text_field">Jumlah Unit</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    <input value="{{$jumlah_unit}}" type="text" name="jumlah_unit" id="jumlah_unit" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>

              <div class="i-label">
                <label for="text_field">Harga Unit</label>
            </div>                                  
            <div class="section-right">
                <div class="section-input">
                    {{-- onkeyup="convertToNum(this)"  --}}
                    <input value="{{$harga_unit}}" type="text" name="harga_unit" id="harga_unit" class="i-text required"></input></div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>            
        -->
        <input type="button" style="margin: 20px !important;" onclick="TambahDataBaru();" class="i-button no-margin" value="Simpan" />

        </form>
    </div>
</div>
        
   

@include('residence.action')
@endsection
@section('script')
    <script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
@stop

