<script>
    
  


function TambahDataBaru() {
    console.log("tambahBaru");

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
$.ajax({
        type: "POST",
        url: base_url + '/residence/tambah',
        data: {
            type: $('#type').val(),
            id_developer: $('#id_developer').val(),
            residence_name: $('#residence_name').val(),
            wilayah: $('#wilayah').val(),
            jumlah_unit: $('#jumlah_unit').val(),
            harga_unit: $('#harga_unit').val(),
            id: $('#id').val()
            

        },
        beforeSend: function () {
        },
        success: function (msg) {

        }
    }).done(function (msg) {
        window.location.href = base_url + '/residence';

    }).fail(function (msg) {

    });
    
}



function HapusDataBaru(id) {
    swal({
        title: "Hapus",
        text: "Anda Yakin akan Hapus?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#e6b034",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: base_url + '/residence/delete/' + id,
                data: {
                    id: id
                },
                beforeSend: function () {
                },
                success: function (msg) {
                    console.log(msg);
                    swal({
                        title: "Informasi",
                        text: "berhasil Terhapus",
                        type: "info",
                        confirmButtonColor: "#8cd4f5",
                        confirmButtonText: "Ya",
                        closeOnConfirm: false
                    }, function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                        }
                    });

                }
            }).done(function (msg) {

            }).fail(function (msg) {
                swal("Terjadi Kesalahan! ");
            });

        } else {
            swal("Dibatalkan!");
        }
    });
}

</script>