@section('content')
<div class="c-forms">
<button class="i-button no-margin" onclick="kembali()">Kembali</button>       
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Approval Process</h3><a href="" class="collapsable"></a></div>
           @if ($message = Session::get('success'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg success-msg ">{{ $message }}<a href="">×</a></div>
            </div>
            @endif


            @if ($message = Session::get('error'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg error-msg">{{ $message }}<a href="">×</a></div>
            </div>
            @endif
         
        <div class="box-content no-padding">
           @if($rc=='200')
            <form method="post" action="{{route('update_analis_nasabah_approval')}}" class="i-validate"> 
            @csrf
            <fieldset>
                   <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nama Nasabah</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly name="nama" @if($data) value="{{$data['namaNasabah']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Plafon</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly name="plafon"  @if($data) value="{{number_format($data['plafon'],0,',','.')}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Jangka Waktu</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly name="jangka_waktu" @if($data) value="{{$data['jangkaWaktu']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">RITI</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly name="riti" @if($data) value="{{number_format($data['riti'],2,',','.')}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">BI Checking</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly name="bi_check" @if($data) value="{{$data['biCheck']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Collateral Coverage</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly name="coll_cover" @if($data) value="{{number_format($data['collCover'],2,',','.')}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Kesimpulan Analisa</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly @if($data) value="{{$data['kesimpulan']}}" @endif name="kesimpulan"  class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Catatan Lainnya</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly name="catatan" @if($data) value="{{$data['notes']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Score</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input">
                                <input type="text" readonly name="score" @if($data) value="{{$data['score']}}" @endif class="i-text i-form-tooltip"></input>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                     <section>                              
                        <div class="section-center">
                            <input type="button" name="submit" id="submit" onclick="approve({{$idpembiayaan}})" class="i-button no-margin" value="Approve" />                       
                            <input type="button" name="submit" id="submit" onclick="reject({{$idpembiayaan}})" class="i-button no-margin" value="Reject" />                         
                            <input type="button" name="submit" id="submit" onclick="balik({{$idpembiayaan}})" class="i-button no-margin" value="Return" /></div>
                        
                    </section> 
                </fieldset>    
            </div>
        </form>
        @else
        <p style="color: red">{{$rm}}</p>
        @endif

    </div>
</div>
</div>


<div id="modal_balik" class="modal-container">
    <div class="modal-head"><h3>Return</h3></div>
    <div class="modal-body">
        <form id="form_upload">
            <div class="i-label">
            <label for="text_field">Pilih Group User</label>
            </div>              
            @php
            $user=\DB::select("select * from ref_user_group where id in(1,2,3)");
            @endphp                    
            <div class="section-right">
                <div class="section-input">
                    <select class="i-text" id="userid" name="userid">
                        @if($user)
                            @foreach($user as $item)
                            <option value="{{$item->id}}">{{$item->group_definition}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="i-divider"></div>
            <div class="clearfix"></div>  
        </form>
    </div>
    <div class="modal-footer">
        <input type="button" onclick="simpan();" class="i-button no-margin" value="Simpan" />
        <div class="clearfix"></div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$('.detail-show').hide();
var form_upload = $('#form_upload');
    var modal_balik=$('#modal_balik');
    function balik(){
        modal_balik.modal({
            onShow: function (dlg) {
                $(dlg.container).css('height', 'auto')
            }
        });
    }
    function simpan() {
 //  _create_form.parsley().validate();
    var _form_data = new FormData(form_upload[0]);
//if(_create_form.parsley().isValid()){
      $.LoadingOverlay("show");
    $.ajax({
        type: 'POST',
        url: base_url + '/simpan/approval',
        data: _form_data,
        processData: false,
        contentType: false,
        success: function (res) {
           var parse = $.parseJSON(res);
            console.log(res);
            console.log(parse);
            if (parse.rc == 0) {
                //console.log(parse);
                //modal_upd.modal('hide');
               // hideLoading(true);
                //showInfo(parse.msg);
                  $.LoadingOverlay("hide");
                form_upload[0].reset();
                  swal({   
                        title: "Berhasil",   
                        text: parse.rm,   
                        type: "success",   
                        confirmButtonColor: "#e6b034",   
                        confirmButtonText: "Tutup",   
                        closeOnConfirm: false
                    }, function(isConfirm){   
                        if (isConfirm) {     
                        location.reload();
                      //window.location.href = base_url +'/billing/table/' + id
                        } 
                    });
                    return false;
            }else if(parse.rc == 2){
                hideLoadingPartial(true);
                showError(parse.msg);
            }else {
                var li = '<ul>';
                $.each(parse.data, function (i, v) {
                    li += '<li>' + v[0] + '</li>';
                });
                li += '</ul>';
                hideLoadingPartial(true);
                showError(li);
            }
        }
    }).always(function () {
       // var loader = $('.ui.dimmable #mainLoader');    
        //loader.dimmer({closable: false}).dimmer('show');
       // location.reload();;
    });
  //  }
}
function reject(id) {
    swal({
        title: "Hapus",
        text: "Anda Yakin akan Hapus?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#e6b034",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: base_url + '/reject?id=' + id,
                beforeSend: function () {
                },
                success: function (msg) {
                    console.log(msg);
                    swal({
                        title: "Informasi",
                        text: "berhasil direject",
                        type: "info",
                        confirmButtonColor: "#8cd4f5",
                        confirmButtonText: "Ya",
                        closeOnConfirm: false
                    }, function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                        }
                    });

                }
            }).done(function (msg) {

            }).fail(function (msg) {
                swal("Terjadi Kesalahan! ");
            });

        } else {
            swal("Dibatalkan!");
        }
    });
}

function approve(id) {
    swal({
        title: "Approve",
        text: "Anda Yakin Approve data?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#e6b034",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: base_url + '/approve?id=' + id,
                beforeSend: function () {
                },
                success: function (msg) {
                    console.log(msg);
                    swal({
                        title: "Informasi",
                        text: msg['rm'],
                        type: "info",
                        confirmButtonColor: "#8cd4f5",
                        confirmButtonText: "Ya",
                        closeOnConfirm: false
                    }, function (isConfirm) {
                        if (isConfirm) {
                            window.location.href = base_url +'/nasabah_approval';
                        }
                    });

                }
            }).done(function (msg) {

            }).fail(function (msg) {
                swal("Terjadi Kesalahan! ");
            });

        } else {
            swal("Dibatalkan!");
        }
    });
}

    function kembali() {
        window.location.href = base_url +'/nasabah_approval';
    }
</script>
@stop