@section('content')
<div class="c-forms">       
    <button class="i-button no-margin" onclick="kembali()">Kembali</button>
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>APPRAISAL</h3><a href="" class="collapsable"></a></div>
           @if ($message = Session::get('success'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg success-msg ">{{ $message }}<a href="">×</a></div>
            </div>
            @endif


            @if ($message = Session::get('error'))
            <div class="section-content offset-one-third">                              
               <div class="alert-msg error-msg">{{ $message }}<a href="">×</a></div>
            </div>
            @endif
         
        <div class="box-content no-padding">
           <form method="post" action="" class="i-validate"> 
                <fieldset>
                   <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Tanggal Permohonan</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" @if($pembiayaan) value="{{date('d-M-Y',strtotime($pembiayaan[0]['tglPermohonan']))}}" @endif name="tglPermohonan" class="i-text i-form-tooltip" readonly></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Tanggal Survey</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" @if($pembiayaan) value="{{date('d-M-Y',strtotime($pembiayaan[0]['tglSurvey']))}}" @endif  name="tglSurvey" class="i-text i-form-tooltip" readonly></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>
                    <section>
                        <div class="section-left-s">
                            <label for="text_tooltip">Nilai Appraisal</label>
                        </div>                                  
                        <div class="section-right">                                         
                            <div class="section-input"><input type="text" @if($pembiayaan) value="{{number_format($pembiayaan[0]['nilaiAppraisal'],0,',','.')}}" @endif name="nilaiAppraisal" class="i-text i-form-tooltip" readonly></input></div>
                        </div>
                        <div class="clearfix"></div>
                    </section>

                </fieldset>
            </form>
    </div>
</div>
    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Jika Agunan Property</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">
            <table cellpadding="0" cellspacing="0" border="0" class="display" id="datatable">
                <thead>
                    <tr>
                        <th>Jenis Agunan</th>
                        <th>Nomor Sertifikat</th>
                        <th>Nama Pemilik Serfifikat</th>
                        <th>Luas Tanah</th>
                        <th>Luas Bangunan</th>
                        <th>Nilai Appraisal</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if($property)
                        @foreach($property as $item)
                            <tr>
                                <td>{{$item['idSubJnsAgunan']}}</td>
                                <td>{{$item['noSertifikat']}}</td>
                                <td>{{$item['namaSertifikat']}}</td>
                                <td>{{$item['luasFisik']}}</td>
                                <td>{{$item['luasImb']}}</td>
                                <td>{{number_format($item['nilaiAppraisal'],0,',','.')}}</td>
                                <td><input type="button" class="button" onclick="detail_property({{$item['id']}})" value="Detail"></td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    function detail_property(id) {
        window.location.href = base_url +'/detail_property_nasabah_approval?id=' + id;
    }
    function kembali() {
        window.location.href = base_url +'/nasabah_approval';
    }
    
</script>
@stop
