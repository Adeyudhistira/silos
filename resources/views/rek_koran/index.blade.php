@section('content')
<div class="c-forms">
     <div class="box-element">
        <div class="box-head-light"><span class="search-16"></span><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding grey-bg">
            <form class="form" id="form-filter" method="GET"> 
                <div class="clearfix"></div>
                <div class="i-label">
                <label for="text_field">No Rekening</label>
                </div>                                  
                <div class="section-right">
                    <div class="section-input">
                        <input type="text" class="i-text" name="no_rek"  @if($norek) value="{{$norek}}" @endif id="no_rek" required>
                    </div>
                </div>
                <div class="i-divider"></div>
                <div class="clearfix"></div>

                <div class="clearfix"></div>
                
                <div class="i-label">
                <label for="text_field">Tanggal Mulai</label>
                </div>                                  
                <div class="section-right">
                    <div class="section-input">
                        <input type="date" class="i-text" name="tgl_awal" @if($tgl_awal) value="{{$tgl_awal}}" @endif  id="tgl_awal" required>
                    </div>
                </div>
                <div class="i-divider"></div>
                <div class="clearfix"></div> 

                <div class="i-label">
                <label for="text_field">Tanggal Akhir</label>
                </div>                                  
                <div class="section-right">
                    <div class="section-input">
                        <input type="date" class="i-text" name="tgl_akhir" @if($tgl_akhir) value="{{$tgl_akhir}}" @endif  id="tgl_akhir" required>
                    </div>
                </div>
                <div class="i-divider"></div>
                <div class="clearfix"></div> 

                <div class="modal-footer">
                    <button type="submit" value="filter" id="filter" name='filter' class="icon16-button">Find</button>
                    <div class="clearfix"></div>
                </div>   
            </form>    
            
        </div>
    </div>

    <div class="box-element">
        <div class="box-head-light"><span class="forms-16"></span><h3>Cetak Rekening Koran</h3><a href="" class="collapsable"></a></div>
        <div class="box-content no-padding">

           <table cellpadding="0" cellspacing="0" border="0" class="display" id="t_wilayah">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tgl Transaksi</th>
                    <th>No Arsip</th>
                    <th>Keterangan</th>
                    <th>Jumlah Mutasi</th>
                    <th>D/K</th>
                    <th>Saldo</th>
                </tr>
            </thead>
            <tbody>
                @if($data)
                    @if($flt)
                        @php $no=0; @endphp
                        @foreach($data as $item)
                        @php $no++ @endphp
                        <tr>
                            <td>{{$no}}</td>
                            <td>{{date('d M Y',strtotime($item['tgl_mutasi']))}}</td>
                            <td>{{$item['no_arsip']}}</td>
                            <td>{{$item['keterangan']}}</td>
                            <td>{{number_format($item['nilai_mutasi'],0,',','.')}}</td>
                            <td>{{$item['jenis_mutasi']}}</td>
                            <td>{{number_format($item['saldo'],0,',','.')}}</td>
                        </tr>
                        @endforeach
                    @else
                        <p><b>Tidak Ada Data</b></p>    
                    @endif    
                @endif
            </tbody>
        </table>
        </div>
    </div>
</div>



@include('developer.action')
@endsection
@section('script')
<script type="text/javascript" src="{{ asset('js/content/dks.js') }}"></script>
<script>
var t_wilayah = $('#t_wilayah').dataTable({
    "sPaginationType": "full_numbers"   
});
</script>
@stop


