<?php

namespace App;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
class NasabahDinamisModel extends Model
{
    protected $primaryKey = 'id_pembiayaan';
    protected $table = 'master_nasabah_dinamis';

     public function deleteData(Request $request, $id){
       $bean = $this->find($id);
        $bean->delete($id);
     	
    }
}