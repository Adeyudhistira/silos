<?php

namespace App\Models;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class MWilayahModel extends Model
{
    protected $primaryKey = 'kode';
    protected $table = 'wilayah';
    public $timestamps = false;

}
