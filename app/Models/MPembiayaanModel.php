<?php

namespace App\Models;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class MPembiayaanModel extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'master_pembiayaan';
    public $timestamps = false;

}
