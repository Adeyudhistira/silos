<?php

namespace App\Models;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class DeveloperModel extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'ref_developer';
    public $timestamps = false;

}
