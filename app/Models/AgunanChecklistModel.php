<?php

namespace App\Models;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class AgunanChecklistModel extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'master_agunan_property_checklist';
    public $timestamps = false;

}
