<?php

namespace App\Models;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class MNasabahModel extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'master_nasabah';

}
