<?php

namespace App\Models;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class UserModel extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'users';
    public $timestamps = false;

}
