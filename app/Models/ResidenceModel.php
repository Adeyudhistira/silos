<?php

namespace App\Models;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ResidenceModel extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'ref_residence';
    public $timestamps = false;

}
