<?php

namespace App\Models;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class MasterCairModel extends Model
{
    protected $primaryKey = 'id_berkas';
    protected $table = 'master_data_idberkas_cair';
    public $timestamps = false;
    

}
