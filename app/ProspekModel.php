<?php

namespace App;

use Datatables, DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
class ProspekModel extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'master_prospek';

     public function deleteData(Request $request, $id){
       // $bean = $this->find($id);
        //$bean->delete($id);
     	$st=DB::table('master_project')
            ->where('client_id', '=',$id)
            ->where('status_id',1)
            ->first();

       if(empty($st)){
        $real_lm = $this->find($id);
        $real_lm->status= 0;
        $real_lm->save();
            }else{
           throw new Exception("Data ini sedang dipakai, tidak bisa melakukan hapus data", 1);
            	
            }
    }
}