<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use App\Http\Requests;
use Auth;

class ApprovalPermohonanCairCbsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(request $request)
    {
        $url = env('API_BASE_URL')."master/nasabah/status?page=".$request->get('page')."&size=".$request->get('size');
        $client = new Client();
        $dataa = array(
            "userId"=> null,
            "branchId"=> Auth::user()->id_branch,
            "statusPembiayaan"=> array(17)
        );
        // dd($data);
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $dataa,
                ]);

            $param1=[];
            $param1= (string) $result->getBody();
            
            $data1 = json_decode($param1, true);
            if($data1['rc']==200){
                $data =$data1['data'];
                $rc=$data1['rc'];
                $rm='';
            }else{
                $data ='';
                $rc=$data1['rc'];
                $rm='';
            }
            
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
            $rc=$response->rc;
            $rm=$response->rm;
        }
        
        $param['data']=$data;
        $param['rc']=$rc;
        $param['rm']=$rm;
		
        if ($request->ajax()) {
            $view = view('approval_permohonan_cair_cbs.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'approval_permohonan_cair_cbs.index',$param);
    }

    public function slik(Request $request)
    {
        $url = env('API_BASE_URL')."master/bi-check/pembiayaan/".$request->get('id');
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
           $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }

         $url1 = env('API_BASE_URL')."master/list/bicheck-status";
        try{
            
            $result1 = $client->get($url1,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param2=[];
            $param2= (string) $result1->getBody();
            $data1 = json_decode($param2, true);
            $data1 =$data1['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data1=$response;
        }


        $param['data1']=$data1;

        $param['data']=$data;

        if ($request->ajax()) {
            $view = view('approval_permohonan_cair_cbs.slik',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'approval_permohonan_cair_cbs.slik',$param);
    }

    public function appraisal(Request $request)
    {
        
        $url = env('API_BASE_URL')."master/agunan/pembiayaan/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $pembiayaan =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $pembiayaan=$response;
        }
        if($pembiayaan){

            $url1 = env('API_BASE_URL')."master/agunan-property/agunan/".$pembiayaan[0]['id'];
            try{
                
                $result1 = $client->get($url1,[
                    RequestOptions::HEADERS => $headers,
                    ]);
                
                
                $param2=[];
                $param2= (string) $result1->getBody();
                $data1 = json_decode($param2, true);
                $property =$data1['data'];

            }catch (BadResponseException $e){
                $response1 = json_decode($e->getResponse()->getBody());
                $property=$response1;
            }
           

        }
        
        $param['pembiayaan']=$pembiayaan;
        $param['property']=$property;
        if ($request->ajax()) {
            $view = view('approval_permohonan_cair_cbs.appraisal',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'approval_permohonan_cair_cbs.appraisal',$param);
    }

    public function analisa(Request $request)
    {
        $url = env('API_BASE_URL')."master/pembiayaan/analisis/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1['data'];
            $rc=$data1['rc'];
            $rm=$data1['rm'];
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
            $rc=$data->rc;
            $rm=$data->rm;
        }

        $param['rc']=$rc;
        $param['rm']=$rm;
        $param['data']=$data;
        $param['idpembiayaan']=$request->get('id');    
        if ($request->ajax()) {
            $view = view('approval_permohonan_cair_cbs.analisa',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'approval_permohonan_cair_cbs.analisa',$param);
    }

    public function ppdpp(Request $request)
    {
        $url = env('API_BASE_URL')."master/agunan-property/agunan/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        $param['data']=$data;
        if ($request->ajax()) {
            $view = view('approval_permohonan_cair_cbs.check_ppdpp',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'approval_permohonan_cair_cbs.check_ppdpp',$param);
    }

    public function detail_property(Request $request)
    {
        $url = env('API_BASE_URL')."master/agunan-property/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        $param['data']=$data;
        if ($request->ajax()) {
            $view = view('approval_permohonan_cair_cbs.detail_property',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'approval_permohonan_cair_cbs.detail_property',$param);
    }

}
