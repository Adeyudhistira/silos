<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use Session;
class HomeController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*
    public function __construct()
    {
        $this->middleware('auth');
    }
    */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
        public function index(Request $request)
    {
        $param['kuota']=\DB::select("select * from master_data_ppdpp_kuota");
        $param['bulan_ini']=\DB::select("select sum(harga_object) as nilai from master_pembiayaan where EXTRACT(YEAR FROM tgl_cair) =EXTRACT(YEAR FROM  now())");
        $param['tahun_ini']=\DB::select("select sum(harga_object) as nilai from master_pembiayaan where EXTRACT(MONTH FROM tgl_cair) =EXTRACT(MONTH FROM  now())");

        $param['login']=\DB::select("select * from logins");
        
        $param['cek']="blm_update";
        $param['kelola']=\DB::select("select saldo_txt as saldo from ref_acc_flpp where id = 1");
        $param['program']=\DB::select("select saldo_txt as saldo from ref_acc_flpp where id = 2");
        $param['operasi']=\DB::select("select saldo_txt as saldo from ref_acc_flpp where id = 3");




        

        if ($request->ajax()) {
            $view = view('home',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'home',$param);
    }

        public function saldo(Request $request)
    {
        $param['kuota']=\DB::select("select * from master_data_ppdpp_kuota");
        $param['bulan_ini']=\DB::select("select sum(harga_object) as nilai from master_pembiayaan where EXTRACT(YEAR FROM tgl_cair) =EXTRACT(YEAR FROM  now())");
        $param['tahun_ini']=\DB::select("select sum(harga_object) as nilai from master_pembiayaan where EXTRACT(MONTH FROM tgl_cair) =EXTRACT(MONTH FROM  now())");

        $param['login']=\DB::select("select * from logins");

        $param['cek']="sdh_update";
        
        $param['kelola']=\DB::select("select saldo from ref_acc_flpp where id = 1");
        $param['program']=\DB::select("select saldo from ref_acc_flpp where id = 2");
        $param['operasi']=\DB::select("select saldo from ref_acc_flpp where id = 3");




        if ($request->ajax()) {
            $view = view('home',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'home',$param);
    }

     public function updatesaldo(){
        $login=\DB::select("select * from logins");
        $url = env('API_BASE_URL')."cbs/saldo";
        $client = new Client();
        $headers = [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '. session('token')
                ];
        try{

            $result = $client->put($url,[
                RequestOptions::HEADERS => $headers


            ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);

            if($data['rc']=='200' || $data['rc']==0){

                //DB::table('master_data_ppdpp_kuota')
                //->where('kode', 1)
                //->update(['kuota_nilai' => $data['data']['quota'] ,'updated_at'=>date('Y-m-d H:i:s') ]);

                return response()->json([
                        'rc' => 1,
                        'rm' => "Data Berhasil Diupdate"
                    ]);

            }else{

                return response()->json([
                        'rc' => 2,
                        'rm' => $data['rm']
                    ]);

            }
            


        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            //var_dump($response);
            return response()->json([
                    'rc' => 2,
                    'rm' => $response->rm
                ]);

        } 

    }



    public function dalam_proses(Request $request)
    {
        if ($request->ajax()) {
            $view = view('dalam_proses.index')->renderSections();
            return json_encode($view);
        }

        return view('master.master')->nest('child', 'dalam_proses.index');
    }

    public function slik_checking(Request $request)
    {
        if ($request->ajax()) {
            $view = view('slik_checking.index')->renderSections();
            return json_encode($view);
        }

        return view('master.master')->nest('child', 'slik_checking.index');
    }    


    public function sudah_disetujui(Request $request)
    {
        if ($request->ajax()) {
            $view = view('sudah_disetujui.index')->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'sudah_disetujui.index');
    }

     public function dati($id){
        
        $dati= \DB::select("select d.* from dati1 d
            join dati2 d2 on d2.kode_dati=d.kode_dati
            where d2.kode_dati2=:id",['id'=>$id]);  
        return json_encode($dati);
    }

    public function updatekuota(){
        $login=\DB::select("select * from logins");
        $url = env('API_BASE_URL_ANGSURAN')."get-kuota-nilai";
        $client = new Client();
        $headers = [
                    'Content-Type' => 'application/json'
                    //'Authorization' => Auth::id()
                ];
        $data = array(
                "batchid"=> $login[0]->batch_id,
                );        

        try{

            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,


            ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);

            if($data['rc']=='200' || $data['rc']==0){
                DB::table('master_data_ppdpp_kuota')
                ->where('kode', 1)
                ->update(['kuota_nilai' => $data['data']['quota'] ,'updated_at'=>date('Y-m-d H:i:s') ]);

                return response()->json([
                        'rc' => 1,
                        'rm' => "Data Berhasil Diupdate"
                    ]);

            }else{

                return response()->json([
                        'rc' => 2,
                        'rm' => $data['rm']
                    ]);

            }
            


        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
          //  var_dump($response);
            return response()->json([
                    'rc' => 2,
                    'rm' => $response->rm
                ]);

        } 

    }

    public function updateunit(){
        $login=\DB::select("select * from logins");
        $url = env('API_BASE_URL_ANGSURAN')."get-kuota-unit";
        $client = new Client();
        $headers = [
                    'Content-Type' => 'application/json'
                    //'Authorization' => Auth::id()
                ];
        $data = array(
                "batchid"=> $login[0]->batch_id,
                );        
        try{

            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,


            ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);


            if($data['rc']=='200' || $data['rc']==0){
                DB::table('master_data_ppdpp_kuota')
                ->where('kode', 1)
                ->update(['kuota_unit' => $data['data']['quota_unit'] ,'updated_at'=>date('Y-m-d H:i:s')]);

                return response()->json([
                        'rc' => 1,
                        'rm' => "Data Berhasil Diupdate"
                    ]);

            }else{

                return response()->json([
                        'rc' => 2,
                        'rm' => $data['rm']
                    ]);

            }

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());

            return response()->json([
                    'rc' => 2,
                    'rm' => $response->rm
                ]);

        } 

    }

     public function updatedata(){
        $login=\DB::select("select * from logins");
        $url = env('API_BASE_URL_UTIL')."sync-nasabah";
        $client = new Client();
        $headers = [
                    'Content-Type' => 'application/json'
                    //'Authorization' => Auth::id()
                ];
        try{

            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,


            ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);


            if($data['rc']=='200' || $data['rc']==0){
               
                return response()->json([
                        'rc' => 1,
                        'rm' => "Data Berhasil Diupdate"
                    ]);

            }else{

                return response()->json([
                        'rc' => 2,
                        'rm' => $data['rm']
                    ]);

            }

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());

            return response()->json([
                    'rc' => 2,
                    'rm' => $response->rm
                ]);

        } 

    }


    public function login_adapter(Request $request){

        $url = env('API_BASE_URL_UTIL')."login";
        $client = new Client();
        $headers = [
                    'Content-Type' => 'application/json'
                    //'Authorization' => Auth::id()
                ];
         $data = array(
                'password'=>$request->input('password')
                );        

        try{

            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,


            ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);


            if($data['rc']=='200' || $data['rc']==0){
               
                return response()->json([
                        'rc' => 1,
                        'rm' => $data['rm']
                    ]);

            }else{

                return response()->json([
                        'rc' => 2,
                        'rm' => $data['rm']
                    ]);

            }

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());

            return response()->json([
                    'rc' => 2,
                    'rm' => $response->rm
                ]);

        } 

    }

     public function change_adapter(Request $request){

        $url = env('API_BASE_URL_UTIL')."change-password";
        $client = new Client();
        $headers = [
                    'Content-Type' => 'application/json'
                    //'Authorization' => Auth::id()
                ];
         $data = array(
                'batchId'=>$request->input('password_lama'),
                'passwordlama'=>$request->input('password_baru'),
                'passwordbaru'=>$request->input('password_lama')
                );        

        try{

            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,


            ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);


            if($data['rc']=='200' || $data['rc']==0){
               
                return response()->json([
                        'rc' => 1,
                        'rm' => $data['rm']
                    ]);

            }else{

                return response()->json([
                        'rc' => 2,
                        'rm' => $data['rm']
                    ]);

            }

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());

            return response()->json([
                    'rc' => 2,
                    'rm' => $response->rm
                ]);

        } 

    }

    public function logout_adapter(Request $request){

        $url = env('API_BASE_URL_UTIL')."logout";
        $client = new Client();
        $headers = [
                    'Content-Type' => 'application/json'
                    //'Authorization' => Auth::id()
                ];
         $data = array(
                'batchid'=>$request->get('id')
                );        

        try{

            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,


            ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);


            if($data['rc']=='200' || $data['rc']==0){
               
                return response()->json([
                        'rc' => 1,
                        'rm' => $data['rm']
                    ]);

            }else{

                 DB::table('logins')
                ->where('id', 1)
                ->update(['batch_id' => '']); 

                return response()->json([
                        'rc' => 2,
                        'rm' => $data['rm']
                    ]);

            }

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());

            DB::table('logins')
            ->where('id', 1)
            ->update(['batch_id' => '']); 


            return response()->json([
                    'rc' => 2,
                    'rm' => $response->rm
                ]);

        } 

    }


    public function getmaster(Request $request){

        $login=\DB::select("select * from logins");
        $url = env('API_BASE_URL_UTIL')."get-master";
        $client = new Client();
        $headers = [
                    'Content-Type' => 'application/json'
                    //'Authorization' => Auth::id()
                ];
         $data = array(
                "tahunbulan" => $request->input('tahun')."-".$request->input('bulan'),
                'batchid'=>$login[0]->batch_id
                );        

        try{

            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,


            ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);


            if($data['rc']=='200' || $data['rc']==0){
               
                return response()->json([
                        'rc' => 0,
                        'rm' => $data['rm']
                    ]);

            }else{

                return response()->json([
                        'rc' => 2,
                        'rm' => $data['rm']
                    ]);

            }

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());

            return response()->json([
                    'rc' => 2,
                    'rm' => $response->rm
                ]);

        } 

    }


    public function kecamatan($id){
        $kec= \DB::select("SELECT * FROM wilayah where kode like'".$id."%' and kode not in ('".$id."') and kode=SUBSTR(kode,1,6)");

        return json_encode($kec);
    }

     public function kelurahan($id){
        $kel= \DB::select("SELECT * FROM wilayah where kode like'".$id."%' and kode not in ('".$id."')");

        return json_encode($kel);
    }

     public function perumahan($id){
        $kel= \DB::select("SELECT * FROM ref_residence where id_developer =".$id);

        return json_encode($kel);
    }

    public function apikelurahan($id){

        // dd($id);
    //    $data = array(  
    //         "namaNasabah"=> $request->input('nama_nasabah')
    //     );

       
        $url = env('API_BASE_URL')."master/list/location?q=".$id;
         
        $client = new Client();
        $headers = [
                // 'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuZmxwcC5iYXN5cy5jby5pZFwvXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU1NzE5OTU5MywiZXhwIjoxNTU3MjAzMTkzLCJuYmYiOjE1NTcxOTk1OTMsImp0aSI6IkdGdUxocUxBSkNyYTgwSlIiLCJzdWIiOjUsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEiLCJpZCI6NSwidXNlcm5hbWUiOiJhb2Jla2FzaSIsImlkX2NoYW5uZWwiOm51bGwsImlkX2JyYW5jaCI6NiwibG9naW5fdHlwZSI6MCwidXNlcl9ncm91cF9pZCI6MSwiYnJhbmNoX3R5cGVfaWQiOjJ9.N_ta3S4_yEHZtIfbbUc1pLj6KM3uHmui0mjI2EFrjN4',
                'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
           
            return json_encode($data_result);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }


        // return json_encode($kec);
    }

     public function apikelurahan1(Request $request){

        // dd($id);
    //    $data = array(  
    //         "namaNasabah"=> $request->input('nama_nasabah')
    //     );

       
        $url = env('API_BASE_URL')."master/list/location?q=".$request->get('id');
         
        $client = new Client();
        $headers = [
                // 'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuZmxwcC5iYXN5cy5jby5pZFwvXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU1NzE5OTU5MywiZXhwIjoxNTU3MjAzMTkzLCJuYmYiOjE1NTcxOTk1OTMsImp0aSI6IkdGdUxocUxBSkNyYTgwSlIiLCJzdWIiOjUsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEiLCJpZCI6NSwidXNlcm5hbWUiOiJhb2Jla2FzaSIsImlkX2NoYW5uZWwiOm51bGwsImlkX2JyYW5jaCI6NiwibG9naW5fdHlwZSI6MCwidXNlcl9ncm91cF9pZCI6MSwiYnJhbmNoX3R5cGVfaWQiOjJ9.N_ta3S4_yEHZtIfbbUc1pLj6KM3uHmui0mjI2EFrjN4',
                'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
           
            return json_encode($data_result);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }


        // return json_encode($kec);
    }
    
     public function dashboard(Request $request){
        $url = env('API_BASE_URL')."master/dashboard";
         
        $client = new Client();
        $headers = [
                'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param1=[];
            $param1= (string) $result->getBody();
            $data_result = json_decode($param1, true);
            $param['newentry']=$data_result['data']['processStatus'][0]['count'];
            $param['newprospect']=$data_result['data']['processStatus'][1]['count'];
            $param['newcustomer']=$data_result['data']['processStatus'][2]['count'];
            $param['appraisal']=$data_result['data']['processStatus'][3]['count'];
            $param['analisa']=$data_result['data']['processStatus'][4]['count'];
            $param['approval']=$data_result['data']['processStatus'][5]['count'];
            $param['approved']=$data_result['data']['processStatus'][6]['count'];
            $param['cair']=$data_result['data']['processStatus'][7]['count'];

            return $param;

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }
    }

}
