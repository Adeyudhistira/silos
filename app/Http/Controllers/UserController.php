<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserModel;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $param['data'] = \DB::select("SELECT * FROM users");
        if ($request->ajax()) {
            $view = view('user.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'user.index',$param);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master.master')->nest('child', 'user.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
    }

     public function tambah(Request $request)
    {
        if ($request->input('type') == "normal") {
            $datan = new UserModel();
        }else{
            $datan = UserModel::find($request->input('id'));
        }
         $datan->created_date = date('Y-m-d');

         $datan->u_first_name = $request->input('nama');
         $datan->u_last_name = $request->input('nama');

         $datan->username = $request->input('username');
         $datan->email = $request->input('email');
         $datan->id_ktp = $request->input('ktp');
         $datan->u_phone_no = $request->input('phone');

         if ($request->input('password')!= "") {
             $datan->password = Hash::make($request->input('password'));
         }
         
         $datan->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $param['data'] = collect(\DB::select("SELECT * FROM users where id = '".$id."'"))->first();

        return view('master.master')->nest('child', 'user.form',$param);
    }

    public function delete($id)
    {
        $data = UserModel::find($id);
        $data->delete();

        // return json_encode(['rc' => 0, 'rm' => 'Data Berhasil']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
