<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ResidenceModel;
use Illuminate\Support\Facades\Hash;
class ResidenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $param['data'] = \DB::select("SELECT * FROM ref_residence");
        if ($request->ajax()) {
            $view = view('residence.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'residence.index',$param);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master.master')->nest('child', 'residence.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
    }

     public function tambah(Request $request)
    {
        $get = collect(\DB::select("SELECT max(id::int) as max_id FROM ref_residence"))->first();

        if ($request->input('type') == "normal") {
            $datan = new ResidenceModel();
            $datan->id = $get->max_id+1;

        }else{
            $datan = ResidenceModel::find($request->input('id'));
        }
      
         $datan->id_developer = $request->input('id_developer');
         $datan->residence_name = $request->input('residence_name');
         $datan->wilayah = $request->input('wilayah');
         $datan->jumlah_unit = $request->input('jumlah_unit');
         $datan->harga_unit = $request->input('harga_unit');
         $datan->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $param['data'] = collect(\DB::select("SELECT * FROM ref_residence where id = '".$id."'"))->first();

        return view('master.master')->nest('child', 'residence.form',$param);
    }

    public function delete($id)
    {
        $data = ResidenceModel::find($id);
        $data->delete();

        // return json_encode(['rc' => 0, 'rm' => 'Data Berhasil']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
