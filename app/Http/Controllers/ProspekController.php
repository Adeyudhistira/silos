<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Yajra\Datatables\Datatables;
use Response;
use Hash;
use Auth;
use Session;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use App\Http\Requests;
use Illuminate\Support\Collection;
use App\ProspekModel;
use DateTime;
use Crypt;
use File;
use Redirect;
//use Image;

class ProspekController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     public function entry_prospek_view(Request $request)
    {
        $query='';
        $harga='';
        $uang_muka='';
        $idprod='';
        $tgl='';
        $query_durasi='';
         if($request->has('simulasi')){
                $harga=$request->input('harga');
                $uang_muka=$request->input('uang_muka');
                $idprod=$request->input('prod');
                $tgl=$request->input('tgl');
                $flt=true;
                 $url = env('API_BASE_URL')."master/simulasi-angsuran";
                 $client = new Client();
                 $data = array(
                    "productId" => $request->input('prod'),
                    "hargaObjek" => str_replace(".","", $request->input('harga')),
                    "uangMuka" => str_replace(".","", $request->input('uang_muka'))
                );
                 $headers = [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '. session('token'),
                    //'Authorization' => Auth::id()
                ];
                try{
                    $result = $client->post($url,[
                        RequestOptions::HEADERS => $headers,
                        RequestOptions::JSON => $data,


                    ]);
                    $param=[];
                    //$param= (string) $result->getBody();
                    //$data = json_decode($param, true);
                    $result = json_decode($result->getBody());
                    //$query=$data;
                    $biday = new DateTime($tgl);
                    $today = new DateTime();
                    $diff = $today->diff($biday);

                    $umur=$diff->y;
                    $max=50;
                    $durasi= $max - $umur;
                    if($umur > 20){

                        $query=new Collection($result->data);
                        $query_durasi = $query->filter(function($pos,$key)use ($durasi){
                            return $pos->tahun <= $durasi;
                        });
                    }else{
                        $query_durasi='';
                    }
                    //return json_encode($query);
                    //return $this->sendResponse('1','Input Billing berhasil',json_decode($result->getBody()));
                }catch (BadResponseException $e){
                    $response = json_decode($e->getResponse()->getBody());
                    $query=new Collection($response);
                  //  return json_encode($query);
                   //return $this->responseData($e->getCode(), $response);
                } 
            }
            
            $param['data']=$query_durasi;
            $param['harga']=$harga;
            $param['uang_muka']=$uang_muka;
            $param['idprod']=$idprod;
            $param['tgl']=$tgl;

        if ($request->ajax()) {
            $view = view('prospek.entry_prospek',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'prospek.entry_prospek',$param);
    }

     public function data_prospek_view(Request $request)
    {
        $product = \DB::select("select * from ref_product where id='".$request->get('idproduk')."'");
        $chanel = \DB::select("select * from channel");
        $job = \DB::select("select * from ref_pekerjaan order by id_pekerjaan asc");
        $dati2 = \DB::select("select * from dati2 order by kode_dati2 asc");
        $cabang = \DB::select("select * from branch order by id asc");

        $param['cabang']=$cabang;
        $param['dati2']=$dati2;
        $param['job']=$job;
        $param['chanel']=$chanel;
        $param['product']=$product;
        $param['harga']=$request->get('harga');
        $param['uangmuka']=$request->get('uangmuka');
        $param['tgl']=$request->get('tgl');
        $param['jangka']=$request->get('jangka');
        $param['angsuran']=$request->get('angsuran');
        if ($request->ajax()) {
            $view = view('prospek.data_prospek',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'prospek.data_prospek',$param);
    }

     public function daftar_prospek_view(Request $request)

    {   
        /*
        $param['data'] = \DB::select("select mp.id,mp.nm_prospek,mp.alamat,p.prod_name,b.branch_name,mp.jangka_waktu,mp.total_angsuran from master_prospek mp 
            left join ref_product p on p.id=mp.id_product 
            left join branch b on b.id_branch=mp.branch_id");
        */
            
        $url = env('API_BASE_URL')."master/prospek";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
           $data =$data['data'];
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        

        $param['data']=$data;
        
        if ($request->ajax()) {
            $view = view('prospek.daftar_prospek_baru',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'prospek.daftar_prospek_baru',$param);
    }


    public function daftar(Request $request)

    {   
        $id=$request->input('id');

        


        if($id){
            $gaji=str_replace(".","", $request->input('gaji'));
            if($gaji>4000001){

                return redirect('ubah_prospek?id='.$id)->with('error','Gaji anda melebihi peraturan PPDPP');

            }

            $url = env('API_BASE_URL')."master/prospek/".$id;
            $data = array(
            "channelId"=> $request->input('chanel'),
            "nmProspek"=> ucfirst($request->input('nama_pelanggan')),
            "noIdentitas"=> $request->input('noktp'),
            "tempatLahir"=> ucfirst($request->input('tmp_lahir')),
            "tglLahir"=> date('Y-m-d',strtotime($request->input('tgl_lahir'))),
            "alamat"=> ucfirst($request->input('alamat')),
            "idWilayah"=> $request->input('lokasiagunan'),
            
            "telp"=> $request->input('no_telp'),
            "npwp"=> $request->input('npwp'),
            "isMenikah"=> $request->input('sm'),
            "namaPasangan"=> $request->input('nmpasangan'),
            "noKtpPasangan"=> $request->input('ktppasangan'),
            "aoNote"=> $request->input('aonote'),
            "productId"=> $request->input('idprod'),
            "pekerjaanId"=> $request->input('job'),
            "officeName"=> ucfirst($request->input('nm_kantor')),
            "officeAddress"=> ucfirst($request->input('alamat_kantor')),
            "officePhone"=> ucfirst($request->input('no_telp_kantor')),
            "pendapatanBulan"=> str_replace(".","", $request->input('gaji')),
            "pendapatanLainnya"=> str_replace(".","", $request->input('pendapatan')),
            "hargaObject"=> str_replace(".","", $request->input('harga_objek')),
            "uangMuka"=> str_replace(".","", $request->input('uang_muka')),
            "jangkaWaktu"=> $request->input('jangka_waktu'),
            "totalAngsuran"=> str_replace(".","", $request->input('angsuran')),
            "idJenisKelamin"=>$request->input('jk'),
            "sumberPendapatanId"=>$request->input('sumber_pendapatan')
        );
        }else{

            $gaji=str_replace(".","", $request->input('gaji'));
            if($gaji>4000001){

                return redirect('data_prospek?idproduk='.$request->input('idprod').'&&harga='.str_replace(".","", $request->input('harga_objek')).'&&uangmuka='.str_replace(".","", $request->input('uang_muka')).'&&tgl='.$request->input('tgl_lahir').'&&jangka='.$request->input('jangka_waktu').'&&angsuran='.str_replace(".","", $request->input('angsuran')))->with('error','Gaji anda melebihi peraturan PPDPP');

            }

            


            $url = env('API_BASE_URL')."master/prospek";
            $data = array(
            "channelId"=> $request->input('chanel'),
            "nmProspek"=> ucfirst($request->input('nama_pelanggan')),
            "noIdentitas"=> $request->input('noktp'),
            "tempatLahir"=> ucfirst($request->input('tmp_lahir')),
            "tglLahir"=> date('Y-m-d',strtotime($request->input('tgl_lahir'))),
            "alamat"=> ucfirst($request->input('alamat')),
            "idWilayah"=> $request->input('lokasiagunan'),
            "telp"=> $request->input('no_telp'),
            "npwp"=> $request->input('npwp'),
            "isMenikah"=> $request->input('sm'),
            "namaPasangan"=> $request->input('nmpasangan'),
            "noKtpPasangan"=> $request->input('ktppasangan'),
            "aoNote"=> $request->input('aonote'),
            "productId"=> $request->input('idprod'),
            "pekerjaanId"=> $request->input('job'),
            "officeName"=> ucfirst($request->input('nm_kantor')),
            "officeAddress"=> ucfirst($request->input('alamat_kantor')),
            "officePhone"=> ucfirst($request->input('no_telp_kantor')),
            "pendapatanBulan"=> str_replace(".","", $request->input('gaji')),
            "pendapatanLainnya"=> str_replace(".","", $request->input('pendapatan')),
            "hargaObject"=> str_replace(".","", $request->input('harga_objek')),
            "uangMuka"=> str_replace(".","", $request->input('uang_muka')),
            "jangkaWaktu"=> $request->input('jangka_waktu') * 12,
            "totalAngsuran"=> str_replace(".","", $request->input('angsuran')),
            "idJenisKelamin"=>$request->input('jk'),
            "sumberPendapatanId"=>$request->input('sumber_pendapatan')
        );
        }

        
        $client = new Client();
        
 
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            if($id){
                $result = $client->put($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
                ]);
            }else{
                $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
                ]);
            }
            


            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
        //    dd($param1);
            if($data['rc']=='200'){
                // dd($data['data']);
                $id=$data['data']['id'];
                return redirect('dokumen?id='.$id)->with('success','Data Prospek Berhasil disimpan');


            }else{

                return redirect('daftar_prospek_baru')->with('error',$data['rm']);
            }
            
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           return redirect('daftar_prospek_baru')->with('error',$response->rm);
        }
 
        /*

        $prospek = new ProspekModel();
        $prospek->id_channel = $request->input('chanel');
        $prospek->jns_kelamin = $request->get('jk');
        $prospek->nm_prospek = ucfirst($request->input('nama_pelanggan'));
        $prospek->alamat = ucfirst($request->input('alamat'));
        $prospek->no_identitas = $request->input('noktp');
        $prospek->tgl_lahir = date('Y-m-d',strtotime($request->input('tgl_lahir')));
        $prospek->pendapatan_bulan = str_replace(".","", $request->input('gaji'));
        $prospek->telp = $request->input('no_telp');
        $prospek->tempat_lahir = ucfirst($request->input('tmp_lahir'));
        $prospek->id_product = $request->input('idprod');
        $prospek->total_angsuran = str_replace(".","", $request->input('angsuran')) ;
        $prospek->jangka_waktu = $request->input('jangka_waktu');
        $prospek->branch_id = $request->input('branch');
        $prospek->harga_object = str_replace(".","", $request->input('harga_objek'));
        $prospek->uang_muka = str_replace(".","", $request->input('uang_muka'));
        $prospek->pendapatan_lainnya = str_replace(".","", $request->input('pendapatan'));
        $prospek->id_pekerjaan = $request->input('job');
        $prospek->city_id = $request->input('kota');
        $prospek->province_id = $request->input('provinsi');
        $prospek->office_name = ucfirst($request->input('nm_kantor'));
        $prospek->office_address = ucfirst($request->input('alamat_kantor'));
        $prospek->office_phone = ucfirst($request->input('no_telp_kantor'));
        //ref status pembiayaan
        $prospek->status_pembiayaan = 0;
        
        if ($prospek->save()) {
            return redirect('daftar_prospek_baru')->with('success','Data Berhasil Disimpan');
        } else {
            return Redirect::route('daftar_prospek_baru')->withInput()->with('error', 'Data Gagal Disimpan');
        }
        */

    }

    public function ubah_prospek(Request $request)
    {
        $id=$request->get('id');
        
        $chanel = \DB::select("select * from channel");
        $job = \DB::select("select * from ref_pekerjaan order by id_pekerjaan asc");
        $cabang = \DB::select("select * from branch order by id asc");
     
        $param['cabang']=$cabang;
        $param['job']=$job;
        $param['chanel']=$chanel;


       $url = env('API_BASE_URL')."/master/prospek/".$id."/show";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{

            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
            ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        

        $param['data']=$data;

        if ($request->ajax()) {
            $view = view('prospek.ubah_prospek',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'prospek.ubah_prospek',$param);
    }


    public function kirim_pengajuan(Request $request)
    {
        $id=$request->get('id');
        $cabang = \DB::select("select * from branch order by id asc");
        $param['id'] = $id;
        $param['cabang']=$cabang;
        if ($request->ajax()) {
            $view = view('prospek.kirim_pengajuan',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'prospek.kirim_pengajuan',$param);
    }


    public function pengajuan(Request $request)
    {
        DB::table('master_prospek')
            ->where('id', $request->input('id'))
            ->update(['branch_id' => $request->input('branch')]);
        return redirect('daftar_prospek_baru')->with('success','Berhasil');
    }

    public function dokumen(Request $request){
        /*
        $data = \DB::select("select b.description,a.* from master_dok_prospek a
        join ref_jenis_dokumen b
        on b.id=a.tipe_dok where a.id_prospek=".$request->get('id'));
        */

        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        $client = new Client();

        $url4 = env('API_BASE_URL')."master/prospek/".$request->get('id')."/show";
        try{
            $result4 = $client->get($url4,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param5=[];
            $param5= (string) $result4->getBody();
            $data4 = json_decode($param5, true);
            $data4=$data4['data'];
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }

        $param['id']=$request->get('id');
        $param['data']=$data4;
        if ($request->ajax()) {
            $view = view('prospek.dokumen',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'prospek.dokumen',$param);
    }

    public function tambah_dokumen(Request $request){
        $param['id']=$request->get('id');

        $url = env('API_BASE_URL')."master/list/jdoc?statusPembiayaan=0";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }

        $param['data'] = $data;
        // dd($data);
        if ($request->ajax()) {
            $view = view('prospek.tambah_dokumen',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'prospek.tambah_dokumen',$param);
    }

    public function insert_dok(Request $request){
        $id=$request->input('id');
        //$key_upload = $request->input('key_file');
      // $this->validate($request, [
        //    'dok' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:1024'
         //]);

        $validators = \Validator::make($request->all(),['dok' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:1024']);
        if($validators->fails()){
           //dd($validators->errors()->messages()['dok'][0]);
            //return Redirect::back()->withErrors($validators);
            return redirect('dokumen?id='.$id)->with('error',$validators->errors()->messages()['dok'][0]);
        }
        $media = $request->file('dok');
        $image_mime = $media->getmimeType();
        $fileContent = File::get($media);

         $url = env('API_BASE_URL')."master/prospek-doc";
         $client = new Client();

       try {
            $result = $client->post(
                $url, [
                    'headers' => [
                        'Authorization'         =>  'Bearer '.session('token')
                    ],
                    'multipart' => [
                        [
                            'name'     => '_file',
                            'contents' => $fileContent,
                            'filename' => 'tes',
                            'Mime-Type'=> $image_mime,
                        ],
                        [
                            'name'     => 'prospek_id',
                            'contents' => $id,
                        ],
                        [
                            'name'     => 'doc_type',
                            'contents' => $request->input('idjenis'),
                        ],
                    ],
                ]
            );

             $param=[];
             $param= (string) $result->getBody();
             $data_result = json_decode($param, true);

            if($data_result['rc']=='200'){
                return redirect('dokumen?id='.$id)->with('success',$data_result['rm']);
            }else{
                return redirect('dokumen?id='.$id)->with('error',$data_result['rm']);
            }
        } catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            //$bad_response = $this->responseData($e->getCode(), $response);
            //dd($response);
            return redirect('dokumen?id='.$id)->with('error',$response->rm);
        }
    }

    public function edit_dokumen(Request $request){
        $data = \DB::select("select * from master_dok_prospek where id=".$request->get('id'));
        $param['id']=$request->get('id');
        $param['data']=$data;
        if ($request->ajax()) {
            $view = view('prospek.edit_dokumen',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'prospek.edit_dokumen',$param);
    }

    public function update_dok(Request $request){
        $id=$request->input('id');
        $idprospek=$request->input('idprospek');
        //$key_upload = $request->input('key_file');
        $validators = \Validator::make($request->all(),['dok_file' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:1024']);
        if($validators->fails()){
           //dd($validators->errors()->messages()['dok'][0]);
            //return Redirect::back()->withErrors($validators);
            return redirect('dokumen?id='.$idprospek)->with('error',$validators->errors()->messages()['dok_file'][0]);
        }
        //$originalImage= $request->file('dok_file');
        //$thumbnailImage = Image::make($originalImage);
        //$thumbnailImage->resize(150,150);
        //$thumbnailImage->save();
       
        $media = $request->file('dok_file');
        $image_mime = $media->getmimeType();
        $fileContent = File::get($media);

         $url = env('API_BASE_URL')."master/prospek-doc/update";
         $client = new Client();

       try {
            $result = $client->post(
                $url, [
                    'headers' => [
                        'Authorization'         =>  'Bearer '.session('token')
                    ],
                    'multipart' => [
                        [
                            'name'     => '_file',
                            'contents' => $fileContent,
                            'filename' => 'tes',
                            'Mime-Type'=> $image_mime,
                        ],
                        [
                            'name'     => 'id',
                            'contents' => $id,
                        ],
                    ],
                ]
            );


             $param=[];
             $param= (string) $result->getBody();
             $data_result = json_decode($param, true);
            if($data_result['rc']=='200'){
                return redirect('dokumen?id='.$idprospek)->with('success',$data_result['rm']);
            }else{
                return redirect('dokumen?id='.$idprospek)->with('error',$data_result['rm']);
            }
        } catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            dd($response);
            return redirect('dokumen?id='.$idprospek)->with('error',$response->error);
        }
    }
   
}
