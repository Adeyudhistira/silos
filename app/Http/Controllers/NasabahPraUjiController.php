<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use App\Http\Requests;
use Auth;

class NasabahPraUjiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request){
        $url = env('API_BASE_URL')."master/nasabah/status?page=".$request->get('page')."&size=".$request->get('size');
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];

        $data = array(
            "userId"=> Auth::user()->id,
            "branchId"=> Auth::user()->id_branch,
            "statusPembiayaan"=> [6]
        );
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            if($data1['rc']==200){
                $data =$data1['data'];
                $rc=$data1['rc'];
                $rm='';
            }else{
                $data ='';
                $rc=$data1['rc'];
                $rm='';
            }
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           
            $data=$response;
            $rc=$response->rc;
            $rm=$response->rm;
        }
        
        $param['data']=$data;
        $param['rc']=$rc;
        $param['rm']=$rm;

        if ($request->ajax()) {
            $view = view('nasabah_pra_uji.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_pra_uji.index',$param);

    }

    public function edit_nasabah(Request $request)
    {
        $id=$request->get('id');
         $param['data']  = collect(\DB::select("SELECT *,mn.id as id_nas,mp.id as id_mp, maap.id as id_maap FROM master_nasabah mn
        join master_pembiayaan mp on mp.nasabah_id = mn.id
        join master_agunan_property maap on maap.id_pembiayaan = mp.id where mn.id = '".$id."'"))->first();

        if ($request->ajax()) {
            $view = view('nasabah_approve.edit_nasabah',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_approve.edit_nasabah',$param);


    }

      public function slik(Request $request)
    {
        $url = env('API_BASE_URL')."master/bi-check/pembiayaan/".$request->get('id');
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
           $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }

         $url1 = env('API_BASE_URL')."master/list/bicheck-status";
        try{
            
            $result1 = $client->get($url1,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param2=[];
            $param2= (string) $result1->getBody();
            $data1 = json_decode($param2, true);
           $data1 =$data1['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data1=$response;
        }


        $param['data1']=$data1;

        $param['data']=$data;

        if ($request->ajax()) {
            $view = view('nasabah_pra_uji.slik',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_pra_uji.slik',$param);
    }

      public function appraisal(Request $request)
    {
        
        $url = env('API_BASE_URL')."master/agunan/pembiayaan/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $pembiayaan =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $pembiayaan=$response;
        }
        if($pembiayaan){

            $url1 = env('API_BASE_URL')."master/agunan-property/agunan/".$pembiayaan[0]['id'];
            try{
                
                $result1 = $client->get($url1,[
                    RequestOptions::HEADERS => $headers,
                    ]);
                
                
                $param2=[];
                $param2= (string) $result1->getBody();
                $data1 = json_decode($param2, true);
                $property =$data1['data'];

            }catch (BadResponseException $e){
                $response1 = json_decode($e->getResponse()->getBody());
                $property=$response1;
            }
           

        }
        


        $param['pembiayaan']=$pembiayaan;
        $param['property']=$property;
        if ($request->ajax()) {
            $view = view('nasabah_pra_uji.appraisal',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_pra_uji.appraisal',$param);
    }

      public function analisa(Request $request)
    {
        $url = env('API_BASE_URL')."master/pembiayaan/analisis/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1['data'];
            $rc=$data1['rc'];
            $rm=$data1['rm'];
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
            $rc=$data->rc;
            $rm=$data->rm;
        }

        $param['rc']=$rc;
        $param['rm']=$rm;
        $param['data']=$data;
        $param['idpembiayaan']=$request->get('id');    
        if ($request->ajax()) {
            $view = view('nasabah_pra_uji.analisa',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_pra_uji.analisa',$param);
    }

        public function proses_approval(Request $request)
    {
        $url = env('API_BASE_URL')."master/pembiayaan/analisis/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1['data'];
            $rc=$data1['rc'];
           $rm=$data1['rm'];
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
            $rc=$data->rc;
            $rm=$data->rm;
        }

        $param['rc']=$rc;
        $param['rm']=$rm;
        $param['data']=$data;
        $param['idpembiayaan']=$request->get('id');    
        if ($request->ajax()) {
            $view = view('nasabah_pra_uji.proses_approval',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_pra_uji.proses_approval',$param);
    }

      public function sla(Request $request)
    {
        $url = env('API_BASE_URL')."master/pembiayaan/log/".$request->get('id');
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        $param['data']=$data;

        if ($request->ajax()) {
            $view = view('nasabah_pra_uji.sla_detail',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_pra_uji.sla_detail',$param);
    }

      public function ppdpp(Request $request)
    {
        $url = env('API_BASE_URL')."master/agunan-property/agunan/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        $param['data']=$data;
        if ($request->ajax()) {
            $view = view('nasabah_pra_uji.check_ppdpp',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_pra_uji.check_ppdpp',$param);
    }


      public function detail_property(Request $request)
    {
        $url = env('API_BASE_URL')."master/agunan-property/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        $param['data']=$data;
        if ($request->ajax()) {
            $view = view('nasabah_pra_uji.detail_property',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_pra_uji.detail_property',$param);
    }

    public function update_property(Request $request){
        $id=$request->input('id');
        $idpembiayaan=$request->input('id_pembiayaan');
        $url = env('API_BASE_URL')."master/agunan-property/".$id;
        $client = new Client();
        $data = array(
            "noSertifikat"=> $request->input('no_sertifikasi'),
            "nmPemilikSertifikat"=> $request->input('nama_pemilik'),
            "luasTanahFisik"=> $request->input('luas_tanah'),
            "luasImb"=> $request->input('luas_bangunan'),
            "nilaiAppraisal"=> str_replace(".","", $request->input('nilai')),
            "alamat"=> $request->input('alamat'),
            "idKotaKab"=> $request->input('kota'),
            "idKecamatan"=> $request->input('kec'),
            "idKelurahan"=> $request->input('kel'),
            "namaDeveloper"=> $request->input('nama_developer')
        );
 
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            $result = $client->put($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
                ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
           
            if($data['rc']=='200'){
                return redirect('appraisal?id='.$idpembiayaan)->with('success',$data['rm']);
            }else{
                return redirect('appraisal?id='.$idpembiayaan)->with('error',$data['rm']);
            }
            
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());

           return redirect('appraisal?id='.$idpembiayaan)->with('error',$response->error);
        }
    }

    public function update_ppdpp(Request $request){
        $check = [];
        $query=$request->subParmId;
        foreach ($query as $item){
            array_push($check,['subParmId' => $item,'valueParmId' => $request->input('valueParmId_'.$item)]);
        }
        $id=$request->input('idAgunanProperty');
        $url = env('API_BASE_URL')."master/agunan-property/checklist/".$id;
        $client = new Client();
        $parsing = json_encode($check);
        $data = array(
            "checklist"=> $check
            ); 
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            $result = $client->put($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
                ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
           
            if($data['rc']=='200'){
                return redirect('nasabah_pra_uji')->with('success',$data['rm']);
            }else{
                return redirect('nasabah_pra_uji')->with('error',$data['rm']);
            }
            
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           return redirect('nasabah_pra_uji')->with('error',$response->error);
        }
    }

    public function update_analis(Request $request){
        
        $check = [];
        $check1 = [];
        $query=$request->idSubGroup;
        foreach ($query as $item){
            array_push($check,['idGroup' => $request->input('idGroup_'.$item),'idSubGroup' => $item,'value'=>$request->input('nilai_'.$item)]);
        }
        $query1=$request->idSubGroup1;
        foreach ($query1 as $item){
            array_push($check1,['idGroup' => $request->input('idGroup1_'.$item),'idSubGroup' => $item,'value'=>$request->input('nilai1_'.$item)]);
        }
        $id=$request->input('idAgunanProperty');
        $url = env('API_BASE_URL')."master/pembiayaan/analisis";
        $client = new Client();
        $parsing = json_encode($check);
        $data = array(
            "pembiayaanId"=>$request->input('idpembiayaan'),
            "idProduct"=>$request->input('idProduct'),
            "riti"=>$request->input('riti'),
            "collCover"=>$request->input('coll_cover'),
            "ratio"=> $check,
            "nonRatio"=> $check
            ); 
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
                ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
           
            if($data['rc']=='200'){
                return redirect('nasabah_pra_uji')->with('success',$data['rm']);
            }else{
                return redirect('nasabah_pra_uji')->with('error',$data['rm']);
            }
            
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           return redirect('nasabah_pra_uji')->with('error',$response->error);
        }   
    }

    public function kirim_approve (Request $request){
       
        $param['combo'] = \DB::select(" SELECT * FROM users where user_group_id=17 and id_branch=0");
        $param['id'] = $request->get('id');
         if ($request->ajax()) {
            $view = view('nasabah_pra_uji.kirim',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_pra_uji.kirim',$param);   
    }

    public function cetak_sp4k(Request $request){
        $id=$request->get('id');
        $param['id']=$id;
        if ($request->ajax()) {
            $view = view('nasabah_pra_uji.sp3k',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_pra_uji.sp3k',$param);
    }

    public function update_sp4k(Request $request){
        DB::table('master_pembiayaan')
            ->where('id', $request->input('id'))
            ->update([
                'no_sp3k' => $request->input('sp3k'),
                'tgl_sp3k' => date('Y-m-d',strtotime($request->input('tgl')))
            ]);

            return redirect('nasabah_pra_uji')->with('success','Berhasil Create SP4k');

    }
    public function kirim_data(Request $request){
        $url = env('API_BASE_URL')."master/pembiayaan/next-wf/".$request->input('id');
        $client = new Client();
        $headers = [
               'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->put($url,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
           

            $users = DB::table('master_pembiayaan')->where('id',$request->input('id'))->get();
            DB::table('master_pembiayaan')
            ->where('id', $request->input('id'))
            ->update(['next_proc_uid' => $request->input('iduser')]);

            $cek_log = DB::table('master_log_workflow')
            ->where('id_pembiayaan',$request->get('id'))
            ->where('status_pembiayaan',6)
            ->Orderby('created_at','DESC')
            ->get();

            if($cek_log){
            
            DB::table('master_log_workflow')
            ->where('id_pembiayaan', $request->get('id'))
            ->where('status_pembiayaan', 6)
            ->update([
                'end_at' => date('Y-m-d H:i:s'),
                'user_end_id' => $cek_log[0]->user_id
                ]);                   

            }

                DB::table('master_log_workflow')->insertGetId(
                        [
                            'id_pembiayaan' => $request->input('id'), 
                            'status_pembiayaan' => 7,
                            'created_at' => date('Y-m-d H:i:s')
                        ]
                    );  

               
            return redirect('nasabah_pra_uji')->with('success','Berhasil Kirim Data');

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
          
            return $bad_response;
        }

    }

    public function approve(Request $request){
         $url = env('API_BASE_URL')."master/pembiayaan/next-wf/".$request->input('id');
        $client = new Client();
        $headers = [
               'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->put($url,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
           

            $users = DB::table('master_pembiayaan')->where('id',$request->input('id'))->get();
            DB::table('master_pembiayaan')
            ->where('id', $request->input('id'))
            ->update(['next_proc_uid' => $users[0]->user_id]);

                DB::table('master_log_workflow')->insertGetId(
                        [
                            'id_pembiayaan' => $request->input('id'), 
                            'status_pembiayaan' => 6,
                            'created_at' => date('Y-m-d H:i:s'),
                            'end_at' => date('Y-m-d H:i:s'),
                            'user_id' => Auth::user()->id
                        ]
                    );  

            return $data_result;   
            
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
          
            return $bad_response;
        }
    }

    public function update_dks(Request $request){

        \DB::table('master_pembiayaan')->whereIn('nasabah_id', $request->get('value'))->update(['no_surat_dks' => $request->get('no_surat'),'tgl_surat_dks'=> date('Y-m-d',strtotime($request->get('tgl')))]);

        return json_encode(['rc' => 0, 'rm' => 'Data Berhasil']);
    }
      
}
