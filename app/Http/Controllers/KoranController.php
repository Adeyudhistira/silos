<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use DB;
use Response;
use Auth;
class KoranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    
        $query='';
        $tgl_awal='';
        $tgl_akhir='';
        $norek=''; 
        $flt=false;

        if($request->has('filter')){
            $flt=true;
            $tgl_awal=$request->get('tgl_awal');
            $tgl_akhir=$request->get('tgl_akhir');
            $norek=$request->get('no_rek');

            //$query=\DB::select("select * from master_transaction_ppdpp where no_rek='".$norek."' and tgl_mutasi between '". $tgl_awal ."' and '". $tgl_akhir ." 24:00:00' order by tgl_mutasi asc");

            $url = env('API_BASE_URL')."cbs/account/statement";
            $client = new Client();
            $data = array(
                "account_no"=> $norek,
                "start"=> date('Y-m-d',strtotime($tgl_awal)),
                "end"=> date('Y-m-d',strtotime($tgl_akhir))
            );
 
            $headers = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '. session('token')
            ];

            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
           
           $query=$data['data'];
         
            
            if($query){
                foreach ($query as $item) {
                        
                    $login=\DB::select("select * from logins");
                    $url = env('API_BASE_URL_ANGSURAN')."set-rekening-koran";
                    $client = new Client();
                    $data = array(
                        "batchid"=> $login[0]->batch_id,
                        "pin"=> "4254256575f3ccee1601f115d8a333551",
                        "norek"=> $item['ACCNBR'],
                        "norek_jenis"=>$item['DBCR'],
                        "no_arsip"=>$item['TXID'],
                        "tgl_mutasi"=>$item['TXDATE'],
                        "jenis_mutasi"=>$item['DBCR'],
                        "keterangan"=>$item['TXMSG'],
                        "nilai_mutasi"=>$item['TXAMT'],
                        "saldo"=>$item['SALDO']
                        );
                        
                        $headers = [
                            'Content-Type' => 'application/json'
                            //'Authorization' => Auth::id()
                        ];

                

                     try{

                            $result = $client->post($url,[
                                RequestOptions::HEADERS => $headers,
                                RequestOptions::JSON => $data,


                            ]);

                            $param1= (string) $result->getBody();
                            $data = json_decode($param1, true);
                             
                            
                        }catch (BadResponseException $e){
                            $response = json_decode($e->getResponse()->getBody());

                        } 
                    /*    
                    $param1= (string) $result->getBody();
                    $data = json_decode($param1, true);  

                    DB::table('master_transaction_ppdpp')
                        ->where('id', $item->id)
                        ->update(['rc_kemenpera' => $data['rc']]); 
                      */   

                }

            }

        }


        
        $param['flt']=$flt;
        $param['data']=$query;
        $param['tgl_awal']=$tgl_awal;
        $param['tgl_akhir']=$tgl_akhir;
        $param['norek']=$norek;

        if ($request->ajax()) {
            $view = view('rek_koran.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'rek_koran.index',$param);
    }

}
