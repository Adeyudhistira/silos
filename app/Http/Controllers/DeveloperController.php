<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DeveloperModel;
use Illuminate\Support\Facades\Hash;
class DeveloperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $param['data'] = \DB::select("SELECT * FROM ref_developer");
        if ($request->ajax()) {
            $view = view('developer.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'developer.index',$param);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master.master')->nest('child', 'developer.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
    }

     public function tambah(Request $request)
    {
        $get = collect(\DB::select("SELECT max(id::int) as max_id FROM ref_developer"))->first();

        if ($request->input('type') == "normal") {
            $datan = new DeveloperModel();
            $datan->id = $get->max_id+1;

        }else{
            $datan = DeveloperModel::find($request->input('id'));
        }
      
         $datan->developer_name = $request->input('developer_name');
         $datan->address = $request->input('address');
         $datan->akta_no = $request->input('akta_no');
         $datan->direktur_utama = $request->input('direktur_utama');
         $datan->siup = $request->input('siup');
         $datan->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $param['data'] = collect(\DB::select("SELECT * FROM ref_developer where id = '".$id."'"))->first();

        return view('master.master')->nest('child', 'developer.form',$param);
    }

    public function delete($id)
    {
        $data = DeveloperModel::find($id);
        $data->delete();

        // return json_encode(['rc' => 0, 'rm' => 'Data Berhasil']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
