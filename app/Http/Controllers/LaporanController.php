<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use Excel;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use App\NasabahDinamisModel;
use App\MyFunc;

class LaporanController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*
    public function __construct()
    {
        $this->middleware('auth');
    }
    */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
        public function index(Request $request)
    {
        /*
        $param['data']= \DB::select("select * from master_nasabah_dinamis where bulan_identitas = EXTRACT(MONTH FROM  now())  and tahun_identitas = EXTRACT(YEAR FROM  now())"); 
        */
        $bulan=date('m');
        $tahun=date('Y');
        $flt=false;
        $query= \DB::select("select d.*,n.no_identitas as ktp,p.id as idpembiayaan,n.id as idnasabah,n.nama_nasabah,p.harga_object,
        p.jangka_waktu,p.total_angsuran,p.plafon
        from master_pembiayaan p 
        left join master_nasabah n on p.nasabah_id=n.id
        left join master_nasabah_dinamis d on d.no_identitas=n.no_identitas where bulan_identitas = '".$bulan."'  and tahun_identitas = '".$tahun."'"); 

        if ($request->has('excel')) {
            $flt=true;
            $bulan=$request->get('bulan');
            $tahun=$request->get('tahun');
            $fields=[];
            $query1= \DB::select("select d.*,n.no_identitas as ktp,p.id as idpembiayaan,n.id as idnasabah,n.nama_nasabah,p.harga_object,
            p.jangka_waktu,p.total_angsuran,p.plafon
            from master_pembiayaan p 
            left join master_nasabah n on p.nasabah_id=n.id
            left join master_nasabah_dinamis d on d.no_identitas=n.no_identitas where bulan_identitas = '".$bulan."'  and tahun_identitas = '".$tahun."'"); 

            
            

            return MyFunc::printto($query1,'Laporan', $fields, 0,['excel'=>'laporan.excel'],'landscape','a4');
        }

        if ($request->has('filter')) {
            $flt=true;
            $bulan=$request->get('bulan');
            $tahun=$request->get('tahun');

            $query= \DB::select("select d.*,n.no_identitas as ktp,p.id as idpembiayaan,n.id as idnasabah,n.nama_nasabah,p.harga_object,
        p.jangka_waktu,p.total_angsuran,p.plafon
        from master_pembiayaan p 
        left join master_nasabah n on p.nasabah_id=n.id
        left join master_nasabah_dinamis d on d.no_identitas=n.no_identitas where bulan_identitas = '".$bulan."'  and tahun_identitas = '".$tahun."'"); 

        }  
         $param['login']=\DB::select("select * from logins");

        $param['data']=$query;
        $param['bulan']=$bulan;
        $param['tahun']=$tahun; 
        if ($request->ajax()) {
            $view = view('laporan.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'laporan.index',$param);
    }

     public function UploadCBS(Request $request)
    {

       if($request->hasFile('file')){

        $path = $request->file('file')->getRealPath();
        $data = Excel::load($path, function($reader) {})->get();
        $cek=0;
            if(!empty($data) && $data->count()){
                foreach ($data->toArray() as $key => $value) {
                   
                    if(isset($value['no_ktp']) && $value['no_ktp']!=null){

                        $cek_no_ktp= \DB::select("select * from master_nasabah where no_identitas='".$value['no_ktp']."'");
                        if($cek_no_ktp){
                            
                            $nasabah = new NasabahDinamisModel();
                            $nasabah->no_identitas = $value['no_ktp'];
                            $nasabah->bulan_identitas = $value['bulan_laporan'];
                            $nasabah->tahun_identitas = $value['tahun_laporan'];
                            $nasabah->nilai_mutasi_pokok_bank = $value['bayar_pokok'];
                            $nasabah->nilai_mutasi_tarif_bank = $value['bayar_tarif'];
                            $nasabah->nilai_denda_pokok_bank = $value['bayar_denda_pokok'];
                            $nasabah->nilai_denda_tarif_bank = $value['bayar_denda_tarif'];
                            $nasabah->tgl_bayar = $value['tanggal_bayar'];
                            $nasabah->tgl_mutasi_pokok = $value['tanggal_bayar'];
                            $nasabah->tgl_mutasi_tarif = $value['tanggal_bayar'];
                            $nasabah->nilai_outstanding_bank = $value['saldo_pokok'];
                            $nasabah->no_rek_pemilik = $value['no_rek_pemilik'];
                            $nasabah->save();

                        $cek1= \DB::select("select * from master_data_angsuranktp_bayar where id_ktp='".$value['no_ktp']."'");    
                            if(empty($cek1)){
                                
                                 DB::table('master_data_angsuranktp_bayar')->insert(
                                [
                                    'id_ktp' => $value['no_ktp'], 
                                    'bulan' => $value['bulan_laporan'],
                                    'tahun' => $value['tahun_laporan'],
                                    'is_bayar'=>false,
                                    'created_at'=>date('Y-m-d H:i:s'),
                                    'user_id'=>Auth::user()->id,
                                    'nilai_mutasi_pokok' => $value['bayar_pokok'],
                                    'nilai_mutasi_tarif' => $value['bayar_tarif'],
                                    'nilai_denda_pokok'  => $value['bayar_denda_pokok'],
                                    'nilai_denda_tarif'  => $value['bayar_denda_tarif']
                                ]
                            );

                            }
                           

                        }else{

                           return response()->json([
                                'rc' => 2,
                                'rm' => "Gagal Simpan Data",
                                'noktp' => $value['no_ktp']
                            ]);    
                        } 
     
                    }
                }
                return response()->json([
                                'rc' => 1,
                                'rm' => "berhasil"
                            ]); 
            }
        }
    }


    public function UpdatePPDPP(Request $request)
    {
       
       $bulan=$request->get('bulan');
       $tahun=$request->get('tahun');
       
       $query= \DB::select("select * from master_nasabah_dinamis where bulan_identitas='".$bulan."' and tahun_identitas='".$tahun."'");

       if($query){
            foreach ($query as $item) {
            
            $cek= \DB::select("select * from master_data_angsuran_idberkas where ktp='".$item->no_identitas."' and d_bulan='".$bulan."' and d_tahun='".$tahun."'");

            $ppdpp= \DB::select("select * from master_data_ppdpp where id_ktp='".$item->no_identitas."'");

            if($cek){

                $nasabah = NasabahDinamisModel::find($item->id_pembiayaan);
                $nasabah->nilai_outstanding = $cek[0]->d_flpp_outstanding;
                $nasabah->nilai_mutasi_pokok = $cek[0]->d_flpp_pokok;
                $nasabah->nilai_mutasi_tarif = $cek[0]->d_flpp_tarif;
                $nasabah->nilai_denda_pokok = 0;
                $nasabah->nilai_denda_tarif = 0;
                $nasabah->save(); 

            }

            if($ppdpp){

                $nasabah = NasabahDinamisModel::find($item->id_pembiayaan);
                $nasabah->no_rek_operasi = $ppdpp[0]->norek_operasi;
                $nasabah->no_rek_pemilik = $ppdpp[0]->no_rek;
                $nasabah->no_rek_program = $ppdpp[0]->norek_program;
                $nasabah->no_rek_kelola = $ppdpp[0]->norek_kelola;
                $nasabah->save(); 

            }
            
                    
                  

            }
            return response()->json([
                    'rc' => 1,
                    'rm' => "Data Berhasil Diupdate"
                ]); 

       }
       return response()->json([
                    'rc' => 2,
                    'rm' => "Tidak Ada Data"
                ]); 

    }


     public function MutasiPPDPP(Request $request)
    {
       $url = env('API_BASE_URL_ANGSURAN')."set-mutasi-byktp";
       $client = new Client();
       $bulan=$request->get('bulan');
       $tahun=$request->get('tahun');
       $login=\DB::select("select * from logins");
       $query= \DB::select("select * from master_nasabah_dinamis where bulan_identitas='".$bulan."' and tahun_identitas='".$tahun."'");

       if($query){
            foreach ($query as $item) {
                
                $data = array(
                "batchid"=> $login[0]->batch_id,    
                'pin'=>"4254256575f3ccee1601f115d8a333551",
                'ktp'=>$item->no_identitas,
                'setbulanktp' =>$item->bulan_identitas,
                'settahunktp' =>$item->tahun_identitas,
                'norekprogram' =>$item->no_rek_program,
                'norekkelola' =>$item->no_rek_kelola,
                'norekoperasi' =>$item->no_rek_operasi, 
                'norekpengirim' =>$item->no_rek_pemilik,
                'nilaimutasipokok' =>$item->nilai_mutasi_pokok,
                'nilaimutasitarif' =>$item->nilai_mutasi_tarif,
                'nilai_denda_pokok' =>$item->nilai_denda_pokok,
                'nilai_denda_tarif' =>$item->nilai_denda_tarif,
                'tgllaporktp' =>date('Y-m-d'),
                'tglmutasipokok' =>$item->tgl_mutasi_pokok,
                'tglmutasitarif' =>$item->tgl_mutasi_tarif,
                );
                
                $headers = [
                    'Content-Type' => 'application/json'
                    //'Authorization' => Auth::id()
                ];
                try{

                    $result = $client->post($url,[
                        RequestOptions::HEADERS => $headers,
                        RequestOptions::JSON => $data,


                    ]);

                    $param=[];
                    $param= (string) $result->getBody();
                    $data = json_decode($param, true);
                    
                    $nasabah = NasabahDinamisModel::find($item->id_pembiayaan);
                    $nasabah->is_lapor = true;

                    if($data['rc']=='00' || $data['rc']==0){
                    
                    $nasabah = NasabahDinamisModel::find($item->id_pembiayaan);
                    $nasabah->is_lapor = true;

                    }else{
                        $nasabah = NasabahDinamisModel::find($item->id_pembiayaan);
                        $nasabah->rc_jadwal = $data['rc'];
                        $nasabah->save();
                    }
                  
                   
                    
                }catch (BadResponseException $e){
                    $response = json_decode($e->getResponse()->getBody());
                    
                    $nasabah = NasabahDinamisModel::find($item->id_pembiayaan);
                    $nasabah->rc_jadwal = $response['rc'];
                    $nasabah->save();
                } 


            }
             return response()->json([
                    'rc' => 1,
                    'rm' => "Data Berhasil Dikirim"
                ]); 

       }

        return response()->json([
                    'rc' => 2,
                    'rm' => "Tidak Ada data"
                ]); 

    }


    public function insert(Request $request){
        $cek_no_ktp= \DB::select("select * from master_nasabah where no_identitas='".$request->input('no_ktp')."'");
        if($cek_no_ktp){
            $nasabah = new NasabahDinamisModel();
            $nasabah->no_identitas = $request->input('no_ktp');
            $nasabah->bulan_identitas = $request->input('bulan');
            $nasabah->tahun_identitas = $request->input('tahun');
            $nasabah->nilai_mutasi_pokok_bank = str_replace(".","", $request->input('bayar_pokok'));
            $nasabah->nilai_mutasi_tarif_bank = str_replace(".","", $request->input('bayar_tarif'));
            $nasabah->nilai_denda_pokok_bank = str_replace(".","", $request->input('denda_pokok'));
            $nasabah->nilai_denda_tarif_bank = str_replace(".","", $request->input('denda_tarif'));
            $nasabah->tgl_bayar = date('Y-m-d',strtotime($request->input('tgl_bayar')));
            $nasabah->nilai_outstanding_bank = str_replace(".","", $request->input('saldo_pokok'));
            $nasabah->save();

            return response()->json([
                        'rc' => 1,
                        'rm' => "Data Berhasil Disimpan"
                    ]); 
        }else{

            return response()->json([
                        'rc' => 2,
                        'rm' => "No KTP ".$request->input('no_ktp')." Tidak Terdaftar Didatabase!"
                    ]); 
        }
        

    }


       public function percepat(Request $request){
        $cek_no_ktp= \DB::select("select * from master_nasabah where no_identitas='".$request->input('noktp')."'");
        $login=\DB::select("select * from logins");
        if($cek_no_ktp){
            
             $url1 = env('API_BASE_URL_ANGSURAN')."get-sisa-pembayaran-byktp";
             $client1 = new Client();
             $data1 = array(
                "batchid"=> $login[0]->batch_id,
                "ktp"=> $request->input('noktp'),
                );
                
                $headers = [
                    'Content-Type' => 'application/json'
                    //'Authorization' => Auth::id()
                ];
                $result1 = $client1->get($url1,[
                    RequestOptions::HEADERS => $headers,
                    RequestOptions::JSON => $data1,


                ]);

                $param1=[];
                $param1= (string) $result1->getBody();
                $data1 = json_decode($param1, true);

            $tarif=\DB::select("select * from master_data_angsuran_idberkas where ktp='".$request->input('noktp')."' and d_bulan='".$request->input('bulan')."' and d_tahun='".$request->input('tahun')."'");        


                    

             $url = env('API_BASE_URL_ANGSURAN')."set-mutasi-byktp-dipercepat";
             $client = new Client();
             $data = array(
                "batchid"=> $login[0]->batch_id,
                "pin"=> "4254256575f3ccee1601f115d8a333551",
                "ktp"=> $request->input('noktp'),
                "setbulanktpcepat"=> $request->input('bulan'),
                "settahunktpcepat"=> $request->input('tahun'),
                "norekprogram"=> $request->input('no_rek_program'),
                "norekkelola"=> $request->input('no_rek_kelola'),
                "norekoperasi"=> $request->input('no_rek_operasi'),
                "norekpengirim"=> $request->input('no_rek_pemilik'),
                "nilaimutasipokok"=> $tarif[0]->d_flpp_pokok,
                "nilaimutasitarif"=> $tarif[0]->d_flpp_tarif,
                "nilai_denda_pokok"=> str_replace(".","", $request->input('nilai_denda_pokok_cepat')),
                "nilai_denda_tarif"=> str_replace(".","", $request->input('nilai_denda_tarif_cepat')),
                "tgllaporcepat"=> date('Y-m-d',strtotime($request->input('tgl_laporan'))),
                "tglmutasipokok"=> date('Y-m-d',strtotime($request->input('tgl_mutasi_pokok'))),
                "tglmutasitarif"=> date('Y-m-d',strtotime($request->input('tgl_mutasi_tarif')))
                );
                
                $headers = [
                    'Content-Type' => 'application/json'
                    //'Authorization' => Auth::id()
                ];
                try{

                    $result = $client->post($url,[
                        RequestOptions::HEADERS => $headers,
                        RequestOptions::JSON => $data,


                    ]);

                    $param=[];
                    $param= (string) $result->getBody();
                    $data = json_decode($param, true);
                    
                    return response()->json([
                        'rc' => $data['rc'],
                        'rm' => $data['rm']
                    ]); 
                    
                    
                }catch (BadResponseException $e){
                    $response = json_decode($e->getResponse()->getBody());
                    
                    return response()->json([
                        'rc' => $response['rc'],
                        'rm' => $response['rm']
                    ]);
                } 


        }else{

            return response()->json([
                        'rc' => 2,
                        'rm' => "No KTP ".$request->input('noktp')." Tidak Terdaftar Didatabase!"
                    ]); 
        }
        

    }

}
