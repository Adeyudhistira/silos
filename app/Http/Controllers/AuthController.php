<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use Session;
use Illuminate\Support\Facades\Redirect;
class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    /*
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }
    */
   
    /*
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    */

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function LoginViaAPI(Request $request)
    {

       // var_dump($request->all());exit();
        $username = $request->input('username');
        $password = $request->input('password');
        $user = collect(\DB::select("SELECT id FROM users where username = '".$username."'"))->first();

        $data = array(
            'username'=> $username,
            'password' => $password
        );

        $url = env('API_BASE_URL')."/auth/login";
         
        $client = new Client();
        $headers = [
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
            
            Auth::loginUsingId($user->id);
            // Session::put('user', $username);
             Session::put('token', $data_result['data']['token']);
           // return "1";
             return redirect('/home');
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return Redirect::route('login')->withInput()->with('error', $response->rm);
            //return $response;
        }

       

    }

    public function login(Request $request)
    {
        $url = env('API_BASE_URL')."auth/login";
        $client = new Client();
        $data = array(
            "username"=> $request->input('username'),
            "password"=> $request->input('password')
        );
        $headers = [
            'Content-Type' => 'application/json',
            //'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuZmxwcC5iYXN5cy5jby5pZFwvYXV0aFwvbG9naW4iLCJpYXQiOjE1NTM1NzI3NTQsImV4cCI6MTU1MzU3NjM1NCwibmJmIjoxNTUzNTcyNzU0LCJqdGkiOiJJdEhtaVNTaVRTZHFaQlNOIiwic3ViIjo1LCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.bucya1zaxC7vF13qXdplAhRf3eLVdyRwfKPbC3WDScI',
            //'Authorization' => 'Bearer '.$request->get('_token'),

        ];

        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,


            ]);
            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);

            $token=$data['token'];
          $credentials = request(['username', 'password']);
        //dd($credentials);
            
            if (! $token = Auth::guard('api')->attempt($credentials)) {
                return response()->json(['error' => 'Unauthorized'], 401);
            }

            DB::table('users')
            ->where('username', $request->input('username'))
            ->where('password', $request->input('password'))
            ->update(['remember_token' => $token]);

            //return $this->respondWithToken($token);
            return view('home');
          //  $credentials = $request->only('username', 'password');
            /*
            if (! $token = Auth::guard('api')->attempt($credentials)) {
                   return response()->json(['error' => 'Unauthorized'], 401);
            }
            */

           // return $this->respondWithToken($token);
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           //return redirect('daftar_prospek_baru')->with('error',$response->error);
        }

        
    }



    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
           // 'expires_in' => auth('api')->factory()->getTTL() * 60
            //'expires_in' => auth()->factory()->getTTL() * 60
            //'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }
}