<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use App\Http\Requests;
use File;
class NasabahController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     public function entry_nasabah_view(Request $request)
    {
        if ($request->ajax()) {
            $view = view('nasabah.entry_nasabah')->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah.entry_nasabah');
    }

     public function daftar_nasabah_view(Request $request)
    {
        if ($request->ajax()) {
            $view = view('nasabah.daftar_nasabah_baru')->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah.daftar_nasabah_baru');
    }

    public function nasabah_on_proses(Request $request){
        $url = env('API_BASE_URL')."master/nasabah/appraisal?page=".$request->get('page')."&size=".$request->get('size');
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
           if($data['rc']==200){

           $data =$data['data'];
           }else{

           $data ='';
           }
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }

        $param['data']=$data;



        if ($request->ajax()) {
            $view = view('nasabah.nasabah_on_proses',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah.nasabah_on_proses',$param);

    }

      public function slik(Request $request)
    {
        $url = env('API_BASE_URL')."master/bi-check/pembiayaan/".$request->get('id');
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
           $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }

        $param['data']=$data;

        if ($request->ajax()) {
            $view = view('nasabah.slik',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah.slik',$param);
    }

      public function appraisal(Request $request)
    {
        
        $url = env('API_BASE_URL')."master/agunan/pembiayaan/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $pembiayaan =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $pembiayaan=$response;
        }
        if($pembiayaan){

            $url1 = env('API_BASE_URL')."master/agunan-property/agunan/".$pembiayaan[0]['id'];
            try{
                
                $result1 = $client->get($url1,[
                    RequestOptions::HEADERS => $headers,
                    ]);
                
                
                $param2=[];
                $param2= (string) $result1->getBody();
                $data1 = json_decode($param2, true);
                $property =$data1['data'];

            }catch (BadResponseException $e){
                $response1 = json_decode($e->getResponse()->getBody());
                $property=$response1;
            }
           

        }
        


        $param['pembiayaan']=$pembiayaan;
        $param['property']=$property;
        if ($request->ajax()) {
            $view = view('nasabah.appraisal',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah.appraisal',$param);
    }

      public function analisa(Request $request)
    {
        $url = env('API_BASE_URL')."master/pembiayaan/analisis/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];
            $rc=$data['rc'];
            $rm=$data['rm'];
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
            $rc=$data->rc;
            $rm=$data->rm;
        }

        $param['rc']=$rc;
        $param['rm']=$rm;
        $param['data']=$data;
        $param['idpembiayaan']=$request->get('id');    
        if ($request->ajax()) {
            $view = view('nasabah.analisa',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah.analisa',$param);
    }

      public function sla(Request $request)
    {
        $url = env('API_BASE_URL')."master/pembiayaan/log/".$request->get('id');
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        $param['data']=$data;

        if ($request->ajax()) {
            $view = view('nasabah.sla_detail',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah.sla_detail',$param);
    }

      public function ppdpp(Request $request)
    {
        $url = env('API_BASE_URL')."master/agunan-property/agunan/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        $param['data']=$data;
        if ($request->ajax()) {
            $view = view('nasabah.check_ppdpp',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah.check_ppdpp',$param);
    }


      public function detail_property(Request $request)
    {
        $url = env('API_BASE_URL')."master/agunan-property/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        $param['data']=$data;
        if ($request->ajax()) {
            $view = view('nasabah.detail_property',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah.detail_property',$param);
    }

    public function update_property(Request $request){
        $id=$request->input('id');
        $idpembiayaan=$request->input('id_pembiayaan');
        $url = env('API_BASE_URL')."master/agunan-property/".$id;
        $client = new Client();
        $data = array(
            "noSertifikat"=> $request->input('no_sertifikasi'),
            "nmPemilikSertifikat"=> $request->input('nama_pemilik'),
            "luasTanahSertifikat"=> $request->input('luas_tanah'),
            "luasImb"=> $request->input('luas_bangunan'),
            "nilaiAppraisal"=> str_replace(".","", $request->input('nilai')),
            "alamat"=> $request->input('alamat'),
            "idWilayah"=> $request->input('lokasiagunan'),
            "idDeveloper"=> $request->input('nama_developer'),
            "idPerumahan"=> $request->input('nama_perumahan'),
            "luasTanahFisik"=> $request->input('luas_tanah_fisik'),
            "luasFisik"=> $request->input('luas_bangunan_fisik')
        );
 
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            $result = $client->put($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
                ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
           
            if($data['rc']=='200'){
                return redirect('appraisal_nasabah_on_proses?id='.$idpembiayaan)->with('success',$data['rm']);
            }else{
                return redirect('appraisal_nasabah_on_proses?id='.$idpembiayaan)->with('error',$data['rm']);
            }
            
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());

           return redirect('appraisal_nasabah_on_proses?id='.$idpembiayaan)->with('error',$response->error);
        }
    }

    public function update_ppdpp(Request $request){
        $check = [];
        $query=$request->subParmId;
        foreach ($query as $item){
            array_push($check,['subParmId' => $item,'valueParmId' => $request->input('valueParmId_'.$item)]);
        }
        $id=$request->input('idAgunanProperty');
        $url = env('API_BASE_URL')."master/agunan-property/checklist/".$id;
        $client = new Client();
        $parsing = json_encode($check);
        $data = array(
            "checklist"=> $check
            ); 
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            $result = $client->put($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
                ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
           
            if($data['rc']=='200'){
                return redirect('nasabah_on_proses')->with('success',$data['rm']);
            }else{
                return redirect('nasabah_on_proses')->with('error',$data['rm']);
            }
            
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           return redirect('nasabah_on_proses')->with('error',$response->error);
        }
    }

    public function update_analis(Request $request){
        
        $check = [];
        $check1 = [];
        $query=$request->idSubGroup;
        foreach ($query as $item){
            array_push($check,['idGroup' => $request->input('idGroup_'.$item),'idSubGroup' => $item,'value'=>$request->input('nilai_'.$item)]);
        }
        $query1=$request->idSubGroup1;
        foreach ($query1 as $item){
            array_push($check1,['idGroup' => $request->input('idGroup1_'.$item),'idSubGroup' => $item,'value'=>$request->input('nilai1_'.$item)]);
        }
        $id=$request->input('idAgunanProperty');
        $url = env('API_BASE_URL')."master/pembiayaan/analisis";
        $client = new Client();
        $parsing = json_encode($check);
        $data = array(
            "pembiayaanId"=>$request->input('idpembiayaan'),
            "idProduct"=>$request->input('idProduct'),
            "riti"=>$request->input('riti'),
            "collCover"=>$request->input('coll_cover'),
            "ratio"=> $check,
            "nonRatio"=> $check
            ); 
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
                ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
           
            if($data['rc']=='200'){
                return redirect('nasabah_on_proses')->with('success',$data['rm']);
            }else{
                return redirect('nasabah_on_proses')->with('error',$data['rm']);
            }
            
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           return redirect('nasabah_on_proses')->with('error',$response->error);
        }   
    }


    public function kirimproses(Request $request){

        $url = env('API_BASE_URL')."master/pembiayaan/next-wf/".$request->get('pembiayaanId');
         
        $client = new Client();
        $headers = [
                // 'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuZmxwcC5iYXN5cy5jby5pZFwvXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU1NzE5OTU5MywiZXhwIjoxNTU3MjAzMTkzLCJuYmYiOjE1NTcxOTk1OTMsImp0aSI6IkdGdUxocUxBSkNyYTgwSlIiLCJzdWIiOjUsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEiLCJpZCI6NSwidXNlcm5hbWUiOiJhb2Jla2FzaSIsImlkX2NoYW5uZWwiOm51bGwsImlkX2JyYW5jaCI6NiwibG9naW5fdHlwZSI6MCwidXNlcl9ncm91cF9pZCI6MSwiYnJhbmNoX3R5cGVfaWQiOjJ9.N_ta3S4_yEHZtIfbbUc1pLj6KM3uHmui0mjI2EFrjN4',
                'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->put($url,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
           
            return $data_result;

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }

    }

    public function tambah_dokumen(Request $request){
        $param['id']=$request->get('id');

        $url = env('API_BASE_URL')."master/list/jdoc?statusPembiayaan=1";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }

        $param['data'] = $data;
        // dd($data);
        if ($request->ajax()) {
            $view = view('nasabah.tambah_dokumen',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah.tambah_dokumen',$param);
    }

    public function insert_dok(Request $request){
        $id=$request->input('id');
        //$key_upload = $request->input('key_file');
      // $this->validate($request, [
        //    'dok' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:1024'
         //]);

        $validators = \Validator::make($request->all(),['dok' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:1024']);
        if($validators->fails()){
          //  dd($validators);
            return Redirect::back()->withErrors($validators);
            //return redirect('dokumen?id='.$id)->with('error','gagal Upload');
        }
        $media = $request->file('dok');
        $image_mime = $media->getmimeType();
        $fileContent = File::get($media);

         $url = env('API_BASE_URL')."master/agunan-property/doc";
         $client = new Client();

       try {
            $result = $client->post(
                $url, [
                    'headers' => [
                        'Authorization'         =>  'Bearer '.session('token')
                    ],
                    'multipart' => [
                        [
                            'name'     => '_file',
                            'contents' => $fileContent,
                            'filename' => 'wadaw',
                            'Mime-Type'=> $image_mime,
                        ],
                        [
                            'name'     => 'agunanPropertyId',
                            'contents' => $id,
                        ],
                        [
                            'name'     => 'jenisDocId',
                            'contents' => $request->input('idjenis'),
                        ],
                    ],
                ]
            );

             $param=[];
             $param= (string) $result->getBody();
             $data_result = json_decode($param, true);

            if($data_result['rc']=='200'){
                return redirect('detail_property_nasabah_on_proses?id='.$id)->with('success',$data_result['rm']);
            }else{
                return redirect('detail_property_nasabah_on_proses?id='.$id)->with('error',$data_result['rm']);
            }
        } catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            //$bad_response = $this->responseData($e->getCode(), $response);
            //dd($response);
            return redirect('detail_property_nasabah_on_proses?id='.$id)->with('error',$response->rm);
        }
    }

}
