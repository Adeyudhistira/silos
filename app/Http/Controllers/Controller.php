<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function sendResponse($code, $msg, $data = null){
        switch ($code){
            case 0:
                return json_encode(['code' => $code, 'msg' => $msg, 'data' => $data->errors()]);
                break;
            case 1:
                return json_encode(['code' => $code, 'msg' => $msg, 'data' => $data]);
                break;       
            case 2:
                return json_encode(['code' => $code, 'msg' => $msg]);
                break;           

        }
        return ;
    }
     public function responseData($code, $message){
        return ['code' => $code, 'message'=> $message];
    }
}
