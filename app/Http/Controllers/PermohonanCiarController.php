<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use App\Http\Requests;

class PermohonanCairController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request){
        $url = env('API_BASE_URL')."master/nasabah/approved?page=".$request->get('page')."&size=".$request->get('size');
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            if($data1['rc']==200){
                $data =$data1['data'];
                $rc=$data1['rc'];
                $rm='';
            }else{
                $data ='';
                $rc=$data1['rc'];
                $rm='';
            }
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
            $rc=$response->rc;
            $rm=$response->rm;
        }
        
        $param['data']=$data;
        $param['rc']=$rc;
        $param['rm']=$rm;

        if ($request->ajax()) {
            $view = view('permohonan_cair.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'permohonan_cair.index',$param);

    }

    
      
}
