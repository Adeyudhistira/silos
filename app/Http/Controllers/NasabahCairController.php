<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use App\Http\Requests;

class NasabahCairController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function index(Request $request){
        $url = env('API_BASE_URL')."master/nasabah/analisa?page=0&size=10";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
           if($data['rc']==200){
            $data =$data['data'];
           }else{
            $data='';
           }
           

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        
        
        $param['data']=$data;

        if ($request->ajax()) {
            $view = view('nasabah_cair.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_cair.index',$param);

    }

       public function slik(Request $request)
    {
        $url = env('API_BASE_URL')."master/bi-check/pembiayaan/".$request->get('id');
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
           $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }

         $url1 = env('API_BASE_URL')."master/list/bicheck-status";
        try{
            
            $result1 = $client->get($url1,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param2=[];
            $param2= (string) $result1->getBody();
            $data1 = json_decode($param2, true);
           $data1 =$data1['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data1=$response;
        }


        $param['data1']=$data1;

        $param['data']=$data;

        if ($request->ajax()) {
            $view = view('nasabah_cair.slik',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_cair.slik',$param);
    }

      public function appraisal(Request $request)
    {
        
        $url = env('API_BASE_URL')."master/agunan/pembiayaan/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $pembiayaan =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $pembiayaan=$response;
        }
        if($pembiayaan){

            $url1 = env('API_BASE_URL')."master/agunan-property/agunan/".$pembiayaan[0]['id'];
            try{
                
                $result1 = $client->get($url1,[
                    RequestOptions::HEADERS => $headers,
                    ]);
                
                
                $param2=[];
                $param2= (string) $result1->getBody();
                $data1 = json_decode($param2, true);
                $property =$data1['data'];

            }catch (BadResponseException $e){
                $response1 = json_decode($e->getResponse()->getBody());
                $property=$response1;
            }
           

        }
        


        $param['pembiayaan']=$pembiayaan;
        $param['property']=$property;
        if ($request->ajax()) {
            $view = view('nasabah_cair.appraisal',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_cair.appraisal',$param);
    }

      public function analisa(Request $request)
    {
        $url = env('API_BASE_URL')."master/pembiayaan/analisis/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1['data'];
            $rc=$data1['rc'];
            $rm=$data1['rm'];
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
            $rc=$data->rc;
            $rm=$data->rm;
        }
        $param['rc']=$rc;
        $param['rm']=$rm;
        $param['data']=$data;
        $param['idpembiayaan']=$request->get('id');    
        if ($request->ajax()) {
            $view = view('nasabah_cair.analisa',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_cair.analisa',$param);
    }

      public function sla(Request $request)
    {
        $url = env('API_BASE_URL')."master/pembiayaan/log/".$request->get('id');
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        $param['data']=$data;

        if ($request->ajax()) {
            $view = view('nasabah_cair.sla_detail',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_cair.sla_detail',$param);
    }

      public function ppdpp(Request $request)
    {
        $url = env('API_BASE_URL')."master/agunan-property/agunan/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        $param['data']=$data;
        if ($request->ajax()) {
            $view = view('nasabah_cair.check_ppdpp',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_cair.check_ppdpp',$param);
    }


      public function detail_property(Request $request)
    {
        $url = env('API_BASE_URL')."master/agunan-property/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        $param['data']=$data;
        if ($request->ajax()) {
            $view = view('nasabah_cair.detail_property',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'nasabah_cair.detail_property',$param);
    }

    public function update_property(Request $request){
        $id=$request->input('id');
        $idpembiayaan=$request->input('id_pembiayaan');
        $url = env('API_BASE_URL')."master/agunan-property/".$id;
        $client = new Client();
        $data = array(
            "noSertifikat"=> $request->input('no_sertifikasi'),
            "nmPemilikSertifikat"=> $request->input('nama_pemilik'),
            "luasTanahFisik"=> $request->input('luas_tanah'),
            "luasImb"=> $request->input('luas_bangunan'),
            "nilaiAppraisal"=> str_replace(".","", $request->input('nilai')),
            "alamat"=> $request->input('alamat'),
            "idKotaKab"=> $request->input('kota'),
            "idKecamatan"=> $request->input('kec'),
            "idKelurahan"=> $request->input('kel'),
            "namaDeveloper"=> $request->input('nama_developer')
        );
 
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            $result = $client->put($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
                ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
           
            if($data['rc']=='200'){
                return redirect('appraisal_nasabah_cair?id='.$idpembiayaan)->with('success',$data['rm']);
            }else{
                return redirect('appraisal_nasabah_cair?id='.$idpembiayaan)->with('error',$data['rm']);
            }
            
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());

           return redirect('appraisal_nasabah_cair?id='.$idpembiayaan)->with('error',$response->error);
        }
    }

    public function update_ppdpp(Request $request){
        $check = [];
        $query=$request->subParmId;
        foreach ($query as $item){
            array_push($check,['subParmId' => $item,'valueParmId' => $request->input('valueParmId_'.$item)]);
        }
        $id=$request->input('idAgunanProperty');
        $url = env('API_BASE_URL')."master/agunan-property/checklist/".$id;
        $client = new Client();
        $parsing = json_encode($check);
        $data = array(
            "checklist"=> $check
            ); 
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            $result = $client->put($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
                ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
           
            if($data['rc']=='200'){
                return redirect('nasabah_cair')->with('success',$data['rm']);
            }else{
                return redirect('nasabah_cair')->with('error',$data['rm']);
            }
            
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           return redirect('nasabah_cair')->with('error',$response->error);
        }
    }

    public function update_analis(Request $request){
        
        $check = [];
        $check1 = [];
        $query=$request->idSubGroup;
        foreach ($query as $item){
            array_push($check,['idGroup' => $request->input('idGroup_'.$item),'idSubGroup' => $item,'value'=>$request->input('nilai_'.$item)]);
        }
        $query1=$request->idSubGroup1;
        foreach ($query1 as $item){
            array_push($check1,['idGroup' => $request->input('idGroup1_'.$item),'idSubGroup' => $item,'value'=>$request->input('nilai1_'.$item)]);
        }
        $id=$request->input('idAgunanProperty');
        $url = env('API_BASE_URL')."master/pembiayaan/analisis";
        $client = new Client();
        $parsing = json_encode($check);
        $data = array(
            "pembiayaanId"=>$request->input('idpembiayaan'),
            "idProduct"=>$request->input('idProduct'),
            "collCover"=> (float) $request->input('coll_cover'),
            "riti"=> (float) $request->input('riti'),
            "notes"=> $request->input('kesimpulan'),
            "kesimpulan"=> $request->input('catatan'),
            "ratio"=> $check1,
            "nonRatio"=> $check
            ); 
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
                ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
           
            if($data['rc']=='200'){
                return redirect('nasabah_cair')->with('success',$data['rm']);
            }else{
                return redirect('nasabah_cair')->with('error',$data['rm']);
            }
            
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            
           return redirect('nasabah_cair')->with('error',$response->error);
        }   
    }
}
