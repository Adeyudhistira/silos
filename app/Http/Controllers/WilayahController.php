<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MWilayahModel;

class WilayahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $param['data'] = \DB::select("SELECT * FROM wilayah ORDER BY kode limit 500");
        if ($request->ajax()) {
            $view = view('wilayah.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'wilayah.index',$param);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master.master')->nest('child', 'wilayah.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //   $kode = $request->input('kode');
        //   $kode_wilayah = $request->input('kode_wilayah');
        
       
       
        // if ( $kode == "kosong") {
        // }else{
        //      $datan = MWilayahModel::find($kode_wilayah);
        // }

        //   $datan = new MWilayahModel();
        
        //  $datan->kode = $request->input('kode_wilayah');
        //  $datan->nama = $request->input('nama_wilayah');
        //  $datan->save();
    }

     public function tambah(Request $request)
    {
        if ($request->input('type') == "normal") {
            $datan = new MWilayahModel();
        }else{
            $datan = MWilayahModel::find($request->input('kode_wilayah'));
        }

          
        
         $datan->kode = $request->input('kode_wilayah');
         $datan->nama = $request->input('nama_wilayah');
         $datan->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $param['data'] = collect(\DB::select("SELECT * FROM wilayah where kode = '".$id."'"))->first();

        return view('master.master')->nest('child', 'wilayah.form',$param);
    }

    public function delete($id)
    {
        $data = MWilayahModel::find($id);
        $data->delete();

        // return json_encode(['rc' => 0, 'rm' => 'Data Berhasil']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
