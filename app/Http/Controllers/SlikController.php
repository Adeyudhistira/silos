<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Hash;
use Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Collection;
use Session;
use File;  
use Redirect;
class SlikController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
       public function permintaan(Request $request)
    {
        $url = env('API_BASE_URL')."master/bi-check/request";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            if($data1['rc']==200)
            {
                $data = $data1['data'];
            } else {
                $data = '';
            }

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        $param['data']=$data;
        
        if ($request->ajax()) {
            $view = view('slik_checking.permintaan',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'slik_checking.permintaan',$param);
    }


      public function slik(Request $request,$id)
    {
        $url = env('API_BASE_URL')."master/bi-check/pembiayaan/".$id;
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
           $data =$data['data'];
           
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
           
        }


        $url1 = env('API_BASE_URL')."master/list/bicheck-status";
        try{
            
            $result1 = $client->get($url1,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param2=[];
            $param2= (string) $result1->getBody();
            $data1 = json_decode($param2, true);
           $data1 =$data1['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data1=$response;
        }


        $param['data1']=$data1;
        $param['data']=$data;

        if ($request->ajax()) {
            $view = view('slik_checkingk.slik',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'slik_checking.slik',$param);
    }

     public function update_checking(Request $request){
        $id=$request->input('id');

        $validators = \Validator::make($request->all(),['file' => 'required|max:102400']);
        if($validators->fails()){
          //  dd($validators);
            return Redirect::back()->withErrors($validators);
            //return redirect('dokumen?id='.$id)->with('error','gagal Upload');
        }
        // $media = $request->file('file');
        if($request->hasFile('file')){
            $file = $request->file('file');

            // Mendapatkan Nama File
            $nama_file = $file->getClientOriginalName();
       
            // Mendapatkan Extension File
            $extension = $file->getClientOriginalExtension();

            $file_mime = $file->getmimeType();

            $fileContent = File::get($file);
        }

        $url = env('API_BASE_URL')."master/bi-check/doc";
        $client = new Client();

       try {
            $result = $client->post(
                $url, [
                    'headers' => [
                        'Authorization'         =>  'Bearer '.session('token')
                    ],
                    'multipart' => [
                        [
                            'name'     => '_file',
                            'contents' => $fileContent,
                            'filename' => $nama_file,
                            'Mime-Type'=> $file_mime,
                            'extension'=> $extension
                        ],
                        [
                            'name'     => 'biCheckId',
                            'contents' => $id,
                        ]
                    ],
                ]
            );

             $param=[];
             $param= (string) $result->getBody();
             $data_result = json_decode($param, true);

            // if($data_result['rc']=='200'){
            //     return redirect('dokumen?id='.$id)->with('success',$data_result['rm']);
            // }else{
            //     return redirect('dokumen?id='.$id)->with('error',$data_result['rm']);
            // }
        } catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            //$bad_response = $this->responseData($e->getCode(), $response);
            //dd($response);
            // return redirect('dokumen?id='.$id)->with('error',$response->rm);
        }

        $url = env('API_BASE_URL')."master/bi-check/".$id;
        $client = new Client();
        $data = array(
                "statusSlik"=> $request->input('status'),
                "notes"=> $request->input('catatan'),
                "totalAngsuran"=> str_replace(".","", $request->input('angsuran'))
        );
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            $result = $client->put($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
                ]);

            $param=[];
            $param= (string) $result->getBody();
            $data = json_decode($param, true);
           
            if($data['rc']=='200'){
                return redirect('permintaan')->with('success',$data['rm']);
            }else{
                return redirect('permintaan')->with('error',$data['rm']);
            }
            
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            
           return redirect('permintaan')->with('error',$response->error);
        }
    }

    public function selesai(Request $request)
    {
        $url = env('API_BASE_URL')."master/bi-check/done";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
           $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }

        $param['data']=$data;
        if ($request->ajax()) {
            $view = view('slik_checking.selesai',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'slik_checking.selesai',$param);
    }

        

}
