<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use App\Http\Requests;
use Auth;

class ApprovalBRController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request){
        $url = env('API_BASE_URL')."master/nasabah/status?page=".$request->get('page')."&size=".$request->get('size');
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];

        $data = array(
            "userId"=> Auth::user()->id,
            "branchId"=> Auth::user()->id_branch,
            "statusPembiayaan"=> [12]
        );
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            if($data1['rc']==200){
                $data =$data1['data'];
                $rc=$data1['rc'];
                $rm='';
            }else{
                $data ='';
                $rc=$data1['rc'];
                $rm='';
            }
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
           
            $data=$response;
            $rc=$response->rc;
            $rm=$response->rm;
        }
        
        $param['data']=$data;
        $param['rc']=$rc;
        $param['rm']=$rm;

        if ($request->ajax()) {
            $view = view('approval_br.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'approval_br.index',$param);

    }

    
      
        public function slik(Request $request)
    {
        $url = env('API_BASE_URL')."master/bi-check/pembiayaan/".$request->get('id');
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
           $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }

         $url1 = env('API_BASE_URL')."master/list/bicheck-status";
        try{
            
            $result1 = $client->get($url1,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param2=[];
            $param2= (string) $result1->getBody();
            $data1 = json_decode($param2, true);
           $data1 =$data1['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data1=$response;
        }


        $param['data1']=$data1;

        $param['data']=$data;

        if ($request->ajax()) {
            $view = view('approval_br.slik',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'approval_br.slik',$param);
    }

      public function appraisal(Request $request)
    {
        
        $url = env('API_BASE_URL')."master/agunan/pembiayaan/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $pembiayaan =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $pembiayaan=$response;
        }
        if($pembiayaan){

            $url1 = env('API_BASE_URL')."master/agunan-property/agunan/".$pembiayaan[0]['id'];
            try{
                
                $result1 = $client->get($url1,[
                    RequestOptions::HEADERS => $headers,
                    ]);
                
                
                $param2=[];
                $param2= (string) $result1->getBody();
                $data1 = json_decode($param2, true);
                $property =$data1['data'];

            }catch (BadResponseException $e){
                $response1 = json_decode($e->getResponse()->getBody());
                $property=$response1;
            }
           

        }
        


        $param['pembiayaan']=$pembiayaan;
        $param['property']=$property;
        if ($request->ajax()) {
            $view = view('approval_br.appraisal',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'approval_br.appraisal',$param);
    }

      public function analisa(Request $request)
    {
        $url = env('API_BASE_URL')."master/pembiayaan/analisis/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1['data'];
            $rc=$data1['rc'];
            $rm=$data1['rm'];
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
            $rc=$data->rc;
            $rm=$data->rm;
        }

        $param['rc']=$rc;
        $param['rm']=$rm;
        $param['data']=$data;
        $param['idpembiayaan']=$request->get('id');    
        if ($request->ajax()) {
            $view = view('approval_br.analisa',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'approval_br.analisa',$param);
    }

    

      public function ppdpp(Request $request)
    {
        $url = env('API_BASE_URL')."master/agunan-property/agunan/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        $param['data']=$data;
        if ($request->ajax()) {
            $view = view('approval_br.check_ppdpp',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'approval_br.check_ppdpp',$param);
    }


      public function detail_property(Request $request)
    {
        $url = env('API_BASE_URL')."master/agunan-property/".$request->get('id');
        $client = new Client();
        $pembiayaan='';
        $property='';

        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        $param['data']=$data;
        if ($request->ajax()) {
            $view = view('approval_br.detail_property',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'approval_br.detail_property',$param);
    }

    public function kirim_approve (Request $request){
       
        $param['combo'] = \DB::select(" SELECT * FROM users where user_group_id=18 and id_branch=0");
        $param['id'] = $request->get('id');
         if ($request->ajax()) {
            $view = view('approval_br.kirim',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'approval_br.kirim',$param);   
    }

    public function kirim_data(Request $request){
        $url = env('API_BASE_URL')."master/pembiayaan/next-wf/".$request->input('id');
        $client = new Client();
        $headers = [
               'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->put($url,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
           

            $users = DB::table('master_pembiayaan')->where('id',$request->input('id'))->get();
            DB::table('master_pembiayaan')
            ->where('id', $request->input('id'))
            ->update(['next_proc_uid' => $request->input('iduser')]);

             $cek_log = DB::table('master_log_workflow')
            ->where('id_pembiayaan',$request->get('id'))
            ->where('status_pembiayaan',12)
            ->Orderby('created_at','DESC')
            ->get();

            if($cek_log){
            
            DB::table('master_log_workflow')
            ->where('id_pembiayaan', $request->get('id'))
            ->where('status_pembiayaan', 12)
            ->update([
                'end_at' => date('Y-m-d H:i:s'),
                'user_end_id' => $cek_log[0]->user_id
                ]);                   

            }

                DB::table('master_log_workflow')->insertGetId(
                        [
                            'id_pembiayaan' => $request->input('id'), 
                            'status_pembiayaan' => 13,
                            'created_at' => date('Y-m-d H:i:s')
                        ]
                    );  

               
            return redirect('nasabah_approve')->with('success','Berhasil Kirim Data');

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
          
            return $bad_response;
        }

    }
}
