<!-- JavaScript at the bottom for fast page loading -->
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="../js/mylibs/excanvas.min.js"></script><![endif]-->
<script language="javascript" type="text/javascript" src="{{ asset('js/libs/jquery-1.6.2.min.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/libs/jquery-ui-1.8.16.custom.min.js') }}"></script>


{{-- <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script> --}}


<!-- scripts -->
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/elfinder.min.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/jquery.flot.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/jquery.flot.pie.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/jquery.flot.resize.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/jquery.flot.stack.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/jquery.flot.crosshair.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/jquery.dataTables.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/jquery.tools.min.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/fullcalendar.min.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/jquery.gritter.min.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/jquery.simplemodal.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/jquery.autogrowtextarea.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/jquery.wysiwyg.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/controls/wysiwyg.image.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/controls/wysiwyg.link.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/controls/wysiwyg.table.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/jquery.idTabs.min.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/jquery.validate.min.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/chosen.jquery.min.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/jquery.jqtransform.js') }}"></script>
<script language="javascript" type="text/javascript" src="{{ asset('js/mylibs/jquery.ba-hashchange.min.js') }}"></script>

<!-- all widget initializations for every section -->
<script defer src="{{ asset('js/init.js') }}"></script>

<script defer src="{{ asset('js/elements.js') }}"></script>

<!-- main javascript file -->
<script defer src="{{ asset('js/dashboard.js') }}"></script>
<script defer src="{{ asset('js/forms.js') }}"></script>
<!-- end scripts-->
<!-- end scripts-->

<script src="{{ asset('js/libs/modernizr-2.0.6.min.js') }}"></script>

<script src="{{ asset('js/_global.js') }}"></script>

<!-- Sweet-Alert  -->
<script src="{{asset('vendors/sweetalert/dist/sweetalert.min.js')}}"></script>
    
<script src="{{asset('js/sweetalert-data.js')}}"></script>  
<script src="{{ asset('js/loadingoverlay.js') }}" type="text/javascript"></script>

 <script type="text/javascript">
   $(document).ready(function() {

    setTimeout(function(){
      $('body').addClass('loaded');
      $('h1').css('color','#222222');
    }, 500);

  });
 </script>




    

    
   
    