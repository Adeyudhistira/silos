<?php
//pra booking review
Route::get('/pra_booking_review','PrabookingreviewController@index')->name('pra_booking_review');
Route::get('/slik_pra_booking_review','PrabookingreviewController@slik');
Route::get('/appraisal_pra_booking_review','PrabookingreviewController@appraisal');
Route::get('/analisa_pra_booking_review','PrabookingreviewController@analisa');
Route::get('/ppdpp_pra_booking_review','PrabookingreviewController@ppdpp');
Route::post( '/kirim_proses_pbr', ['as'=>'kirim_proses_pbr', 'uses'=> 'PrabookingreviewController@kirim_pbr']);

//appraisal pbr
Route::get('/detail_property_pra_booking_review','PrabookingreviewController@detail_property');

//pra booking review done
Route::get('/pra_booking_review_done','PrabookingreviewdoneController@index')->name('pra_booking_review_done');
Route::get('/slik_pra_booking_review_done','PrabookingreviewdoneController@slik');
Route::get('/appraisal_pra_booking_review_done','PrabookingreviewdoneController@appraisal');
Route::get('/analisa_pra_booking_review_done','PrabookingreviewdoneController@analisa');
Route::get('/ppdpp_pra_booking_review_done','PrabookingreviewdoneController@ppdpp');

Route::post( '/kirim_data_pra_booking_review_done', ['as'=>'kirim_data_pra_booking_review_done', 'uses'=> 'PrabookingreviewdoneController@kirim_data']);
Route::get('/kirim_pra_booking_review_done','PrabookingreviewdoneController@kirim_approve');

//detail appraisal pbr done
Route::get('/detail_property_pra_booking_review_done','PrabookingreviewdoneController@detail_property');

//approval permohonan cair ppdpp
Route::get('/approval_permohonan_cair_ppdpp','ApprovalPermohonanCairPpdppController@index')->name('approval_permohonan_cair_ppdpp');
Route::get('/slik_approval_permohonan_cair_ppdpp','ApprovalPermohonanCairPpdppController@slik');
Route::get('/appraisal_approval_permohonan_cair_ppdpp','ApprovalPermohonanCairPpdppController@appraisal');
Route::get('/analisa_approval_permohonan_cair_ppdpp','ApprovalPermohonanCairPpdppController@analisa');
Route::get('/ppdpp_approval_permohonan_cair_ppdpp','ApprovalPermohonanCairPpdppController@ppdpp');

Route::post( '/kirim_data_approval_permohonan_cair_ppdpp', ['as'=>'kirim_data_approval_permohonan_cair_ppdpp', 'uses'=> 'ApprovalPermohonanCairPpdppControllerr@kirim_data']);
Route::get('/kirim_approval_permohonan_cair_ppdpp','ApprovalPermohonanCairPpdppController@kirim_approve');

//detail appraisal permohonan cair ppdpp
Route::get('/detail_property_approval_permohonan_cair_ppdpp','ApprovalPermohonanCairPpdppController@detail_property');


//update status siap cair
Route::get('/update_status_siap_cair','UpdateStatusSiapCairController@index')->name('update_status_siap_cair');
Route::get('/slik_update_status_siap_cair','UpdateStatusSiapCairController@slik');
Route::get('/appraisal_update_status_siap_cair','UpdateStatusSiapCairController@appraisal');
Route::get('/analisa_update_status_siap_cair','UpdateStatusSiapCairController@analisa');
Route::get('/ppdpp_update_status_siap_cair','UpdateStatusSiapCairController@ppdpp');
Route::post( '/kirim_data_update_status_siap_cair', ['as'=>'kirim_data_update_status_siap_cair', 'uses'=> 'UpdateStatusSiapCairController@kirim_data']);
Route::get('/kirim_update_status_siap_cair','UpdateStatusSiapCairController@kirim_approve');

//detail appraisal update status siap cair
Route::get('/detail_property_update_status_siap_cair','UpdateStatusSiapCairController@detail_property');

//permohonan cair cbs
Route::get('/permohonan_cair_cbs','PermohonanCairCbsController@index')->name('permohonan_cair_cbs');
Route::get('/slik_permohonan_cair_cbs','PermohonanCairCbsController@slik');
Route::get('/appraisal_permohonan_cair_cbs','PermohonanCairCbsController@appraisal');
Route::get('/analisa_permohonan_cair_cbs','PermohonanCairCbsController@analisa');
Route::get('/ppdpp_permohonan_cair_cbs','PermohonanCairCbsController@ppdpp');
Route::post( '/kirim_data_permohonan_cair_cbs', ['as'=>'kirim_data_permohonan_cair_cbs', 'uses'=> 'PermohonanCairCbsController@kirim_data']);
Route::get('/kirim_permohonan_cair_cbs','PermohonanCairCbsController@kirim_approve');

//detail appraisal update status siap cair
Route::get('/detail_property_permohonan_cair_cbs','UpdateStatusSiapCairController@detail_property');

//approval permohonan cair cbs
Route::get('/approval_permohonan_cair_cbs','ApprovalPermohonanCairCbsController@index')->name('approval_permohonan_cair_cbs');
Route::get('/slik_approval_permohonan_cair_cbs','ApprovalPermohonanCairCbsController@slik');
Route::get('/appraisal_approval_permohonan_cair_cbs','ApprovalPermohonanCairCbsController@appraisal');
Route::get('/analisa_approval_permohonan_cair_cbs','ApprovalPermohonanCairCbsController@analisa');
Route::get('/ppdpp_approval_permohonan_cair_cbs','ApprovalPermohonanCairCbsController@ppdpp');
// Route::post( '/kirim_proses', ['as'=>'kirim_proses', 'uses'=> 'ApprovalPermohonanCairCbsController@kirim_proses']);

//detail appraisal update status siap cair
Route::get('/detail_property_approval_permohonan_cair_cbs','ApprovalPermohonanCairCbsController@detail_property');

?>