<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Response;
use DB;
use Hash;
use Auth;
use App\Models\MNasabahModel;
use App\Models\MAPropertyModel;
use App\Models\MPembiayaanModel;
use App\Models\MasterCairModel;
use App\Models\AgunanChecklistModel;

use App\MyFunc;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;

use Excel;

class PPDPPController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     public function permohonan_uji(Request $request)
    {
        $param['data'] = \DB::select("
        select n.id,n.nama_nasabah,a.perumahan,p.harga_object,p.jangka_waktu,p.total_angsuran
        ,p.plafon,residence_name
        from
        master_pembiayaan p 
        join master_nasabah n on p.nasabah_id=n.id
        join master_agunan_property a on a.id_pembiayaan=p.id
		left join ref_residence rs on rs.id = a.id_perumahan
        where p.no_surat_dks is null or p.no_surat_dks=''");
         $param['login']=\DB::select("select * from logins");

        if ($request->ajax()) {
            $view = view('ppdpp.permohonan_uji',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'ppdpp.permohonan_uji',$param);
    }

    public function edit_permohonan_uji($id,$type,Request $request)
    {

        $param['type'] = $type;
         $param['data']  = collect(\DB::select("SELECT *,mn.id as id_nas,mp.id as id_mp, maap.id as id_maap FROM master_nasabah mn
        join master_pembiayaan mp on mp.nasabah_id = mn.id
        join master_agunan_property maap on maap.id_pembiayaan = mp.id where mn.id = '".$id."'"))->first();

        if ($request->ajax()) {
            $view = view('ppdpp.edit_permohonan_uji',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'ppdpp.edit_permohonan_uji',$param);


        // return json_encode($data);


    }

    public function GetJadwalAngsur(Request $request)
    {
        $id = $request->input('id');

        $data = array(
            "idberkas" => $id,
            "batchid" => $request->get('batchid')
        );

        $url = env('API_BASE_URL_UTIL')."/get-angsuran";
         
        $client = new Client();
        $headers = [
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
           
            return $data_result;

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return "99";
        }
    }

    public function simulasi_angsuran(Request $request)
    {
        $plafon = $request->input('plafon');
        $jangka_waktu = $request->input('jangka_waktu');

        $plafon = str_replace(".","",$plafon);
         
        $data = array(
            "product_id" => 1,
            "harga_objek" => $plafon,
            "uang_muka" => 0
        );

        $url = env('API_BASE_URL')."/master/simulasi-angsuran?jangka-waktu=".$jangka_waktu;
         
        $client = new Client();
        $headers = [
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
           
            return number_format($data_result['data']['angsuran'],0,',','.');

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return "99";
        }
    }

    public function add_permohonan_uji(Request $request)
    {




         $id_nas = $request->input('id_nas');
         $id_mp = $request->input('id_mp');
         $id_maap = $request->input('id_maap');

        //  dd($id_mp);

        if ( $id_nas == "kosong" || $id_mp == "kosong" || $id_maap == "kosong") {
            $datan = new MNasabahModel();
            $data = new MPembiayaanModel();
            $datap = new MAPropertyModel();
        }else{
             $datan = MNasabahModel::find($id_nas);
             $data = MPembiayaanModel::find($id_mp);
             $datap = MAPropertyModel::find($id_maap);
        }
        
        
         $datan->no_identitas = $request->input('no_identitas');
         $datan->nama_nasabah = $request->input('nama_nasabah');
         $datan->jns_kelamin = $request->input('jk');
         $datan->id_pekerjaan = $request->input('id_pekerjaan'); 
         $datan->npwp = $request->input('npwp'); 
         $datan->pendapatan_bulan = $request->input('gp'); 
         $datan->nama_p = $request->input('nama_p'); 
         $datan->ktp_p = $request->input('ktp_p'); 
         $datan->telp = $request->input('telp'); 
         $datan->gaji_pokok = $request->input('gp'); 
         $datan->kd_jns_kpr = $request->input('kd_jns_kpr');

         $datan->save();

         
        
         $data->harga_object = str_replace('.','',$request->input('harga_object'));
         $data->jangka_waktu = $request->input('jangka_waktu');
         $data->total_angsuran = str_replace('.','',$request->input('total_angsuran'));
         $data->plafon = str_replace('.','',$request->input('plafon'));
         $data->nasabah_id = $datan->id;
         $data->plafon_flpp = str_replace('.','',$request->input('plafon_flpp'));
         $data->bunga = str_replace(',','.',$request->input('bunga'));
         $data->no_sp3k = $request->input('no_sp3k');
         $data->no_id_uji = $request->input('no_id_uji');
         $data->tgl_sp3k = $request->input('tgl_sp3k');

         $data->save();

         $datap->kota_kab = $request->input('kota_kab');
         $datap->id_developer = $request->input('id_developer');
         $datap->id_perumahan = $request->input('id_perumahan');

         $datap->nama_developer = $request->input('nama_developer');
         $datap->alamat = $request->input('alamat');
         $datap->kodepos = $request->input('kodepos');
         $datap->luas_tanah_fisik = $request->input('luas_tanah_fisik');
         $datap->luas_imb = $request->input('luas_imb');
         $datap->perumahan = $request->input('perumahan');
         $datap->id_pembiayaan = $data->id;
         $datap->save();
         return json_encode(['rc' => 0, 'rm' => 'Data Berhasil']);

         //return Redirect('nasabah_uji');


    }

    public function update_dks(Request $request){

        \DB::table('master_pembiayaan')->whereIn('nasabah_id', $request->get('value'))->update(['no_surat_dks' => $request->get('no_surat'),'tgl_surat_dks'=> date('Y-m-d',strtotime($request->get('tgl')))]);

        return json_encode(['rc' => 0, 'rm' => 'Data Berhasil']);
    }

     public function nasabah_uji(Request $request)
    {
        $param['data'] = \DB::select("select no_surat_dks,tgl_surat_dks,count(no_surat_dks) as jumlah
        from master_pembiayaan
				where no_surat_dks <> '' or no_surat_dks <> null
        GROUP BY no_surat_dks,tgl_surat_dks");
         $param['login']=\DB::select("select * from logins");
        if ($request->ajax()) {
            $view = view('ppdpp.nasabah_uji',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'ppdpp.nasabah_uji',$param);
    }

      public function lihat_nasabah(Request $request)
    {

        //$id = str_replace('"',"'",$id);
        $id=$request->get('id');
        $param['kode'] = $id;
        $param['data'] = \DB::select("SELECT n.id,n.no_identitas,n.nama_nasabah,n.npwp,n.ktp_p,n.nama_p,d.developer_name,
            case when n.jns_kelamin=1 then 'L' else 'P' end as kelamin,n.pendapatan_bulan,
            p.harga_object,p.plafon,p.bunga,p.jangka_waktu,p.total_angsuran,p.plafon_flpp,
            r.residence_name,a.alamat,a.kodepos,a.luas_tanah_fisik,a.luas_imb,
            a.kode_dati2,n.id_pekerjaan,p.no_sp3k,p.tgl_sp3k,n.kd_jns_kpr,
            no_id_uji,a.kota_kab,is_approve,rc.keterangan,rc.rc
            FROM master_pembiayaan p
            left join master_nasabah n on n.id=p.nasabah_id
            left join master_agunan_property a on a.id_pembiayaan=p.id
            left join ref_developer d on d.id=a.id_developer
            left join ref_residence r on r.id=a.id_perumahan
            left join rc_uji rc on rc.rc=p.rc_uji
       where p.no_surat_dks='".$id."'");


        $param['login']=\DB::select("select * from logins");
        if ($request->ajax()) {
            $view = view('ppdpp.lihat_nasabah',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'ppdpp.lihat_nasabah',$param);
    }

     public function master_ppdpp(Request $request)
    {
        $param['data'] = \DB::select("SELECT master_data_ppdpp.id_berkas,no_permintaan, no_cair, tgl_pencairan,sum(nilai_flpp) as tarif_flpp, sum(nilai_flpp), count(id_ktp),mdic.is_cair 
        FROM master_data_ppdpp
        left join master_data_idberkas_cair mdic on  mdic.id_berkas = master_data_ppdpp.id_berkas
        GROUP BY master_data_ppdpp.id_berkas,no_permintaan ,no_cair, tgl_pencairan,mdic.is_cair");
        $param['login']=\DB::select("select * from logins");
        if ($request->ajax()) {
            $view = view('ppdpp.master_ppdpp',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'ppdpp.master_ppdpp',$param);
    }

    public function detail_master_ppdpp($id,Request $request)
    {
        
        $id = str_replace('"',"'",$id);
        $param['data'] = \DB::select("SELECT norek_kelola,id_ktp, no_cair,nama, tgl_pencairan, sum(nilai_flpp) as nilai_flpp, sum(nilai_angsuran) as nilai_angsuran FROM master_data_ppdpp
            WHERE id_berkas = ".$id."
            GROUP BY id_ktp, no_cair, tgl_pencairan,norek_kelola,nama");
        if ($request->ajax()) {
            $view = view('ppdpp.detail_master_ppdpp',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'ppdpp.detail_master_ppdpp',$param);
    }

      public function lihat_detail_nasabah_sudah_cair($id,Request $request)
    {
         $param['login']=\DB::select("select * from logins");   
        $id = str_replace('"',"'",$id);
        $param['kode'] = $id;
        $param['data'] = \DB::select("select n.id,n.nama_nasabah,a.perumahan,p.harga_object,p.jangka_waktu,p.total_angsuran
        ,p.plafon
        from
        master_pembiayaan p 
        left join master_nasabah n on p.nasabah_id=n.id
       left join master_agunan_property a on a.id_pembiayaan=p.id where p.id_berkas=".$id);
        if ($request->ajax()) {
            $view = view('ppdpp.lihat_nasabah_sudah_cair',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'ppdpp.lihat_nasabah_sudah_cair',$param);
    }



    public function lihat_nasabah_siap_cair(Request $request)
    {
        $param['data'] = \DB::select("select no_identitas,p.tgl_pencairan,no_cair,id_berkas,no_surat_dks,n.id,n.nama_nasabah,a.perumahan,p.harga_object,p.jangka_waktu,p.total_angsuran
        ,p.plafon
        from
        master_pembiayaan p 
        left join master_nasabah n on p.nasabah_id=n.id
       left join master_agunan_property a on a.id_pembiayaan=p.id where is_cair_ppdpp = 't' and is_cair_smf = 't'");
        $param['login']=\DB::select("select * from logins");
        if ($request->ajax()) {
            $view = view('ppdpp.nasabah_siap_cair',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'ppdpp.nasabah_siap_cair',$param);
    }

     public function lihat_nasabah_sudah_cair(Request $request)
    {
        $param['data'] = \DB::select("select no_identitas,p.tgl_pencairan,no_cair,id_berkas,no_surat_dks,n.id,n.nama_nasabah,a.perumahan,p.harga_object,p.jangka_waktu,p.total_angsuran
        ,p.plafon
        from
        master_pembiayaan p 
        left join master_nasabah n on p.nasabah_id=n.id
       left join master_agunan_property a on a.id_pembiayaan=p.id where is_cair_ppdpp = 't' and is_cair_smf = 't' and is_cair_cbs = 't' ");
        if ($request->ajax()) {
            $view = view('ppdpp.nasabah_sudah_cair',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'ppdpp.nasabah_sudah_cair',$param);
    }

    public function table($id){
        $query = \DB::select("select n.id,n.nama_nasabah,a.perumahan,p.harga_object,p.jangka_waktu,p.total_angsuran
        ,p.plafon
        from
        master_pembiayaan p 
        left join master_nasabah n on p.nasabah_id=n.id
       left join master_agunan_property a on a.id_pembiayaan=p.id where p.no_surat_dks='".$id."'");

        $data = Datatables::of($query)
        ->addColumn('action', function ($query) {
            return '
            <button class="i-button no-margin" onclick="kirimDataUji('.$query->id.')">Kirim Ulang Data Uji</button>
            <button class="i-button no-margin" onclick="showAkad('.$query->id.')">Upload Nomor Akad</button> 
            <button class="i-button no-margin" onclick="updateStatusCairPPDPP('.$query->id.')">Upload Status Cair PPDPP</button> 
            <button class="i-button no-margin" onclick="updateStatusCairSMF('.$query->id.')">Upload Status Cair SMF</button> 

                    ';
            })->make(true);

        return $data;
    }

    public function ActdksBaru(Request $request)
    {
        $nosuratdks = $request->input('nosuratdks');
        $tglsuratdks = $request->input('tglsuratdks');
        $total_nik = $request->input('total_nik');
        $login=\DB::select("select * from logins");
        $data = array(
            'nosuratdks'=> $nosuratdks,
            'tglsuratdks' => $tglsuratdks,
            'total_nik' => $total_nik,
            "batchid" => $login[0]->batch_id
        );

        $url = env('API_BASE_URL_PRAUJI')."/set-new-dks";
         
        $client = new Client();
        $headers = [
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
            

            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);

            return $data_result;

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return "99";
        }
    }

    public function ActambilIdUji(Request $request)
    {
        $nosuratdks = $request->input('nosuratdks');
        $login=\DB::select("select * from logins");
        $data = array(
            'nosuratdks'=> $nosuratdks,
            "batchid" => $login[0]->batch_id
        );

        $url = env('API_BASE_URL_PRAUJI')."/get-masterdks-nosurat";
         
        $client = new Client();
        $headers = [
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
            
            
            MPembiayaanModel::where('no_surat_dks', $nosuratdks)
                ->update(['no_id_uji' => $data_result['data']['no_iduji'] ]);

            return $data_result;

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return "99";
        }
    }

     public function ActkirimIdUji(Request $request)
    {
        $nosuratdks = $request->input('nosuratdks');
        $login=\DB::select("select * from logins");

        $get_datas = \DB::select("SELECT p.nasabah_id,n.no_identitas,n.nama_nasabah,n.npwp,n.ktp_p,n.nama_p,d.developer_name,
            case when n.jns_kelamin=1 then 'L' else 'P' end as kelamin,n.pendapatan_bulan,
            p.harga_object,p.plafon,p.bunga,p.jangka_waktu,p.total_angsuran,p.plafon_flpp,
            r.residence_name,a.alamat,a.kodepos,a.luas_tanah_fisik,a.luas_imb,
            a.kode_dati2,n.id_pekerjaan,p.no_sp3k,p.tgl_sp3k,n.kd_jns_kpr,
            no_id_uji,a.kota_kab
            FROM master_pembiayaan p
            left join master_nasabah n on n.id=p.nasabah_id
            left join master_agunan_property a on a.id_pembiayaan=p.id
            left join ref_developer d on d.id=a.id_developer
            left join ref_residence r on r.id=a.id_perumahan
            where p.no_surat_dks='".$nosuratdks."'");

        if($get_datas){

            foreach ($get_datas as $item) {
            
                $data = array(
                    "batchid"=> $login[0]->batch_id,
                    "ktp" => $item->no_identitas,
                    "nama"=> $item->nama_nasabah,
                    "npwp"=> $item->npwp,
                    "ktp_p"=> $item->ktp_p,
                    "nama_p"=> $item->nama_p,
                    "badan_hukum"=> "PT",
                    "nama_badan_hukum"=> $item->developer_name,
                    "jns_kelamin"=> $item->kelamin,
                    "gaji_pokok"=> $item->pendapatan_bulan,
                    "harga_rumah"=> $item->harga_object,
                    "nilai_kpr"=> $item->plafon,
                    "suku_bunga_kpr"=> $item->bunga,
                    "tenor"=> $item->jangka_waktu,
                    "angsuran"=> $item->total_angsuran,
                    "nilai_flpp"=> $item->plafon_flpp,
                    "nama_perumahan"=> $item->residence_name,
                    "agn_alamat"=> $item->alamat,
                    "agn_kodepos"=> $item->kodepos,
                    "luas_tanah"=> $item->luas_tanah_fisik,
                    "luas_bangunan"=> $item->luas_imb,
                    "kd_jns_kpr"=> $item->kd_jns_kpr,
                    "no_sp3k"=> $item->no_sp3k,
                    "tgl_sp3k"=> $item->tgl_sp3k,
                    "agn_kota_kab"=> $item->kota_kab,
                    "pekerjaan"=> $item->id_pekerjaan,
                    "no_iduji"=> $item->no_id_uji
                );

                $url = env('API_BASE_URL_PRAUJI')."/set-uji-nik";
                 
                $client = new Client();
                $headers = [
                        'Content-Type' => 'application/json'
                    ];
            
                try{
                    $result = $client->post($url,[
                        RequestOptions::HEADERS => $headers,
                        RequestOptions::JSON => $data,
                    ]);
                
                    $param=[];
                    $param= (string) $result->getBody();
                    $data_result = json_decode($param, true);
                    
                    
                    MPembiayaanModel::where('no_surat_dks', $nosuratdks)->where('nasabah_id',$item->nasabah_id)
                        ->update(['rc_uji' => $data_result['rc'] ]);

                    //return $data_result;

                }catch (BadResponseException $e){
                    $response = json_decode($e->getResponse()->getBody());
                    $bad_response = $this->responseData($e->getCode(), $response);
                    
                    return "99";
                }   


            }

        }else{

            return "99";
        }

         return json_encode(['rc' => 0, 'rm' => 'Data Berhasil Terkirim']);

        
    }

     public function ActkirimDataUji(Request $request)
    {
        $val_id = $request->input('val_id');
        $login=\DB::select("select * from logins");
        $get_datas = \DB::select("SELECT p.no_surat_dks,n.id,n.no_identitas,n.nama_nasabah,n.npwp,n.ktp_p,n.nama_p,d.developer_name,
            case when n.jns_kelamin=1 then 'L' else 'P' end as kelamin,n.pendapatan_bulan,
            p.harga_object,p.plafon,p.bunga,p.jangka_waktu,p.total_angsuran,p.plafon_flpp,
            r.residence_name,a.alamat,a.kodepos,a.luas_tanah_fisik,a.luas_imb,
            a.kode_dati2,n.id_pekerjaan,p.no_sp3k,p.tgl_sp3k,n.kd_jns_kpr,
            no_id_uji,a.kota_kab,a.id_kota_kab
            FROM master_pembiayaan p
            left join master_nasabah n on n.id=p.nasabah_id
            left join master_agunan_property a on a.id_pembiayaan=p.id
            left join ref_developer d on d.id=a.id_developer
            left join ref_residence r on r.id=a.id_perumahan
            where p.nasabah_id = '".$val_id."'");

        if($get_datas){

            foreach ($get_datas as $item) {
            
                $data = array(
                    "batchid"=> $login[0]->batch_id,
                    "ktp" => $item->no_identitas,
                    "nama"=> $item->nama_nasabah,
                    "npwp"=> $item->npwp,
                    "ktp_p"=> $item->ktp_p,
                    "nama_p"=> $item->nama_p,
                    "badan_hukum"=> "PT",
                    "nama_badan_hukum"=> $item->developer_name,
                    "jns_kelamin"=> $item->kelamin,
                    "gaji_pokok"=> $item->pendapatan_bulan,
                    "harga_rumah"=> $item->harga_object,
                    "nilai_kpr"=> $item->plafon,
                    "suku_bunga_kpr"=> $item->bunga,
                    "tenor"=> $item->jangka_waktu,
                    "angsuran"=> $item->total_angsuran,
                    "nilai_flpp"=> $item->plafon_flpp,
                    "nama_perumahan"=> $item->residence_name,
                    "agn_alamat"=> $item->alamat,
                    "agn_kodepos"=> $item->kodepos,
                    "luas_tanah"=> $item->luas_tanah_fisik,
                    "luas_bangunan"=> $item->luas_imb,
                    "kd_jns_kpr"=> $item->kd_jns_kpr,
                    "no_sp3k"=> $item->no_sp3k,
                    "tgl_sp3k"=> $item->tgl_sp3k,
                    "agn_kota_kab"=> $item->id_kota_kab,
                    "pekerjaan"=> $item->id_pekerjaan,
                    "no_iduji"=> $item->no_id_uji
                );

                $url = env('API_BASE_URL_PRAUJI')."/set-uji-nik";
                 
                $client = new Client();
                $headers = [
                        'Content-Type' => 'application/json'
                    ];
            
                try{
                    $result = $client->post($url,[
                        RequestOptions::HEADERS => $headers,
                        RequestOptions::JSON => $data,
                    ]);
                
                    $param=[];
                    $param= (string) $result->getBody();
                    $data_result = json_decode($param, true);
                    
                    
                    MPembiayaanModel::where('no_surat_dks', $item->no_surat_dks)->where('nasabah_id',$item->id)
                        ->update(['rc_uji' => $data_result['rc'] ]);

                    return $data_result;

                }catch (BadResponseException $e){
                    $response = json_decode($e->getResponse()->getBody());
                    $bad_response = $this->responseData($e->getCode(), $response);
                    
                    return "99";
                }   


            }

        }else{

            return "99";
        }


    }

     public function ActupdateStatusCairPPDPP(Request $request)
    {
        $val_id = $request->input('val_id');

          MPembiayaanModel::where('nasabah_id', $val_id)
                ->update(['is_cair_ppdpp' => 't' ]);

         return json_encode(['rc' => 0, 'rm' => 'Data Berhasil Terkirim']);
    }

    public function ActupdateStatusCairSMF(Request $request)
    {
        $val_id = $request->input('val_id');

          MPembiayaanModel::where('nasabah_id', $val_id)
                ->update(['is_cair_smf' => 't' ]);

         return json_encode(['rc' => 0, 'rm' => 'Data Berhasil Terkirim']);
    }

    public function ActuupdateNoAkad(Request $request)
    {
        $no_akad = $request->input('no_akad');
        $tgl_akad = date('Y-m-d',strtotime($request->input('tgl_akad')));
        $id_nasabah = $request->input('id_nasabah');

        MPembiayaanModel::where('nasabah_id', $id_nasabah)
                ->update([
                    'no_akad' => $no_akad,
                    'tgl_akad' => $tgl_akad ]);

        $data = MNasabahModel::find($id_nasabah);
        $data->no_identitas;
        $login=\DB::select("select * from logins");

         $data = array(
            "batchid"=> $login[0]->batch_id,
            'tglakad'=> $tgl_akad,
            'norekening' => $no_akad,
            'ktp' =>  $data->no_identitas

        );

        $url = env('API_BASE_URL_PRAUJI')."/set-akad-rekening";
         
        $client = new Client();
        $headers = [
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
            
            return $data_result;

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return "99";
        }


    }

    public function ambilIdBerkas(Request $request)
    {
        $ktp = $request->input('ktp');
        $id_nasabah = $request->input('id_nasabah');
        $login=\DB::select("select * from logins");
         $data = array(
            "batchid"=> $login[0]->batch_id,
            'ktp'=> $ktp
        );

        $url = env('API_BASE_URL_ANGSURAN')."/get-angsuran-allbyktp";
         
        $client = new Client();
        $headers = [
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);

            $data_id_berkas = $data_result['data'][0]['id_berkas'];

            MPembiayaanModel::where('nasabah_id', $id_nasabah)
                ->update([
                    'id_berkas' => $data_id_berkas ]);

            
            return $data_result;

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return "99";
        }


    }

    public function updateStatusCair(Request $request)
    {
        $id_nasabah = $request->input('id_nasabah');
         MPembiayaanModel::where('nasabah_id', $id_nasabah)
                ->update([
                    'is_cair_cbs' => 't' ]);
    }

     public function setPencairan(Request $request)
    {
        $id_berkas = $request->input('id_berkas');
        $nilaimutasicair = (int)$request->input('nilaimutasicair');
        $login=\DB::select("select * from logins");

        $query=\DB::select(" SELECT norek_kelola,id_ktp,norek_program, no_cair,nama, tgl_pencairan, sum(nilai_flpp) as nilai_flpp, sum(nilai_angsuran) as nilai_angsuran FROM master_data_ppdpp
            WHERE id_berkas = '".$id_berkas."'
            GROUP BY id_ktp, no_cair, tgl_pencairan,norek_kelola,norek_program,nama");

        if($query){

            foreach ($query as $item) {
                    
                  $data = array(
                    "batchid"=> $login[0]->batch_id,
                    "pin"=> "4254256575f3ccee1601f115d8a333551",
                    "idberkas"=> $id_berkas,
                    "nilaimutasicair" => $nilaimutasicair,
                    "tanggallmutasicair" => date('Y-m-d'),
                    "norekeningkelola"=> $item->norek_kelola,
                    "norekeningprogram"=> $item->norek_program
                );

        // return $data;

                  $url = env('API_BASE_URL_ANGSURAN')."/set-pencairan";
                  
                  $client = new Client();
                  $headers = [
                    'Content-Type' => 'application/json'
                ];
                
                try{
                    $result = $client->post($url,[
                        RequestOptions::HEADERS => $headers,
                        RequestOptions::JSON => $data,
                    ]);
                    
                    $param=[];
                    $param= (string) $result->getBody();
                    $data_result = json_decode($param, true);
                    
                    
                        $datap = new MasterCairModel();
                        $datap->id_berkas = $id_berkas;
                        $datap->is_cair = 't';
                        $datap->user_id = 1;
                        $datap->created_at = date('Y-m-d h:i:s');
                        $datap->save();    
                    

                    

                    //return $data_result;

                }catch (BadResponseException $e){
                    $response = json_decode($e->getResponse()->getBody());
                    $bad_response = $this->responseData($e->getCode(), $response);
                    
                    return response()->json([
                    'rc' => 99,
                    'rm' => "Internal Server Error"
                ]); 
                }  

            }
            return response()->json([
                    'rc' => 1,
                    'rm' => "Berhasil"
                ]); 

        }

            return response()->json([
                    'rc' => 2,
                    'rm' => "Tidak ad data"
                ]); 


        
    }

    public function getHasilUji(Request $request)
    {
        $no_dks = $request->input('no_dks'); 
        $login=\DB::select("select * from logins");
        $query = \DB::select("select no_identitas,p.nasabah_id
            from
            master_pembiayaan p 
            left join master_nasabah n on p.nasabah_id=n.id
           left join master_agunan_property a on a.id_pembiayaan=p.id 
           left join rc_uji rc on rc.rc=p.rc_uji
           where p.no_surat_dks='".$no_dks."'");

        if($query){

            foreach ($query as $item) {
                
                $data = array(
                    "batchid"=> $login[0]->batch_id,
                    "ktp"=> $item->no_identitas
                );

        // return $data;

                $url = env('API_BASE_URL_PRAUJI')."/get-info-uji";

                $client = new Client();
                $headers = [
                    'Content-Type' => 'application/json'
                ];

                try{
                    $result = $client->get($url,[
                        RequestOptions::HEADERS => $headers,
                        RequestOptions::JSON => $data,
                    ]);

                    $param=[];
                    $param= (string) $result->getBody();
                    $data_result = json_decode($param, true);

                    $rc=$data_result['rc'];
                    //var_dump($data_result);

                    MPembiayaanModel::where('no_surat_dks', $no_dks)->where('nasabah_id',$item->nasabah_id)
                    ->update(['is_approve' => 't','rc_uji' => $rc  ]);


                    //return $data_result;

                }catch (BadResponseException $e){
                    $response = json_decode($e->getResponse()->getBody());
                    $bad_response = $this->responseData($e->getCode(), $response);

                    return response()->json([
                        'rc' => 99,
                        'rm' => "Internal Server Error"
                    ]); 
                }


            }

            return response()->json([
                    'rc' => 1,
                    'rm' => "Berhasil"
                ]); 

        }

        return response()->json([
                    'rc' => 2,
                    'rm' => "Data tidak ada"
                ]); 

    }

    public function jadwalAngsurByKTP(Request $request){
        $bulan=date('m');
        $tahun=date('Y');
        
        $query = \DB::select("SELECT * FROM master_data_angsuran_idberkas where ktp='". $request->get('id') ."' ORDER BY d_tahun asc,d_bulan asc");
        if($request->has('reset')){
            $query = \DB::select("SELECT * FROM master_data_angsuran_idberkas where ktp='". $request->get('id') ."' ORDER BY d_tahun asc,d_bulan asc");
        }

        if ($request->has('filter')) {
                $flt=true;
                $bulan=$request->get('bulan');
                $tahun=$request->get('tahun');

                $query= \DB::select("SELECT * FROM master_data_angsuran_idberkas where ktp='". $request->get('id') ."' and d_bulan='".$bulan."' and d_tahun='".$tahun."' ORDER BY d_tahun asc,d_bulan asc"); 

            }  
        $param['data']=$query;    
        $param['id']=$request->get('id');    
        $param['bulan']=$bulan;
        $param['tahun']=$tahun;
        if ($request->ajax()) {
            $view = view('ppdpp.detail_angsur_byktp',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'ppdpp.detail_angsur_byktp',$param);

    }

     public function jadwalAngsurByIDberkas(Request $request){

        $param['data'] = \DB::select("SELECT id_berkas,d_bulan,d_tahun,sum(d_sisapokok) as SisaPokok,sum(d_flpp_pokok + d_flpp_tarif) as nilaiAngsuran  
            ,sum(d_flpp_pokok) as pokok,sum(d_flpp_tarif) as tarif , sum(d_flpp_outstanding) as outstanding 
            FROM master_data_angsuran_idberkas where id_berkas='".$request->get('id')."' 
            GROUP BY id_berkas,d_bulan,d_tahun
            ORDER BY d_tahun asc,d_bulan asc");
        if ($request->ajax()) {
            $view = view('ppdpp.detail_angsur_byberkas',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'ppdpp.detail_angsur_byberkas',$param);

    }

    public function GetSisaPembayaran(Request $request)
    {
        $ktp = $request->input('ktp');
        $batchid = $request->input('batchid');


         $data = array(
            "ktp"=> $ktp,
            'batchid'=> $batchid
        );

        $url = env('API_BASE_URL_ANGSURAN')."/get-sisa-pembayaran-byktp";
         
        $client = new Client();
        $headers = [
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
            

            $flpp_pkok = number_format($data_result['data']['0']['sisa_flpp_pokok'],0,',','.');
            $flpp_tarif = number_format($data_result['data']['0']['sisa_flpp_tarif'],0,',','.');

            $for_show = "Sisa FLPP Pokok : ".$flpp_pkok."\n 
            Sisa FLPP Tarif : ".$flpp_tarif;
            
            //  $for_show =  $data_result['data']['0']['sisa_flpp_pokok'];
            return $for_show;

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return "99";
        }
    }

    public function ceklis_agunan($id)
    {
        // $param['data']  = collect(\DB::select("SELECT *,mn.id as id_nas,mp.id as id_mp, maap.id as id_maap FROM master_nasabah mn
        // join master_pembiayaan mp on mp.nasabah_id = mn.id
        // join master_agunan_property maap on maap.id_pembiayaan = mp.id where mn.id = '".$id."'"))->first();
        $data = \DB::select("SELECT * FROM master_agunan_property_checklist WHERE id_agunan_property = ".$id);
        
        if(count($data) == 0){
            for ($i=1; $i <= 11; $i++) { 
                $datap = new AgunanChecklistModel();
                $datap->id_agunan_property = $id;
                $datap->sub_parm_id = $i;
                $datap->value_parm_id = 1;
                $datap->save();
            }
        }else{

        }

        $param['data'] = \DB::select("SELECT * 
FROM master_agunan_property_checklist mapc
join ref_sub_parm_agunan rspa on rspa.id = mapc.sub_parm_id WHERE id_agunan_property = ".$id);

        
        return view('master.master')->nest('child', 'ppdpp.checklist_agunan',$param);
    }

    public function ceklis_agunan_edit(Request $request)
    {
        $id = $request->input('id');
        $info = $request->input('info');
        /*
        $delete = AgunanChecklistModel::find($id);
        $delete->delete();
        */
        DB::table('master_agunan_property_checklist')->where('id_agunan_property', $id)->delete();

          for ($i=1; $i <= 11; $i++) { 
                $datap = new AgunanChecklistModel();
                $datap->id_agunan_property = $id;
                $datap->sub_parm_id = $i;
                $datap->value_parm_id = $info[$i];
                $datap->save();
            }

    }


    

    public function cetakexcel(Request $request){
        $fields=[];
        $query1= \DB::select("SELECT * FROM master_data_angsuran_idberkas where ktp='". $request->get('id') ."' ORDER BY d_tahun asc,d_bulan asc"); 

        return MyFunc::printto($query1,'Laporan Angsuran By KTP '.$request->get('id'), $fields, 0,['excel'=>'ppdpp.excel'],'landscape','a4');

    }

     public function cetakpdf(Request $request){
        $fields=[];
        $query1= \DB::select("SELECT * FROM master_data_angsuran_idberkas where ktp='". $request->get('id') ."' ORDER BY d_tahun asc,d_bulan asc"); 

        return MyFunc::pdf($query1,'Laporan Angsuran By KTP '.$request->get('id') ,$fields,'ppdpp.pdf');

    }

    public function cetakexcel1(Request $request){
        $fields=[];
        $query1= \DB::select("SELECT id_berkas,sum(d_sisapokok) as SisaPokok,sum(d_flpp_pokok + d_flpp_tarif) as nilaiAngsuran  
            ,sum(d_flpp_pokok) as pokok,sum(d_flpp_tarif) as tarif , sum(d_flpp_outstanding) as outstanding
            FROM master_data_angsuran_idberkas where id_berkas='".$request->get('id')."' 
            GROUP BY id_berkas");

        return MyFunc::printto($query1,'Laporan Angsuran batch '.$request->get('id'), $fields, 0,['excel'=>'ppdpp.excel1'],'landscape','a4');

    }

     public function cetakpdf1(Request $request){
        $fields=[];
        $query1=\DB::select("SELECT id_berkas,d_bulan,d_tahun,sum(d_sisapokok) as SisaPokok,sum(d_flpp_pokok + d_flpp_tarif) as nilaiAngsuran  
            ,sum(d_flpp_pokok) as pokok,sum(d_flpp_tarif) as tarif , sum(d_flpp_outstanding) as outstanding 
            FROM master_data_angsuran_idberkas where id_berkas='".$request->get('id')."' 
            GROUP BY id_berkas,d_bulan,d_tahun
            ORDER BY d_tahun asc,d_bulan asc");

        return MyFunc::pdf($query1,'Laporan Angsuran batch '.$request->get('id') ,$fields,'ppdpp.pdf1');

    }

    public function upload_data_uji(Request $request)
    {


        $cek=0;

       if($request->hasFile('file')){

        $path = $request->file('file')->getRealPath();
        $data = Excel::load($path, function($reader) {})->get();

        if(!empty($data) && $data->count()){

            foreach ($data->toArray() as $key => $value) {

                    if(!empty($value)){

                        $id_nas = $request->input('id_nas');
                         $id_mp = $request->input('id_mp');
                         $id_maap = $request->input('id_maap');

                        //  dd($id_mp);

                        //if ( $id_nas == "kosong" || $id_mp == "kosong" || $id_maap == "kosong") {
                            
                        /*
                        }else{
                             $datan = MNasabahModel::find($id_nas);
                             $data = MPembiayaanModel::find($id_mp);
                             $datap = MAPropertyModel::find($id_maap);
                        }
                        */
                        
                        if($value['no_identitas']!=NULL || $value['no_identitas']!='NULL' || $value['no_identitas']!=''){
                           // var_dump($value);
                            $datan = new MNasabahModel();
                            $data = new MPembiayaanModel();
                            $datap = new MAPropertyModel();

                            $datan->no_identitas = ($value ? $value['no_identitas'] : '0');
                             $datan->nama_nasabah = ($value ? $value['nama_nasabah'] : '0') ;
                             $datan->jns_kelamin = ($value ? $value['jenis_kelamin'] : '0');
                             $datan->id_pekerjaan = ($value ? $value['pekerjaan'] : '0'); 
                             $datan->npwp = ($value ? $value['npwp'] : '0'); 
                             $datan->pendapatan_bulan = ($value ? $value['gaji_pokok'] : '0'); 
                             $datan->nama_p = ($value ? $value['nama_pasangan'] : '0'); 
                             $datan->ktp_p = ($value ? $value['ktp_pasangan'] : '0'); 
                             $datan->telp = ($value ? $value['telp'] : '0'); 
                             $datan->gaji_pokok = ($value ? $value['gaji_pokok'] : '0'); 
                             $datan->kd_jns_kpr = ($value ? $value['kode_jenis_kpr'] : '0');
                             $datan->kode_cabang = ($value ? $value['kode_cabang'] : '0');
                             $datan->save();

                             
                            
                             $data->harga_object = ($value ? $value['harga_rumah'] : '0');
                             $data->jangka_waktu = ($value ? $value['tenor'] : '0');
                             $data->total_angsuran = ($value ? $value['angsuran'] : '0');
                             $data->plafon = ($value ? $value['nilai_kpr'] : '0');
                             $data->nasabah_id = $datan->id;
                             $data->plafon_flpp  = ($value ? $value['nilai_flpp'] : '0');
                             $data->bunga = ($value ? $value['suku_bunga_kpr'] : '0');
                             $data->no_sp3k = ($value ? $value['no_sp3k'] : '0');
                             $data->tgl_sp3k = ($value ? date('Y-m-d',strtotime($value['tgl_sp3k'])) : date('Y-m-d'));

                             $data->save();

                             $datap->id_kota_kab = ($value ? $value['kota_kabupaten'] : '0');
                             $datap->id_kecamatan = ($value ? $value['kecamatan'] : '0');
                             $datap->id_kelurahan = ($value ? $value['kelurahan'] : '0');
                             
                             $datap->id_developer = ($value ? $value['developer'] : '0');
                             $datap->id_perumahan = ($value ? $value['residence'] : '0');

                             //$datap->nama_developer = $value['residence'] $request->input('nama_developer');
                             $datap->alamat = ($value ? $value['alamat'] : '0');
                             $datap->kodepos = ($value ? $value['kodepos'] : '0');
                             $datap->luas_tanah_fisik =($value ? $value['luas_tanah'] : '0');
                             $datap->luas_imb = $value ? $value['luas_bangunan'] : '0';
                             //$datap->perumahan = $request->input('perumahan');
                             $datap->id_pembiayaan = $data->id;
                             $datap->save();

                        }
                         

                    }
                        
               
                }
            }
        }
        
        return json_encode(['rc' => 0, 'rm' => 'Data Berhasil diupload']);
    }

}
