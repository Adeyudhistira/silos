<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});


*/
Auth::routes();
/*
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login1', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});
*/


Route::post('login1',['as'=>'login1', 'uses'=> 'AuthController@LoginViaAPI']);

Route::get('/home','HomeController@index')->name('home');
Route::get( '/', ['as'=>'home', 'uses'=> 'HomeController@index']);

///prospek
Route::get('/entry_prospek','ProspekController@entry_prospek_view')->name('entry_prospek');
Route::get('/data_prospek','ProspekController@data_prospek_view')->name('data_prospek');
Route::get('/daftar_prospek_baru','ProspekController@daftar_prospek_view')->name('daftar_prospek_baru');
Route::post('simulasi',['as'=>'simulasi', 'uses'=> 'ProspekController@simulasi']);
Route::post('daftar',['as'=>'daftar', 'uses'=> 'ProspekController@daftar']);
Route::get('/ubah_prospek','ProspekController@ubah_prospek');
Route::get('/kirim_pengajuan','ProspekController@kirim_pengajuan');
Route::post('pengajuan',['as'=>'pengajuan', 'uses'=> 'ProspekController@pengajuan']);

Route::get('/dokumen','ProspekController@dokumen');
Route::get('/tambah_dokumen','ProspekController@tambah_dokumen');
Route::get('/edit_dokumen','ProspekController@edit_dokumen');
Route::post('insert_dok',['as'=>'insert_dok', 'uses'=> 'ProspekController@insert_dok']);
Route::post('update_dok',['as'=>'update_dok', 'uses'=> 'ProspekController@update_dok']);


Route::get('/tambah_dokumen_entry_nasabah','EntryNasabahController@tambah_dokumen');
Route::post('insert_dok_entry_nasabah',['as'=>'insert_dok_entry_nasabah', 'uses'=> 'EntryNasabahController@insert_dok']);

//nasabah
Route::get('/entry_nasabah','NasabahController@entry_nasabah_view')->name('entry_nasabah');
Route::get('/daftar_nasabah_baru','NasabahController@daftar_nasabah_view')->name('daftar_nasabah_baru');


//menu
Route::get( '/dalam_proses', ['as'=>'dalam_proses', 'uses'=> 'HomeController@dalam_proses']);
//Route::get('/dalam_proses','HomeController@dalam_proses')->name('dalam_proses');
Route::get('/sudah_disetujui','HomeController@sudah_disetujui')->name('sudah_disetujui');
Route::get('/slik_checking','HomeController@slik_checking')->name('slik_checking');


//select
Route::get('/select/dati/{id}','HomeController@dati');

Route::get('/select/dashboard','HomeController@dashboard');


Route::resource('wilayah', 'WilayahController');
Route::post('/wilayah/tambah','WilayahController@tambah')->name('wilayah.tambah');
Route::post('/wilayah/delete/{id}','WilayahController@delete')->name('wilayah.delete');

Route::post('/prospek/delete/{id}','PPDPPController@delete')->name('PPDPP.delete');


Route::resource('user', 'UserController');
Route::post('/user/tambah','UserController@tambah')->name('user.tambah');
Route::post('/user/delete/{id}','UserController@delete')->name('user.delete');


Route::resource('developer', 'DeveloperController');
Route::post('/developer/tambah','DeveloperController@tambah')->name('developer.tambah');
Route::post('/developer/delete/{id}','DeveloperController@delete')->name('developer.delete');


Route::resource('residence', 'ResidenceController');
Route::post('/residence/tambah','ResidenceController@tambah')->name('residence.tambah');
Route::post('/residence/delete/{id}','ResidenceController@delete')->name('residence.delete');

//ppdpp
Route::get('/permohonan_uji','PPDPPController@permohonan_uji')->name('permohonan_uji');
Route::post('/permohonan_uji/add','PPDPPController@add_permohonan_uji')->name('permohonan_uji.add');
Route::get('/permohonan_uji/show/{id}/{type}','PPDPPController@edit_permohonan_uji')->name('permohonan_uji.show');

Route::get('/ceklis_agunan/show/{id}','PPDPPController@ceklis_agunan')->name('ceklis_agunan.show');
Route::post('/checklist_agunan/edit','PPDPPController@ceklis_agunan_edit')->name('permohonan_uji.edit');

Route::post('/ppdpp/upload','PPDPPController@upload_data_uji');



Route::post('/simulasi_angsuran','PPDPPController@simulasi_angsuran')->name('simulasi_angsuran');


Route::post('dks/update','PPDPPController@update_dks');
Route::get('/nasabah_uji','PPDPPController@nasabah_uji')->name('nasabah_uji');
Route::get('dks/table/{id}','PPDPPController@table');
Route::get('/lihat_nasabah','PPDPPController@lihat_nasabah')->name('lihat_nasabah');
Route::get('/lihat_nasabah_siap_cair','PPDPPController@lihat_nasabah_siap_cair')->name('lihat_nasabah_siap_cair');
Route::get('/lihat_nasabah_sudah_cair','PPDPPController@lihat_nasabah_sudah_cair')->name('lihat_nasabah_sudah_cair');
Route::get('/lihat_detail_nasabah_sudah_cair/{id}','PPDPPController@lihat_detail_nasabah_sudah_cair')->name('lihat_detail_nasabah_sudah_cair');
Route::get('/master_ppdpp','PPDPPController@master_ppdpp')->name('master_ppdpp');
Route::get('/detail_master_ppdpp/{id}','PPDPPController@detail_master_ppdpp')->name('detail_master_ppdpp');
Route::get('/jadwalAngsurByKTP','PPDPPController@jadwalAngsurByKTP');
Route::get('/cetakexcel','PPDPPController@cetakexcel');
Route::get('/cetakpdf','PPDPPController@cetakpdf');
Route::get('/cetakexcel1','PPDPPController@cetakexcel1');
Route::get('/cetakpdf1','PPDPPController@cetakpdf1');
Route::get('/jadwalAngsurByIDberkas','PPDPPController@jadwalAngsurByIDberkas');




///nasabah approve
Route::get('/slik_nasabah_approve','NasabahApproveController@slik');
Route::get('/appraisal_nasabah_approve','NasabahApproveController@appraisal');
Route::get('/analisa_nasabah_approve','NasabahApproveController@analisa');
Route::get('/sla_nasabah_approve','NasabahApproveController@sla');
Route::get('/ppdpp_nasabah_approve','NasabahApproveController@ppdpp');
Route::get('/proses_approval','NasabahApproveController@proses_approval');
Route::get('/reject','NasabahApproveController@reject');
Route::post('/approve','NasabahApproveController@approve');
Route::post('/simpan/approval','NasabahApproveController@approval');

//appraisal
Route::get('/detail_property_nasabah_approve','NasabahApproveController@detail_property');
Route::post( '/update_property_nasabah_approve', ['as'=>'update_property_nasabah_approve', 'uses'=> 'NasabahApproveController@update_property']);

//check ppdpp
Route::post( '/update_ppdpp_nasabah_approve', ['as'=>'update_ppdpp_nasabah_approve', 'uses'=> 'NasabahApproveController@update_ppdpp']);

//analisa
Route::post( '/update_analis_nasabah_approve', ['as'=>'update_analis_nasabah_approve', 'uses'=> 'NasabahApproveController@update_analis']);
Route::post( '/update_sp4k', ['as'=>'update_sp4k', 'uses'=> 'NasabahApproveController@update_sp4k']);
Route::post( '/kirim_data', ['as'=>'kirim_data', 'uses'=> 'NasabahApproveController@kirim_data']);
Route::get('/nasabah_approve','NasabahApproveController@index')->name('nasabah_approve');
Route::get('/edit_nasabah','NasabahApproveController@edit_nasabah');
Route::get('/kirim_approve','NasabahApproveController@kirim_approve');
Route::get('/cetak_sp4k','NasabahApproveController@cetak_sp4k');

//pra booking review
Route::get('/pra_booking_review','PrabookingreviewController@index')->name('pra_booking_review');

//pra booking review done
Route::get('/pra_booking_review_done','PrabookingreviewdoneController@index')->name('pra_booking_review_done');

//approval permohonan cair ppdpp
Route::get('/approval_permohonan_cair_ppdpp','ApprovalPermohonanCairPpdppController@index')->name('approval_permohonan_cair_ppdpp');

//update status siap cair
Route::get('/update_status_siap_cair','UpdateStatusSiapCairController@index')->name('update_status_siap_cair');

<<<<<<< HEAD
//hasil pra uji
Route::get('/hasil_pra_uji','HasilPraUjiController@index')->name('hasil_pra_uji');


//akad
Route::get('/akad','AkadController@index')->name('akad');


//booking review
Route::get('/booking_review','BookingReviewController@index')->name('booking_review');


//approval br
Route::get('/approval_br','ApprovalBRController@index')->name('approval_br');


//permohonan cair
Route::get('/permohonan_cair','PermohonanCairController@index')->name('permohonan_cair');


=======
//permohonan cair cbs
Route::get('/permohonan_cair_cbs','PermohonanCairCbsController@index')->name('permohonan_cair_cbs');
>>>>>>> master

//approval permohonan cair cbs
Route::get('/approval_permohonan_cair_cbs','ApprovalPermohonanCairCbsController@index')->name('approval_permohonan_cair_cbs');

//FROM SERVICE
Route::post('service/dksbaru','PPDPPController@ActdksBaru')->name('service.dksbaru');
Route::post('service/ambilIdUji','PPDPPController@ActambilIdUji');
Route::post('service/kirimIdUji','PPDPPController@ActkirimIdUji');
Route::post('service/kirimDataUji','PPDPPController@ActkirimDataUji');

Route::post('act/updateStatusCairPPDPP','PPDPPController@ActupdateStatusCairPPDPP');
Route::post('act/updateStatusCairSMF','PPDPPController@ActupdateStatusCairSMF');
Route::post('act/updateNoAkad','PPDPPController@ActuupdateNoAkad');

Route::post('service/ambilIdBerkas','PPDPPController@ambilIdBerkas');
Route::post('act/updateStatusCair','PPDPPController@updateStatusCair');
Route::post('service/setPencairan','PPDPPController@setPencairan');
Route::post('service/getHasilUji','PPDPPController@getHasilUji');
Route::post('service/GetJadwalAngsur','PPDPPController@GetJadwalAngsur');
Route::post('service/GetSisaPembayaran','PPDPPController@GetSisaPembayaran');


//laporan
Route::get('/laporan','LaporanController@index')->name('laporan');
Route::post('/laporan/uploadcbs','LaporanController@UploadCBS');
Route::post('/laporan/updatePPDPP','LaporanController@UpdatePPDPP');
Route::post('/laporan/mutasiPPDPP','LaporanController@MutasiPPDPP');
Route::post('/laporan/insert','LaporanController@insert');
Route::post('/laporan/percepat','LaporanController@percepat');


//dashboard
Route::post('/updatekuota','HomeController@updatekuota');
Route::post('/updateunit','HomeController@updateunit');
Route::post('/updatedata','HomeController@updatedata');
Route::post('/login_adapter','HomeController@login_adapter');
Route::post('/change_adapter','HomeController@change_adapter');
Route::post('/logout_adapter','HomeController@logout_adapter');



Route::post('/updatesaldo','HomeController@updatesaldo');
///dev

Route::resource('developer', 'DeveloperController');




///get master
Route::post('/get-master-data','HomeController@getmaster');



//select
Route::get('/select/kecamatan/{id}','HomeController@kecamatan');
Route::get('/select/kelurahan/{id}','HomeController@kelurahan');
Route::get('/select/perumahan/{id}','HomeController@perumahan');

Route::get('/select/apikelurahan/{id}','HomeController@apikelurahan');
Route::post('/select/apikelurahan1','HomeController@apikelurahan1');
Route::post('/kirimslik_entry_nasabah','EntryNasabahController@kirimslik');
Route::post('/kirimproses','NasabahController@kirimproses');




//rekening koran
Route::resource('rekening_koran', 'KoranController');

// ENTRY NASABAH
Route::get('/entry_nasabah/daftar_prospek_lengkap','EntryNasabahController@daftar_prospek_lengkap')->name('entry_nasabah.daftar_prospek_lengkap');
Route::get('/entry_nasabah/daftar_prospek_nasabah','EntryNasabahController@daftar_prospek')->name('entry_nasabah.daftar_prospek_nasabah');
Route::get('/entry_nasabah/cari_core_banking/{nama}/{id}','EntryNasabahController@cari_core_banking')->name('entry_nasabah.cari_core_banking');
Route::post('/entry_nasabah/max_plafon','EntryNasabahController@maxPlafon')->name('entry_nasabah.maxPlafon');
Route::post('/entry_nasabah/hitungAngsuran','EntryNasabahController@hitungAngsuran')->name('entry_nasabah.hitungAngsuran');
Route::post('/entry_nasabah/addDokumen','EntryNasabahController@addDokumen')->name('entry_nasabah.addDokumen');
Route::get('/entry_nasabah/form_appraisal/{id}','EntryNasabahController@form_appraisal')->name('entry_nasabah.form_appraisal');
Route::get('/entry_nasabah/list_ua','EntryNasabahController@list_ua')->name('entry_nasabah.list_ua');
Route::get('/entry_nasabah/list_uana','EntryNasabahController@list_uana')->name('entry_nasabah.list_uana');
Route::post('/entry_nasabah/store_appraisal','EntryNasabahController@store_appraisal')->name('entry_nasabah.store_appraisal');
Route::post('/entry_nasabah/store_slik','EntryNasabahController@store_slik')->name('entry_nasabah.store_slik');

Route::get('/entry_nasabah/form_slik/{id}/{id_nasabah}','EntryNasabahController@form_slik')->name('entry_nasabah.form_slik');

Route::get('/entry_nasabah/form_prospek/{id}/{type}','EntryNasabahController@form_prospek')->name('entry_nasabah.form_prospek');

Route::get('/entry_nasabah/form_edit/{id}/{id_nasabah}/{id_prospek}','EntryNasabahController@form_edit')->name('entry_nasabah.form_edit');


Route::post('/entry_nasabah/update_nasabah','EntryNasabahController@update')->name('entry_nasabah.update_nasabah');

Route::resource('entry_nasabah', 'EntryNasabahController');

//nasabah on proses
Route::get( '/nasabah_on_proses', ['as'=>'nasabah_on_proses', 'uses'=> 'NasabahController@nasabah_on_proses']);
Route::get('/slik_nasabah_on_proses','NasabahController@slik');
Route::get('/appraisal_nasabah_on_proses','NasabahController@appraisal');
Route::get('/analisa_nasabah_on_proses','NasabahController@analisa');
Route::get('/sla_nasabah_on_proses','NasabahController@sla');
Route::get('/ppdpp_nasabah_on_proses','NasabahController@ppdpp');

//appraisal
Route::get('/detail_property_nasabah_on_proses','NasabahController@detail_property');
Route::post( '/update_property_nasabah_on_proses', ['as'=>'update_property_nasabah_on_proses', 'uses'=> 'NasabahController@update_property']);

//check ppdpp
Route::post( '/update_ppdpp_nasabah_on_proses', ['as'=>'update_ppdpp_nasabah_on_proses', 'uses'=> 'NasabahController@update_ppdpp']);

//analisa
Route::post( '/update_analis_nasabah_on_proses', ['as'=>'update_analis_nasabah_on_proses', 'uses'=> 'NasabahController@update_analis']);




// NASABAH cair
Route::get( '/nasabah_cair', ['as'=>'nasabah_cair', 'uses'=> 'NasabahCairController@index']);
Route::get('/slik_nasabah_cair','NasabahCairController@slik');
Route::get('/appraisal_nasabah_cair','NasabahCairController@appraisal');
Route::get('/analisa_nasabah_cair','NasabahCairController@analisa');
Route::get('/sla_nasabah_cair','NasabahCairController@sla');
Route::get('/ppdpp_nasabah_cair','NasabahCairController@ppdpp');

//appraisal
Route::get('/detail_property_nasabah_cair','NasabahCairController@detail_property');
Route::post( '/update_property_nasabah_cair', ['as'=>'update_property_nasabah_cair', 'uses'=> 'NasabahCairController@update_property']);

//check ppdpp
Route::post( '/update_ppdpp_nasabah_cair', ['as'=>'update_ppdpp_nasabah_cair', 'uses'=> 'NasabahCairController@update_ppdpp']);

//analisa
Route::post( '/update_analis_nasabah_cair', ['as'=>'update_analis_nasabah_cair', 'uses'=> 'NasabahCairController@update_analis']);






//nasabah approval
Route::get( '/nasabah_approval', ['as'=>'nasabah_approval', 'uses'=> 'NasabahApprovalController@nasabah_approval']);
Route::get('/slik_nasabah_approval','NasabahApprovalController@slik');
Route::get('/appraisal_nasabah_approval','NasabahApprovalController@appraisal');
Route::get('/analisa_nasabah_approval','NasabahApprovalController@analisa');
Route::get('/sla_nasabah_approval','NasabahApprovalController@sla');
Route::get('/ppdpp_nasabah_approval','NasabahApprovalController@ppdpp');
Route::get('/proses_approval','NasabahApprovalController@proses_approval');
Route::get('/reject','NasabahApprovalController@reject');
Route::post('/approve','NasabahApprovalController@approve');
Route::post('/simpan/approval','NasabahApprovalController@approval');

//appraisal
Route::get('/detail_property_nasabah_approval','NasabahApprovalController@detail_property');
Route::post( '/update_property_nasabah_approval', ['as'=>'update_property_nasabah_approval', 'uses'=> 'NasabahApprovalController@update_property']);

//check ppdpp
Route::post( '/update_ppdpp_nasabah_approval', ['as'=>'update_ppdpp_nasabah_approval', 'uses'=> 'NasabahApprovalController@update_ppdpp']);

//analisa
Route::post( '/update_analis_nasabah_approval', ['as'=>'update_analis_nasabah_approval', 'uses'=> 'NasabahApprovalController@update_analis']);


Route::get('/permintaan','SlikController@permintaan')->name('permintaan');
Route::get('/selesai','SlikController@selesai')->name('selesai');
Route::get('/slik_checking/{id}','SlikController@slik');
Route::post( '/update_checking', ['as'=>'update_checking', 'uses'=> 'SlikController@update_checking']);



//nasabah approval 2
//nasabah approval
Route::get( '/nasabah_approval2', ['as'=>'nasabah_approval2', 'uses'=> 'NasabahApproval2Controller@nasabah_approval2']);
Route::get('/slik_nasabah_approval2','NasabahApproval2Controller@slik');
Route::get('/appraisal_nasabah_approval2','NasabahApproval2Controller@appraisal');
Route::get('/analisa_nasabah_approval2','NasabahApproval2Controller@analisa');
Route::get('/sla_nasabah_approval2','NasabahApproval2Controller@sla');
Route::get('/ppdpp_nasabah_approval2','NasabahApproval2Controller@ppdpp');
Route::get('/proses_approval','NasabahApproval2Controller@proses_approval');
Route::get('/reject','NasabahApproval2Controller@reject');
Route::post('/approve_approval2','NasabahApproval2Controller@approve');
Route::post('/simpan/approval','NasabahApproval2Controller@approval');

//appraisal
Route::get('/detail_property_nasabah_approval2','NasabahApproval2Controller@detail_property');
Route::post( '/update_property_nasabah_approval2', ['as'=>'update_property_nasabah_approval2', 'uses'=> 'NasabahApproval2Controller@update_property']);

//check ppdpp
Route::post( '/update_ppdpp_nasabah_approval2', ['as'=>'update_ppdpp_nasabah_approval2', 'uses'=> 'NasabahApproval2Controller@update_ppdpp']);

//analisa
Route::post( '/update_analis_nasabah_approval2', ['as'=>'update_analis_nasabah_approval2', 'uses'=> 'NasabahApproval2Controller@update_analis']);

