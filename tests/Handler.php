<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        /*
        if ($exception instanceof AuthenticationException) {
             return response()->view('errors.500', [], 500);
            //return redirect('login');
        }

        if ($exception instanceof TokenMismatchException) {
             return response()->view('errors.500', [], 500);
            //return redirect(route('login'))->with('message', 'Your session expired');
        }

        if ($exception instanceof \ErrorException) {
            return response()->view('errors.500', [], 500);
        }

        if ($this->isHttpException($exception)==405) {
            
            return response()->view('auth.login');
        }
        
        if($this->isHttpException($exception)){
            if (view()->exists('errors.'.$e->getStatusCode()))
            {
                return response()->view('errors.'.$e->getStatusCode(), [], $e->getStatusCode());
            }
        }
    */
        return parent::render($request, $exception);
    }
}
