<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});


*/
Auth::routes();
/*
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login1', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});
*/


Route::post('login1',['as'=>'login1', 'uses'=> 'AuthController@LoginViaAPI']);

Route::get('/home','HomeController@index')->name('home');
Route::get( '/', ['as'=>'home', 'uses'=> 'HomeController@index']);

Route::post('/prospek/delete/{id}','PPDPPController@delete')->name('PPDPP.delete');

///prospek
Route::get('/entry_prospek','ProspekController@entry_prospek_view')->name('entry_prospek');
Route::get('/data_prospek','ProspekController@data_prospek_view')->name('data_prospek');
Route::get('/daftar_prospek_baru','ProspekController@daftar_prospek_view')->name('daftar_prospek_baru');
Route::post('simulasi',['as'=>'simulasi', 'uses'=> 'ProspekController@simulasi']);
Route::post('daftar',['as'=>'daftar', 'uses'=> 'ProspekController@daftar']);
Route::get('/ubah_prospek','ProspekController@ubah_prospek');
Route::get('/kirim_pengajuan','ProspekController@kirim_pengajuan');
Route::post('pengajuan',['as'=>'pengajuan', 'uses'=> 'ProspekController@pengajuan']);


//nasabah
Route::get('/entry_nasabah','NasabahController@entry_nasabah_view')->name('entry_nasabah');
Route::get('/daftar_nasabah_baru','NasabahController@daftar_nasabah_view')->name('daftar_nasabah_baru');


//menu
Route::get( '/dalam_proses', ['as'=>'dalam_proses', 'uses'=> 'HomeController@dalam_proses']);
//Route::get('/dalam_proses','HomeController@dalam_proses')->name('dalam_proses');
Route::get('/sudah_disetujui','HomeController@sudah_disetujui')->name('sudah_disetujui');


//select
Route::get('/select/dati/{id}','HomeController@dati');


Route::resource('wilayah', 'WilayahController');
Route::post('/wilayah/tambah','WilayahController@tambah')->name('wilayah.tambah');
Route::post('/wilayah/delete/{id}','WilayahController@delete')->name('wilayah.delete');

Route::resource('user', 'UserController');
Route::post('/user/tambah','UserController@tambah')->name('user.tambah');
Route::post('/user/delete/{id}','UserController@delete')->name('user.delete');


Route::resource('developer', 'DeveloperController');
Route::post('/developer/tambah','DeveloperController@tambah')->name('developer.tambah');
Route::post('/developer/delete/{id}','DeveloperController@delete')->name('developer.delete');


Route::resource('residence', 'ResidenceController');
Route::post('/residence/tambah','ResidenceController@tambah')->name('residence.tambah');
Route::post('/residence/delete/{id}','ResidenceController@delete')->name('residence.delete');

//ppdpp
Route::get('/permohonan_uji','PPDPPController@permohonan_uji')->name('permohonan_uji');
Route::post('/permohonan_uji/add','PPDPPController@add_permohonan_uji')->name('permohonan_uji.add');
Route::get('/permohonan_uji/show/{id}/{type}','PPDPPController@edit_permohonan_uji')->name('permohonan_uji.show');

Route::get('/ceklis_agunan/show/{id}','PPDPPController@ceklis_agunan')->name('ceklis_agunan.show');
Route::post('/checklist_agunan/edit','PPDPPController@ceklis_agunan_edit')->name('permohonan_uji.edit');

Route::post('/ppdpp/upload','PPDPPController@upload_data_uji');



Route::post('/simulasi_angsuran','PPDPPController@simulasi_angsuran')->name('simulasi_angsuran');


Route::post('dks/update','PPDPPController@update_dks');
Route::get('/nasabah_uji','PPDPPController@nasabah_uji')->name('nasabah_uji');
Route::get('dks/table/{id}','PPDPPController@table');
Route::get('/lihat_nasabah','PPDPPController@lihat_nasabah')->name('lihat_nasabah');
Route::get('/lihat_nasabah_siap_cair','PPDPPController@lihat_nasabah_siap_cair')->name('lihat_nasabah_siap_cair');
Route::get('/lihat_nasabah_sudah_cair','PPDPPController@lihat_nasabah_sudah_cair')->name('lihat_nasabah_sudah_cair');
Route::get('/lihat_detail_nasabah_sudah_cair/{id}','PPDPPController@lihat_detail_nasabah_sudah_cair')->name('lihat_detail_nasabah_sudah_cair');
Route::get('/master_ppdpp','PPDPPController@master_ppdpp')->name('master_ppdpp');
Route::get('/detail_master_ppdpp/{id}','PPDPPController@detail_master_ppdpp')->name('detail_master_ppdpp');
Route::get('/jadwalAngsurByKTP','PPDPPController@jadwalAngsurByKTP');
Route::get('/cetakexcel','PPDPPController@cetakexcel');
Route::get('/cetakpdf','PPDPPController@cetakpdf');
Route::get('/cetakexcel1','PPDPPController@cetakexcel1');
Route::get('/cetakpdf1','PPDPPController@cetakpdf1');
Route::get('/jadwalAngsurByIDberkas','PPDPPController@jadwalAngsurByIDberkas');




//FROM SERVICE
Route::post('service/dksbaru','PPDPPController@ActdksBaru')->name('service.dksbaru');
Route::post('service/ambilIdUji','PPDPPController@ActambilIdUji');
Route::post('service/kirimIdUji','PPDPPController@ActkirimIdUji');
Route::post('service/kirimDataUji','PPDPPController@ActkirimDataUji');

Route::post('act/updateStatusCairPPDPP','PPDPPController@ActupdateStatusCairPPDPP');
Route::post('act/updateStatusCairSMF','PPDPPController@ActupdateStatusCairSMF');
Route::post('act/updateNoAkad','PPDPPController@ActuupdateNoAkad');

Route::post('service/ambilIdBerkas','PPDPPController@ambilIdBerkas');
Route::post('act/updateStatusCair','PPDPPController@updateStatusCair');
Route::post('service/setPencairan','PPDPPController@setPencairan');
Route::post('service/getHasilUji','PPDPPController@getHasilUji');
Route::post('service/GetJadwalAngsur','PPDPPController@GetJadwalAngsur');
Route::post('service/GetSisaPembayaran','PPDPPController@GetSisaPembayaran');


//laporan
Route::get('/laporan','LaporanController@index')->name('laporan');
Route::post('/laporan/uploadcbs','LaporanController@UploadCBS');
Route::post('/laporan/updatePPDPP','LaporanController@UpdatePPDPP');
Route::post('/laporan/mutasiPPDPP','LaporanController@MutasiPPDPP');
Route::post('/laporan/insert','LaporanController@insert');
Route::post('/laporan/percepat','LaporanController@percepat');


//dashboard
Route::post('/updatekuota','HomeController@updatekuota');
Route::post('/updateunit','HomeController@updateunit');
Route::post('/updatedata','HomeController@updatedata');
Route::post('/login_adapter','HomeController@login_adapter');
Route::post('/change_adapter','HomeController@change_adapter');
Route::post('/logout_adapter','HomeController@logout_adapter');



Route::post('/updatesaldo','HomeController@updatesaldo');
///dev

Route::resource('developer', 'DeveloperController');




///get master
Route::post('/get-master-data','HomeController@getmaster');



//select
Route::get('/select/kecamatan/{id}','HomeController@kecamatan');
Route::get('/select/kelurahan/{id}','HomeController@kelurahan');


//rekening koran
Route::resource('rekening_koran', 'KoranController');

// ENTRY NASABAH
Route::resource('entry_nasabah', 'EntryNasabahController');

//nasabah on proses
Route::get( '/nasabah_on_proses', ['as'=>'nasabah_on_proses', 'uses'=> 'NasabahController@nasabah_on_proses']);
Route::get('/slik','NasabahController@slik');
Route::get('/appraisal','NasabahController@appraisal');
Route::get('/analisa','NasabahController@analisa');
Route::get('/sla','NasabahController@sla');
Route::get('/ppdpp','NasabahController@ppdpp');



