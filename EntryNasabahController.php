<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Yajra\Datatables\Datatables;
use Response;
use Hash;
use Auth;
use Session;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\RequestOptions;
use App\Http\Requests;
use Illuminate\Support\Collection;
use App\ProspekModel;
use DateTime;
use Crypt;
use File;
use Redirect;

class EntryNasabahController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    
   public function index(Request $request)
    {
        $param['data'] = \DB::select("SELECT * FROM ref_developer");
        if ($request->ajax()) {
            $view = view('entry_nasabah.index',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'entry_nasabah.index',$param);
    }

    public function store_appraisal(Request $request)
    {
        $url = env('API_BASE_URL')."master/pembiayaan/processor/".$request->input('id');
     
          $data = array(  
            "appraisal"=> $request->input('list_ua'),
            "analis"=> $request->input('list_uana'),
        );

        $client = new Client();
        $headers = [
                'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->put($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
            return $data_result;
            
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }
    }

    public function store_slik(Request $request)
    {
        $url = env('API_BASE_URL')."master/bi-check";
     
          $data = array(
                "notes"=> $request->input('catatan'),
                "angsuran"=> $request->input('total_angsuran'),
                "pembiayaanId"=> $request->input('id'),
                "nasabahId"=> $request->input('id_nasabah'),
                "noIdentitas"=> $request->input('no_identitas'),
                "namaNasabah"=> $request->input('nama_nasabah'),
                "alamat"=> $request->input('alamat'),
                "tempatLahir"=> $request->input('tempat_lahir'),
                "tanggalLahir"=> $request->input('tgl_lahir')
        );

        $client = new Client();
        $headers = [
                'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
           
            return $data_result;
            
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }
    }

    public function form_appraisal(Request $request,$id)
    {
        // dd($id);
         $param['id']=$id;
          if ($request->ajax()) {
            $view = view('entry_nasabah.apraisal_form',$param)->renderSections();
            return json_encode($view);
        }

        return view('master.master')->nest('child', 'entry_nasabah.apraisal_form',$param);
    }

    public function form_slik(Request $request,$id,$id_nasabah)
    {
        // dd($id);
         $param['id']=$id;
         $param['id_nasabah']=$id_nasabah;


        $url = env('API_BASE_URL')."master/bi-check/pembiayaan/".$id;
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
           $data =$data['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }

        $data1 = \DB::select("SELECT * FROM master_nasabah where id =".$id_nasabah);
        


         $url2 = env('API_BASE_URL')."master/list/bicheck-status";
        try{
            
            $result2 = $client->get($url2,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param3=[];
            $param3= (string) $result2->getBody();
            $data2 = json_decode($param3, true);
           $data2 =$data2['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data1=$response;
        }


        $param['data2']=$data2;


        $param['data']=$data;
        $param['data1']=$data1;


          if ($request->ajax()) {
            $view = view('entry_nasabah.slik_form',$param)->renderSections();
            return json_encode($view);
        }

        return view('master.master')->nest('child', 'entry_nasabah.slik_form',$param);
    }


    public function cari_core_banking($nama,$id)
    {
        $data = array(  
            "nmProspek"=> $nama
        );

        $url = env('API_BASE_URL')."master/nasabah/cbs";
         
        $client = new Client();
        $headers = [
                'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
        
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
           $data =$data['data'];
            
                
            if (count($data) == 0) {
                // $param['data'] = "waw";
                $param['nama']=$nama;
                $param['id']=$id;

                return view('master.master')->nest('child', 'entry_nasabah.kosong',$param);
            }else {
                $param['data']=$data;

                return view('master.master')->nest('child', 'entry_nasabah.index',$param);
                
            }

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }
    }

    public function daftar_prospek(Request $request)
    {
        // $param['data'] = \DB::select("select mp.id,mp.nm_prospek,mp.alamat,p.prod_name,b.branch_name,mp.jangka_waktu,mp.total_angsuran from master_prospek mp 
        //     left join ref_product p on p.id=mp.id_product 
        //     join branch b on b.id=mp.branch_id");
        // if ($request->ajax()) {
        //     $view = view('entry_nasabah.daftar_prospek',$param)->renderSections();
        //     return json_encode($view);
        // }
        // return view('master.master')->nest('child', 'entry_nasabah.daftar_prospek',$param);

        $url = env('API_BASE_URL')."master/prospek/ao?param=new";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
           $data =$data['data'];
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
        }
        

        $param['data']=$data;

        // dd($param);
        
        if ($request->ajax()) {
            $view = view('entry_nasabah.daftar_prospek',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'entry_nasabah.daftar_prospek',$param);
    }

     public function daftar_prospek_lengkap(Request $request)
    {
        // $param['data'] = \DB::select("select mp.id,mp.nm_prospek,mp.alamat,p.prod_name,b.branch_name,mp.jangka_waktu,mp.total_angsuran from master_prospek mp 
        //     left join ref_product p on p.id=mp.id_product 
        //     join branch b on b.id=mp.branch_id");
        // if ($request->ajax()) {
        //     $view = view('entry_nasabah.daftar_prospek',$param)->renderSections();
        //     return json_encode($view);
        // }
        // return view('master.master')->nest('child', 'entry_nasabah.daftar_prospek',$param);

        $url = env('API_BASE_URL')."master/prospek/ao?param=complete";
        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers,
                ]);
            
            
            $param1=[];
            $param1= (string) $result->getBody();
            $data1 = json_decode($param1, true);
            $data =$data1['data'];
            $rc=$data1['rc'];
            $rm=$data1['rm'];
            

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $data=$response;
            $rc=$response->rc;
            $rm=$response->rm;
            
        }
        

        $param['data']=$data;
        $param['rc']=$rc;
        $param['rm']=$rm;

       //dd($data);
        
        if ($request->ajax()) {
            $view = view('entry_nasabah.daftar_prospek_lengkap',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'entry_nasabah.daftar_prospek_lengkap',$param);
    }

    public function edit($id)
    {

      

        $url = env('API_BASE_URL')."master/prospek/".$id."/show";
      
        $client = new Client();
        $headers = [
                'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            $data =$data['data'];
       

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }


        // $param['data'] = collect(\DB::select("SELECT * FROM master_prospek where nm_prospek = '".$id."'"))->first();

        // dd($data);
        $param['id_prospek_new'] = $id;
        $param['data']=$data;
        return view('master.master')->nest('child', 'entry_nasabah.form',$param);
    }

    public function form_prospek($id,$type)
    {

        $url = env('API_BASE_URL')."master/prospek/".$id."/show";
      
        $client = new Client();
        $headers = [
                'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            
            if($data['rc']==200){
                $rm=$data['rm'];
                $rc=$data['rc'];
                $data =$data['data'];
            }else{
                $rc=$data['rc'];
                $rm=$data['rm'];
            }

       

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }


        // $param['data'] = collect(\DB::select("SELECT * FROM master_prospek where nm_prospek = '".$id."'"))->first();

        $param['type_form'] = $type;
        $param['id_prospek_new'] = $id;
        $param['data']=$data;
        $param['rm']=$rm;
        $param['rc']=$rc;
        return view('master.master')->nest('child', 'entry_nasabah.form',$param);
    }

    public function maxPlafon(Request $request)
    {
        $id_product = $request->input('id_product');
        $harga_object = str_replace('.','',$request->input('harga_object'));
        $jangka_waktu = $request->input('jangka_waktu');

        $product = collect(\DB::select("SELECT * FROM ref_product where id = '".$id_product."'"))->first();

         $data = array(  
           	"pendapatanBulan"=> str_replace('.','',$request->input('pendapatan_bulan')),
            "pendapatanLainnya"=> str_replace('.','',$request->input('pendapatan_lainnya')),
            "tenor"=> $request->input('jangka_waktu'),
            "eqRate"=> $product->eq_rate,
            "maxGaji"=> $product->max_gaji
        );

       
        $url = env('API_BASE_URL')."master/calc-plafon";
         
        $client = new Client();
        $headers = [
                 'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
            $plafon=(float) $data_result['data']['plafon'];
            $pla['data'] = number_format($data_result['data']['plafon'],0,',','.');
            $pla['angsuran'] = number_format($data_result['data']['angsuranBulan'],0,',','.');

            $pla['uang_muka']= number_format($harga_object - $plafon,0,',','.');
            $pla['nilaiflpp']=number_format($plafon * 0.75,0,',','.');

            $a=(float) $data_result['data']['angsuranBulan'];
            $b=(float) $plafon;
            $pla['margin'] = number_format(($a * $jangka_waktu) - $b,0,',','.');

            // dd($data_result['data']['plafon']);
            // return number_format($data_result['data']['plafon'],0,',','.');
                // return $data_result['data']['plafon'];

                 
                return $pla;

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }
    }

    public function store(Request $request)
    {

        
        $nasabah=[
            'idProspect' => (int)$request->input('id_prospek_new'),
            'namaNasabah'=> $request->input('nama_nasabah'),
            'noIdentitas'=> $request->input('no_identitas'),
            'alamat'=> $request->input('alamat'),
            'idWilayah'=> $request->input('lokasipengguna'),
            'telp'=> $request->input('telp'),
            'noHp'=> $request->input('no_hp'),
            'tptLahir'=> $request->input('tpt_lahir'),
            'tglLahir'=> $request->input('tgl_lahir'),
            'sidNgik'=> $request->input('ibukandung'),
            'jnsKelamin'=> $request->input('jns_kelamin'),
            'isMenikah'=> $request->input('stat_menikah'),
            'pendapatanBulan'=> str_replace('.','',$request->input('pendapatan_bulan')),
            'pendapatanLainnya'=> str_replace('.','',$request->input('pendapatan_lainnya')),
            'idPekerjaan'=> $request->input('id_pekerjaan'),
            'sumberPendapatan'=> $request->input('sumber_pendapatan'),
            'ibuKandung'=> $request->input('ibukandung')
        ];
        $pembiayaan=[
            'hargaObject'=> str_replace('.','',$request->input('harga_object')),
            'uangMuka'=> str_replace('.','',$request->input('uang_muka')),
            'productId'=> $request->input('id_product'),
            'jangkaWaktu'=> $request->input('jangka_waktu'),
            'margin'=> str_replace('.','',$request->input('margin')),
            'totalAngsuran'=> str_replace('.','',$request->input('total_angsuran')),
            'plafonFlpp'=> str_replace('.','',$request->input('plafon_flpp'))

        ];
        $agunan=[
            'tglPermohonan'=> date('Y-m-d'),
            'tglSurvey'=> null,
            'tglLaporan'=> null,
            'seqPenilaian'=> 1,
            'processorUid'=> -1 
        ];
        $agunanProperty=[
            'idJenisAgunan'=> 1,
            'idDeveloper'=> $request->input('id_developer'),
            'idPerumahan'=> $request->input('id_perumahan'),
            'alamat'=> $request->input('alamat1'),
            'idWilayah'=> $request->input('lokasiagunan'),
            'luasTanahSertifikat'=> $request->input('luas_tanah_fisik'),
            'luasImb'=> $request->input('luas_imb')  
        ];      

        $kalimat = $request->input('alamat1');

        if(preg_match("/Blok/i", $kalimat) && preg_match("/Nomor/i", $kalimat) || preg_match("/No/i", $kalimat)) {

        }else{
          return response()->json([
                    'rc' => 1,
                    'rm' => 'Blok'
                ]);
  
        }
        $url = env('API_BASE_URL')."master/nasabah/complete";
        $client = new Client();
        $data = array(
            "nasabah"=>$nasabah,
            "pembiayaan"=>$pembiayaan,
            "agunan"=>$agunan,
            "agunanProperty"=>$agunanProperty
            ); 
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
                ]);

            $param=[];
            $param= (string) $result->getBody();

            $data = json_decode($param, true);
         
            return $data;

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;

        }    


    }

    public function addDokumen(Request $request)
    {
        // dd((int)$request->input('key_file'));
    
        $key_upload = $request->input('key_file');

        $media = $request->file('doc_file'.$key_upload);
        $image_mime = $media->getmimeType();
        $fileContent = File::get($media);

         $url = env('API_BASE_URL')."master/prospek-doc/update";
         $client = new Client();

       try {
            $response = $client->post(
                $url, [
                    'headers' => [
                        'Authorization'         =>  'Bearer '.session('token')
                    ],
                    'multipart' => [
                        [
                            'name'     => '_file',
                            'contents' => $fileContent,
                            'filename' => 'wadaw',
                            'Mime-Type'=> $image_mime,
                        ],
                          [
                            'name'     => 'id',
                            'contents' => (int)$request->input('id_doc'.$key_upload),
                        ],
                    ],
                ]
            );

            //  $param=[];
            // $param= (string) $result->getBody();
            // $data_result = json_decode($param, true);
           
            // return $data_result;

           return $response->getBody()->getContents();
        } catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }
    }

    public function list_ua(Request $request){

   
       
        $url = env('API_BASE_URL')."master/list/user-apprisal";
         
        $client = new Client();
        $headers = [
                // 'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuZmxwcC5iYXN5cy5jby5pZFwvXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU1NzE5OTU5MywiZXhwIjoxNTU3MjAzMTkzLCJuYmYiOjE1NTcxOTk1OTMsImp0aSI6IkdGdUxocUxBSkNyYTgwSlIiLCJzdWIiOjUsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEiLCJpZCI6NSwidXNlcm5hbWUiOiJhb2Jla2FzaSIsImlkX2NoYW5uZWwiOm51bGwsImlkX2JyYW5jaCI6NiwibG9naW5fdHlwZSI6MCwidXNlcl9ncm91cF9pZCI6MSwiYnJhbmNoX3R5cGVfaWQiOjJ9.N_ta3S4_yEHZtIfbbUc1pLj6KM3uHmui0mjI2EFrjN4',
                'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
           
            return json_encode($data_result);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }


        // return json_encode($kec);
    }

    public function list_uana(Request $request){

   
       
        $url = env('API_BASE_URL')."master/list/user-analyst";
         
        $client = new Client();
        $headers = [
                // 'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hcGkuZmxwcC5iYXN5cy5jby5pZFwvXC9hdXRoXC9sb2dpbiIsImlhdCI6MTU1NzE5OTU5MywiZXhwIjoxNTU3MjAzMTkzLCJuYmYiOjE1NTcxOTk1OTMsImp0aSI6IkdGdUxocUxBSkNyYTgwSlIiLCJzdWIiOjUsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEiLCJpZCI6NSwidXNlcm5hbWUiOiJhb2Jla2FzaSIsImlkX2NoYW5uZWwiOm51bGwsImlkX2JyYW5jaCI6NiwibG9naW5fdHlwZSI6MCwidXNlcl9ncm91cF9pZCI6MSwiYnJhbmNoX3R5cGVfaWQiOjJ9.N_ta3S4_yEHZtIfbbUc1pLj6KM3uHmui0mjI2EFrjN4',
                'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
           
            return json_encode($data_result);

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }


        // return json_encode($kec);
    }

     public function hitungAngsuran(Request $request)
    {
        $id_product = $request->input('id_product');
        $harga_object = str_replace('.','',$request->input('harga_object'));
        $uang_muka = str_replace('.','',$request->input('uang_muka'));
        $jangka_waktu = str_replace('.','',$request->input('jangka_waktu'));

         $data = array(  
            "productId"=> $id_product,
            "hargaObjek"=> $harga_object,
            "uangMuka"=> $uang_muka
        );

       
        $url = env('API_BASE_URL')."master/simulasi-angsuran?jangka-waktu=".$jangka_waktu;
         
        $client = new Client();
        $headers = [
                 'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
           
            $pla['data'] = number_format($data_result['data']['angsuran'],0,',','.');
            $plafon=$harga_object - $uang_muka;
            $pla['plafon'] = number_format($harga_object - $uang_muka,0,',','.');
            $pla['nilaiflpp'] = number_format($plafon * 0.75,0,',','.');
            $a=(float) $data_result['data']['angsuran'];
            $b=(float) $plafon;
            $pla['margin'] = number_format(($a * $jangka_waktu) - $b,0,',','.');
            // dd($data_result['data']['plafon']);
            // return number_format($data_result['data']['plafon'],0,',','.');
                // return $data_result['data']['plafon'];
            //dd($data_result);
            return $pla;

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }
    }

     public function kirimslik(Request $request)
    {
        $pembiayaanId = $request->input('pembiayaanId');
        $nasabahId = $request->input('nasabahId');
        $noIdentitas = $request->input('noIdentitas');
        $namaNasabah = $request->input('namaNasabah');
        $alamat = $request->input('alamat');
        $tempatLahir = $request->input('tempatLahir');
        $tanggalLahir = date('Y-m-d',strtotime($request->input('tanggalLahir')));

         $data = array(  
            "notes"=> "",
            "angsuran"=> 0,
            "pembiayaanId"=> $pembiayaanId,
            "nasabahId"=> $nasabahId,
            "noIdentitas"=> $noIdentitas,
            "namaNasabah"=> $namaNasabah,
            "alamat"=> $alamat,
            "tempatLahir"=> $tempatLahir,
            "tanggalLahir"=> $tanggalLahir
        );

       
        $url = env('API_BASE_URL')."master/bi-check";
         
        $client = new Client();
        $headers = [
                 'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
           
            $pla['data'] =$data_result;
           
            return $pla;

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }
    }

     public function form_edit($idpembiayaan,$idnasabah,$idprospek)
    {

        $client = new Client();
        $headers = [
            'Authorization' => 'Bearer '.session('token'),  
            'Content-Type' => 'application/json'
        ];


        $url = env('API_BASE_URL')."master/nasabah/".$idnasabah."/show";
        try{
            $result = $client->get($url,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param1=[];
            $param1= (string) $result->getBody();
            $data = json_decode($param1, true);
            
            $data =$data['data'];

       }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }

        $url1 = env('API_BASE_URL')."master/pembiayaan/nasabah/".$idnasabah;
        try{
            $result1 = $client->get($url1,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param2=[];
            $param2= (string) $result1->getBody();
            $data1 = json_decode($param2, true);
            
            $data1 =$data1['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }

    
        $url2 = env('API_BASE_URL')."master/agunan/pembiayaan/".$idpembiayaan;
        try{
            $result2 = $client->get($url2,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param3=[];
            $param3= (string) $result2->getBody();
            $data2 = json_decode($param3, true);
            
            $data2 =$data2['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }

        $url3 = env('API_BASE_URL')."master/agunan-property/agunan/".$data2[0]['id'];
        try{
            $result3 = $client->get($url3,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param4=[];
            $param4= (string) $result3->getBody();
            $data3 = json_decode($param4, true);
            
            $data3 =$data3['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }


        $url4 = env('API_BASE_URL')."master/prospek/".$idprospek."/show";
        try{
            $result4 = $client->get($url4,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param5=[];
            $param5= (string) $result4->getBody();
            $data4 = json_decode($param5, true);
            
            $data4 =$data4['data'];

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }

        
        $param['data']=$data;
        $param['data1']=$data1;
        $param['data3']=$data3;
        $param['data4']=$data4;
        $param['idpembiayaan']=$idpembiayaan;
        $param['idnasabah']=$idnasabah;
        $param['idprospek']=$idprospek;
        return view('master.master')->nest('child', 'entry_nasabah.form_edit',$param);
    }

    function update_data(Request $request){

        $pembiayaanId = $request->input('pembiayaanId');
        $nasabahId = $request->input('nasabahId');
        $noIdentitas = $request->input('noIdentitas');
        $namaNasabah = $request->input('namaNasabah');
        $alamat = $request->input('alamat');
        $tempatLahir = $request->input('tempatLahir');

        $tanggalLahir = date('Y-m-d',strtotime($request->input('tanggalLahir')));

         $data = array(  
            "notes"=> "",
            "angsuran"=> 0,
            "pembiayaanId"=> $pembiayaanId,
            "nasabahId"=> $nasabahId,
            "noIdentitas"=> $noIdentitas,
            "namaNasabah"=> $namaNasabah,
            "alamat"=> $alamat,
            "tempatLahir"=> $tempatLahir,
            "tanggalLahir"=> $tanggalLahir
        );

       
        $url = env('API_BASE_URL')."master/bi-check";
         
        $client = new Client();
        $headers = [
                 'Authorization' => 'Bearer '.session('token'),  
                'Content-Type' => 'application/json'
            ];
        
        try{
            $result = $client->post($url,[
                RequestOptions::HEADERS => $headers,
                RequestOptions::JSON => $data,
            ]);
        
            $param=[];
            $param= (string) $result->getBody();
            $data_result = json_decode($param, true);
           
            $pla['data'] =$data_result;
           
            return $pla;

        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }
        
    }

    public function update(Request $request){
         $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        $client = new Client();

         $kalimat = $request->input('alamat1');

        if(preg_match("/Blok/i", $kalimat) && preg_match("/Nomor/i", $kalimat) || preg_match("/No/i", $kalimat)) {

        }else{
          return response()->json([
                    'rc' => 1,
                    'rm' => 'Blok'
                ]);
  
        }

        $url = env('API_BASE_URL')."master/nasabah/".$request->input('idnasabah');
        $data = array(
            "namaNasabah"=> $request->input('nama_nasabah'),
            "noIdentitas"=> $request->input('no_identitas'),
            "alamat"=> $request->input('alamat'),
            "idWilayah"=> $request->input('lokasipengguna'),
            "telp"=> $request->input('no_hp'),
            "noHp"=> $request->input('telp'),
            "tptLahir"=> $request->input('tpt_lahir'),
            "tglLahir"=> date('Y-m-d',strtotime($request->input('tgl_lahir'))),
            "sidNgik"=> $request->input('ibukandung'),
            "jnsKelamin"=> $request->input('jns_kelamin'),
            "isMenikah"=> $request->input('stat_menikah'),
            "pendapatanBulan"=> str_replace(".","", $request->input('pendapatan_bulan')),
            "pendapatanLainnya"=> str_replace(".","", $request->input('pendapatan_lainnya')),
            "idPekerjaan"=> $request->input('id_pekerjaan'),
            "sumberPendapatan"=> $request->input('sumber_pendapatan'),
            "status"=> 1
        );
 
        $result = $client->put($url,[
            RequestOptions::HEADERS => $headers,
            RequestOptions::JSON => $data,
        ]);

        $url1 = env('API_BASE_URL')."master/pembiayaan/".$request->input('idpembiayaan');
        $data1 = array(
            "nasabahId"=> $request->input('idnasabah'),
            "hargaObject"=> str_replace(".","", $request->input('harga_object')),
            "uangMuka"=> str_replace(".","", $request->input('uang_muka')),
            "jangkaWaktu"=> $request->input('jangka_waktu'),
            "margin"=> str_replace(".","", $request->input('margin')),
            "totalAngsuran"=> str_replace(".","", $request->input('total_angsuran')),
            "aoNote"=>$request->input('aoNote'),
            "plafonFlpp"=> str_replace(".","", $request->input('plafon_flpp'))
        );
 
        $result1 = $client->put($url1,[
            RequestOptions::HEADERS => $headers,
            RequestOptions::JSON => $data1,
        ]);

        // $url2 = env('API_BASE_URL')."master/pembiayaan/".$request->input('idpembiayaan');
        // $data2 = array(
        //     "nasabahId"=> $request->input('idnasabah'),
        //     "hargaObject"=> str_replace(".","", $request->input('harga_object')),
        //     "uangMuka"=> str_replace(".","", $request->input('uang_muka')),
        //     "jangkaWaktu"=> $request->input('jangka_waktu'),
        //     "margin"=> str_replace(".","", $request->input('margin')),
        //     "totalAngsuran"=> str_replace(".","", $request->input('total_angsuran')),
        //     "plafonFlpp"=> str_replace(".","", $request->input('plafon_flpp'))
        // );
 
        // $result2 = $client->put($url2,[
        //     RequestOptions::HEADERS => $headers,
        //     RequestOptions::JSON => $data2,
        // ]);



          $url3 = env('API_BASE_URL')."master/agunan/pembiayaan/".$request->input('idpembiayaan');
            try{
                $result3 = $client->get($url3,[
                    RequestOptions::HEADERS => $headers
                ]);
            
                $param3=[];
                $param3= (string) $result3->getBody();
                $data3 = json_decode($param3, true);
                
                $data3 =$data3['data'];

            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                $bad_response = $this->responseData($e->getCode(), $response);
                
                return $bad_response;
            }

        $url4 = env('API_BASE_URL')."master/agunan-property/".$data3[0]['id'];
             $data4 = array(
                "noSertifikat"=> "",
                "nmPemilikSertifikat"=> "",
                "luasTanahSertifikat"=> $request->input('luas_tanah_fisik'),
                "luasImb"=> $request->input('luas_imb'),
                "nilaiAppraisal"=> 0,
                "alamat"=> $request->input('alamat1'),
                "idWilayah"=> $request->input('lokasiagunan'),
                "idDeveloper"=> $request->input('id_developer'),
                "idPerumahan"=> $request->input('id_perumahan'),
                "luasTanahFisik"=> null,
                "luasFisik"=> null
            );
            try{
                $result4 = $client->get($url4,[
                    RequestOptions::HEADERS => $headers,
                    RequestOptions::JSON => $data4,
                ]);
            
                $param4=[];
                $param4= (string) $result4->getBody();
                $data4 = json_decode($param4, true);
                
                return $data4;

            }catch (BadResponseException $e){
                $response = json_decode($e->getResponse()->getBody());
                $bad_response = $this->responseData($e->getCode(), $response);
                
                return $bad_response;
            }
        }


    public function tambah_dokumen(Request $request){
         $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '. session('token')
        ];
        $client = new Client();

        $url4 = env('API_BASE_URL')."master/list/jdoc";
        try{
            $result4 = $client->get($url4,[
                RequestOptions::HEADERS => $headers
            ]);
        
            $param5=[];
            $param5= (string) $result4->getBody();
            $data4 = json_decode($param5, true);
            $data4=$data4['data'];
        }catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            $bad_response = $this->responseData($e->getCode(), $response);
            
            return $bad_response;
        }

        $param['id']=$request->get('id');
        $param['data4']=$data4;
        if ($request->ajax()) {
            $view = view('entry_nasabah.tambah_dokumen',$param)->renderSections();
            return json_encode($view);
        }
        return view('master.master')->nest('child', 'entry_nasabah.tambah_dokumen',$param);
    }
     public function insert_dok(Request $request){
        $id=$request->input('id');
        //$key_upload = $request->input('key_file');
      // $this->validate($request, [
        //    'dok' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:1024'
         //]);

        $validators = \Validator::make($request->all(),['dok' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:1024']);
        if($validators->fails()){
          //  dd($validators);
            return Redirect::back()->withErrors($validators);
            //return redirect('dokumen?id='.$id)->with('error','gagal Upload');
        }
        $media = $request->file('dok');
        $image_mime = $media->getmimeType();
        $fileContent = File::get($media);

         $url = env('API_BASE_URL')."master/prospek-doc";
         $client = new Client();

       try {
            $result = $client->post(
                $url, [
                    'headers' => [
                        'Authorization'         =>  'Bearer '.session('token')
                    ],
                    'multipart' => [
                        [
                            'name'     => '_file',
                            'contents' => $fileContent,
                            'filename' => 'wadaw',
                            'Mime-Type'=> $image_mime,
                        ],
                        [
                            'name'     => 'prospek_id',
                            'contents' => $id,
                        ],
                        [
                            'name'     => 'doc_type',
                            'contents' => $request->input('idjenis'),
                        ],
                    ],
                ]
            );

             $param=[];
             $param= (string) $result->getBody();
             $data_result = json_decode($param, true);

            if($data_result['rc']=='200'){
                return redirect('entry_nasabah/form_prospek/'.$id.'/1')->with('success',$data_result['rm']);
            }else{
                return redirect('entry_nasabah/form_prospek/'.$id.'/1')->with('error',$data_result['rm']);
            }
        } catch (BadResponseException $e){
            $response = json_decode($e->getResponse()->getBody());
            //$bad_response = $this->responseData($e->getCode(), $response);
            //dd($response);
            return redirect('entry_nasabah/form_prospek/'.$id.'/1')->with('error',$response->rm);
        }
    }
}
