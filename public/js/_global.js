var base_url = '//' + window.location.host + "/silos";
var dataTable;
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(function () {
    //!!!!!!!!!!!!!
    // initializing
    //!!!!!!!!!!!!!
    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), "Cache-Control": "no-cache"}});
    initLib();
    initCommon();
    initForms();
    initMenu();
});
toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};
function initLib() {

}

function initDropDown() {
    if ($('.ui-select').length > 0) {
        $('.ui-select').each(function () {
            var element = $(this);
            element.select2();
        });
    }
}

window.addEventListener('popstate', function (event) {
    $('.modal').modal('hide');
    $('.ui.dimmable #loader').dimmer('destroy');
    $('body .modals').remove();
    if (event.state == null) {
        window.location.href = base_url;
    } else {
        if (event.state.hasOwnProperty('url')) {
            showPage(event.state.url, true);
        }
    }
}, false);

function showError(message) {
    toastr.clear();
    toastr['error'](message, "Terjadi Kesalahan");
}

function showInfo(message) {
    toastr.clear();
    toastr['success'](message, "Berhasil");
}

function showLoading() {
    var loader = $('.ui.dimmable #mainLoader');
    loader.dimmer({closable: false}).dimmer('show');
}

function hideLoading(destroy) {
    var loader = $('.ui.dimmable #mainLoader');
    loader.dimmer({closable: false}).dimmer('hide');
    if (destroy) {
        $('.modal').modal('hide');
        $('.ui.dimmable #mainLoader').dimmer('destroy');
        $('body .modals').remove();
    }
}

function hideLoadingPartial(destroy) {
    var loader = $('.ui.dimmable #mainLoader');
    loader.dimmer({closable: false}).dimmer('hide');
    if (destroy) {
        $('.ui.dimmable #mainLoader').dimmer('destroy');
    }
}

function ajaxMenu(e, url) {
    e.preventDefault();
    showPage(url);
}
function showPage(url, skip) {
    var state = {name: "name", page: 'History', url: url};
    subPage(url);
    if (typeof skip === 'undefined') {
        window.history.pushState(state, "History", base_url + "/" + url);
        location.reload();
    } else {
        window.history.replaceState(state, "History", base_url + "/" + url);
        location.reload();
    }
}
function subPage(url) {
    //showLoading();
    $.get(url, function (response, status, xhr) {
     //   hideLoading(true);
        var res = $.parseJSON(response);
        $('#data_content').html(res.content);
        $('#data_modal').html(res.modal);
        $('#data_script').html(res.script);
        initLib();
    }).fail(function (response) {
       // hideLoading(false);
        if (response.status === 401) {
            window.location.href = base_url;
        } else {
            showError('Terjadi Kesalahan');
        }
    });
}
function showEdit(elm, url) {
    showLoading();
    var id = $(elm).closest('tr').attr('id');
    if (id !== undefined){
        url = url + '/' + id;
    }
    $.get(url, function (response, status, xhr) {
        hideLoading(false);
        var res = $.parseJSON(response);
        $('#data_modal').html(res.content);
        $('#modalEdit').modal('show');
        initLib();
    }).fail(function (response) {
        hideLoading(false);
        if (response.status === 401) {
            window.location.href = base_url;
        } else {
            showError('Terjadi Kesalan');
        }
    });
}
function saveEdit(id, url) {
    var form = $('#formEdit'),
        formData = new FormData(form[0]);
    form.parsley().validate();
    if (!form.parsley().isValid()) {
        return false;
    } else {
        if (id !== null){
            url = url + '/' + id;
        }
        $.ajax({
            processData: false,
            contentType: false,
            type: 'POST',
            cache: false,
            url: url,
            data: formData,
            success: function (res) {
                // hideLoading(false);
                if (res.rc == 0) {
                    form[0].reset();
                    $('#modalEdit').modal('hide');
                    // dataTable.ajax.reload();
                    dataTable.draw();
                    // alert(res.rm);
                    showInfo(res.rm);
                } else {
                    showError(res.rm);
                }
            },
            error: function (data) {
                // hideLoading(false);
                $("#btnSubmit").removeClass('disabled');
                showError('Terjadi Kesalan');
            }
        });
        return false;
    }

}

function doDelete(elm, url) {
    var rowId = $(elm).closest('tr').attr('id');
    swal({
        title: "Anda Yakin?",
        text: "Data Akan Dihapus",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#e6b034",
        confirmButtonText: "Submit",
        allowOutsideClick: true,
        closeOnConfirm: false
    }, function () {
        del(rowId, url);
    });
}

function del(id, url) {
    url = url + '/' + id;
    $('.modal').each(function () {
        $(this).modal('hide');
    });
    showLoading();
    $.get(url, function (response, status, xhr) {
        hideLoading(false);
        if (response.rc == 0) {
            // dataTable.ajax.reload();
            dataTable.draw();
            swal("Berhasil!", "Data Sudah Dihapus", "success");
        } else {
            swal("Gagal", response.rm, "error");
        }
    }).fail(function (response) {
        hideLoading(false);
        swal("Error", "Terjadi Kesalahan", "error");
    });
}

function clearDropdown(elm) {
    elm.html('').select2({
        data: [{id: '', text: ''}]
    })
}

function getOptRef(elm, url) {
    clearDropdown(elm);
    elm.attr('disabled', 'disable');
    getRef(url, function (response) {
        elm.removeAttr('disabled');
        if (response != null) {
            if (response.length > 0) {
                for (var i = 0; i < response.length; i++) {
                    var newOption = new Option(response[i].print, response[i].value, false, false);
                    elm.append(newOption);
                }
            }
        }
    });
}
function getRef(url, callback) {
    $.get(url, function (response, status, xhr) {
        callback(response);
    }).fail(function (response) {
        showError('Terjadi Kesalahan');
        callback(null);
    });
}
function postDataCallback(formData, url, callback) {
    showLoading();
    $.ajax({
        processData: false,
        contentType: false,
        type: 'POST',
        url: url,
        data: formData,
        success: function (res) {
            hideLoading(true);
            callback(res);
        },
        error: function (data) {
            hideLoading(false);
            showError('Terjadi Kesalahan');
            callback(null);
        }
    });
}

function submitdatachange(url, data, table, modal, form) {
    showLoading();
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        success: function (res) {
            var parse = $.parseJSON(res);
            if (parse.code == 1) {
                modal.modal('hide');
                hideLoading(true);
                showInfo(parse.msg);
                form.reset();
            }else if(parse.code == 2){
                hideLoadingPartial(true);
                showError(parse.msg);
            }else {
                var li = '<ul>';
                $.each(parse.data, function (i, v) {
                    li += '<li>' + v[0] + '</li>';
                });
                li += '</ul>';
                hideLoadingPartial(true);
                showError(li);
            }
        }
    }).always(function () {
        table.ajax.reload();
    });
}

function submitdata(url, data, table, modal, form) {
    showLoading();
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        processData: false,
        contentType: false,
        success: function (res) {
            var parse = $.parseJSON(res);
            if (parse.code == 1) {
                modal.modal('hide');
                hideLoading(true);
                showInfo(parse.msg);
                form.reset();
            }else if(parse.code == 2){
                hideLoadingPartial(true);
                showError(parse.msg);
            }else {
                var li = '<ul>';
                $.each(parse.data, function (i, v) {
                    li += '<li>' + v[0] + '</li>';
                });
                li += '</ul>';
                hideLoadingPartial(true);
                showError(li);
            }
        }
    }).always(function () {
        table.ajax.reload();
    });
}
Date.prototype.getWeek = function() {
    var jan4th = new Date(this.getFullYear(),0,4);
    return Math.ceil((((this - jan4th) / 86400000) + jan4th.getDay()+1)/7);
};

function hanyaAngka(evt) {
var charCode = (evt.which) ? evt.which : event.keyCode
if (charCode > 31 && (charCode < 48 || charCode > 57))
return false;
return true;
    }

 function convertToRupiah (objek) { 
    
 separator = "."; 
 a = objek.value; 
 b = a.replace(/[^\d]/g,""); 
 c = ""; 
 panjang = b.length; 
 j = 0; for (i = panjang; i > 0; i--) { 
 j = j + 1; if (((j % 3) == 1) && (j != 1)) { 
 c = b.substr(i-1,1) + separator + c; } else { 
 c = b.substr(i-1,1) + c; } } objek.value = c; 
}

function huruf(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if ((charCode < 65 || charCode > 90)&&(charCode < 97 || charCode > 122)&&charCode>32)
            return false;
        return true;
}

function Alphabets(e) {
        var keyCode = e.keyCode || e.which;
        var lblError = document.getElementById("lblError");
        lblError.innerHTML = "";
 
        //Regex for Valid Characters i.e. Alphabets and Numbers.
        var regex = /^[A-Za-z0-9]+$/;
 
        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            lblError.innerHTML = "Only Alphabets and Numbers allowed.";
        }
 
        return isValid;
    }





