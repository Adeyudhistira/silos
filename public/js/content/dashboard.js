$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



function UpdateKuota(id) {

    if(id==null || id==''){
         swal("Info!", "Silahkan Login Adapter Terlebih Dahulu", "info");
    }else{

        swal({   
            title: "Informasi",   
            text: "Update Kuota Nilai?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {    
                $.LoadingOverlay("show"); 
                $.ajax({
                    type: 'POST',
                    url: base_url + '/updatekuota',
                    processData: false,
                    contentType: false,
                    success: function (res) {
                //var parse = $.parseJSON(res);
                if (res.rc == 1) {
                    //modal_upd.modal('hide');
                   // hideLoading(true);
                    //showInfo(parse.msg);
                    //form_cbs[0].reset();
                  //  $("#form_cbs").trigger('reset');
                  $.LoadingOverlay("hide");
                  swal({   
                    title: "Berhasil",   
                    text: "Berhasil Diupdate",   
                    type: "success",   
                    confirmButtonColor: "#8cd4f5",   
                    confirmButtonText: "Ya",     
                    closeOnConfirm: false
                }, function(isConfirm){   
                    if (isConfirm) {     
                        location.reload();
                    } 
                });
                  return false;
              }else if(res.rc == 2){
              $.LoadingOverlay("hide");
                swal({   
                    title: "Informasi",   
                    text: res.rm,   
                    type: "error",   
                    confirmButtonColor: "#8cd4f5",   
                    confirmButtonText: "Ya",     
                    closeOnConfirm: false
                });

            }else {
                var li = '<ul>';
                $.each(res.data, function (i, v) {
                    li += '<li>' + v[0] + '</li>';
                });
                li += '</ul>';
                hideLoadingPartial(true);
                showError(li);
            }
        }
    });

            }else{
                swal("Info!", "Batal Update Kuota Nilai", "info");
            } 
        });
        return false;

    }

}

function UpdateUnit(id) {
     if(id==null || id==''){
         swal("Info!", "Silahkan Login Adapter Terlebih Dahulu", "info");
    }else{

     swal({   
        title: "Informasi",   
        text: "Update Kuota Unit?",   
        type: "info",   
        showCancelButton: true,   
        confirmButtonColor: "#e6b034",   
        confirmButtonText: "Ya",   
        cancelButtonText: "Tidak",   
        closeOnConfirm: false,   
        closeOnCancel: false 
    }, function(isConfirm){   
        if (isConfirm) {    
            $.LoadingOverlay("show"); 
            $.ajax({
                type: 'POST',
                url: base_url + '/updateunit',
                processData: false,
                contentType: false,
                success: function (res) {
                //var parse = $.parseJSON(res);
                if (res.rc == 1) {
                    //modal_upd.modal('hide');
                   // hideLoading(true);
                    //showInfo(parse.msg);
                    //form_cbs[0].reset();
                  //  $("#form_cbs").trigger('reset');
                  $.LoadingOverlay("hide");
                  swal({   
                    title: "Berhasil",   
                    text: "Berhasil Diupdate",   
                    type: "success",   
                    confirmButtonColor: "#8cd4f5",   
                    confirmButtonText: "Ya",     
                    closeOnConfirm: false
                }, function(isConfirm){   
                    if (isConfirm) {     
                        location.reload();
                    } 
                });
                  return false;
              }else if(res.rc == 2){
               $.LoadingOverlay("hide");
                swal({   
                    title: "Informasi",   
                    text: res.rm,   
                    type: "error",   
                    confirmButtonColor: "#8cd4f5",   
                    confirmButtonText: "Ya",     
                    closeOnConfirm: false
                });

            }else {
                var li = '<ul>';
                $.each(res.data, function (i, v) {
                    li += '<li>' + v[0] + '</li>';
                });
                li += '</ul>';
                hideLoadingPartial(true);
                showError(li);
            }
        }
    });

        }else{
            swal("Info!", "Batal Update Kuota Unit", "info");
        } 
    });
     return false;

 }

}

function UpdateData(id) {
    if(id==null || id==''){
         swal("Info!", "Silahkan Login Adapter Terlebih Dahulu", "info");
    }else{
   swal({   
    title: "Informasi",   
    text: "Update Data?",   
    type: "info",   
    showCancelButton: true,   
    confirmButtonColor: "#e6b034",   
    confirmButtonText: "Ya",   
    cancelButtonText: "Tidak",   
    closeOnConfirm: false,   
    closeOnCancel: false 
}, function(isConfirm){   
    if (isConfirm) {    
    $.LoadingOverlay("show"); 
          $.ajax({
            type: 'POST',
            url: base_url + '/updatedata',
            processData: false,
            contentType: false,
            success: function (res) {
                //var parse = $.parseJSON(res);
                if (res.rc == 1) {
                    //modal_upd.modal('hide');
                   // hideLoading(true);
                    //showInfo(parse.msg);
                    //form_cbs[0].reset();
                  //  $("#form_cbs").trigger('reset');
                    $.LoadingOverlay("hide");
                       swal({   
                            title: "Berhasil",   
                            text: "Berhasil Diupdate",   
                            type: "success",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                location.reload();
                            } 
                        });
                        return false;
                }else if(res.rc == 2){
                    $.LoadingOverlay("hide");
                        swal({   
                            title: "Informasi",   
                            text: res.rm,   
                            type: "error",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        });

                }else {
                    var li = '<ul>';
                    $.each(res.data, function (i, v) {
                        li += '<li>' + v[0] + '</li>';
                    });
                    li += '</ul>';
                    hideLoadingPartial(true);
                    showError(li);
                }
            }
        });

        }else{
            swal("Info!", "Batal Update", "info");
        } 
    });
    return false;

    }
}


var form_login = $('#form_login');
var modal_login = $('#modal_login');

function show_login(){

      modal_login.modal({
        onShow: function (dlg) {
            $(dlg.container).css('height', 'auto')
        }
    });
}


function processLogin() {
     //_create_form.parsley().validate();
    var _form_data = new FormData(form_login[0]);
    $.LoadingOverlay("show");
  $.ajax({
        type: 'POST',
        url: base_url + '/login_adapter',
        data: _form_data,
        processData: false,
        contentType: false,
        success: function (res) {
            //var parse = $.parseJSON(res);
            if (res.rc == 1) {
                //modal_upd.modal('hide');
               // hideLoading(true);
                //showInfo(parse.msg);
//                form_tambah[0].reset();
                form_login.trigger('reset');
               $.LoadingOverlay("hide");
                   swal({
                        title: "Berhasil",     
                        text: res.rm,   
                        type: "success",   
                        confirmButtonColor: "#8cd4f5",   
                        confirmButtonText: "Ya",     
                        closeOnConfirm: false
                    }, function(isConfirm){   
                        if (isConfirm) {     
                            location.reload();
                        } 
                    });
                    return false;
            }else{
              $.LoadingOverlay("hide");
                    swal({   
                        title: "Informasi",   
                        text: res.rm,   
                        type: "error",   
                        confirmButtonColor: "#8cd4f5",   
                        confirmButtonText: "Ya",     
                        closeOnConfirm: false
                    });

            }
        }
    });
}   


var form_change = $('#form_change');
var modal_change = $('#modal_change');

function show_change(batchid){
    $('batch_id').val(batchid);
      modal_change.modal({
        onShow: function (dlg) {
            $(dlg.container).css('height', 'auto')
        }
    });
}

function processChange(){
      //_create_form.parsley().validate();
    var _form_data = new FormData(form_change[0]);
    $.LoadingOverlay("show");
  $.ajax({
        type: 'POST',
        url: base_url + '/change_adapter',
        data: _form_data,
        processData: false,
        contentType: false,
        success: function (res) {
            //var parse = $.parseJSON(res);
            if (res.rc == 1) {
                //modal_upd.modal('hide');
               // hideLoading(true);
                //showInfo(parse.msg);
//                form_tambah[0].reset();
                form_login.trigger('reset');
                $.LoadingOverlay("hide");
                   swal({   
                        text: res.rm,   
                        type: "success",   
                        confirmButtonColor: "#8cd4f5",   
                        confirmButtonText: "Ya",     
                        closeOnConfirm: false
                    }, function(isConfirm){   
                        if (isConfirm) {     
                            location.reload();
                        } 
                    });
                    return false;
            }else{
                $.LoadingOverlay("hide");
                    swal({   
                        title: "Informasi",   
                        text: res.rm,   
                        type: "error",   
                        confirmButtonColor: "#8cd4f5",   
                        confirmButtonText: "Ya",     
                        closeOnConfirm: false
                    });

            }
        }
    });
}

function logout(batchid){

$.LoadingOverlay("show");
  $.ajax({
        type: 'POST',
        url: base_url + '/logout_adapter?id='+batchid,
        processData: false,
        contentType: false,
        success: function (res) {
            //var parse = $.parseJSON(res);
            if (res.rc == 1) {
                //modal_upd.modal('hide');
               // hideLoading(true);
                //showInfo(parse.msg);
//                form_tambah[0].reset();
                $.LoadingOverlay("hide");
                   swal({
                        title: "Berhasil",   
                        text: res.rm,   
                        type: "success",   
                        confirmButtonColor: "#8cd4f5",   
                        confirmButtonText: "Ya",     
                        closeOnConfirm: false
                    }, function(isConfirm){   
                        if (isConfirm) {     
                            location.reload();
                        } 
                    });
                    return false;
            }else{
               $.LoadingOverlay("hide");
                    swal({   
                        title: "Informasi",   
                        text: res.rm,   
                        type: "error",   
                        confirmButtonColor: "#8cd4f5",   
                        confirmButtonText: "Ya",     
                        closeOnConfirm: false
                    });

            }
        }
    });

}

function UpdateSaldo(id){

$.LoadingOverlay("show");
    $.ajax({
        type: 'POST',
        url: base_url + '/updatesaldo?id='+id,
        processData: false,
        contentType: false,
        success: function (res) {
             console.log(res);
             $.LoadingOverlay("hide");
            if (res.rc == 1) {
                //modal_upd.modal('hide');
               // hideLoading(true);
                //showInfo(parse.msg);
//                form_tambah[0].reset();
               $.LoadingOverlay("hide");
                   swal({
                        title: "Berhasil",   
                        text: res.rm,   
                        type: "success",   
                        confirmButtonColor: "#8cd4f5",   
                        confirmButtonText: "Ya",     
                        closeOnConfirm: false
                    }, function(isConfirm){   
                        if (isConfirm) {     
                            location.reload();
                        } 
                    });
                    return false;
            }else{
                $.LoadingOverlay("hide");
                    swal({   
                        title: "Informasi",   
                        text: res.rm,   
                        type: "error",   
                        confirmButtonColor: "#8cd4f5",   
                        confirmButtonText: "Ya",     
                        closeOnConfirm: false
                    });

            }
            
        }
    });
}