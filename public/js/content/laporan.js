$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function currencyFormatter(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

var form_dks = $('#form_dks');
var modal_cbs = $('#modal_cbs');
var form_cbs = $('#form_cbs');
var modal_tambah = $('#modal_tambah');
var form_tambah = $('#form_tambah');

var modal_percepat = $('#modal_percepat');
var form_percepat = $('#form_percepat');


var t_laporan = $('#t_laporan').dataTable({
        "sPaginationType": "full_numbers",
        "bDestroy": true,
        "bAutoWidth": false,
        "sScrollX": '100%'
       
    });

$('#show-cbs').click(function(){
    modal_cbs.modal({onShow: function(dlg) {
        $(dlg.container).css('height','auto')
    }});
});


function processUploadCBS() {
     //_create_form.parsley().validate();
    var _form_data = new FormData(form_cbs[0]);
    $.LoadingOverlay("show");
  $.ajax({
        type: 'POST',
        url: base_url + '/laporan/uploadcbs',
        data: _form_data,
        processData: false,
        contentType: false,
        success: function (res) {
            //var parse = $.parseJSON(res);
            if (res.rc == 1) {
                //modal_upd.modal('hide');
               // hideLoading(true);
                //showInfo(parse.msg);
                //form_cbs[0].reset();
                $("#form_cbs").trigger('reset');
                $.LoadingOverlay("hide");
                   swal({   
                        title: "Berhasil",   
                        text: "Data Berhasil Dikirim",   
                        type: "success",   
                        confirmButtonColor: "#8cd4f5",   
                        confirmButtonText: "Ya",     
                        closeOnConfirm: false
                    }, function(isConfirm){   
                        if (isConfirm) {     
                            location.reload();
                        } 
                    });
                    return false;
            }else{
                $.LoadingOverlay("hide");
                    swal({   
                        title: "Informasi",   
                        text: "No KTP " + res.noktp + " Tidak sesuai dengan database",   
                        type: "info",   
                        confirmButtonColor: "#8cd4f5",   
                        confirmButtonText: "Ya",     
                        closeOnConfirm: false
                    });

            }
        }
    });
}

function UpdatePPDPP(bulan,tahun,batchid) {
    if(batchid==null || batchid==''){
         swal("Info!", "Silahkan Login Adapter Terlebih Dahulu", "info");
    }else{

   swal({   
    title: "Informasi",   
    text: "Anda yakin ingin Update PPDPP?",   
    type: "info",   
    showCancelButton: true,   
    confirmButtonColor: "#e6b034",   
    confirmButtonText: "Ya",   
    cancelButtonText: "Tidak",   
    closeOnConfirm: false,   
    closeOnCancel: false 
}, function(isConfirm){   
    if (isConfirm) {    
    $.LoadingOverlay("show"); 
          $.ajax({
            type: 'POST',
            url: base_url + '/laporan/updatePPDPP?bulan='+bulan+'&&tahun='+tahun,
            processData: false,
            contentType: false,
            success: function (res) {
                //var parse = $.parseJSON(res);
                if (res.rc == 1) {
                    //modal_upd.modal('hide');
                   // hideLoading(true);
                    //showInfo(parse.msg);
                    //form_cbs[0].reset();
                  //  $("#form_cbs").trigger('reset');
                    $.LoadingOverlay("hide");
                       swal({   
                            title: "Berhasil",   
                            text: "Data Berhasil Diupdate",   
                            type: "success",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                location.reload();
                            } 
                        });
                        return false;
                }else if(res.rc == 2){
                    $.LoadingOverlay("hide");
                        swal({   
                            title: "Informasi",   
                            text: res.rm,   
                            type: "info",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        });

                }else {
                    var li = '<ul>';
                    $.each(res.data, function (i, v) {
                        li += '<li>' + v[0] + '</li>';
                    });
                    li += '</ul>';
                    hideLoadingPartial(true);
                    showError(li);
                }
            }
        });

        }else{
            swal("Info!", "Batal Update Data PPDPP", "info");
        } 
    });
    return false;

}
}

function MutasiPPDPP(bulan,tahun,batchid) {
    if(batchid==null || batchid==''){
         swal("Info!", "Silahkan Login Adapter Terlebih Dahulu", "info");
    }else{

   swal({   
    title: "Informasi",   
    text: "Anda yakin ingin mengirim record?",   
    type: "info",   
    showCancelButton: true,   
    confirmButtonColor: "#e6b034",   
    confirmButtonText: "Ya",   
    cancelButtonText: "Tidak",   
    closeOnConfirm: false,   
    closeOnCancel: false 
}, function(isConfirm){   
    if (isConfirm) {   
    $.LoadingOverlay("show");  
        $.ajax({
            type: 'POST',
            url: base_url + '/laporan/mutasiPPDPP?bulan='+bulan+'&&tahun='+tahun,
            processData: false,
            contentType: false,
            success: function (res) {
                //var parse = $.parseJSON(res);
                if (res.rc == 1) {
                    //modal_upd.modal('hide');
                   // hideLoading(true);
                    //showInfo(parse.msg);
                   // form_cbs[0].reset();
                    $("#form_cbs").trigger('reset');
                    $.LoadingOverlay("hide");
                       swal({   
                            title: "Berhasil",   
                            text: "Data Berhasil Dikirim",   
                            type: "success",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        }, function(isConfirm){   
                            if (isConfirm) {     
                                location.reload();
                            } 
                        });
                        return false;
                }else if(res.rc == 2){
                    $.LoadingOverlay("hide");
                        swal({   
                            title: "Informasi",   
                            text: res.rm,   
                            type: "info",   
                            confirmButtonColor: "#8cd4f5",   
                            confirmButtonText: "Ya",     
                            closeOnConfirm: false
                        });

                }else {
                    var li = '<ul>';
                    $.each(res.data, function (i, v) {
                        li += '<li>' + v[0] + '</li>';
                    });
                    li += '</ul>';
                    hideLoadingPartial(true);
                    showError(li);
                }
            }
        });

        }else{
            swal("Info!", "Batal Kirim Data PPDPP", "info");
        } 
    });
       return false;

   }    
}


$('#show-tambah').click(function(){
    modal_tambah.modal('show');
});
 
 function processInsert() {
     //_create_form.parsley().validate();
    var _form_data = new FormData(form_tambah[0]);
    $.LoadingOverlay("show");
  $.ajax({
        type: 'POST',
        url: base_url + '/laporan/insert',
        data: _form_data,
        processData: false,
        contentType: false,
        success: function (res) {
            //var parse = $.parseJSON(res);
            if (res.rc == 1) {
                //modal_upd.modal('hide');
               // hideLoading(true);
                //showInfo(parse.msg);
//                form_tambah[0].reset();
                $("#form_tambah").trigger('reset');
                $.LoadingOverlay("hide");
                   swal({   
                        title: "Berhasil",   
                        text: "Data Berhasil Disimpan",   
                        type: "success",   
                        confirmButtonColor: "#8cd4f5",   
                        confirmButtonText: "Ya",     
                        closeOnConfirm: false
                    }, function(isConfirm){   
                        if (isConfirm) {     
                            location.reload();
                        } 
                    });
                    return false;
            }else if(res.rc == 2){
                $.LoadingOverlay("hide");
                    swal({   
                        title: "Informasi",   
                        text: res.rm,   
                        type: "info",   
                        confirmButtonColor: "#8cd4f5",   
                        confirmButtonText: "Ya",     
                        closeOnConfirm: false
                    });

            }else {
                var li = '<ul>';
                $.each(res.data, function (i, v) {
                    li += '<li>' + v[0] + '</li>';
                });
                li += '</ul>';
                hideLoadingPartial(true);
                showError(li);
            }
        }
    });
}   

function showPercepat(id,bulan,tahun,program,kelola,operasi,pemilik,pokok,tarif,batchid){
    if(batchid==null || batchid==''){
         swal("Info!", "Silahkan Login Adapter Terlebih Dahulu", "info");
    }else{
    $('#noktp').val(id);
    $('#bulan_cepat').val(bulan);
    $('#tahun_cepat').val(tahun);
    $('#no_rek_program').val(program);
    $('#no_rek_kelola').val(kelola);
    $('#no_rek_operasi').val(operasi);
    $('#no_rek_pengirim').val(pemilik);

    $('#nilai_mutasi_pokok_cepat').val(pokok);
    $('#nilai_mutasi_tarif_cepat').val(tarif);
    modal_percepat.modal('show');
    }
}


function processPercepat() {
     //_create_form.parsley().validate();
    var _form_data = new FormData(form_percepat[0]);
    $.LoadingOverlay("show");
  $.ajax({
        type: 'POST',
        url: base_url + '/laporan/percepat',
        data: _form_data,
        processData: false,
        contentType: false,
        success: function (res) {
            //var parse = $.parseJSON(res);
            if (res.rc == 0 || res.rc == '00') {
                //modal_upd.modal('hide');
               // hideLoading(true);
                //showInfo(parse.msg);
//                form_tambah[0].reset();
                form_percepat.trigger('reset');
                $.LoadingOverlay("hide");
                   swal({   
                        title: "Berhasil",   
                        text: "Data Berhasil Dikirim",   
                        type: "success",   
                        confirmButtonColor: "#8cd4f5",   
                        confirmButtonText: "Ya",     
                        closeOnConfirm: false
                    }, function(isConfirm){   
                        if (isConfirm) {     
                            location.reload();
                        } 
                    });
                    return false;
            }else{
                $.LoadingOverlay("hide");
                    swal({   
                        title: "Informasi",   
                        text: res.rm,   
                        type: "info",   
                        confirmButtonColor: "#8cd4f5",   
                        confirmButtonText: "Ya",     
                        closeOnConfirm: false
                    });

            }
        }
    });
}   
