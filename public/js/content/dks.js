$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$("#total_angsuran").css({ "pointer-events": "none", "cursor": "default", "background": "#eeeeee" });

function currencyFormatter(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}



function convertToNum(objek) {
    var separator = "";
    var a = objek.value;
    var b = a.replace(/[^\d]/g, "");
    var c = "";
    var panjang = b.length;
    var j = 0; for (var i = panjang; i > 0; i--) {
        j = j + 1; if (((j % 3) == 1) && (j != 1)) {
            c = b.substr(i - 1, 1) + separator + c;
        } else {
            c = b.substr(i - 1, 1) + c;
        }
    } objek.value = c;
}

function convertToRupiah(objek) {
    var separator = ".";
    var a = objek.value;
    var b = a.replace(/[^\d]/g, "");
    var c = "";
    var panjang = b.length;
    var j = 0; for (var i = panjang; i > 0; i--) {
        j = j + 1; if (((j % 3) == 1) && (j != 1)) {
            c = b.substr(i - 1, 1) + separator + c;
        } else {
            c = b.substr(i - 1, 1) + c;
        }
    } objek.value = c;
}

function TambahWilayah() {
    // console.log("teess");

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    console.log($('#kode_wilayah').val());
    console.log($('#nama_wilayah').val());
    
    $.ajax({
        type: "POST",
        url: base_url + '/wilayah/tambah',
        data: {
            type: $('#type').val(),
            kode_wilayah: $('#kode_wilayah').val(),
            nama_wilayah: $('#nama_wilayah').val()
        },
        beforeSend: function () {
        },
        success: function (msg) {

        }
    }).done(function (msg) {
        window.location.href = base_url + '/wilayah';

    }).fail(function (msg) {

    });
    
}

function sAngsuran(object) {

    // convertToRupiah(object);
    
    var plafon = $('#plafon').val();
    var jangka_waktu = $('#jangka_waktu').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "POST",
        url: base_url + '/simulasi_angsuran',
        data: {
            plafon: plafon,
            jangka_waktu: jangka_waktu
    
        },
        beforeSend: function () {
        },
        success: function (msg) {
            // console.log(msg['data']['angsuran']);

        }
    }).done(function (msg) {
        $('#total_angsuran').val(msg);
     

    }).fail(function (msg) {

    });
    
    var plafon = $('#plafon').val();
    var res = plafon.replace(/\./g, '');
    var max = (res * 75) / 100;


    max = max.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");

    $('#totalmax_flpp').html("total maximal FLPP : " + max);

}

function maxFLPP() {
    console.log("wadaw");

  

    console.log(max);
    
    // $('#plafon_flpp').val(res);
    
}


var form_dks = $('#form_dks');
var modal_dks = $('#modal_dks');
var modal_tambah = $('#modal_tambah_data');
var upload = $('#modal_upload');
var form_upload = $('#form_upload');

var modal_detail = $('#modal_detail');


var t_dks = $('#t_dks').dataTable({
        "sPaginationType": "full_numbers"
    });






$('#showTambah').click(function () {
    modal_tambah.modal({
        onShow: function (dlg) {
            $(dlg.container).css('height', 'auto')
        }
    });
});


$('#showTambah').click(function () {
    modal_tambah.modal({
        onShow: function (dlg) {
            $(dlg.container).css('height', 'auto')
        }
    });
});

$('#showUpload').click(function () {
    upload.modal({
        onShow: function (dlg) {
            $(dlg.container).css('height', 'auto')
        }
    });
});



$('#example-select-all').click(function (e) {
    $(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
});




function show_batch(id){

if(id==null || id==''){
    swal("Informasi","Silahkan Login Adapter Terlebih Dahulu","info");
}else{

    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal("Peringatan!", "Minimal pilih satu", "warning");
    }else{
        
        modal_dks.modal({onShow: function(dlg) {
            $(dlg.container).css('height','auto')
        }}); 
    }

}
}

function processDKS(nosurat,tgl) {
     //_create_form.parsley().validate();
   // var _form_data = new FormData(form_dks[0]);

    /*for (var pair of _form_data.entries()) {
        console.log(pair[0]+ ', ' + pair[1]);
    }*/
   // if(_create_form.parsley().isValid()){
      //  submitdata('dks/update',_form_data,t_dks,modal_dks,form_dks[0]);
    //}

    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal("Peringatan!", "Minimal pilih satu", "warning");
    }else{
         swal({   
            title: "Informasi",   
            text: "Anda yakin ingin mengirim record yang dipilih?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                modal_dks.modal('hide');
                $.ajax({
                    type: 'POST',
                    url: base_url+'/dks/update?no_surat='+ nosurat +'&&tgl='+ tgl,
                    data: {value: value},
                    async: false,
                    success: res => {
                        console.log(res);
                    }
                }).always(() => {
                    value = [];
                     
                    //t_dks.reload();
                    //t_dks.draw();
                });
                form_dks[0].reset();

                swal({   
                    title: "Berhasil",   
                    text: "Data Berhasil Dikirim",   
                    type: "success",   
                    confirmButtonColor: "#8cd4f5",   
                    confirmButtonText: "Ya",     
                    closeOnConfirm: false
                }, function(isConfirm){   
                    if (isConfirm) {     
                        location.reload();
                    } 
                });
                
            }else{
                 swal("Batal!", "Batal", "info");
            } 
        });
        return false;
        
    }
    
}

function upload_data_uji() {
 //  _create_form.parsley().validate();
    var _form_data = new FormData(form_upload[0]);
//if(_create_form.parsley().isValid()){
      $.LoadingOverlay("show");
    $.ajax({
        type: 'POST',
        url: base_url + '/ppdpp/upload',
        data: _form_data,
        processData: false,
        contentType: false,
        success: function (res) {
           var parse = $.parseJSON(res);
            console.log(res);
            console.log(parse);
            if (parse.rc == 0) {
                //console.log(parse);
                //modal_upd.modal('hide');
               // hideLoading(true);
                //showInfo(parse.msg);
                  $.LoadingOverlay("hide");
                form_upload[0].reset();
                  swal({   
                        title: "Berhasil",   
                        text: parse.rm,   
                        type: "success",   
                        confirmButtonColor: "#e6b034",   
                        confirmButtonText: "Tutup",   
                        closeOnConfirm: false
                    }, function(isConfirm){   
                        if (isConfirm) {     
                        location.reload();
                      //window.location.href = base_url +'/billing/table/' + id
                        } 
                    });
                    return false;
            }else if(parse.rc == 2){
                hideLoadingPartial(true);
                showError(parse.msg);
            }else {
                var li = '<ul>';
                $.each(parse.data, function (i, v) {
                    li += '<li>' + v[0] + '</li>';
                });
                li += '</ul>';
                hideLoadingPartial(true);
                showError(li);
            }
        }
    }).always(function () {
       // var loader = $('.ui.dimmable #mainLoader');    
        //loader.dimmer({closable: false}).dimmer('show');
       // location.reload();;
    });
  //  }
}



function TambahData(type) {
    console.log("tambahProses");
    console.log(base_url + '/permohonan_uji/add');
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "POST",
        url: base_url + '/permohonan_uji/add',
        data: {
            id_nas: $('#id_nas').val(),
            id_mp: $('#id_mp').val(),
            id_maap: $('#id_maap').val(),
            
            no_identitas: $('#no_identitas').val(),
            nama_nasabah: $('#nama_nasabah').val(),
            perumahan: $('#perumahan').val(), 
            harga_object: $('#harga_object').val(), 
            jangka_waktu: $('#jangka_waktu').val(), 
            id_pekerjaan: $('#id_pekerjaan').val(),
            jk: $('#jk').val(),
            npwp: $('#npwp').val(),
            gp: $('#gp').val(),
            telp: $('#telp').val(),
            ktp_p: $('#ktp_p').val(),
            nama_p: $('#nama_p').val(),
            plafon: $('#plafon').val(),
            bunga: $('#bunga').val(),
            plafon_flpp: $('#plafon_flpp').val(),
            alamat: $('#alamat').val(),
            kodepos: $('#kodepos').val(),
            luas_tanah_fisik: $('#luas_tanah_fisik').val(),
            luas_imb: $('#luas_imb').val(),
            kd_jns_kpr: $('#kd_jns_kpr').val(),
            no_sp3k: $('#no_sp3k').val(),
            tgl_sp3k: $('#tgl_sp3k').val(),
            no_id_uji: $('#no_id_uji').val(),
            id_developer: $('#id_developer').val(),
            id_perumahan: $('#id_perumahan').val(),
            kota_kab: $("#kab_kota").val(),
            total_angsuran: $('#total_angsuran').val()

        },
        beforeSend: function () {
        },
        success: function (msg) {

        }
    }).done(function (msg) {

        if (type == "nasabah") {
            window.location.href = base_url + '/nasabah_uji';
        }else{
            window.location.href = base_url + '/permohonan_uji';

        }

    }).fail(function (msg) {

    });

}



function HapusWilayah(id) {
    swal({
        title: "Hapus",
        text: "Anda Yakin akan Hapus?",
        type: "info",
        showCancelButton: true,
        confirmButtonColor: "#e6b034",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "POST",
                url: base_url + '/wilayah/delete/' + id,
                data: {
                    id: id
                },
                beforeSend: function () {
                },
                success: function (msg) {
                    console.log(msg);
                    swal({
                        title: "Informasi",
                        text: "berhasil Terhapus",
                        type: "info",
                        confirmButtonColor: "#8cd4f5",
                        confirmButtonText: "Ya",
                        closeOnConfirm: false
                    }, function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                        }
                    });

                }
            }).done(function (msg) {

            }).fail(function (msg) {
                swal("Terjadi Kesalahan! ");
            });

        } else {
            swal("Dibatalkan!");
        }
    });
}



function JadwalAngsur(id){

     window.location.href = base_url +'/jadwalAngsurByKTP?id='+ id;
}

function back(id){

     window.location.href = base_url +'/jadwalAngsurByKTP?id='+ id;
}

function cetakExcel(id){
    window.location.href = base_url +'/cetakexcel?id='+ id;
}

function cetakPdf(id){
    window.location.href = base_url +'/cetakpdf?id='+ id;
}

function show_detail(id){
    window.location.href = base_url +'/lihat_nasabah?id='+ id;
}

function JadwalAngsurBy_idberkas(id){

     window.location.href = base_url +'/jadwalAngsurByIDberkas?id='+ id;
}

function cetakExcel1(id){
    window.location.href = base_url +'/cetakexcel1?id='+ id;
}

function cetakPdf1(id){
    window.location.href = base_url +'/cetakpdf1?id='+ id;
}

$(document).ready(function(){
    $("#kota_kab").change(function(){

            var _items = "";
            $.ajax({
                type: 'GET',
                url: 'select/kecamatan/' + this.value,
                success: function (res) {
                    var data = $.parseJSON(res);
                    _items = "<option value=''>Pilih Kecamatan</option>";
                    $.each(data, function (k,v) {
                        _items += "<option value='"+v.kode+"'>"+v.nama+"</option>";
                    });

                    $("#kecamatan").html(_items);
                }
            });


    });

$("#kecamatan").change(function () {
    console.log("asup");
    var _items = "";
    $.ajax({
        type: 'GET',
        url: 'select/kelurahan/' + this.value,
        success: function (res) {
            var data = $.parseJSON(res);
            _items = "<option value=''>Pilih Kelurahan</option>";
            $.each(data, function (k,v) {
                _items += "<option value='"+v.kode+"'>"+v.nama+"</option>";
            });

            $('#kelurahan').html(_items);
        }
    });

});



}); 

 
