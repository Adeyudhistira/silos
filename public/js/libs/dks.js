$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$("#total_angsuran").css({ "pointer-events": "none", "cursor": "default", "background": "#eeeeee" });

function currencyFormatter(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}


function convertToNum(objek) {
    var separator = "";
    var a = objek.value;
    var b = a.replace(/[^\d]/g, "");
    var c = "";
    var panjang = b.length;
    var j = 0; for (var i = panjang; i > 0; i--) {
        j = j + 1; if (((j % 3) == 1) && (j != 1)) {
            c = b.substr(i - 1, 1) + separator + c;
        } else {
            c = b.substr(i - 1, 1) + c;
        }
    } objek.value = c;
}

function convertToRupiah(objek) {
    var separator = ".";
    var a = objek.value;
    var b = a.replace(/[^\d]/g, "");
    var c = "";
    var panjang = b.length;
    var j = 0; for (var i = panjang; i > 0; i--) {
        j = j + 1; if (((j % 3) == 1) && (j != 1)) {
            c = b.substr(i - 1, 1) + separator + c;
        } else {
            c = b.substr(i - 1, 1) + c;
        }
    } objek.value = c;
}

function sAngsuran() {
    var plafon = $('#plafon').val();
    var jangka_waktu = $('#jangka_waktu').val();
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "POST",
        url: base_url + '/simulasi_angsuran',
        data: {
            plafon: plafon,
            jangka_waktu: jangka_waktu
    
        },
        beforeSend: function () {
        },
        success: function (msg) {
            console.log(msg['data']['angsuran']);
            $('#total_angsuran').val(msg['data']['angsuran']);

        }
    }).done(function (msg) {
     

    }).fail(function (msg) {

    });
    
}


var form_dks = $('#form_dks');
var modal_dks = $('#modal_dks');
var modal_tambah = $('#modal_tambah_data');

var modal_detail = $('#modal_detail');


var t_dks = $('#t_dks').dataTable({
        "sPaginationType": "full_numbers"
    });

$('#show-dks').click(function(){
    modal_dks.modal({onShow: function(dlg) {
        $(dlg.container).css('height','auto')
    }});
});


$('#showTambah').click(function () {
    modal_tambah.modal({
        onShow: function (dlg) {
            $(dlg.container).css('height', 'auto')
        }
    });
});



$('#example-select-all').click(function (e) {
    $(this).closest('table').find('td input[type="checkbox"]').prop('checked', this.checked);
});

function processDKS(nosurat,tgl) {
     //_create_form.parsley().validate();
   // var _form_data = new FormData(form_dks[0]);

    /*for (var pair of _form_data.entries()) {
        console.log(pair[0]+ ', ' + pair[1]);
    }*/
   // if(_create_form.parsley().isValid()){
      //  submitdata('dks/update',_form_data,t_dks,modal_dks,form_dks[0]);
    //}

    let value = [];
    $('input[type="checkbox"]').each(function(){
        if(this.checked){
            value.push(this.value);
        }
    });

    if(value.length == 0){
        swal("Peringatan!", "Minimal pilih satu", "warning");
    }else{
         swal({   
            title: "Informasi",   
            text: "Anda yakin ingin mengirim record yang dipilih?",   
            type: "info",   
            showCancelButton: true,   
            confirmButtonColor: "#e6b034",   
            confirmButtonText: "Ya",   
            cancelButtonText: "Tidak",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                modal_dks.modal('hide');
                $.ajax({
                    type: 'POST',
                    url: base_url+'/dks/update?no_surat='+ nosurat +'&&tgl='+ tgl,
                    data: {value: value},
                    async: false,
                    success: res => {
                        console.log(res);
                    }
                }).always(() => {
                    value = [];
                     
                    //t_dks.reload();
                    //t_dks.draw();
                });
                form_dks[0].reset();

                swal({   
                    title: "Berhasil",   
                    text: "Data Berhasil Dikirim",   
                    type: "success",   
                    confirmButtonColor: "#8cd4f5",   
                    confirmButtonText: "Ya",     
                    closeOnConfirm: false
                }, function(isConfirm){   
                    if (isConfirm) {     
                        location.reload();
                    } 
                });
                
            } 
        });
        return false;
        
    }
    
}

function ShowEditPermohonan(id) {


    $('#title_form').html("waawww");

    $('#no_identitas').val("");
    // $('#nama_nasabah').val("");
    // $('#id_pekerjaan').val("");
    // $('#npwp').val("");
    // $('#gp').val("");
    // $('#telp').val("");
    // $('#nama_p').val("");
    // $('#ktp_p').val("");
    // $('#perumahan').val("");
    // $('#harga_object').val("");
    // $('#plafon').val("");
    // $('#jangka_waktu').val("");
    // $('#total_angsuran').val("");

    modal_tambah.modal({
        onShow: function (dlg) {
            $(dlg.container).css('height', 'auto')
        }
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    console.log(base_url + '/permohonan_uji/show/' + id);
    
    $.ajax({
        url: base_url + '/permohonan_uji/show/'+id,
        type: 'GET',
        success: function (res) {
            var data = $.parseJSON(res);
            console.log(data);

            $.each(data, function (k, v) {
                

                $('#no_identitas').val(v.no_identitas);
                $('#nama_nasabah').val(v.nama_nasabah);
                $('#id_pekerjaan').val(v.id_pekerjaan);
                $('#npwp').val(v.npwp);
                $('#gp').val(v.gp);
                $('#telp').val(v.telp);
                $('#nama_p').val(v.nama_p);
                $('#ktp_p').val(v.ktp_p);
                $('#perumahan').val(v.perumahan);
                $('#harga_object').val(v.harga_object);
                $('#plafon').val(v.plafon);
                $('#jangka_waktu').val(v.jangka_waktu);
                $('#total_angsuran').val(v.total_angsuran);
            });
        }
    }).done(function (msg) {
        // endLoadingModal();
    });
}


function TambahData() {
    console.log("tambahProses");
    console.log(base_url + '/permohonan_uji/add');
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "POST",
        url: base_url + '/permohonan_uji/add',
        data: {
            id_nas: $('#id_nas').val(),
            id_mp: $('#id_mp').val(),
            id_maap: $('#id_maap').val(),
            
            no_identitas: $('#no_identitas').val(),
            nama_nasabah: $('#nama_nasabah').val(),
            perumahan: $('#perumahan').val(), 
            harga_object: $('#harga_object').val(), 
            jangka_waktu: $('#jangka_waktu').val(), 
            id_pekerjaan: $('#id_pekerjaan').val(),
            jk: $('#jk').val(),
            npwp: $('#npwp').val(),
            gp: $('#gp').val(),
            telp: $('#telp').val(),
            ktp_p: $('#ktp_p').val(),
            nama_p: $('#nama_p').val(),
            plafon: $('#plafon').val(),
            bunga: $('#bunga').val(),
            plafon_flpp: $('#plafon_flpp').val(),
            nama_developer: $('#nama_developer').val(),
            alamat: $('#alamat').val(),
            kodepos: $('#kodepos').val(),
            luas_tanah_fisik: $('#luas_tanah_fisik').val(),
            luas_imb: $('#luas_imb').val(),
            kd_jns_kpr: $('#kd_jns_kpr').val(),
            no_sp3k: $('#no_sp3k').val(),
            tgl_sp3k: $('#tgl_sp3k').val(),
            no_id_uji: $('#no_id_uji').val(),
            total_angsuran: $('#total_angsuran').val()

        },
        beforeSend: function () {
        },
        success: function (msg) {

        }
    }).done(function (msg) {
        window.location.href = base_url + '/permohonan_uji';

    }).fail(function (msg) {

    });


    // $.ajax({
    //     type: 'POST',
    //     url: base_url + '/permohonan_uji/add',
    //     data: { 
    //         nama_nasabah: nama_nasabah,
    //         perumahan: perumahan, 
    //         harga_object: harga_object, 
    //         jangka_waktu: jangka_waktu, 
    //         total_angsuran: total_angsuran,
    //         plafon: plafon

    //     },
    //     async: false,
    //     success: res => {
    //         console.log(res);
    //         swal({
    //             title: "Berhasil",
    //             text: "Data Berhasil Dikirim",
    //             type: "success",
    //             confirmButtonColor: "#8cd4f5",
    //             confirmButtonText: "Ya",
    //             closeOnConfirm: false
    //         }, function (isConfirm) {
    //             if (isConfirm) {
    //                 location.reload();
    //             }
    //         });
    //     }
    // });
}

